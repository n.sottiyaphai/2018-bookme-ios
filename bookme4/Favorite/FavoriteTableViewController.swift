 //
 //  FavoriteTableViewController.swift
 //  bookme4
 //
 //  Created by Naphat Sottiyaphai on 27/4/2561 BE.
 //  Copyright © 2561 Naphat Sottiyaphai. All rights reserved.
 //
 
 import UIKit
 import Alamofire
 import SVProgressHUD
 import Optik
 import StatusAlert
 import TagListView
 import SwiftHash
 
 class FavoriteTableViewController: UITableViewController , back2profile {
    
    func go2profile(id: String) {
        let vc = profileViewController.instantiateFromStoryboard()
        vc.uid = id
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    
    @IBOutlet weak var optionCollection: UICollectionView!
    
    var userUID = String()
    var proList_id = [String]()
    var proList = [NSDictionary]()
    var catagory = [NSDictionary]()
    
    var sortting = [String]()
    let sortingPostition = 0
    var filter = Int()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "Favorite"
        
        let userDefault = UserDefaults.standard
        let uid = userDefault.value(forKey: "user_uid") as! String
        
        self.userUID = uid
        
        optionCollection.delegate = self
        
        let btn2 = UIButton(type: .custom)
        btn2.setImage(UIImage(named: "icon_search"), for: .normal)
        btn2.frame = CGRect(x: 0, y: 0, width: 40, height: 18)
        btn2.addTarget(self, action: #selector(self.go2search), for: .touchUpInside)
        let item2 = UIBarButtonItem(customView: btn2)
        self.navigationItem.setRightBarButtonItems([item2], animated: true)
        
        self.setBackgroundColor()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        
        sortting.removeAll()
        sortting.append("All")
        
        if #available(iOS 11.0, *) {
            self.navigationController?.navigationBar.isHidden = false
            self.navigationController?.navigationBar.prefersLargeTitles = true
            
        } else {
            print("It's older than iOS 11")
        }
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            self.callProfile()
            self.callCat()
        }
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        self.sortting.removeAll()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
    }
    
    @objc func go2search() {
        let vc = searchViewController.instantiateFromStoryboard()
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func sizeHeaderToFit() {
        let headerView = tableView.tableHeaderView!
        
        headerView.setNeedsLayout()
        headerView.layoutIfNeeded()
        
        let height = headerView.systemLayoutSizeFitting(UIView.layoutFittingCompressedSize).height
        var frame = headerView.frame
        frame.size.height = height
        headerView.frame = frame
        
        self.tableView.tableHeaderView = headerView
    }
    
    
    @IBAction func FavPush(_ sender: UIButton) {
        
        let userDefaults = UserDefaults.standard
        let index = sender.tag
        let data = proList[index]
        //        var pids = String()
        
        guard let pids = data.value(forKey: "pId") else{
            SVProgressHUD.showError(withStatus: "Data error")
            self.navigationController?.popViewController(animated: true)
            return
        }
        
        
        
        
        // Data From UserDefault
        let uid = userDefaults.value(forKey: "user_uid") as! String
        
        if (sender.image(for: .normal) == UIImage(named: "icon_fav_red")){
            // Already Favorited
            
            let alertAction = UIAlertController.init(title: "Are you sure ?", message: "Do you want to unfavorite this professional" , preferredStyle: .alert)
            
            let ok = UIAlertAction.init(title: "OK", style: .default) { (action) in
                self.favoriteDelete(uid: uid, pid: pids as! String)
            }
            
            let cancel = UIAlertAction.init(title: "Cancel", style: .cancel) { (action) in
                self.dismiss(animated: true, completion: nil)
            }
            
            alertAction.addAction(ok)
            alertAction.addAction(cancel)
            
            self.present(alertAction, animated: true, completion: nil)
            
            
        }
        else {
            // NOT Favorite
            self.favoriteAdd(uid: uid, pid: pids as! String)
        }
        
    }
    
    func callProfile() {
        
        let ud = UserDefaults.standard
        var accesstoken = String()
        
        if(ud.object(forKey: "accessToken") != nil){
            accesstoken = ud.value(forKey: "accessToken") as! String
        }
        
        let header : HTTPHeaders = [
            "accessToken" : accesstoken
        ]
        
        self.proList.removeAll()
        
        Alamofire.request(baseURL.url + "favorite/get-u-id/\(self.userUID)", method: .get, encoding: JSONEncoding.default , headers : header)
            .responseJSON(completionHandler: { response in
                
                if let result = response.result.value {
                    let data = result as! NSDictionary
                    
                    if let response = data.value(forKey: "response"){
                        let respond = response as! Bool
                        if(!respond){
                            self.proList.removeAll()
                            self.tableView.reloadData()
                        }
                        else{
                            if(data.value(forKey: "data") is NSDictionary){
                                let JSON = data.value(forKey: "data") as! NSDictionary
                                
                                if let favArrays = JSON.value(forKey: "favorite") {
                                    let favArray =  favArrays as! NSArray
                                    
                                    for favoriteData in favArray{
                                        if( favoriteData is NSDictionary ){
                                            let objectData = favoriteData as! NSDictionary
                                            self.proList.append(objectData)
                                        }
                                        else{
                                            SVProgressHUD.showError(withStatus: "Not Favorite in list")
                                            
                                            DispatchQueue.main.asyncAfter(deadline: .now() + 0.5){
                                                SVProgressHUD.dismiss()
                                            }
                                        }
                                    }
                                    self.tableView.reloadData()
                                }
                            }
                        }
                    }
                }
                else{
                    SVProgressHUD.showError(withStatus: "Network Error")
                }
            })
    }
    
    func callFavByCat(catid : String) {
        
        let ud = UserDefaults.standard
        var accesstoken = String()
        
        if(ud.object(forKey: "accessToken") != nil){
            accesstoken = ud.value(forKey: "accessToken") as! String
        }
        
        let header : HTTPHeaders = [
            "accessToken" : accesstoken
        ]
        
        self.proList.removeAll()
        
        Alamofire.request(baseURL.url + "favorite/\(self.userUID)/get-cat-id/\(catid)", method: .get, encoding: JSONEncoding.default , headers : header)
            .responseJSON(completionHandler: { response in
                
                
                
                if let result = response.result.value {
                    let data = result as! NSDictionary
                    if(data.value(forKey: "data") is NSDictionary){
                        let JSON = data.value(forKey: "data") as! NSDictionary
                        
                        if let favArrays = JSON.value(forKey: "favorite") {
                            let favArray =  favArrays as! NSArray
                            
                            for favoriteData in favArray{
                                if( favoriteData is NSDictionary ){
                                    let objectData = favoriteData as! NSDictionary
                                    self.proList.append(objectData)
                                }
                                else{
                                    print("it's Not object")
                                    SVProgressHUD.showError(withStatus: "Not Favorite in list")
                                    
                                    DispatchQueue.main.asyncAfter(deadline: .now() + 0.5){
                                        SVProgressHUD.dismiss()
                                    }
                                }
                            }
                            self.tableView.reloadData()
                        }
                    }
                }
                else{
                    SVProgressHUD.showError(withStatus: "Network Error")
                }
            })
    }
    
    func callCat() {
        
        let ud = UserDefaults.standard
        var accesstoken = String()
        
        if(ud.object(forKey: "accessToken") != nil){
            accesstoken = ud.value(forKey: "accessToken") as! String
        }
        
        let header : HTTPHeaders = [
            "accessToken" : accesstoken
        ]
        
        self.catagory.removeAll()
        
        Alamofire.request(baseURL.url + "categories", method: .get, encoding: JSONEncoding.default , headers : header)
            .responseJSON(completionHandler: { response in
                
                if let result = response.result.value {
                    
                    let JSON = result as! NSDictionary
                    let JsonResult = JSON.value(forKey: "response") as! Bool
                    
                    if(JsonResult){
                        let data = JSON.value(forKey: "data") as! NSArray
                        for dataInArr in data {
                            let dictData = dataInArr as! NSDictionary
                            self.catagory.append(dictData)
                            
                            let catName = dictData.value(forKey: "catName") as! String
                            self.sortting.append(catName)
                        }
                        
                        self.optionCollection.reloadData()
                    }
                }
                else{
                    SVProgressHUD.showError(withStatus: "Network Error")
                }
            })
    }
    
    func favoriteAdd(uid : String , pid : String) {
        
        let ud = UserDefaults.standard
        var accesstoken = String()
        
        if(ud.object(forKey: "accessToken") != nil){
            accesstoken = ud.value(forKey: "accessToken") as! String
        }
        
        let header : HTTPHeaders = [
            "accessToken" : accesstoken
        ]
        
        let param : [String:Any] = [
            "pId" : pid
        ]
        
        Alamofire.request(baseURL.url + "favorite/" + "\(uid)", method: .post , parameters: param , encoding: JSONEncoding.default , headers : header)
            .responseJSON(completionHandler: { response in
                
                if let result = response.result.value {
                    let respond = result as! NSDictionary
                    
                    if(respond.value(forKey: "response") as! Bool){
                        
                        // Creating StatusAlert instance
                        let statusAlert = StatusAlert()
                        
                        statusAlert.image = commonImage.iconFavBig
                        statusAlert.title = "Favorited"
                        statusAlert.message = "You can open favorites list in favorited tab."
                        statusAlert.canBePickedOrDismissed = true
                        
                        // Presenting created instance
                        statusAlert.showInKeyWindow()
                        
                        DispatchQueue.main.async {
                            self.callProfile()
                        }
                    }
                }
                else{
                    SVProgressHUD.showError(withStatus: "Error")
                }
            })
    }
    
    func favoriteDelete(uid : String , pid : String) {
        
        let ud = UserDefaults.standard
        var accesstoken = String()
        
        if(ud.object(forKey: "accessToken") != nil){
            accesstoken = ud.value(forKey: "accessToken") as! String
        }
        
        let header : HTTPHeaders = [
            "accessToken" : accesstoken
        ]
        
        let param : [String:Any] = [
            "pId" : pid
        ]
        
        Alamofire.request(baseURL.url + "favorite/delete/" + "\(uid)", method: .post , parameters: param , encoding: JSONEncoding.default , headers : header)
            .responseJSON(completionHandler: { response in
                
                if let result = response.result.value {
                    let respond = result as! NSDictionary
                    
                    if(respond.value(forKey: "response") as! Bool){
                        // Creating StatusAlert instance
                        let statusAlert = StatusAlert()
                        
                        statusAlert.image = commonImage.iconUnfavBig
                        statusAlert.title = "Unfavorite"
                        statusAlert.message =  "unfavorite complete."
                        statusAlert.canBePickedOrDismissed = true
 
                        // Presenting created instance
                        statusAlert.showInKeyWindow()
                    }
                    else{
                        print("ERROR : Data type not correct.")
                    }
                    
                    DispatchQueue.main.async {
                        self.callProfile()
                    }
                }
                else{
                    SVProgressHUD.showError(withStatus: "Error")
                }
            })
    }
    
    // MARK: - Table view data source
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if(proList.count == 0){
            self.tableView.setEmptyMessage("let's make a some people to your favorite list")
        }
        else{
            self.tableView.restore()
        }
        return self.proList.count
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return commonValue.profileCellHeight
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "profiles2", for: indexPath) as! profile2TableCell
        
        let data = proList[indexPath.row]
        
        if(data.value(forKey: "profile") is NSDictionary){
            
            var catName = String()
            let profile = data.value(forKey: "profile") as! NSDictionary
            let favCount = data.value(forKey: "favCount")
            let profileImage = profile.value(forKey: "profilePhoto") as? String
            let firstName = profile.value(forKey: "firstName") as? String
            let lastName = profile.value(forKey: "lastName") as? String
            //            let title = profile.value(forKey: "tittle") as? String
            let category = data.value(forKey: "categories") as! NSDictionary
            //
            if(category.object(forKey: "catName") != nil){
                catName = category.value(forKey: "catName") as! String
                cell.catName.text = catName
            }
            else{
                cell.catName.text = ""
            }
            
            if let first = firstName , let last = lastName {
                cell.profileName.text = "\(first) \(last)"
            }
            
            cell.profileImage.sd_setImage(with: URL(string: profileImage!), placeholderImage: UIImage(named : "icon_profile"))
            cell.profileImage.setRounded()
            cell.profileDest.text = ""
            
            
            cell.onlineIndecator.setRounded()
            cell.onlineIndecator.layer.borderWidth = 2
            cell.onlineIndecator.layer.borderColor = UIColor.white.cgColor
            cell.onlineIndecator.isHidden = true
            
            cell.collectionView.delegate = self
            
            if(favCount is String){
                let showCount = favCount as! String
                cell.favCount.text = showCount
            }else{
                let showCount = favCount as! Int
                cell.favCount.text = "\(showCount)"
            }
            
            cell.favButton.setImage(UIImage(named: "icon_fav_red"), for: .normal)
            cell.favButton.tag = indexPath.row
            
            // Lang Setup
            cell.bgView.backgroundColor = UIColor.white
            cell.bgView.setRoundWithR(r: commonValue.viewRadius)
        }
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        guard let tableViewCell = cell as? profile2TableCell else { return }
        //        guard let tableViewCell = cell as? profileTableCell else { return }
        tableViewCell.setCollectionViewDataSourceDelegate(self, forRow: indexPath.row)
        
        
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let vc = profileViewController.instantiateFromStoryboard()
        
        let data = proList[indexPath.row]
        let uids = data.value(forKey: "pId") as! String
        vc.uid = uids
        
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
 }
 
 extension FavoriteTableViewController : UICollectionViewDelegate , UICollectionViewDataSource , UICollectionViewDelegateFlowLayout {
    
    // MARK: - Collection View data source
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        switch collectionView {
        case optionCollection:
            return sortting.count
        default:
            let data = proList[collectionView.tag]
            var Gallery = Array<Any>()
            
            if((data.object(forKey: "Gallery")) != nil){
                Gallery = data.value(forKey: "Gallery") as! Array<Any>
            }
            else{
                let value = data.value(forKey: "portfolio") as! NSDictionary
                Gallery = value.value(forKey: "Gallery") as! Array<Any>
            }
            
            return Gallery.count
        }
        
        
        
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        switch collectionView {
        case optionCollection:
            
            if(filter == indexPath.row){
                let item = collectionView.dequeueReusableCell(withReuseIdentifier: "sort", for: indexPath) as! sortCollectionCell
                
                item.bgView.setRoundWithShadow(r: Float(collectionView.bounds.height/2-4))
                item.bgView.clipsToBounds = true
                
                item.textLabel.text = sortting[indexPath.row]
                
                // Lang Setup
//                let userDefault = UserDefaults.standard
//                let lang = userDefault.value(forKey: "langDevice") as! String
    
                return item
            }
                
            else {
                let item = collectionView.dequeueReusableCell(withReuseIdentifier: "sort2", for: indexPath) as! sortCollectionCell2
                
                item.bgView.setRoundWithR(r: Float(collectionView.bounds.height/2-4))
                //                item.bgView.setRoundWithShadow(r: Float(collectionView.bounds.height/2-11))
                item.bgView.clipsToBounds = true
                
                item.textLabel.text = sortting[indexPath.row]
                
                // Lang Setup
//                let userDefault = UserDefaults.standard
//                let lang = userDefault.value(forKey: "langDevice") as! String
      
                return item
            }
            
//            let item = collectionView.dequeueReusableCell(withReuseIdentifier: "sort", for: indexPath) as! sortCollectionCell
//
//            item.bgView.layer.cornerRadius = collectionView.bounds.height/2-3.5
//            item.bgView.clipsToBounds = true
//
//            item.textLabel.text = sortting[indexPath.row]
//
//            // Lang Setup
//            let userDefault = UserDefaults.standard
//            let lang = userDefault.value(forKey: "langDevice") as! String
//            if(lang == "th-TH"){
//                item.textLabel.setFontFromLang(lang: "th", style: "medium")
//            }
//            return item
//
        default:
            
            let item = collectionView.dequeueReusableCell(withReuseIdentifier: "image", for: indexPath) as! imageCollectionItem
            
            let data = proList[collectionView.tag]
            
            var pro_uid = String()
            var Gallery = Array<Any>()
            var dictData = NSDictionary()
            
            // User ID Format Detection
            if ( data.object(forKey: "profile") != nil ) {
                if(data.value(forKey: "profile") is NSDictionary){
                    dictData = data.value(forKey: "profile") as! NSDictionary
                }
                pro_uid = dictData.value(forKey: "_id") as! String
            }else{
                pro_uid = data.value(forKey: "uId") as! String
            }
            
            // Gallery Format Detection
            if((data.object(forKey: "Gallery")) != nil){
                Gallery = data.value(forKey: "Gallery") as! Array<Any>
            }
            else{
                let value = data.value(forKey: "portfolio") as! NSDictionary
                Gallery = value.value(forKey: "Gallery") as! Array<Any>
            }

            let url = Gallery[collectionView.tag] as! String
            
            item.image.sd_setImage(with: URL(string: url), placeholderImage: UIImage(named: "bookme_loading"))
            item.image.setRoundWithR(r: commonValue.subTadius)
            
            
            return item
        }
        
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        switch collectionView {
        case optionCollection:
            return CGSize(width: collectionView.bounds.width/3, height: collectionView.bounds.height)
        default:
            return CGSize(width: collectionView.bounds.height, height: collectionView.bounds.height)
        }
    }
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        switch collectionView {
        case optionCollection:
            SVProgressHUD.show()
            
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.3){
                self.filter = indexPath.row
                if(indexPath.row == 0){
                    self.callProfile()
                }else{
                    let cat = self.catagory[indexPath.row-1]
                    let catID = cat.value(forKey: "_id") as! String
                    self.callFavByCat(catid: catID)
                }
                
                self.optionCollection.reloadData()
                SVProgressHUD.dismiss()
            }
            
        default:
            //            SVProgressHUD.show()
            
            let data = proList[collectionView.tag]
            let Gallery = data.value(forKey: "Gallery") as? Array<String>
            let uids = data.value(forKey: "pId") as! String
            
            let profile = data.value(forKey: "profile") as! NSDictionary
            let profress_uid  = profile.value(forKey: "_id") as! String
            let profileImage = profile.value(forKey: "profilePhoto") as? String
            let firstName = profile.value(forKey: "firstName") as? String
            let lastName = profile.value(forKey: "lastName") as? String
            let md5_uid = MD5(profress_uid)
            
            var url = [URL]()
            
            for URLs in Gallery!{
                let full_url = "\(baseURL.photoURL)\(md5_uid.lowercased())/storages/\(URLs)"
                url.append( URL(string: full_url)! )
            }
            
            let vc = PreviewViewController.instantiateFromStoryboard()
            
            vc.urlArray = url
            vc.profileID = uids
            vc.imageProfileURL = profileImage!
            vc.indexPage = indexPath.row
            
            if let first = firstName , let last = lastName {
                vc.profileNameString = "\(first) \(last)"
            }
            
            vc.delegate = self
            
            SVProgressHUD.dismiss()
            present(vc, animated: true, completion: nil)
        }
        
        
    }
    
 }
 
