//
//  AppDelegate.swift
//  bookme4
//
//  Created by Naphat Sottiyaphai on 14/3/2561 BE.
//  Copyright © 2561 Naphat Sottiyaphai. All rights reserved.
//

import UIKit
import FirebaseMessaging
import FirebaseInstanceID
import FirebaseDatabase
import Firebase
import GoogleMaps
import GooglePlaces
import GoogleSignIn
import Alamofire
import SVProgressHUD
import IQKeyboardManagerSwift
import Kingfisher
import Fabric
import UserNotifications
import SwiftMessages
import FBSDKCoreKit
import FBSDKLoginKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate  {
    
    var window: UIWindow?
    let gcmMessageIDKey = "gcm.message_id"

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        
        // Firebase Config
        FirebaseApp.configure()
        GMSServices.provideAPIKey(apiKey.googleMapKey)
        GMSPlacesClient.provideAPIKey(apiKey.googlePlaceKey)
        Fabric.sharedSDK().debug = true
        Fabric.with([Crashlytics.self])
        
        // Google Sign in Delegate
        GIDSignIn.sharedInstance().clientID = apiKey.googleSignIn
        
        // IQKeyboard Enable
        IQKeyboardManager.shared.enable = true
        
        // SVProcessHUD Config
        SVProgressHUD.setDefaultMaskType(.black)
        SVProgressHUD.setBackgroundColor(.clear)
        SVProgressHUD.setForegroundColor(.white)
        SVProgressHUD.setRingThickness(4)
        
        // For iOS 10 data message (sent via FCM)
        Messaging.messaging().delegate = self
        Messaging.messaging().shouldEstablishDirectChannel = true
        
        // Kingfisher Cache Setting
        
        // In disk only
//        ImageCache.default.maxMemoryCost = 1
        
        // In Ram
//        let cache = ImageCache.default
//        cache.memoryStorage.config.totalCostLimit = 300 * 1024 * 1024
//        cache.memoryStorage.config.countLimit = 150
//        cache.diskStorage.config.sizeLimit = 1000 * 1024 * 1024
//        cache.memoryStorage.config.expiration = .seconds(600)
        
        UINavigationBar.appearance().prefersLargeTitles = true
        UINavigationBar.appearance().largeTitleTextAttributes =
            [NSAttributedString.Key.foregroundColor: UIColor.black,
             NSAttributedString.Key.font: UIFont(name: "SukhumvitSet-Bold", size: 30) ??
                UIFont.systemFont(ofSize: 30)]
        
        
//        PGWSDK.builder()
//            .merchantID(apiKey.merchantID2c2p)
//            .apiEnvironment(APIEnvironment.PRODUCTION)
//            .initialize()
        
        
        if #available(iOS 10.0, *) {
            // For iOS 10 display notification (sent via APNS)
            UNUserNotificationCenter.current().delegate = self
            
            let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
            UNUserNotificationCenter.current().requestAuthorization(
                options: authOptions,
                completionHandler: {_, _ in })
        } else {
            let settings: UIUserNotificationSettings =
                UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
            application.registerUserNotificationSettings(settings)
        }
        
        application.registerForRemoteNotifications()
        
        let userdefaults = UserDefaults.standard
//        let screenSize = UIScreen.main.bounds
        
        // Set NavigationBar Appearance
        UINavigationBar.appearance().setBackgroundImage(UIImage(), for: .default)
        UINavigationBar.appearance().shadowImage = UIImage()
        UINavigationBar.appearance().backgroundColor = .clear
        UINavigationBar.appearance().isTranslucent = true
        UINavigationBar.appearance().layer.borderWidth = 0.0
        UINavigationBar.appearance().clipsToBounds = true
        UINavigationBar.appearance().tintColor = UIColor.black
        UINavigationBar.appearance().barTintColor = UIColor.white
        
        // Set TabBar Appearance
        UITabBar.appearance().layer.borderWidth = 0.0
        UITabBar.appearance().clipsToBounds = true
        UITabBar.appearance().tintColor = UIColor.black
        UITabBar.appearance().barTintColor = UIColor.white
        
        if Reachability.isConnectedToNetwork(){
            if (userdefaults.object(forKey: "Login") == nil ){
                
//                let api = Instagram.shared
//                api.logout()
                
                GIDSignIn.sharedInstance().signOut()
                
                let DeviceLang = Locale.preferredLanguages[0]
                userdefaults.set(DeviceLang, forKey: "langDevice")
                
                let vc = LoginViewController.instantiateFromStoryboard()
                self.window?.rootViewController = vc
            }
            else{
                let isNonlogin = userdefaults.value(forKey: "isNonLogin") as! Bool
                if(isNonlogin){
                    
                }
                else{
                    self.updateBluweoToken2Bookme()
//                    self.refreshBluweoToken()
                }
            }
        }
        else{
//            let api = Instagram.shared
//            api.logout()
            
            GIDSignIn.sharedInstance().signOut()
            
            let DeviceLang = Locale.preferredLanguages[0]
            userdefaults.set(DeviceLang, forKey: "langDevice")
            
            let vc = LoginViewController.instantiateFromStoryboard()
            self.window?.rootViewController = vc
        }
        
        return true
    }
    
    
    func application(_ application: UIApplication, willEncodeRestorableStateWith coder: NSCoder) {
        print("AppDelegate willEncodeRestorableStateWith")
        if #available(iOS 13, *) {

        } else {
        // Trigger saving of the root view controller
            coder.encode(self.window?.rootViewController, forKey: "root")
        }
    }
    
    func application(_ application: UIApplication, didDecodeRestorableStateWith coder: NSCoder) {
        print("AppDelegate didDecodeRestorableStateWith")
    }

    func application(_ application: UIApplication, shouldSaveApplicationState coder: NSCoder) -> Bool {
        print("AppDelegate shouldSaveApplicationState")
        if #available(iOS 13, *) {
            return false
        } else {
            return true
        }
    }

    func application(_ application: UIApplication, shouldRestoreApplicationState coder: NSCoder) -> Bool {
        print("AppDelegate shouldRestoreApplicationState")
        if #available(iOS 13, *) {
            return false
        } else {
            return true
        }
    }
        
    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
        print("applicationWillResignActive")
        
        InstanceID.instanceID().instanceID { (result, error) in
            if let error = error {
                print("Error fetching remote instange ID: \(error)")
            } else if let result = result {
                let ud = UserDefaults.standard
                if(ud.object(forKey: "user_uid") != nil){
                    let uid = ud.value(forKey: "user_uid") as! String
                    let onlineStatusRef = Database.database().reference().child("user").child(uid)
                    
                    let messageItem : [String : Any] = [
                        "isOnline": false,
                        "token" : result.token
                    ]
                    
                    onlineStatusRef.updateChildValues(messageItem)
                }
            }
        }
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
        Messaging.messaging().shouldEstablishDirectChannel = false
        print("applicationDidEnterBackground")
        
        InstanceID.instanceID().instanceID { (result, error) in
            if let error = error {
                print("Error fetching remote instange ID: \(error)")
            } else if let result = result {
                
                let ud = UserDefaults.standard
                if(ud.object(forKey: "user_uid") != nil){
                    let uid = ud.value(forKey: "user_uid") as! String
                    let onlineStatusRef = Database.database().reference().child("user").child(uid)
                    
                    let messageItem : [String : Any] = [
                        "isOnline": false,
                        "token" : result.token
                    ]
                    
                    onlineStatusRef.updateChildValues(messageItem)
                }
            }
        }
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
        print("applicationWillEnterForeground")
        // Back to online
        
        InstanceID.instanceID().instanceID { (result, error) in
            if let error = error {
                print("Error fetching remote instange ID: \(error)")
            } else if let result = result {
                let ud = UserDefaults.standard
                if(ud.object(forKey: "user_uid") != nil){
                    let uid = ud.value(forKey: "user_uid") as! String
                    let onlineStatusRef = Database.database().reference().child("user").child(uid)
                    
                    let messageItem : [String : Any] = [
                        "isOnline": true,
                        "token" : result.token
                    ]
                    
                    onlineStatusRef.updateChildValues(messageItem)
                }
            }
        }
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
        
        print("applicationDidBecomeActive")
        AppEvents.activateApp()
        
        InstanceID.instanceID().instanceID { (result, error) in
            if let error = error {
                print("Error fetching remote instange ID: \(error)")
            } else if let result = result {
                let ud = UserDefaults.standard
                if(ud.object(forKey: "user_uid") != nil){
                    let uid = ud.value(forKey: "user_uid") as! String
                    let onlineStatusRef = Database.database().reference().child("user").child(uid)
                    
                    let messageItem : [String : Any] = [
                        "isOnline": true,
                        "token" : result.token
                    ]
                    
                    onlineStatusRef.updateChildValues(messageItem)
                }
            }
        }
    }

    func applicationWillTerminate(_ application: UIApplication) {
        print("App will terminate")
    }
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any]) {
        
    }
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any],
                     fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        
        completionHandler(UIBackgroundFetchResult.newData)
    }
    
    
//    func application(_ application: UIApplication, open url: URL, sourceApplication: String?, annotation: Any) -> Bool {
//
//        SVProgressHUD.show()
//
//        let googleDidHandle = GIDSignIn.sharedInstance().handle(url,sourceApplication: sourceApplication,annotation: annotation)
//        let facebookDidHandle = ApplicationDelegate.shared.application(application, open: url, sourceApplication: sourceApplication, annotation: annotation)
//
//        return googleDidHandle || facebookDidHandle
//    }
//
//    @available(iOS 9.0, *)
//    func application(_ app: UIApplication, open url: URL, options: [UIApplication.OpenURLOptionsKey : Any]) -> Bool {
//
//        let googleDidHandle = GIDSignIn.sharedInstance().handle(url,
//                                                                sourceApplication:options[UIApplication.OpenURLOptionsKey.sourceApplication] as? String,
//                                                                annotation: [:])
//
//        let facebookDidHandle = ApplicationDelegate.shared.application(app, open: url, sourceApplication: options[UIApplication.OpenURLOptionsKey.sourceApplication] as? String, annotation: nil)
//
//        return googleDidHandle || facebookDidHandle
//    }
    
    func application(_ application: UIApplication, open url: URL, sourceApplication: String?, annotation: Any) -> Bool {
           
           let googleDidHandle = GIDSignIn.sharedInstance()?.handle(url)
           let facebookDidHandle = ApplicationDelegate.shared.application(application, open: url, sourceApplication: sourceApplication, annotation: annotation)
           
           return googleDidHandle! || facebookDidHandle
       }
       
       @available(iOS 9.0, *)
       func application(_ app: UIApplication, open url: URL, options: [UIApplication.OpenURLOptionsKey : Any]) -> Bool {
           
           let googleDidHandle = GIDSignIn.sharedInstance()?.handle(url)
           let facebookDidHandle = ApplicationDelegate.shared.application(app, open: url, sourceApplication: options[UIApplication.OpenURLOptionsKey.sourceApplication] as? String, annotation: nil)
           
           return googleDidHandle! || facebookDidHandle
       }

    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        print("Unable to register for remote notifications: \(error.localizedDescription)")
    }
    
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
//        print("APNs token retrieved: \(deviceToken)")
    }
}

@available(iOS 10, *)
extension AppDelegate : UNUserNotificationCenterDelegate {
    
    // Receive displayed notifications for iOS 10 devices.
    func userNotificationCenter(_ center: UNUserNotificationCenter,
                                willPresent notification: UNNotification,
                                withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        
        print("Noti when app opening")
        let userInfo = notification.request.content.userInfo
        if let messageID = userInfo[gcmMessageIDKey] {
            print("Message ID: \(messageID)")
        }
        print(userInfo)
        
        let apn = userInfo["aps"] as! NSDictionary

        let apnData = apn
        let alert = apnData.value(forKey: "alert") as! NSDictionary
        let title = alert.value(forKey: "title") as! String
        let body = alert.value(forKey: "body") as! String

//        let warning = MessageView.viewFromNib(layout: .cardView)
//        warning.configureTheme(.success)
//        warning.configureDropShadow()
//
//        warning.configureContent(title: "\(title) Send", body: body, iconText: "📩")
//        warning.button?.isHidden = true
//        var warningConfig = SwiftMessages.defaultConfig
//        warningConfig.presentationContext = .window(windowLevel: UIWindow.Level.statusBar)
        
//        SwiftMessages.show(config: warningConfig, view: warning)
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter,
                                didReceive response: UNNotificationResponse,
                                withCompletionHandler completionHandler: @escaping () -> Void) {
        
        print("noti from background")
        let userInfo = response.notification.request.content.userInfo
        // Print message ID.
        if let messageID = userInfo[gcmMessageIDKey] {
            print("Message ID: \(messageID)")
        }
        
        let messageData = userInfo as NSDictionary
        // Print full message.
        print(userInfo)
        print(messageData)
        
        if (messageData.object(forKey: "roomName") != nil){
            print("noti from chat")
            
            let senderId = messageData.value(forKey: "senderID") as! String
            let reciverID = messageData.value(forKey: "reciverID") as! String
            let senderAvatar = messageData.value(forKey: "avatarURL") as! String
            let senderName = messageData.value(forKey: "senderName") as! String
            
            let userDefaults = UserDefaults.standard
            let uid = userDefaults.value(forKey: "user_uid") as! String
            
            if(uid == reciverID){
                let vc = chatViewController.instantiateFromStoryboard()
                vc.chatSlug = senderId
                vc.roomName = senderName
                vc.avatarURL = senderAvatar
                vc.from = "noti"
                
                self.window = UIWindow(frame: UIScreen.main.bounds)
                let nav1 = UINavigationController()
                nav1.viewControllers = [vc]
                self.window!.rootViewController = nav1
                self.window?.makeKeyAndVisible()
            }
        }
        else{
            if(messageData.object(forKey: "bookingId") != nil){
                let bookingID = messageData.value(forKey: "bookingId") as! String
                
                let vc = jobDetailViewController.instantiateFromStoryboard()
                
                vc.callDataByID = true
                vc.bookingID = bookingID
                
                self.window = UIWindow(frame: UIScreen.main.bounds)
                let nav1 = UINavigationController()
                nav1.viewControllers = [vc]
                self.window!.rootViewController = nav1
                self.window?.makeKeyAndVisible()
            }
            else{
               print("from other")
            }
        }
        completionHandler()
    }
}


extension AppDelegate : MessagingDelegate {
    // [START refresh_token]
    private func messaging(_ messaging: Messaging, didRefreshRegistrationToken fcmToken: String) {
        print("Firebase registration token: \(fcmToken)")
        Messaging.messaging().shouldEstablishDirectChannel = true
        let userDefaults = Foundation.UserDefaults.standard
        userDefaults.set(fcmToken, forKey: "token")
    }
}

extension AppDelegate {
    
    func refreshBluweoToken() {
        print("bluweo refresh token working")
        
        let userDefaults = UserDefaults.standard
        let refreshToken = userDefaults.value(forKey: "bluweoRefreshToken") as! String
        
        let url = "\(baseURL.bluweoConnectAuth)refresh_token"
        
        let headers = [
            "client_id" : "1001",
            "secret_key" : "bookme",
            "Content-Type": "application/x-www-form-urlencoded"
        ]
        
        let params = [
            "refresh_token": refreshToken
        ]
        
        Alamofire.request(URL(string: url)!, method: .post , parameters: params, encoding: URLEncoding(destination: .httpBody) , headers: headers).responseJSON(completionHandler: { (respond) in
            
            if let result = respond.result.value{
                let responsed = result as! NSDictionary
                let response = responsed.value(forKey: "response") as! Bool
                
                if(response){
                    let data = responsed.value(forKey: "data") as! NSDictionary
                    let access_token = data.value(forKey: "access_token") as! String
                    
                    let userDefaults = UserDefaults.standard
                    userDefaults.set(access_token, forKey: "bluweoAccessToken")
                    
//                    self.updateBluweoToken2Bookme()
                }
                else{
                    let data = responsed.value(forKey: "data") as! NSDictionary
                    let message = data.value(forKey: "message") as! NSArray
                    let msg = message[0] as! NSDictionary
                    let errMsg = msg.value(forKey: "message") as! String
                 
                    if(errMsg == "access token not valid or expired" || errMsg == "refresh token not valid"){
                        
//                        let api = Instagram.shared
//                        api.logout()
                        
                        GIDSignIn.sharedInstance().signOut()
                        
                        let domain = Bundle.main.bundleIdentifier!
                        UserDefaults.standard.removePersistentDomain(forName: domain)
                        UserDefaults.standard.synchronize()
                        
                        SVProgressHUD.showError(withStatus: "You had login in another device.")
                        
                        DispatchQueue.main.asyncAfter(deadline: .now() + 2){
                            let vc = LoginViewController.instantiateFromStoryboard()
                            self.window?.rootViewController = vc
                            SVProgressHUD.dismiss()
                        }
                    }
                    else{
                        SVProgressHUD.showError(withStatus: errMsg)
                        DispatchQueue.main.asyncAfter(deadline: .now() + 2){
                            SVProgressHUD.dismiss()
                        }
                    }
                    
                    
                }
            }
            else{
                print(respond.error?.localizedDescription ?? "")
                SVProgressHUD.dismiss()
            }
        })
    }
    
    func updateBluweoToken2Bookme() {
        print("bookme update token")

        let ud = UserDefaults.standard
        var accesstoken = String()
        var user_uid = String()
        
        if(ud.object(forKey: "accessToken") != nil){
            accesstoken = ud.value(forKey: "accessToken") as! String
        }
        if(ud.object(forKey: "user_uid") != nil){
            user_uid = ud.value(forKey: "user_uid") as! String
        }
        
        let header : HTTPHeaders = [
            "accessToken" : accesstoken
        ]
        
        Alamofire.request(baseURL.url + "profile/updateBluweo/" + user_uid , method: .get, encoding: JSONEncoding.default , headers : header )
            .responseJSON(completionHandler: { response in
                
                if let result = response.result.value {
                    if(result is NSDictionary){
                        let data = result as! NSDictionary
                        
                        if(data.value(forKey: "data") is NSDictionary){
                            let JSON = data.value(forKey: "data") as! NSDictionary
                            _ = JSON.value(forKey: "_id") as! String
                            
                            if(data.value(forKey: "err") is String){
                                if(data.value(forKey: "err") as! String == "Profile is already Exist."){
                                    return
                                }
                            }
                        }
                    }
                }
                else{
                    SVProgressHUD.showError(withStatus: "Server Error")
                    
                    DispatchQueue.main.asyncAfter(deadline: .now() + 2){
                        SVProgressHUD.dismiss()
                    }
                }
            })
    }
}

extension UIApplication {
//    var statusBar : UIView? {
//        return value(forKey: "statusBar") as? UIView
//    }
}

