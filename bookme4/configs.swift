//
//  configs.swift
//  bookme4
//
//  Created by Naphat Sottiyaphai on 15/3/2561 BE.
//  Copyright © 2561 Naphat Sottiyaphai. All rights reserved.
//

import Foundation
import UIKit

struct baseURL {
    static let bluweoConnectAuth        = "aHR0cHM6Ly9jb25uZWN0LmJsdXdlby5jb20vYXV0aC8=".C()
    static let bluweoConnectPassword    = "aHR0cHM6Ly9jb25uZWN0LmJsdXdlby5jb20vYXV0aC9wYXNzd29yZC8=".C()
    static let bluweoMedia              = "aHR0cHM6Ly9tZWRpYS5ibHV3ZW8uY29tLw==".C()
    static let bluweoPayment            = "aHR0cHM6Ly9jb25uZWN0LmJsdXdlby5jb20vcGF5bWVudC8=".C()
    static let bluweoProfile            = "aHR0cHM6Ly9jb25uZWN0LmJsdXdlby5jb20vcHJvZmlsZQ==".C()
    static let connect                  = "aHR0cHM6Ly9jb25uZWN0LmJsdXdlby5jb20v" .C()
    static let firebaseFunction         = "aHR0cHM6Ly91cy1jZW50cmFsMS1ib29rbWUtMTUxNTQ5MjU5MjA4MS5jbG91ZGZ1bmN0aW9ucy5uZXQv".C()
    static let media                    = "aHR0cDovL21lZGlhLmJsdXdlby5jb206MzAyMi8=".C()
    static let photoURL                 = "aHR0cDovL21lZGlhLmJsdXdlby5jb206MzAyMi91cGxvYWRzLw==".C()
    
    // Dev Server
        static let url                    = "aHR0cDovL2Nvbm5lY3QtZGV2LmFwcGJvb2ttZS5jb206MzMyNy9hcGkv".C()
    
    // Production Server
//        static let url                      = "aHR0cDovL2Nvbm5lY3QuYXBwYm9va21lLmNvbTozMzIyL2FwaS8=".C()
    
    
//        static let url = "http://192.168.0.152:3327/api/"
    //    static let url = "http://192.168.0.161:3327/api/"
    //    static let url = "http://192.168.31.7:3327/api/"
}

struct apiKey {
    static let appId          = "5b36e15d4baa431e69e45441"
    static let clientId       = "e106a0be59fbe1c6a925c8f262ba4dc4"
    static let googleMapKey   = "AIzaSyC9Wpo6ZHwLm7pX1ACtUTBsVr6aCEole3c"
    static let googlePlaceKey = "AIzaSyBES3EOtxgx48YxiCis9YGFLzfLdpQi0A0"
    static let googleSignIn   = "498910981164-5g274aah5pb2jmch78iilet4q334f7ic.apps.googleusercontent.com"
    static let merchantID2c2p = "764764000001952"
    static let secretId       = "a07a84b7a4d5bd36cf7ed320f7cf6750"
    static let secretKey2c2p  = "CEF79108ED721B77FC94405EC4340EC5CE6257063263101142194F64AF2AC819"
}

struct commonValue {
    static let descSize  : CGFloat         = 13.0
    static let headerSize  : CGFloat       = 22.0
    static let igCount : Int               = 12
    static let imageCompress : CGFloat     = 0.3
    static let lHeaderSize  : CGFloat      = 26.0
    static let profileCellHeight : CGFloat = 200
    static let subTadius  : Float          = 10.0
    static let subTextSize  : CGFloat      = 15.0
    static let textSize  : CGFloat         = 17.0
    static let viewRadius : Float          = 10.0
}

struct tableViewValue {
    static let headerViewSize        = 35
    static let profileHeaderViewSize = 50
}

struct commonColor {
    static let bgColor            = UIColor(red: 242.0/255.0, green: 242.0/255.0, blue: 242.0/255.0, alpha: 1.0)
    static let darkBlueColor      = UIColor(red: 32.0/255.0, green: 58.0/255.0, blue: 78.0/255.0, alpha: 1.0)
    static let darkYellow         = UIColor(red: 218.0/255.0, green: 156.0/255.0, blue: 21.0/255.0, alpha: 1.0)
    static let lightDarkBlueColor = UIColor(red: 98.0/255.0, green: 133.0/255.0, blue: 144.0/255.0, alpha: 1.0)
    static let lightkBlueColor    = UIColor(red: 144.0/255.0, green: 177.0/255.0, blue: 187.0/255.0, alpha: 1.0)
    static let mainBlueColor      = UIColor(red: 43.0/255.0, green: 177.0/255.0, blue: 234.0/255.0, alpha: 1.0)
    static let mainYellow         = UIColor(red: 253.0/255.0, green: 194.0/255.0, blue: 15.0/255.0, alpha: 1.0)
    static let onlineGreen        = UIColor(red: 114.0/255.0, green: 178.0/255.0, blue: 84.0/255.0, alpha: 1.0)
    static let redError           = UIColor(red: 187.0/255.0, green: 76.0/255.0, blue: 70.0/255.0, alpha: 1.0)
    static let subBlue            = UIColor (red: 204.0/255.0, green: 237.0/255.0, blue: 249.0/255.0, alpha: 1.0)
}


struct commonImage {
    static let iconFavBig   = UIImage(named: "icon_fav_big")
    static let iconUnfavBig = UIImage(named: "icon_unfav_big")
}
