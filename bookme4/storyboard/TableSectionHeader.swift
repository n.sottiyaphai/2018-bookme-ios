//
//  TableSectionHeader.swift
//  TOR
//
//  Created by Naphat Sottiyaphai on 8/26/2560 BE.
//
//

import UIKit

class TableSectionHeader: UITableViewHeaderFooterView {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var bgView: UIView!
}
