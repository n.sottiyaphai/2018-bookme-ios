//
//  ProfileTableSelectionHeader.swift
//  bookme
//
//  Created by Naphat Sottiyaphai on 19/5/2562 BE.
//  Copyright © 2562 Naphat Sottiyaphai. All rights reserved.
//

import UIKit

class ProfileTableSelectionHeader: UITableViewHeaderFooterView {
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var bgView: UIView!
}
