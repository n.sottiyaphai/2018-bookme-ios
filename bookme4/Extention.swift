//
//  Extention.swift
//  BookMe_2
//
//  Created by Naphat Sottiyaphai on 9/4/2560 BE.
//  Copyright © 2560 Naphat Sottiyaphai. All rights reserved.
//

import Foundation
import UIKit
import SystemConfiguration
import Alamofire

extension UIDevice {
    var iPhone: Bool {
        return UIDevice().userInterfaceIdiom == .phone
    }
    enum ScreenType: String {
        case iPhone4
        case iPhone5
        case iPhone6
        case iPhone6Plus
        case iPhoneX
        case unknown
    }
    var screenType: ScreenType {
        guard iPhone else { return .unknown }
        switch UIScreen.main.nativeBounds.height {
        case 960:
            return .iPhone4
        case 1136:
            return .iPhone5
        case 1334:
            return .iPhone6
        case 2208:
            return .iPhone6Plus
        case 2436:
            return .iPhoneX
        default:
            return .unknown
        }
    }
}

extension String {
    func guessLanguage() -> String {
        let length = self.utf16.count
        let languageCode = CFStringTokenizerCopyBestStringLanguage(self as CFString, CFRange(location: 0, length: length)) as String? ?? ""
        
        let locale = Locale(identifier: languageCode)
        return locale.localizedString(forLanguageCode: languageCode) ?? "Unknown"
    }
    
    func C() -> String {
        
        let decodedData = Data(base64Encoded: self)!
        let decodedString = String(data: decodedData, encoding: .utf8)!
        
        return decodedString
    }
    
}

extension UISearchBar {
    
    private func getViewElement<T>(type: T.Type) -> T? {
        
        let svs = subviews.flatMap { $0.subviews }
        guard let element = (svs.filter { $0 is T }).first as? T else { return nil }
        return element
    }
    
    func getSearchBarTextField() -> UITextField? {
        
        return getViewElement(type: UITextField.self)
    }
    
    func setTextColor(color: UIColor) {
        
        if let textField = getSearchBarTextField() {
            textField.textColor = color
        }
    }
    
    func setTextFieldColor(color: UIColor) {
        
        if let textField = getViewElement(type: UITextField.self) {
            switch searchBarStyle {
            case .minimal:
                textField.layer.backgroundColor = color.cgColor
                textField.layer.cornerRadius = 10
                
                
            case .prominent, .default:
                textField.backgroundColor = color
                textField.layer.cornerRadius = 10
            }
        }
    }
    
    func setTextFieldClearButtonColor(color: UIColor) {
        
        if let textField = getSearchBarTextField() {
            
            let button = textField.value(forKey: "clearButton") as! UIButton
            if let image = button.imageView?.image {
                button.setImage(image.transform(withNewColor: color), for: .normal)
            }
        }
    }
    
    func setSearchImageColor(color: UIColor) {
        
        if let imageView = getSearchBarTextField()?.leftView as? UIImageView {
            imageView.image = imageView.image?.transform(withNewColor: color)
        }
    }
}

extension UITextView{
    
    func resizeTextView(descText : UITextView) -> CGFloat {
        let contentSize = descText.sizeThatFits(descText.bounds.size)
        var frame = descText.frame
        frame.size.height = contentSize.height
        descText.frame = frame
        
        return frame.size.height
    }
    
    func setBigHeaderTextBold() {
        self.font = UIFont.init(name: "SukhumvitSet-Bold", size: commonValue.lHeaderSize)
    }
    
    func setHeaderText() {
        self.font = UIFont.init(name: "SukhumvitSet-Text", size: commonValue.headerSize)
    }
    
    func setHeaderTextBold() {
        self.font = UIFont.init(name: "SukhumvitSet-Bold", size: commonValue.headerSize)
    }
    
    func setText() {
        self.font = UIFont.init(name: "SukhumvitSet-Text", size: commonValue.textSize)
    }
    
    func setTextBold() {
        self.font = UIFont.init(name: "SukhumvitSet-Bold", size: commonValue.textSize)
    }
    
    func setTextSemiBold() {
        self.font = UIFont.init(name: "SukhumvitSet-SemiBold", size: commonValue.textSize)
    }
    
    func setTextMedium() {
        self.font = UIFont.init(name: "SukhumvitSet-text", size: commonValue.textSize)
    }
    
    func setTextLight() {
        self.font = UIFont.init(name: "SukhumvitSet-Light", size: commonValue.textSize)
    }
    
    func setTextThin() {
        self.font = UIFont.init(name: "SukhumvitSet-Thin", size: commonValue.textSize)
    }
    
    func setSubText() {
        self.font = UIFont.init(name: "SukhumvitSet-Text", size: commonValue.subTextSize)
    }
    
    func setSubTextBold() {
        self.font = UIFont.init(name: "SukhumvitSet-Bold", size: commonValue.subTextSize)
    }
    
    func setSubTextSemiBold() {
        self.font = UIFont.init(name: "SukhumvitSet-SemiBold", size: commonValue.subTextSize)
    }
    
    func setSubTextMedium() {
        self.font = UIFont.init(name: "SukhumvitSet-text", size: commonValue.subTextSize)
    }
    
    func setSubTextLight() {
        self.font = UIFont.init(name: "SukhumvitSet-Light", size: commonValue.subTextSize)
    }
    
    func setSubTextThin() {
        self.font = UIFont.init(name: "SukhumvitSet-Thin", size: commonValue.subTextSize)
    }
    
    func setDescText() {
        self.font = UIFont.init(name: "SukhumvitSet-Text", size: commonValue.descSize)
    }
    
    func setDescTextBold() {
        self.font = UIFont.init(name: "SukhumvitSet-Bold", size: commonValue.descSize)
    }
    
    func setDescTextSemiBold() {
        self.font = UIFont.init(name: "SukhumvitSet-SemiBold", size: commonValue.descSize)
    }
    
    func setDescTextMedium() {
        self.font = UIFont.init(name: "SukhumvitSet-text", size: commonValue.descSize)
    }
    
    func setDescTextLight() {
        self.font = UIFont.init(name: "SukhumvitSet-Light", size: commonValue.descSize)
    }
    
    func setDescTextThin() {
        self.font = UIFont.init(name: "SukhumvitSet-Thin", size: commonValue.descSize)
    }
}

extension UITextField{
    
    func setRoundDefaultStyle() {
        self.setSubText() 
        self.setRoundWithCommonRwithBGC_f8()
        self.setLeftPaddingPoints(15)
    }
    
    func setLeftPaddingPoints(_ amount:CGFloat){
        let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: amount, height: self.frame.size.height))
        self.leftView = paddingView
        self.leftViewMode = .always
    }
    func setRightPaddingPoints(_ amount:CGFloat) {
        let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: amount, height: self.frame.size.height))
        self.rightView = paddingView
        self.rightViewMode = .always
    }
    
    func setDefaultTextFieldLayout() {
        self.layer.cornerRadius = CGFloat(commonValue.viewRadius)
        self.setLeftPaddingPoints(15)
        self.setRightPaddingPoints(15)
        self.layer.masksToBounds = true
        self.clipsToBounds = true
    }
    
    func setBigHeaderTextBold() {
        self.font = UIFont.init(name: "SukhumvitSet-Bold", size: commonValue.lHeaderSize)
    }
    
    func setHeaderText() {
        self.font = UIFont.init(name: "SukhumvitSet-Text", size: commonValue.headerSize)
    }
    
    func setHeaderTextBold() {
        self.font = UIFont.init(name: "SukhumvitSet-Bold", size: commonValue.headerSize)
    }
    
    func setText() {
        self.font = UIFont.init(name: "SukhumvitSet-Text", size: commonValue.textSize)
    }
    
    func setTextBold() {
        self.font = UIFont.init(name: "SukhumvitSet-Bold", size: commonValue.textSize)
    }
    
    func setTextSemiBold() {
        self.font = UIFont.init(name: "SukhumvitSet-SemiBold", size: commonValue.textSize)
    }
    
    func setTextMedium() {
        self.font = UIFont.init(name: "SukhumvitSet-text", size: commonValue.textSize)
    }
    
    func setTextLight() {
        self.font = UIFont.init(name: "SukhumvitSet-Light", size: commonValue.textSize)
    }
    
    func setTextThin() {
        self.font = UIFont.init(name: "SukhumvitSet-Thin", size: commonValue.textSize)
    }
    
    func setSubText() {
        self.font = UIFont.init(name: "SukhumvitSet-Text", size: commonValue.subTextSize)
    }
    
    func setSubTextBold() {
        self.font = UIFont.init(name: "SukhumvitSet-Bold", size: commonValue.subTextSize)
    }
    
    func setSubTextSemiBold() {
        self.font = UIFont.init(name: "SukhumvitSet-SemiBold", size: commonValue.subTextSize)
    }
    
    func setSubTextMedium() {
        self.font = UIFont.init(name: "SukhumvitSet-text", size: commonValue.subTextSize)
    }
    
    func setSubTextLight() {
        self.font = UIFont.init(name: "SukhumvitSet-Light", size: commonValue.subTextSize)
    }
    
    func setSubTextThin() {
        self.font = UIFont.init(name: "SukhumvitSet-Thin", size: commonValue.subTextSize)
    }
    
    func setDescText() {
        self.font = UIFont.init(name: "SukhumvitSet-Text", size: commonValue.descSize)
    }
    
    func setDescTextBold() {
        self.font = UIFont.init(name: "SukhumvitSet-Bold", size: commonValue.descSize)
    }
    
    func setDescTextSemiBold() {
        self.font = UIFont.init(name: "SukhumvitSet-SemiBold", size: commonValue.descSize)
    }
    
    func setDescTextMedium() {
        self.font = UIFont.init(name: "SukhumvitSet-text", size: commonValue.descSize)
    }
    
    func setDescTextLight() {
        self.font = UIFont.init(name: "SukhumvitSet-Light", size: commonValue.descSize)
    }
    
    func setDescTextThin() {
        self.font = UIFont.init(name: "SukhumvitSet-Thin", size: commonValue.descSize)
    }
}

extension UIButton {
    func setRoundedwithR(r : Float) {
        self.layer.cornerRadius = CGFloat(r)
        self.layer.masksToBounds = true
        self.clipsToBounds = true
    }
}


protocol UIViewLoading {}
extension UIView : UIViewLoading {}

extension UIViewLoading where Self : UIView {
    
    // note that this method returns an instance of type `Self`, rather than UIView
    static func loadFromNib() -> Self {
        let nibName = "\(self)".split{$0 == "."}.map(String.init).last!
        let nib = UINib(nibName: nibName, bundle: nil)
        return nib.instantiate(withOwner: self, options: nil).first as! Self
    }
}

extension UIColor {
    static var peach: UIColor {
        return  UIColor(red: 1, green: 0.5764705882, blue: 0.5843137255, alpha: 1)
    }
    
    static var orange: UIColor {
        return UIColor(red: 1, green: 0.5764705882, blue: 0.462745098, alpha: 1)
    }
}

extension UIView {
    
    func applyGradient() {
        let gradient = CAGradientLayer()
        gradient.frame = bounds
        gradient.locations = [0, 1]
        gradient.startPoint = CGPoint(x: 0, y: 0)
        gradient.endPoint = CGPoint(x: 1, y: 1)
        gradient.colors = [
            UIColor.peach.cgColor,
            UIColor.orange.cgColor
        ]
        layer.insertSublayer(gradient, at: 0)
    }
    
    
    func applyGradientByColorArray(colorsArr : [CGColor]) {
        let gradient = CAGradientLayer()
        gradient.frame = bounds
        gradient.locations = [0, 1]
        gradient.startPoint = CGPoint(x: 0, y: 0)
        gradient.endPoint = CGPoint(x: 1, y: 1)
        gradient.colors = [
            colorsArr[0],
            colorsArr[1]
        ]
        layer.insertSublayer(gradient, at: 0)
    }
    
    func setRounded() {
        let radius = (self.frame.height)/2
        self.layer.cornerRadius = radius
        self.layer.masksToBounds = true
        self.clipsToBounds = true
    }
    
    func setRoundWithCommonR() {
        self.layer.cornerRadius = CGFloat(commonValue.viewRadius)
        self.layer.masksToBounds = true
        self.clipsToBounds = true
    }
    
    func setRoundWithCommonRwithBGC_fe() {
        self.layer.cornerRadius = CGFloat(commonValue.viewRadius)
        self.layer.backgroundColor = UIColor(red: 254/255.0, green: 254/255.0, blue: 254/255.0, alpha: 1.0).cgColor
        self.layer.masksToBounds = true
        self.clipsToBounds = true
    }
    
    func setRoundWithCommonRwithBGC_f8() {
        self.layer.cornerRadius = CGFloat(commonValue.viewRadius)
        self.layer.backgroundColor = UIColor(red: 248.0/255.0, green: 248.0/255.0, blue: 248.0/255.0, alpha: 1.0).cgColor
        self.layer.masksToBounds = true
        self.clipsToBounds = true
    }
    
    func setRoundWithCommonRwithBGC_f2() {
        self.layer.cornerRadius = CGFloat(commonValue.viewRadius)
        self.layer.backgroundColor = UIColor(red: 242.0/255.0, green: 242.0/255.0, blue: 242.0/255.0, alpha: 1.0).cgColor
        self.layer.masksToBounds = true
        self.clipsToBounds = true
    }
    
    func setRoundWithCommonRWithBorder() {
        self.layer.cornerRadius = CGFloat(commonValue.viewRadius)
        self.layer.borderColor = UIColor(red: 242.0/255.0, green: 242.0/255.0, blue: 242.0/255.0, alpha: 1.0).cgColor
        self.layer.borderWidth = 1.5
        self.layer.masksToBounds = true
        self.clipsToBounds = true
    }
    
    func setRoundWithR( r : Float) {
        let radius = r
        self.layer.cornerRadius = CGFloat(radius)
        self.layer.masksToBounds = true
        self.clipsToBounds = true
    }
    
    func setRoundWithShadow(r : Float) {
        self.layer.cornerRadius = CGFloat(r)
        self.layer.masksToBounds = true
        
        self.layer.masksToBounds = false
        self.layer.shadowOffset = .zero
        self.layer.shadowColor = UIColor.black.cgColor
        self.layer.shadowOpacity = 0.2
        self.layer.shadowRadius = 5
    }
    
    func clearRoundWithShadow(r : Float) {
        self.layer.cornerRadius = CGFloat(r)
        self.layer.masksToBounds = true
        
        self.layer.masksToBounds = false
        self.layer.shadowOffset = .zero
        self.layer.shadowColor = UIColor.white.cgColor
        self.layer.shadowOpacity = 0
        //        self.layer.shadowRadius = 10
    }
    
    func setRoundOnlyRightWithR(r : Float) {
        self.clipsToBounds = true
        self.layer.cornerRadius = CGFloat(r)
        if #available(iOS 11.0, *) {
            self.layer.maskedCorners = [.layerMaxXMinYCorner, .layerMaxXMaxYCorner]
        } else {
            // Fallback on earlier versions
        }
    }
    
    func setRoundOnlyTop(r : Float) {
        self.clipsToBounds = true
        self.layer.cornerRadius = CGFloat(r)
        if #available(iOS 11.0, *) {
            self.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner]
        } else {
            // Fallback on earlier versions
        }
    }
    
    func setRoundOnlyDown(r : Float) {
        self.clipsToBounds = true
        self.layer.cornerRadius = CGFloat(r)
        if #available(iOS 11.0, *) {
            self.layer.maskedCorners = [.layerMinXMaxYCorner, .layerMaxXMaxYCorner]
        } else {
            // Fallback on earlier versions
        }
    }
    
    // OUTPUT 1
    func dropShadow(scale: Bool = true) {
        self.layer.masksToBounds = false
        self.layer.shadowColor = UIColor.black.cgColor
        self.layer.shadowOpacity = 0.1
        self.layer.shadowOffset = .zero
        self.layer.shadowRadius = 1
        
        self.layer.shadowPath = UIBezierPath(rect: self.bounds).cgPath
        self.layer.shouldRasterize = true
        self.layer.rasterizationScale = scale ? UIScreen.main.scale : 1
    }
    
    func clearDropShadow(scale: Bool = true) {
        self.layer.masksToBounds = false
        self.layer.shadowColor = UIColor.white.cgColor
        self.layer.shadowOpacity = 0.1
        self.layer.shadowOffset = .zero
        self.layer.shadowRadius = 1
        
        self.layer.shadowPath = UIBezierPath(rect: self.bounds).cgPath
        self.layer.shouldRasterize = true
        self.layer.rasterizationScale = scale ? UIScreen.main.scale : 1
    }
    
    func setOnlyDrop() {
        self.layer.masksToBounds = false
        self.layer.shadowColor = UIColor.black.cgColor
        self.layer.shadowOpacity = 0.1
        self.layer.shadowOffset = .zero
        self.layer.shadowRadius = 2
    }
}

public extension UIImage {
    
    func transform(withNewColor color: UIColor) -> UIImage {
        UIGraphicsBeginImageContextWithOptions(size, false, scale)
        
        let context = UIGraphicsGetCurrentContext()!
        context.translateBy(x: 0, y: size.height)
        context.scaleBy(x: 1.0, y: -1.0)
        context.setBlendMode(.normal)
        
        let rect = CGRect(x: 0, y: 0, width: size.width, height: size.height)
        context.clip(to: rect, mask: cgImage!)
        
        color.setFill()
        context.fill(rect)
        
        let newImage = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        return newImage
    }
    
    convenience init?(color: UIColor, size: CGSize = CGSize(width: 1, height: 1)) {
        let rect = CGRect(origin: .zero, size: size)
        UIGraphicsBeginImageContextWithOptions(rect.size, false, 0.0)
        color.setFill()
        UIRectFill(rect)
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        guard let cgImage = image?.cgImage else { return nil }
        self.init(cgImage: cgImage)
    }
}

public class Reachability {
    
    class func isConnectedToNetwork() -> Bool {
        
        var zeroAddress = sockaddr_in(sin_len: 0, sin_family: 0, sin_port: 0, sin_addr: in_addr(s_addr: 0), sin_zero: (0, 0, 0, 0, 0, 0, 0, 0))
        zeroAddress.sin_len = UInt8(MemoryLayout.size(ofValue: zeroAddress))
        zeroAddress.sin_family = sa_family_t(AF_INET)
        
        let defaultRouteReachability = withUnsafePointer(to: &zeroAddress) {
            $0.withMemoryRebound(to: sockaddr.self, capacity: 1) {zeroSockAddress in
                SCNetworkReachabilityCreateWithAddress(nil, zeroSockAddress)
            }
        }
        
        var flags: SCNetworkReachabilityFlags = SCNetworkReachabilityFlags(rawValue: 0)
        if SCNetworkReachabilityGetFlags(defaultRouteReachability!, &flags) == false {
            return false
        }
        
        /* Only Working for WIFI
         let isReachable = flags == .reachable
         let needsConnection = flags == .connectionRequired
         
         return isReachable && !needsConnection
         */
        
        // Working for Cellular and WIFI
        let isReachable = (flags.rawValue & UInt32(kSCNetworkFlagsReachable)) != 0
        let needsConnection = (flags.rawValue & UInt32(kSCNetworkFlagsConnectionRequired)) != 0
        let ret = (isReachable && !needsConnection)
        
        return ret
        
    }
}

extension Int {
    func formatnumber(groupingSeparator: String?) -> String {
        let formater = NumberFormatter()
        formater.groupingSeparator = (groupingSeparator != nil) ? groupingSeparator! : ","
        formater.numberStyle = .decimal
        return formater.string(from: NSNumber(value: self))!
    }
}

extension UITableView {
    
    func setEmptyMessage(_ message: String) {
        let messageLabel = UILabel(frame: CGRect(x: 0, y: 0, width: self.bounds.size.width, height: self.bounds.size.height))
        messageLabel.backgroundColor = UIColor(red: 242.0/255.0, green: 242.0/255.0, blue: 242.0/255.0, alpha: 1.0)
        messageLabel.text = message
        messageLabel.textColor = UIColor.lightGray
        messageLabel.numberOfLines = 0;
        messageLabel.textAlignment = .center;
        messageLabel.font = UIFont(name: "SukhumvitSet-Text", size: 15)
        messageLabel.sizeToFit()
        
        self.backgroundView = messageLabel;
        self.separatorStyle = .none;
    }
    
    func restore() {
        self.backgroundColor = UIColor.clear
        self.backgroundView = nil
        self.separatorStyle = .singleLine
    }
}

extension UINavigationBar {
    func transparentNavigationBar() {
        self.setBackgroundImage(UIImage(), for: .default)
        self.shadowImage = UIImage()
        self.isTranslucent = true
    }
    
    func addShadownavbar() {
        self.layer.masksToBounds = false
        self.layer.shadowColor = UIColor.lightGray.cgColor
        self.layer.shadowOpacity = 0.8
        self.layer.shadowOffset = CGSize(width: 0, height: 2.0)
        self.layer.shadowRadius = 2
    }
}


extension UIImage {
    class func colorForNavBar(color: UIColor) -> UIImage {
        
        let rect = CGRect(origin: CGPoint(x: 0,y :0), size: CGSize(width: 1.0, height: 1.0))
        
        UIGraphicsBeginImageContext(rect.size)
        let context = UIGraphicsGetCurrentContext()
        
        context!.setFillColor(color.cgColor)
        context!.fill(rect)
        
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        
        return image!
    }
}

extension UIViewController{
    
    func bookmeTokenCreate() {
        
        let url = "\(baseURL.url)auth"
        
        Alamofire.request(url, method: .post).responseJSON { (response) in
            if let result = response.result.value{
                print(result as! NSDictionary)
                print("Token Created")
                
                let data = (result as! NSDictionary).value(forKey: "data") as! NSDictionary
                let accessToken = data.value(forKey: "accessToken") as! String
                
                let userDefault = UserDefaults.standard
                userDefault.set(accessToken, forKey: "accessToken")
            }
            else{
                print("err : Login view / bookmeTokenCreate()")
            }
            
        }
    }
    
    func setBackgroundColor()  {
        self.view.backgroundColor = commonColor.bgColor
    }
    
    func setBackgroudWallpaper() {
        let backgroundImage = UIImageView(frame: UIScreen.main.bounds)
        backgroundImage.image = UIImage(named: "wallpaper")
        backgroundImage.contentMode = UIView.ContentMode.scaleAspectFill
        self.view.insertSubview(backgroundImage, at: 0)
    }
    
    func setClearNavBar() {
        self.navigationController?.navigationBar.backgroundColor = UIColor(red: 1, green: 1, blue: 1, alpha: 0)
//        UIApplication.shared.statusBar?.backgroundColor = UIColor(red: 1, green: 1, blue: 1, alpha: 0)
    }
}

extension Double {
    func roundToDecimal(_ fractionDigits: Int) -> Double {
        let multiplier = pow(10, Double(fractionDigits))
        return Darwin.round(self * multiplier) / multiplier
    }
}

extension UILabel {
    
    func setBigHeaderTextBold() {
        self.font = UIFont.init(name: "SukhumvitSet-Bold", size: commonValue.lHeaderSize)
    }
    
    func setHeaderText() {
        self.font = UIFont.init(name: "SukhumvitSet-Text", size: commonValue.headerSize)
    }
    
    func setHeaderTextBold() {
        self.font = UIFont.init(name: "SukhumvitSet-Bold", size: commonValue.headerSize)
    }
    
    func setText() {
        self.font = UIFont.init(name: "SukhumvitSet-Text", size: commonValue.textSize)
    }
    
    func setTextBold() {
        self.font = UIFont.init(name: "SukhumvitSet-Bold", size: commonValue.textSize)
    }
    
    func setTextSemiBold() {
        self.font = UIFont.init(name: "SukhumvitSet-SemiBold", size: commonValue.textSize)
    }
    
    func setTextMedium() {
        self.font = UIFont.init(name: "SukhumvitSet-text", size: commonValue.textSize)
    }
    
    func setTextLight() {
        self.font = UIFont.init(name: "SukhumvitSet-Light", size: commonValue.textSize)
    }
    
    func setTextThin() {
        self.font = UIFont.init(name: "SukhumvitSet-Thin", size: commonValue.textSize)
    }
    
    func setSubText() {
        self.font = UIFont.init(name: "SukhumvitSet-Text", size: commonValue.subTextSize)
    }
    
    func setSubTextBold() {
        self.font = UIFont.init(name: "SukhumvitSet-Bold", size: commonValue.subTextSize)
    }
    
    func setSubTextSemiBold() {
        self.font = UIFont.init(name: "SukhumvitSet-SemiBold", size: commonValue.subTextSize)
    }
    
    func setSubTextMedium() {
        self.font = UIFont.init(name: "SukhumvitSet-text", size: commonValue.subTextSize)
    }
    
    func setSubTextLight() {
        self.font = UIFont.init(name: "SukhumvitSet-Light", size: commonValue.subTextSize)
    }
    
    func setSubTextThin() {
        self.font = UIFont.init(name: "SukhumvitSet-Thin", size: commonValue.subTextSize)
    }
    
    func setDescText() {
        self.font = UIFont.init(name: "SukhumvitSet-Text", size: commonValue.descSize)
    }
    
    func setDescTextBold() {
        self.font = UIFont.init(name: "SukhumvitSet-Bold", size: commonValue.descSize)
    }
    
    func setDescTextSemiBold() {
        self.font = UIFont.init(name: "SukhumvitSet-SemiBold", size: commonValue.descSize)
    }
    
    func setDescTextMedium() {
        self.font = UIFont.init(name: "SukhumvitSet-text", size: commonValue.descSize)
    }
    
    func setDescTextLight() {
        self.font = UIFont.init(name: "SukhumvitSet-Light", size: commonValue.descSize)
    }
    
    func setDescTextThin() {
        self.font = UIFont.init(name: "SukhumvitSet-Thin", size: commonValue.descSize)
    }
    
}
