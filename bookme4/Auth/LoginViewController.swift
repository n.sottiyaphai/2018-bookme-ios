 //
 //  LoginViewController.swift
 //  bookme4
 //
 //  Created by Naphat Sottiyaphai on 22/3/2561 BE.
 //  Copyright © 2561 Naphat Sottiyaphai. All rights reserved.
 //
 
 import UIKit
 import FBSDKCoreKit
 import FBSDKLoginKit
 import SVProgressHUD
 import Alamofire
 import GoogleSignIn
 import FirebaseDatabase
 import FirebaseAuth
 import FirebaseInstanceID
 import FirebaseMessaging
 import SwiftHash
 import SwiftDate
 import MapKit
 
 class LoginViewController: UIViewController  , GIDSignInDelegate , CLLocationManagerDelegate{
    
    @IBOutlet weak var bgview: UIView!
    @IBOutlet weak var facebookButton: UIButton!
    @IBOutlet weak var googleButton: UIButton!
    @IBOutlet weak var loginButton: UIButton!
    @IBOutlet weak var skipButton: UIButton!
    
    @IBOutlet weak var usernameTextfield: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var userGBView: UIView!
    
    var curentLat = Double()
    var curentLng = Double()
    var reLogin = false
    var firebaseID = String()
    
    var locationManager = CLLocationManager()
    var currentLocation: CLLocation!
    
    private lazy var userRef: DatabaseReference = Database.database().reference().child("user")
    
    class func instantiateFromStoryboard() -> LoginViewController {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: String(describing: self)) as! LoginViewController
    }
    
    //MARK: - LIFE CYCLE
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //        getAccessToken()
        bookmeTokenCreate()
        
        userGBView.setRoundWithR(r: commonValue.viewRadius)
        bgview.setRoundWithR(r: commonValue.viewRadius)
        facebookButton.setRoundWithR(r: commonValue.subTadius)
        googleButton.setRoundWithR(r: commonValue.subTadius)
        loginButton.setRoundWithR(r: commonValue.subTadius)
        usernameTextfield.setRoundWithR(r: commonValue.subTadius)
        passwordTextField.setRoundWithR(r: commonValue.subTadius)
        
        usernameTextfield.setLeftPaddingPoints(40)
        passwordTextField.setLeftPaddingPoints(40)
        
        usernameTextfield.delegate = self
        passwordTextField.delegate = self
        
        if #available(iOS 13.0, *) {
            self.isModalInPresentation = isModalInPresentation
        } else {
            // Fallback on earlier versions
        }
        
        let userdefaults = UserDefaults.standard
        if let isNonlogin = userdefaults.object(forKey: "isNonLogin"){
            let boolCheck = isNonlogin as! Bool
            if(boolCheck){
                skipButton.setTitle("Close", for: .normal)
            }
        }
        
        //        UIApplication.shared.statusBar?.backgroundColor = UIColor.clear
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        if Reachability.isConnectedToNetwork(){
            print("Internet Connection Available!")
        }else{
            SVProgressHUD.showError(withStatus: "Connection error. Please check your network connection.")
            DispatchQueue.main.asyncAfter(deadline: .now() + 2){
                SVProgressHUD.dismiss()
            }
        }
        
        self.navigationController?.isNavigationBarHidden = true
    }
    
    
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        self.navigationController?.isNavigationBarHidden = false
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Di3spose of any resources that can be recreated.
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if(segue.identifier == "authSuccess"){
            let vc = segue.destination
            vc.modalPresentationStyle = .overFullScreen
        }
    }
    
    @IBAction func skipLogin(_ sender: Any) {
        
        let userdefaults = UserDefaults.standard
        if let isNonlogin = userdefaults.object(forKey: "isNonLogin"){
            let boolCheck = isNonlogin as! Bool
            if(boolCheck){
                self.dismiss(animated: true, completion: nil)
            }
            else{
                regisNonLoginToBookme ()
            }
        }
        else{
            regisNonLoginToBookme ()
        }
        
    }
    
    func regisNonLoginToBookme () {
        
        
        let ud = UserDefaults.standard
        var accesstoken = String()
        
        if(ud.object(forKey: "accessToken") != nil){
            accesstoken = ud.value(forKey: "accessToken") as! String
        }
        
        let header : HTTPHeaders = [
            "accessToken" : accesstoken
        ]
        
        Alamofire.request(baseURL.url + "profile/createNonLogin", method: .post, encoding: JSONEncoding.default , headers : header )
            .responseJSON(completionHandler: { response in
                
                if let result = response.result.value {
                    if(result is NSDictionary){
                        let data = result as! NSDictionary
                        print(data)
                        
                        if(data.value(forKey: "data") is NSDictionary){
                            let JSON = data.value(forKey: "data") as! NSDictionary
                            let uid = JSON.value(forKey: "_id") as! String
                            
                            if(data.value(forKey: "err") is String){
                                if(data.value(forKey: "err") as! String == "Profile is already Exist."){
                                    print("id \(uid)")
                                    
                                    // Get profile
                                    self.getProfile(id: uid)
                                    return
                                }
                            }
                            
                            let firstName = JSON.value(forKey: "firstName") as! String
                            let lastName = JSON.value(forKey: "lastName") as! String
                            let avatarURL = JSON.value(forKey: "profilePhoto") as! String
                            let fullName = "\(firstName) \(lastName)"
                            
                            print(JSON)
                            let DeviceLang = Locale.preferredLanguages[0]
                            
                            let userDefault = Foundation.UserDefaults.standard
                            
                            userDefault.set(DeviceLang, forKey: "langDevice")
                            userDefault.set(uid, forKey: "user_uid")
                            userDefault.set(fullName, forKey: "user_fullName")
                            userDefault.set(avatarURL, forKey: "user_avatarURL")
                            userDefault.set("1", forKey: "Login")
                            userDefault.set(true, forKey: "isNonLogin")
                            
                            SVProgressHUD.dismiss()
                            self.performSegue(withIdentifier: "authSuccess", sender: self)
                        }
                        else{
                            print(data)
                        }
                    }
                    else{
                        print(result)
                    }
                }
                else{
                    SVProgressHUD.showError(withStatus: "Server Error")
                    
                    DispatchQueue.main.asyncAfter(deadline: .now() + 2){
                        SVProgressHUD.dismiss()
                    }
                }
            })
    }
    
    
    
    // MARK: - Google Signin Method
    @IBAction func googleAction(_ sender: Any) {
        if Reachability.isConnectedToNetwork(){
            GIDSignIn.sharedInstance()?.presentingViewController = self
            GIDSignIn.sharedInstance().delegate = self
            GIDSignIn.sharedInstance().signIn()
        }
        else{
            SVProgressHUD.showError(withStatus: "Internet Connection not Available!")
            DispatchQueue.main.asyncAfter(deadline: .now() + 2){
                SVProgressHUD.dismiss()
            }
        }
    }
    
    func sign(inWillDispatch signIn: GIDSignIn!, error: Error!) {
        print("inWillDispatch")
    }
    
    func sign(_ signIn: GIDSignIn!, dismiss viewController: UIViewController!) {
        print("dismiss")
    }
    
    func sign(_ signIn: GIDSignIn!, present viewController: UIViewController!) {
        self.performSegue(withIdentifier: "authSuccess", sender: self)
    }
    
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error?) {
        
        if error != nil {
            return
        }
        
        guard let authentication = user.authentication else { return }
        
        //        print(authentication.accessToken)
        self.callBluweoConnectGoogle(googleToken: authentication.accessToken)
        
    }
    
    func callBluweoConnectGoogle(googleToken : String) {
        
        SVProgressHUD.show()
        
        let url = "\(baseURL.bluweoConnectAuth)signin/google"
        
        let headers = [
            "client_id" : "1001",
            "secret_key" : "bookme",
            "Content-Type": "application/x-www-form-urlencoded"
        ]
        
        let params = [
            "access_token": googleToken
        ]
        
        Alamofire.request(URL(string: url)!, method: .post , parameters: params, encoding: URLEncoding(destination: .httpBody) , headers: headers).responseJSON(completionHandler: { (respond) in
            
            if let result = respond.result.value{
                let responsed = result as! NSDictionary
                print(responsed)
                
                let response = responsed.value(forKey: "response") as! Bool
                if(response){
                    let data = responsed.value(forKey: "data") as! NSDictionary
                    let access_token = data.value(forKey: "access_token") as! String
                    let refresh_token = data.value(forKey: "refresh_token") as! String
                    
                    let userDefaults = UserDefaults.standard
                    userDefaults.set(access_token, forKey: "bluweoAccessToken")
                    userDefaults.set(refresh_token, forKey: "bluweoRefreshToken")
                    
                    SVProgressHUD.dismiss()
                    self.regisToBookme(user: data)
                    //                    self.performSegue(withIdentifier: "authSuccess", sender: self)
                }
                else{
                    let data = responsed.value(forKey: "data") as! NSDictionary
                    let message = data.value(forKey: "message") as! NSArray
                    let msg = message[0] as! NSDictionary
                    let errMsg = msg.value(forKey: "message") as! String
                    print(errMsg)
                    
                    
                    SVProgressHUD.showError(withStatus: errMsg)
                    DispatchQueue.main.asyncAfter(deadline: .now() + 2){
                        SVProgressHUD.dismiss()
                    }
                }
            }
            else{
                print(respond.error?.localizedDescription ?? "")
                SVProgressHUD.dismiss()
            }
        })
    }
    
    // MARK: - Facebook Signin Method
    @IBAction func facebookAction(_ sender: Any) {
        if Reachability.isConnectedToNetwork(){
            callFacebookLogin()
        }
        else{
            SVProgressHUD.showError(withStatus: "Internet Connection not Available!")
            DispatchQueue.main.asyncAfter(deadline: .now() + 2){
                SVProgressHUD.dismiss()
            }
        }
        
    }
    
    func callFacebookLogin() {
        let fbLoginManager = LoginManager()
        
        fbLoginManager.logOut()
        fbLoginManager.logIn(permissions: ["public_profile", "email"], from: self) { (result, error) in
            
            print("result : \(String(describing: result))\n Error : \(String(describing: error))")
            
            if let error = error {
                print("Failed to login: \(error.localizedDescription)")
                return
            }
            
            guard let accessToken = AccessToken.current else {
                print("Failed to get access token")
                return
            }
            
            print(accessToken.tokenString)
            self.callBluweoConnectFacebook(facebookToken: accessToken.tokenString)
        }
    }
    
    func callBluweoConnectFacebook(facebookToken : String) {
        
        SVProgressHUD.show()
        
        let url = "\(baseURL.bluweoConnectAuth)signin/facebook"
        
        let headers = [
            "client_id" : "1001",
            "secret_key" : "bookme",
            "Content-Type": "application/x-www-form-urlencoded"
        ]
        
        let params = [
            "access_token": facebookToken
        ]
        
        Alamofire.request(URL(string: url)!, method: .post , parameters: params, encoding: URLEncoding(destination: .httpBody) , headers: headers).responseJSON(completionHandler: { (respond) in
            
            if let result = respond.result.value{
                let responsed = result as! NSDictionary
                print(responsed)
                
                let response = responsed.value(forKey: "response") as! Bool
                if(response){
                    let data = responsed.value(forKey: "data") as! NSDictionary
                    let access_token = data.value(forKey: "access_token") as! String
                    let refresh_token = data.value(forKey: "refresh_token") as! String
                    
                    let userDefaults = UserDefaults.standard
                    userDefaults.set(access_token, forKey: "bluweoAccessToken")
                    userDefaults.set(refresh_token, forKey: "bluweoRefreshToken")
                    
                    SVProgressHUD.dismiss()
                    self.regisToBookme(user: data)
                    
                    //                    self.performSegue(withIdentifier: "authSuccess", sender: self)
                }
                else{
                    let data = responsed.value(forKey: "data") as! NSDictionary
                    let message = data.value(forKey: "message") as! NSArray
                    let msg = message[0] as! NSDictionary
                    let errMsg = msg.value(forKey: "message") as! String
                    print(errMsg)
                    
                    
                    SVProgressHUD.showError(withStatus: errMsg)
                    DispatchQueue.main.asyncAfter(deadline: .now() + 2){
                        SVProgressHUD.dismiss()
                    }
                }
            }
            else{
                print(respond.error?.localizedDescription ?? "")
                SVProgressHUD.dismiss()
            }
        })
    }
    
    
    // MARK: - Bluweo Sigin Method
    // Action Button
    @IBAction func signInWithEmail(_ sender: Any) {
        if Reachability.isConnectedToNetwork(){
            self.bluweoLogin(email: usernameTextfield.text!, pwd: passwordTextField.text!)
        }
        else{
            SVProgressHUD.showError(withStatus: "Internet Connection not Available!")
            DispatchQueue.main.asyncAfter(deadline: .now() + 2){
                SVProgressHUD.dismiss()
            }
        }
    }
    
    // BLUWEO EMAIL LOGIN
    func bluweoLogin(email: String, pwd: String) {
        
        SVProgressHUD.show()
        
        let url = "\(baseURL.bluweoConnectAuth)signin"
        
        let headers = [
            "client_id" : "1001",
            "secret_key" : "bookme",
            "Content-Type": "application/x-www-form-urlencoded"
        ]
        let params = [
            "email": email,
            "password" : pwd
        ]
        
        Alamofire.request(URL(string: url)!, method: .post , parameters: params, encoding: URLEncoding(destination: .httpBody) , headers: headers).responseJSON(completionHandler: { (respond) in
            
            if let result = respond.result.value{
                let responsed = result as! NSDictionary
                print(responsed)
                
                let response = responsed.value(forKey: "response") as! Bool
                if(response){
                    let data = responsed.value(forKey: "data") as! NSDictionary
                    let access_token = data.value(forKey: "access_token") as! String
                    let refresh_token = data.value(forKey: "refresh_token") as! String
                    
                    let userDefaults = UserDefaults.standard
                    userDefaults.set(access_token, forKey: "bluweoAccessToken")
                    userDefaults.set(refresh_token, forKey: "bluweoRefreshToken")
                    
                    SVProgressHUD.dismiss()
                    self.regisToBookme(user: data)
                    //                    self.performSegue(withIdentifier: "authSuccess", sender: self)
                }
                else{
                    let data = responsed.value(forKey: "data") as! NSDictionary
                    let message = data.value(forKey: "message") as! NSArray
                    let msg = message[0] as! NSDictionary
                    let errMsg = msg.value(forKey: "message") as! String
                    print(errMsg)
                    
                    
                    SVProgressHUD.showError(withStatus: errMsg)
                    DispatchQueue.main.asyncAfter(deadline: .now() + 2){
                        SVProgressHUD.dismiss()
                    }
                }
            }
            else{
                print(respond.error?.localizedDescription ?? "")
                SVProgressHUD.dismiss()
            }
        })
    }
    
    //MARK: - Bookme Sigin Method
    
    func regisToBookme( user : NSDictionary ) {
        
        let profileID = user["profile_id"] as! String
        let email = user["email"] as! String
        let ProfileUrl = user["profile_photo"] as! String
        let firstName = user["first_name"] as! String
        let lastName = user["last_name"] as! String
        let birthDate = Date().toISO()
        
        let bluweoAccessToken = user["access_token"] as! String
        let bluweoRefreshToken = user["refresh_token"] as! String
        
        print(birthDate)
        print(user)
        
        let ud = UserDefaults.standard
        var accesstoken = String()
        
        if(ud.object(forKey: "accessToken") != nil){
            accesstoken = ud.value(forKey: "accessToken") as! String
        }
        
        let header : HTTPHeaders = [
            "accessToken" : accesstoken
        ]
        
        let parameters: [String: Any] = [
            "firstName": firstName,
            "lastName": lastName,
            "email": email,
            "profile_id" : profileID,
            "profilePhoto" : ProfileUrl,
            "refresh_token" : bluweoRefreshToken,
            "access_token" : bluweoAccessToken,
            "tokenFirebase" : Messaging.messaging().fcmToken,
            "birthDate" : Date().toISO(),
            "location_lat" : 0.0,
            "location_lng" : 0.0
        ]
        
        print(parameters)
        
        Alamofire.request(baseURL.url + "profile", method: .post, parameters: parameters, encoding: JSONEncoding.default , headers : header )
            .responseJSON(completionHandler: { response in
                
                if let result = response.result.value {
                    if(result is NSDictionary){
                        let data = result as! NSDictionary
                        print(data)
                        
                        if(data.value(forKey: "data") is NSDictionary){
                            let JSON = data.value(forKey: "data") as! NSDictionary
                            let uid = JSON.value(forKey: "_id") as! String
                            
                            if(data.value(forKey: "err") is String){
                                if(data.value(forKey: "err") as! String == "Profile is already Exist."){
                                    print("id \(uid)")
                                    
                                    // Get profile
                                    self.getProfile(id: uid)
                                    
                                    //                                    self.updateProfile(parameters: parameters, id: uid)
                                    
                                    return
                                }
                            }
                            
                            let firstName = JSON.value(forKey: "firstName") as! String
                            let lastName = JSON.value(forKey: "lastName") as! String
                            let avatarURL = JSON.value(forKey: "profilePhoto") as! String
                            let fullName = "\(firstName) \(lastName)"
                            
                            print(JSON)
                            let DeviceLang = Locale.preferredLanguages[0]
                            
                            let userDefault = Foundation.UserDefaults.standard
                            
                            userDefault.set(DeviceLang, forKey: "langDevice")
                            userDefault.set(uid, forKey: "user_uid")
                            userDefault.set(fullName, forKey: "user_fullName")
                            userDefault.set(avatarURL, forKey: "user_avatarURL")
                            userDefault.set("1", forKey: "Login")
                            userDefault.set(false, forKey: "isNonLogin")
                            //                            userDefault.set(JSON, forKey: "userData")
                            
                            SVProgressHUD.dismiss()
                            self.performSegue(withIdentifier: "authSuccess", sender: self)
                        }
                        else{
                            print(data)
                        }
                    }
                    else{
                        print(result)
                    }
                }
                else{
                    SVProgressHUD.showError(withStatus: "Server Error")
                    
                    DispatchQueue.main.asyncAfter(deadline: .now() + 2){
                        SVProgressHUD.dismiss()
                    }
                }
            })
    }
    
    func getProfile(id : String){
        let ud = UserDefaults.standard
        var accesstoken = String()
        
        if(ud.object(forKey: "accessToken") != nil){
            accesstoken = ud.value(forKey: "accessToken") as! String
        }
        
        let header : HTTPHeaders = [
            "accessToken" : accesstoken
        ]
        
        Alamofire.request(baseURL.url + "profile/\(id)", method: .get , encoding: JSONEncoding.default , headers : header)
            .responseJSON(completionHandler: { response in
                
                if let result = response.result.value {
                    let data = result as! NSDictionary
                    print(data)
                    
                    let JSON = data.value(forKey: "data") as! NSDictionary
                    let firstName = JSON.value(forKey: "firstName") as! String
                    let lastName = JSON.value(forKey: "lastName") as! String
                    let avatarURL = JSON.value(forKey: "profilePhoto") as! String
                    let fullName = "\(firstName) \(lastName)"
                    
                    print(JSON)
                    let DeviceLang = Locale.preferredLanguages[0]
                    
                    let userDefault = Foundation.UserDefaults.standard
                    
                    userDefault.set(DeviceLang, forKey: "langDevice")
                    userDefault.set(id, forKey: "user_uid")
                    userDefault.set(fullName, forKey: "user_fullName")
                    userDefault.set(avatarURL, forKey: "user_avatarURL")
                    userDefault.set("1", forKey: "Login")
                    //                    userDefault.set(JSON, forKey: "userData")
                    
                    SVProgressHUD.dismiss()
                    self.performSegue(withIdentifier: "authSuccess", sender: self)
                }
                else{
                    SVProgressHUD.showError(withStatus: "Server Error")
                    return
                }
            })
    }
    
    //    func updateProfile(parameters : Dictionary<String, Any> , id : String) {
    //
    //        let ud = UserDefaults.standard
    //        var accesstoken = String()
    //
    //        if(ud.object(forKey: "accessToken") != nil){
    //            accesstoken = ud.value(forKey: "accessToken") as! String
    //        }
    //
    //        let header : HTTPHeaders = [
    //            "accessToken" : accesstoken
    //        ]
    //
    //        Alamofire.request(baseURL.url + "profile/\(id)", method: .put, parameters: parameters, encoding: JSONEncoding.default , headers : header)
    //            .responseJSON(completionHandler: { response in
    //
    //                if let result = response.result.value {
    //                    let data = result as! NSDictionary
    //                    print(data)
    //
    //                    let JSON = data.value(forKey: "data") as! NSDictionary
    //                    let firstName = JSON.value(forKey: "firstName") as! String
    //                    let lastName = JSON.value(forKey: "lastName") as! String
    //                    let avatarURL = JSON.value(forKey: "profilePhoto") as! String
    //                    let fullName = "\(firstName) \(lastName)"
    //
    //                    print(JSON)
    //                    let DeviceLang = Locale.preferredLanguages[0]
    //
    //                    let userDefault = Foundation.UserDefaults.standard
    //
    //                    userDefault.set(DeviceLang, forKey: "langDevice")
    //                    userDefault.set(id, forKey: "user_uid")
    //                    userDefault.set(fullName, forKey: "user_fullName")
    //                    userDefault.set(avatarURL, forKey: "user_avatarURL")
    //                    userDefault.set("1", forKey: "Login")
    ////                    userDefault.set(JSON, forKey: "userData")
    //
    //                    SVProgressHUD.dismiss()
    //                    self.performSegue(withIdentifier: "authSuccess", sender: self)
    //                    return
    //                }
    //                else{
    //                    SVProgressHUD.showError(withStatus: "Server Error")
    //                    return
    //                }
    //            })
    //    }
    //
    //    func bookmeTokenCreate() {
    //
    //        let url = "\(baseURL.url)auth"
    //
    //        Alamofire.request(url, method: .post).responseJSON { (response) in
    //            if let result = response.result.value{
    //                print(result as! NSDictionary)
    //                print("Token Created")
    //
    //                let data = (result as! NSDictionary).value(forKey: "data") as! NSDictionary
    //                let accessToken = data.value(forKey: "accessToken") as! String
    //
    //                let userDefault = UserDefaults.standard
    //                userDefault.set(accessToken, forKey: "accessToken")
    //            }
    //            else{
    //                print("err : Login view / bookmeTokenCreate()")
    //            }
    //
    //        }
    //    }
    
    
    //MARK: - Action button
    
    @IBAction func forgotPassword(_ sender: Any) {
        self.performSegue(withIdentifier: "forgotPassword", sender: self)
        
    }
    
    @IBOutlet weak var signUp: UIButton!
    @IBAction func go2regis(_ sender: Any) {
        
        print("Go to Registation")
        self.performSegue(withIdentifier: "reg", sender: self)
    }
    
    
    @IBAction func openTerm(_ sender: Any) {
        guard let url = URL(string: "https://www.appbookme.com/terms") else {
            return //be safe
        }
        
        if #available(iOS 10.0, *) {
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        } else {
            UIApplication.shared.openURL(url)
        }
    }
 }
 
 //MARK: -
 
 extension LoginViewController : UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {   //delegate method
        
        if(textField == usernameTextfield) {
            self.passwordTextField.becomeFirstResponder()
        }
        if(textField == passwordTextField){
            self.bluweoLogin(email: usernameTextfield.text!, pwd: passwordTextField.text!)
        }
        return true
    }
 }
 
 //MARK: - UIViewController Extention
 
 extension UIViewController {
    func refreshBluweoToken() {
        print("bluweo refresh token working")
        
        let userDefaults = UserDefaults.standard
        let refreshToken = userDefaults.value(forKey: "bluweoRefreshToken") as! String
        
        let url = "\(baseURL.bluweoConnectAuth)refresh_token"
        
        let headers = [
            "client_id" : "1001",
            "secret_key" : "bookme",
            "Content-Type": "application/x-www-form-urlencoded"
        ]
        
        let params = [
            "refresh_token": refreshToken
        ]
        
        Alamofire.request(URL(string: url)!, method: .post , parameters: params, encoding: URLEncoding(destination: .httpBody) , headers: headers).responseJSON(completionHandler: { (respond) in
            
            if let result = respond.result.value{
                let responsed = result as! NSDictionary
                let response = responsed.value(forKey: "response") as! Bool
                
                if(response){
                    let data = responsed.value(forKey: "data") as! NSDictionary
                    let access_token = data.value(forKey: "access_token") as! String
                    
                    let userDefaults = UserDefaults.standard
                    userDefaults.set(access_token, forKey: "bluweoAccessToken")
                    
                    self.updateBluweoToken2Bookme(user: data)
                }
                else{
                    let data = responsed.value(forKey: "data") as! NSDictionary
                    let message = data.value(forKey: "message") as! NSArray
                    let msg = message[0] as! NSDictionary
                    let errMsg = msg.value(forKey: "message") as! String
                    
                    SVProgressHUD.showError(withStatus: errMsg)
                    DispatchQueue.main.asyncAfter(deadline: .now() + 2){
                        SVProgressHUD.dismiss()
                        
                        let vc = LoginViewController.instantiateFromStoryboard()
                        self.navigationController?.present(vc, animated: true, completion: nil)
                    }
                }
            }
            else{
                print(respond.error?.localizedDescription ?? "")
                SVProgressHUD.dismiss()
            }
        })
    }
    
    func updateBluweoToken2Bookme( user : NSDictionary ) {
        print("bookme update token")
        
        let profileID = user["profile_id"] as! String
        let email = user["email"] as! String
        let ProfileUrl = user["profile_photo"] as! String
        let firstName = user["first_name"] as! String
        let lastName = user["last_name"] as! String
        
        let bluweoAccessToken = user["access_token"] as! String
        let bluweoRefreshToken = user["refresh_token"] as! String
        
        let ud = UserDefaults.standard
        var accesstoken = String()
        
        if(ud.object(forKey: "accessToken") != nil){
            accesstoken = ud.value(forKey: "accessToken") as! String
        }
        
        let header : HTTPHeaders = [
            "accessToken" : accesstoken
        ]
        
        let parameters: [String: Any] = [
            "firstName": firstName,
            "lastName": lastName,
            "email": email,
            "profile_id" : profileID,
            "profilePhoto" : ProfileUrl,
            "refresh_token" : bluweoRefreshToken,
            "access_token" : bluweoAccessToken,
            "location_lat" : 0.0,
            "location_lng" : 0.0
        ]
        
        Alamofire.request(baseURL.url + "profile", method: .post, parameters: parameters, encoding: JSONEncoding.default , headers : header )
            .responseJSON(completionHandler: { response in
                
                if let result = response.result.value {
                    if(result is NSDictionary){
                        let data = result as! NSDictionary
                        
                        if(data.value(forKey: "data") is NSDictionary){
                            let JSON = data.value(forKey: "data") as! NSDictionary
                            let uid = JSON.value(forKey: "_id") as! String
                            
                            if(data.value(forKey: "err") is String){
                                if(data.value(forKey: "err") as! String == "Profile is already Exist."){
                                    return
                                }
                            }
                        }
                    }
                }
                else{
                    SVProgressHUD.showError(withStatus: "Server Error")
                    
                    DispatchQueue.main.asyncAfter(deadline: .now() + 2){
                        SVProgressHUD.dismiss()
                    }
                }
            })
    }
    
 }
 
