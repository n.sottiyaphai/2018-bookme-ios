//
//  forgotPasswordViewController.swift
//  bookme
//
//  Created by Naphat Sottiyaphai on 11/9/2561 BE.
//  Copyright © 2561 Naphat Sottiyaphai. All rights reserved.
//

import UIKit
import ACFloatingTextfield_Swift
import Alamofire
import SVProgressHUD

class forgotPasswordViewController: UIViewController {
    
    @IBOutlet weak var detailDest: UILabel!
    @IBOutlet weak var titleDest: UILabel!
    
    @IBOutlet weak var emailTF: UITextField!
    @IBOutlet weak var bgView: UIView!
    @IBOutlet weak var sendLinkButton: UIButton!
    
    @IBAction func sendLink(_ sender: Any) {
        SVProgressHUD.show()
        self.callForgotPassword(email: self.emailTF.text!)
    }
    
    @IBAction func goBack(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBOutlet weak var back: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        titleDest.setHeaderTextBold()
        detailDest.setText()
        
        bgView.setRoundWithShadow(r: commonValue.viewRadius)
        sendLinkButton.setRoundedwithR(r: commonValue.viewRadius)
        
        emailTF.delegate = self
        emailTF.setRoundWithCommonRwithBGC_f8()
        emailTF.setLeftPaddingPoints(15)
        
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    func callForgotPassword (email : String) {
        
        let url = "\(baseURL.bluweoConnectPassword)forgot"
        
        let headers = [
            "client_id" : "1001",
            "secret_key" : "bookme",
            "Content-Type": "application/x-www-form-urlencoded"
        ]
        let params = [
            "email" : email
        ]
        
        Alamofire.request(URL(string: url)!, method: .post , parameters: params, encoding: URLEncoding(destination: .httpBody) , headers: headers).responseJSON(completionHandler: { (respond) in
            
            if let result = respond.result.value{
                let responsed = result as! NSDictionary
                print(responsed)
                
                SVProgressHUD.dismiss()
                let response = responsed.value(forKey: "response") as! Bool
                if(response){

                    print("Send Success")
                    
                    let Alert = UIAlertController(title: "Success", message: "Please check reset link in your email within 5 mins.", preferredStyle: .alert)
                    
                    let ok = UIAlertAction(title: "Ok", style: .default) { (UIAlertAction) in
                        self.navigationController?.popViewController(animated: true)
                    }
                    
                    Alert.addAction(ok)
                    
                    self.present(Alert, animated: true, completion: nil)
                    
                }
                else{
                    let data = responsed.value(forKey: "data") as! NSDictionary
                    let message = data.value(forKey: "message") as! NSArray
                    let msg = message[0] as! NSDictionary
                    let errMsg = msg.value(forKey: "message") as! String
                    print(errMsg)
                    
                    SVProgressHUD.showError(withStatus: errMsg)
                    DispatchQueue.main.asyncAfter(deadline: .now() + 2){
                        SVProgressHUD.dismiss()
                    }
                }
            }
            else{
                print(respond.error?.localizedDescription)
                SVProgressHUD.dismiss()
            }
        }) 
    }

}

extension forgotPasswordViewController : UITextFieldDelegate {
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        
        print("check confirm password")
        if(textField == emailTF){
            let text = textField.text
            if (text?.filter{$0 == "@"}.isEmpty)!{
//                self.emailTF.showErrorWithText(errorText: "Incorrect email format.")
                SVProgressHUD.showInfo(withStatus: "Incorrect email format.")
                DispatchQueue.main.asyncAfter(deadline: .now() + 2 ){
                    SVProgressHUD.dismiss()
                }
            }
            else{

            }
        }
    }
}
