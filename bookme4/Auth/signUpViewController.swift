//
//  signUpViewController.swift
//  bookme
//
//  Created by Naphat Sottiyaphai on 24/8/2561 BE.
//  Copyright © 2561 Naphat Sottiyaphai. All rights reserved.
//

import UIKit
import Alamofire
import ACFloatingTextfield_Swift
import BEMCheckBox
import SVProgressHUD
import ActionSheetPicker_3_0
import SwiftyJSON
import SwiftDate
import FirebaseAuth
import FirebaseInstanceID
import FirebaseMessaging

class signUpViewController: UIViewController {
    
    @IBOutlet weak var bgView: UIView!
    @IBOutlet weak var signUp: UIButton!
    
    @IBOutlet weak var emailTF: UITextField!
    @IBOutlet weak var passwordTF: UITextField!
    @IBOutlet weak var confirmPasswordTF: UITextField!
    @IBOutlet weak var titleTF: UITextField!
    @IBOutlet weak var firstNameTF: UITextField!
    @IBOutlet weak var lastNameTF: UITextField!
    @IBOutlet weak var mobileNumberTF: UITextField!
    @IBOutlet weak var birthDateTF: UITextField!
    @IBOutlet weak var termsCheckbox: BEMCheckBox!
    
    var reRegis = false
    var recount = 0
    private var dataChecker = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        bgView.setRoundWithShadow(r: commonValue.viewRadius)
        signUp.setRoundedwithR(r: commonValue.viewRadius)
        
        emailTF.delegate = self
        passwordTF.delegate = self
        confirmPasswordTF.delegate = self
        titleTF.delegate = self
        mobileNumberTF.delegate = self
        birthDateTF.delegate = self
        
        getAccessToken()
        
        emailTF.setRoundDefaultStyle()
        passwordTF.setRoundDefaultStyle()
        confirmPasswordTF.setRoundDefaultStyle()
        titleTF.setRoundDefaultStyle()
        mobileNumberTF.setRoundDefaultStyle()
        birthDateTF.setRoundDefaultStyle()
        firstNameTF.setRoundDefaultStyle()
        lastNameTF.setRoundDefaultStyle()
        
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func getAccessToken() {
        let headers = [
            "Content-Type": "application/x-www-form-urlencoded"
        ]
        let params = [
            "email": "naphat@dnamaker.com",
            "keys" : "1234",
            "password" : "Supervis!@#$%."
        ]
        
        //        print(headers)
        //        print(params)
        
        Alamofire.request(URL(string: "\(baseURL.connect)access_token")!, method: .post , parameters: params, encoding: URLEncoding(destination: .httpBody) , headers: headers).responseJSON(completionHandler: { (respond) in
            
            let result = respond.result.value
            var access_Token = String()
            
            if(result is NSDictionary){
                let JSON = result as! NSDictionary
                if(JSON.object(forKey: "token") != nil){
                    access_Token = JSON.value(forKey: "token") as! String
                    let userDefault = UserDefaults.standard
                    userDefault.set(access_Token, forKey: "access_token")
                    print("Recollect token : Success")
                    
                    if(self.reRegis){
                        self.reRegis = false
                        self.regisWithEmail()
                        self.recount = 0
                    }
                }
            }
            else{
                print("Recollect token : Fail")
                print(respond.error ?? "")
                
                self.recount += 1
                
                
                if(self.recount >= 10){
                    SVProgressHUD.showError(withStatus: "Something wrong!\nPlease try agin later")
                    DispatchQueue.main.asyncAfter(deadline: .now() + 2){
                        SVProgressHUD.dismiss()
                    }
                    return
                }
                else{
                    self.getAccessToken()
                }
            }
        })
    }
    
    func regisWithEmail() {
        
        SVProgressHUD.show(withStatus: "Registation in process.")
        
        // Prepare Data
        let email = self.emailTF.text!
        let pwd = self.confirmPasswordTF.text!
        let title = self.titleTF.text!
        let fname = self.firstNameTF.text!
        let lname = self.lastNameTF.text!
        let number = self.mobileNumberTF.text!
        
        var bd = String()
        if(self.birthDateTF.text! == ""){
            bd = Date().toISO()
        }else{
            bd = self.birthDateTF.text!
        }
        
        
        // Sending Regis Bluweo
        let url = "\(baseURL.bluweoProfile)"
        
        let headers = [
            "client_id" : "1001",
            "secret_key" : "bookme",
            "Content-Type": "application/x-www-form-urlencoded"
        ]
        
        let params = [
            "email": email,
            "password" : pwd,
            "title" : title,
            "first_name" : fname,
            "last_name" : lname,
            "birth_date" : bd,
            "mobile" : number
        ]
        
        Alamofire.request(URL(string: url)!, method: .post , parameters: params, encoding: URLEncoding(destination: .httpBody) , headers: headers).responseJSON(completionHandler: { (respond) in
            
            if let result = respond.result.value{
                let responsed = result as! NSDictionary
                print(responsed)
                
                let response = responsed.value(forKey: "response") as! Bool
                if(response){
                    print("Response True")
                    
                    let data = responsed.value(forKey: "data") as! NSDictionary
                    let access_token = data.value(forKey: "access_token") as! String
                    let refresh_token = data.value(forKey: "refresh_token") as! String
                    
                    let userDefaults = UserDefaults.standard
                    userDefaults.set(access_token, forKey: "bluweoAccessToken")
                    userDefaults.set(refresh_token, forKey: "bluweoRefreshToken")
                    
                    SVProgressHUD.dismiss()
                    self.regisToBookme(user: data)
                    
                }
                else{
                    let data = responsed.value(forKey: "data") as! NSDictionary
                    let message = data.value(forKey: "message") as! NSArray
                    let msg = message[0] as! NSDictionary
                    let errMsg = msg.value(forKey: "message") as! String
                    print(errMsg)
                    
                    SVProgressHUD.showError(withStatus: errMsg)
                    DispatchQueue.main.asyncAfter(deadline: .now() + 2){
                        SVProgressHUD.dismiss()
                    }
                }
            }
            else{
                print(respond.error?.localizedDescription ?? "")
                SVProgressHUD.dismiss()
            }
        })
        
        
        //        var data = Dictionary<String, Any>()
        //
        //        data["email"] = email
        //        data["passwd"] = pwd
        //        data["title"] = title
        //        data["firstname"] = fname
        //        data["lastname"] = lname
        //        data["status"] = "single"
        //        data["birth_date"] = bd
        //        data["avatar"] = "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQmbS3s9tJoqSsKvj2HmqSMyxYzOAyuMuNGWoO9D-tUqCSlK3bj"
        //        data["phone_number"] = number
        ////        data["profile_name"] = "\(fname) \(lname)"
        ////        data["profile_slug"] = "\(fname)\(lname)"
        //
        //        let json = JSON(data)
        //        let jsonString = json.rawString(.utf8)!
        //
        //        let headers = [
        //            "Content-Type": "application/x-www-form-urlencoded"
        //        ]
        //        let params : [String : Any] = [
        //            "access_token" : acc_token,
        //            "data" : jsonString
        //        ]
        //
        //        print(headers)
        //        print(params)
        //
        //        Alamofire.request(URL(string: "\(baseURL.connect)firebase/register")!, method: .post , parameters: params, encoding: URLEncoding(destination: .httpBody) , headers: headers).validate().responseJSON(completionHandler: { (respond) in
        //
        //
        //            let result = respond.result.value
        //            let request = respond.request
        //
        //            print(request)
        //            print(result)
        //
        //            var access_Token = String()
        //
        //            if(result is NSDictionary){
        //                let JSON = result as! NSDictionary
        //
        //                if(JSON.object(forKey: "error") != nil){
        //                    print("ERROR !!")
        //                    print(JSON)
        //
        //                    self.reRegis = true
        //                    self.getAccessToken()
        //                    //                    SVProgressHUD.showError(withStatus: "Something wrong!\nPlease try agin later")
        //                    //                    DispatchQueue.main.asyncAfter(deadline: .now() + 2){
        //                    //                        SVProgressHUD.dismiss()
        //                    //                    }
        //
        //                    return
        //                }
        //                if(JSON.object(forKey: "token") != nil){
        //                    access_Token = JSON.value(forKey: "token") as! String
        //                    let userDefault = UserDefaults.standard
        //                    userDefault.set(access_Token, forKey: "access_token")
        //                    print("Recollect token : Success")
        //
        //                    self.reRegis = true
        //                    self.getAccessToken()
        //                }
        //                else{
        //                    print("Auth with connect Success")
        //
        //                    let JSON = result as! NSDictionary
        //                    let firebase = JSON.value(forKey: "firebase") as! NSDictionary
        //                    let account = JSON.value(forKey: "account") as! NSDictionary
        //                    let accProfile = account.value(forKey: "profile") as! NSDictionary
        //
        //                    let avatar = accProfile.value(forKey: "avatar") as! NSDictionary
        //                    let avatarURL = avatar.value(forKey: "url") as! String
        //
        //                    let email = accProfile.value(forKey: "email") as! String
        //                    let phoneNumber = accProfile.value(forKey: "phone_number") as! String
        //                    let firstName = accProfile.value(forKey: "firstname") as! String
        //                    let lastName = accProfile.value(forKey: "lastname") as! String
        //                    let name = "\(firstName) \(lastName)"
        //
        //                    let firebaseUser = firebase.value(forKey: "user") as! NSDictionary
        //                    let firebaseID = firebaseUser.value(forKey: "id") as! String
        //
        //                    // Prepare Date to register
        //                    var user = Dictionary<String, Any>()
        //
        //                    user["email"] = email
        //                    user["name"] = name
        //                    user["picture_url"] = avatarURL
        //                    user["first_name"] = firstName
        //                    user["last_name"] = lastName
        //                    user["phone_number"] = phoneNumber
        //                    user["fid"] = firebaseID
        //
        //                    SVProgressHUD.showSuccess(withStatus: "Registation successful")
        //
        //                    self.dismiss(animated: true, completion: nil)
        //                    self.navigationController?.popViewController(animated: true)
        //                }
        //
        //            }
        //            else{
        //                print("Recollect token : Fail")
        //                print(respond.error ?? "")
        //
        //                self.reRegis = true
        //                self.getAccessToken()
        //
        //                //                SVProgressHUD.showError(withStatus: "Something wrong!\nPlease try agin later")
        //                //                DispatchQueue.main.asyncAfter(deadline: .now() + 2){
        //                //                    SVProgressHUD.dismiss()
        //                //                }
        //            }
        //        })
        
        //
        
    }
    
    func validateFill() -> Bool {
        var results = false
        
        if ((emailTF.text?.isEmpty)! || (passwordTF.text?.isEmpty)! || (confirmPasswordTF.text?.isEmpty)! || (titleTF.text?.isEmpty)! || (titleTF.text?.isEmpty)! || (firstNameTF.text?.isEmpty)! || (lastNameTF.text?.isEmpty)! || (mobileNumberTF.text?.isEmpty)! ){
            
            SVProgressHUD.showInfo(withStatus: "Please enter all of fills.")
            DispatchQueue.main.asyncAfter(deadline: .now() + 3) {
                SVProgressHUD.dismiss()
            }
        }
        else if (!termsCheckbox.on){
            SVProgressHUD.showInfo(withStatus: "Please agree our terms and conditions.")
            DispatchQueue.main.asyncAfter(deadline: .now() + 3) {
                SVProgressHUD.dismiss()
            }
        }
        else{
            results = true
        }
        
        return results
    }
    
    @IBAction func backAction(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBOutlet weak var signUpAction: UIButton!
    @IBAction func signUp(_ sender: Any) {
        if(validateFill() && dataChecker){
            print("fill is ok")
            
            regisWithEmail()
            
        }
    }
    
    func regisToBookme( user : NSDictionary ) {
        
        let profileID = user["profile_id"] as! String
        let email = user["email"] as! String
        let ProfileUrl = user["profile_photo"] as! String
        let firstName = user["first_name"] as! String
        let lastName = user["last_name"] as! String
        let birthDate = Date().toISO()
        
        let bluweoAccessToken = user["access_token"] as! String
        let bluweoRefreshToken = user["refresh_token"] as! String
        
        print(birthDate)
        print(user)
        
        let ud = UserDefaults.standard
        var accesstoken = String()
        
        if(ud.object(forKey: "accessToken") != nil){
            accesstoken = ud.value(forKey: "accessToken") as! String
        }
        
        let header : HTTPHeaders = [
            "accessToken" : accesstoken
        ]
        
        let parameters: [String: Any] = [
            "firstName": firstName,
            "lastName": lastName,
            "email": email,
            "profile_id" : profileID,
            "profilePhoto" : ProfileUrl,
            "refresh_token" : bluweoRefreshToken,
            "access_token" : bluweoAccessToken,
            "tokenFirebase" : Messaging.messaging().fcmToken!,
            "birthDate" : Date().toISO(),
            "location_lat" : 0.0,
            "location_lng" : 0.0
        ]
        
        print(parameters)
        
        Alamofire.request(baseURL.url + "profile", method: .post, parameters: parameters, encoding: JSONEncoding.default , headers : header )
            .responseJSON(completionHandler: { response in
                
                if let result = response.result.value {
                    if(result is NSDictionary){
                        let data = result as! NSDictionary
                        print(data)
                        
                        if(data.value(forKey: "data") is NSDictionary){
                            let JSON = data.value(forKey: "data") as! NSDictionary
                            let uid = JSON.value(forKey: "_id") as! String
                            
                            if(data.value(forKey: "err") is String){
                                if(data.value(forKey: "err") as! String == "Profile is already Exist."){
                                    print("id \(uid)")
                                    
                                    // Get profile
                                    self.getProfile(id: uid)
                                    
                                    //                                    self.updateProfile(parameters: parameters, id: uid)
                                    
                                    return
                                }
                            }
                            
                            let firstName = JSON.value(forKey: "firstName") as! String
                            let lastName = JSON.value(forKey: "lastName") as! String
                            let avatarURL = JSON.value(forKey: "profilePhoto") as! String
                            let fullName = "\(firstName) \(lastName)"
                            
                            print(JSON)
                            let DeviceLang = Locale.preferredLanguages[0]
                            
                            let userDefault = Foundation.UserDefaults.standard
                            
                            userDefault.set(DeviceLang, forKey: "langDevice")
                            userDefault.set(uid, forKey: "user_uid")
                            userDefault.set(fullName, forKey: "user_fullName")
                            userDefault.set(avatarURL, forKey: "user_avatarURL")
                            userDefault.set("1", forKey: "Login")
                            //                            userDefault.set(JSON, forKey: "userData")
                            
                            SVProgressHUD.dismiss()
                            self.performSegue(withIdentifier: "authSuccess", sender: self)
                        }
                        else{
                            print(data)
                        }
                    }
                    else{
                        print(result)
                    }
                }
                else{
                    SVProgressHUD.showError(withStatus: "Server Error")
                    
                    DispatchQueue.main.asyncAfter(deadline: .now() + 2){
                        SVProgressHUD.dismiss()
                    }
                }
            })
    }
    
    func getProfile(id : String){
        let ud = UserDefaults.standard
        var accesstoken = String()
        
        if(ud.object(forKey: "accessToken") != nil){
            accesstoken = ud.value(forKey: "accessToken") as! String
        }
        
        let header : HTTPHeaders = [
            "accessToken" : accesstoken
        ]
        
        Alamofire.request(baseURL.url + "profile/\(id)", method: .get , encoding: JSONEncoding.default , headers : header)
            .responseJSON(completionHandler: { response in
                
                if let result = response.result.value {
                    let data = result as! NSDictionary
                    print(data)
                    
                    let JSON = data.value(forKey: "data") as! NSDictionary
                    let firstName = JSON.value(forKey: "firstName") as! String
                    let lastName = JSON.value(forKey: "lastName") as! String
                    let avatarURL = JSON.value(forKey: "profilePhoto") as! String
                    let fullName = "\(firstName) \(lastName)"
                    
                    print(JSON)
                    let DeviceLang = Locale.preferredLanguages[0]
                    
                    let userDefault = Foundation.UserDefaults.standard
                    
                    userDefault.set(DeviceLang, forKey: "langDevice")
                    userDefault.set(id, forKey: "user_uid")
                    userDefault.set(fullName, forKey: "user_fullName")
                    userDefault.set(avatarURL, forKey: "user_avatarURL")
                    userDefault.set("1", forKey: "Login")
                    //                    userDefault.set(JSON, forKey: "userData")
                    
                    SVProgressHUD.dismiss()
                    self.performSegue(withIdentifier: "authSuccess", sender: self)
                }
                else{
                    SVProgressHUD.showError(withStatus: "Server Error")
                    return
                }
            })
    }
    
    func pushThankyouView(Confirm : Bool ,Booking : Bool ,  Payment : Bool , state : Int) {
        
        let vc = thankYouBookingViewController.instantiateFromStoryboard()
        
        vc.makeBooking = Booking
        vc.comfirmation = Confirm
        vc.makePayment = Payment
        vc.stateIndex = state
        
        self.navigationController?.pushViewController(vc, animated: true)
    }
}

extension signUpViewController : UITextFieldDelegate {
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if(textField == birthDateTF){
            
            textField.resignFirstResponder()
            self.view.endEditing(true)
            
            let defaultDate =  Date().dateAt(.endOfDay) - 25.years
            
            let datePicker = ActionSheetDatePicker(title: "select you birthday", datePickerMode: UIDatePicker.Mode.date, selectedDate: defaultDate, doneBlock: {
                picker, value, index in
                
                let formatter = DateFormatter()
                formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
                
                let myString = formatter.string(from: value as! Date)
                let yourDate = formatter.date(from: myString)
                formatter.dateFormat = "dd MMM yyyy"
                let myStringafd = formatter.string(from: yourDate!)
                
                print("select date : \(myStringafd)")
                
                textField.text = "\(myStringafd)"
                textField.resignFirstResponder()
                
                return
            }, cancel: { ActionStringCancelBlock in return }, origin: (textField as AnyObject).superview!?.superview)
            
            datePicker?.maximumDate = Date()
            datePicker?.show()
        }
        if(textField == titleTF){
            
            textField.resignFirstResponder()
            self.view.endEditing(true)
            
            let picker = ActionSheetStringPicker(title: "Select your title", rows: ["Mr." , "Mrs." , "Miss"], initialSelection: 0, doneBlock: { (picker, index, value) in
                
                textField.text = (value as! String)
                
                return
                
            }, cancel: { (picker) in
                
                return
            }, origin: (textField as AnyObject).superview!?.superview )
            
            picker?.show()
        }
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if(textField == titleTF){
            self.view.endEditing(true)
        }
        if(textField == birthDateTF){
            self.view.endEditing(true)
        }
        
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {   
        textField.resignFirstResponder()
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        
        print("check confirm password")
        
        if(textField == emailTF){
            let text = textField.text
            if (text?.filter{$0 == "@"}.isEmpty)!{
                //                self.emailTF.showErrorWithText(errorText: "Incorrect email format.")
                SVProgressHUD.showInfo(withStatus: "Incorrect email format.")
                DispatchQueue.main.asyncAfter(deadline: .now() + 2 ){
                    SVProgressHUD.dismiss()
                }
                dataChecker = false
            }
            else{
                dataChecker = true
            }
        }
        
        if(textField == confirmPasswordTF){
            if(self.passwordTF.text == textField.text){
                print("Correct")
                dataChecker = true
            }
            else {
                print("incorrect")
                //                self.confirmPasswordTF.showErrorWithText(errorText: "Password not match")
                SVProgressHUD.showInfo(withStatus: "Password not match")
                DispatchQueue.main.asyncAfter(deadline: .now() + 2 ){
                    SVProgressHUD.dismiss()
                }
                dataChecker = false
            }
        }
        if (textField == mobileNumberTF){
            if(textField.text?.count != 10){
                //                self.mobileNumberTF.showErrorWithText(errorText: "Incorrect mobile phone number.")
                SVProgressHUD.showInfo(withStatus: "Incorrect mobile phone number.")
                DispatchQueue.main.asyncAfter(deadline: .now() + 2 ){
                    SVProgressHUD.dismiss()
                }
                dataChecker = false
            }
            else{
                dataChecker = true
            }
        }
    }
    
    
}
