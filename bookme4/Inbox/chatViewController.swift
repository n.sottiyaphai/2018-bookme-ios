//
//  chatViewController.swift
//  BookMe_2
//
//  Created by Naphat Sottiyaphai on 19/10/2560 BE.
//  Copyright © 2560 Naphat Sottiyaphai. All rights reserved.
//

import UIKit
import FirebaseDatabase
import FirebaseStorage
import FirebaseInstanceID
import JSQMessagesViewController
import SVProgressHUD
import Alamofire
import AVKit
import AVFoundation
import OpenTok

class chatViewController: JSQMessagesViewController {
    
    class func instantiateFromStoryboard() -> chatViewController {
        let storyboard = UIStoryboard(name: "Inbox", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: String(describing: self)) as! chatViewController
    }
    
    // MARK: Properties
    private let imageURLNotSetKey = "NOTSET"
    
    private lazy var channelRef: DatabaseReference = Database.database().reference().child("chat")
    private lazy var roomRef: DatabaseReference = Database.database().reference().child("room")
    private lazy var userRef: DatabaseReference = Database.database().reference().child("user")
    private lazy var messageRef: DatabaseReference = self.channelRef.child("Message")
    fileprivate lazy var storageRef: StorageReference = Storage.storage().reference(forURL: "gs://bookme-1515492592081.appspot.com/")
    
    private var newMessageRefHandle: DatabaseHandle?
    private var updatedMessageRefHandle: DatabaseHandle?
    private var messages: [JSQMessage] = []
    private var photoMessageMap = [String: JSQPhotoMediaItem]()
    private var videoMessageMap = [String: JSQVideoMediaItem]()
    private var localTyping = false
    
    var channel : String = "Petch"
    var roomName : String = "Naphat Sottiyaphai"
    var chatSlug : String = "Petch"
    var avatarURL : String = ""
    var photoURL : String = ""
    var Unread : Int = 0
    var img = UIImage()
    
    var firstName = String()
    var lastName = String()
    var imageURL = String()
    var email = String()
    
    var from = "normal"
    var sendingState = false
    
    

    
    var isTyping: Bool {
        get {
            return localTyping
        }
        set {
            localTyping = newValue
        }
    }
    
    lazy var outgoingBubbleImageView: JSQMessagesBubbleImage = self.setupOutgoingBubble()
    lazy var incomingBubbleImageView: JSQMessagesBubbleImage = self.setupIncomingBubble()
    
    
    
    // MARK: View Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        
        if(from == "noti"){
            let back = UIButton(type: .custom)
            back.setImage(UIImage(named: "icon-arrow-left"), for: .normal)
            back.setTitleColor(UIColor.black, for: .normal)
            back.imageEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
            back.frame = CGRect(x: 0, y: 0, width: 20  , height: 20)
            back.addTarget(self, action: #selector(self.back), for: .touchUpInside)
            let Leftitem = UIBarButtonItem(customView: back)
            self.navigationItem.setLeftBarButton(Leftitem, animated: true)
        }
        
        print("chanel = \(channelRef)")
        print("Message = \(messageRef)")

        let userDefault = UserDefaults.standard
        let uid = userDefault.value(forKey: "user_uid") as! String
        
        self.callProfile(uid: uid)
        self.senderId = uid
        self.senderDisplayName = roomName
        self.title = "\(self.roomName)"
        
        observeMessages()
        collectionView!.collectionViewLayout.outgoingAvatarViewSize = CGSize.zero
        
        self.setBackgroundColor()
        
//        let btn2 = UIButton(type: .custom)
//        btn2.setImage(UIImage(named: "icon_videocall"), for: .normal)
//        btn2.frame = CGRect(x: 0, y: 0, width: 40, height: 18)
//        btn2.addTarget(self, action: #selector(self.go2search), for: .touchUpInside)
//        let item2 = UIBarButtonItem(customView: btn2)
//        self.navigationItem.setRightBarButtonItems([item2], animated: true)
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
//        self.automaticallyAdjustsScrollViewInsets = true
        
    }
    
    @objc func go2search() {
//        doConnect()
        let vc = videoCallViewController.instantiateFromStoryboard()
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    
    
    
    @objc func done() { // remove @objc for Swift 3
        
    }
    
    @objc func back() {
        let vc = tabBarViewController.instantiateFromStoryboard()
        self.present(vc, animated: true, completion: nil)
    }
    
    deinit {
        if let refHandle = newMessageRefHandle {
            messageRef.removeObserver(withHandle: refHandle)
        }
        if let refHandle = updatedMessageRefHandle {
            messageRef.removeObserver(withHandle: refHandle)
        }
    }
    
    func callProfile(uid : String){
    
        let ud = UserDefaults.standard
        var accesstoken = String()
        
        if(ud.object(forKey: "accessToken") != nil){
            accesstoken = ud.value(forKey: "accessToken") as! String
        }
        
        let header : HTTPHeaders = [
            "accessToken" : accesstoken
        ]
        
        Alamofire.request(baseURL.url + "profile/\(uid)", method: .get, encoding: JSONEncoding.default , headers : header)
            .responseJSON(completionHandler: { response in
                
                if let result = response.result.value {
                    
                    let data =  result as! NSDictionary
                    var birthDate = String()
                    
                    if(data.object(forKey: "data") != nil){
                        if(data.value(forKey: "data") is NSDictionary){
                            let JSON = data.value(forKey: "data") as! NSDictionary
                            
                            self.firstName = JSON.value(forKey: "firstName") as! String
                            self.lastName = JSON.value(forKey: "lastName") as! String
                            self.imageURL = JSON.value(forKey: "profilePhoto") as! String
                            self.email = JSON.value(forKey: "email") as! String
                        }
                    }
                    else{
                        self.firstName = data.value(forKey: "firstName") as! String
                        self.lastName = data.value(forKey: "lastName") as! String
                        self.imageURL = data.value(forKey: "profilePhoto") as! String
                        self.email = data.value(forKey: "email") as! String
                    }
                    
                }
                else{
                    // Request Error
                    let alert = UIAlertController(title: "Loading Data Error", message: "Please try again later.", preferredStyle: .alert)
                    
                    let ok = UIAlertAction(title: "OK", style: .default, handler: { (action) in
                         print("Capture action OK")
                    })
                    
                    alert.addAction(ok)
                    
                    self.present(alert, animated: true, completion: nil)
                }
            })
        
    }
    
    // MARK: Collection view data source (and related) methods
    
    override func collectionView(_ collectionView: JSQMessagesCollectionView!, messageDataForItemAt indexPath: IndexPath!) -> JSQMessageData! {
        return messages[indexPath.item]
    }
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return messages.count
    }
    
    override func collectionView(_ collectionView: JSQMessagesCollectionView!, messageBubbleImageDataForItemAt indexPath: IndexPath!) -> JSQMessageBubbleImageDataSource! {
        let message = messages[indexPath.item] // 1
        if message.senderId == senderId { // 2
            return outgoingBubbleImageView
        } else { // 3
            return incomingBubbleImageView
        }
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = super.collectionView(collectionView, cellForItemAt: indexPath) as! JSQMessagesCollectionViewCell
        
        let message = messages[indexPath.item]
        
        if message.senderId == senderId { // 1
            cell.textView?.textColor = UIColor.white // 2
        } else {
            cell.textView?.textColor = UIColor.black // 3
        }
        
        cell.avatarImageView.sd_setImage(with: URL(string: self.avatarURL), placeholderImage: nil)
        cell.avatarImageView.setRounded()
        cell.avatarImageView.clipsToBounds = true
        
        return cell
    }
    
    override func collectionView(_ collectionView: JSQMessagesCollectionView!, avatarImageDataForItemAt indexPath: IndexPath!) -> JSQMessageAvatarImageDataSource! {
        
        return nil
    }
    
    override func collectionView(_ collectionView: JSQMessagesCollectionView!, layout collectionViewLayout: JSQMessagesCollectionViewFlowLayout!, heightForMessageBubbleTopLabelAt indexPath: IndexPath!) -> CGFloat {
        return 15
    }
    
    override func collectionView(_ collectionView: JSQMessagesCollectionView!, attributedTextForCellBottomLabelAt indexPath: IndexPath!) -> NSAttributedString! {
        
        var attributedString = NSMutableAttributedString()
        let attrString = NSAttributedString.init(string: "seen", attributes: [NSAttributedString.Key.underlineStyle : NSUnderlineStyle.single.rawValue])
        
        attributedString.append(attrString)
        
        return attributedString
        
    }
    
    override func collectionView(_ collectionView: JSQMessagesCollectionView!, didTapMessageBubbleAt indexPath: IndexPath!) {
        print("Did tap message")
//        print(messages[indexPath.item])
        
        
        if (messages[indexPath.item].value(forKey: "media") is JSQVideoMediaItem) {
            print("Video tap")
            
            let media = messages[indexPath.item].value(forKey: "media") as! JSQVideoMediaItem
            
            print(media.fileURL)
            
            let videoURL = media.fileURL.absoluteURL
            print(videoURL)
            
            let player = AVPlayer(url: videoURL)
            let playerViewController = AVPlayerViewController()
            playerViewController.player = player
            self.present(playerViewController, animated: true) {
                playerViewController.player!.play()
            }
            
            
        }
        else {
            let media = messages[indexPath.item].value(forKey: "media") as! JSQPhotoMediaItem
            let image = media.image
//            let vc = Optik.imageViewer(withImages: [image!])
//            present(vc, animated: true, completion: nil)
        }
    }
    
    // MARK: Firebase related methods
    
    private func observeMessages() {
        //        SVProgressHUD.show()
        
        let userDefault = UserDefaults.standard
        let uid = userDefault.value(forKey: "user_uid") as! String
        
        
        //        let userID = Auth.auth().currentUser!.uid
        if(uid > self.chatSlug){
            messageRef = channelRef.child("\(uid)-\(self.chatSlug)")
        }
        else{
            messageRef = channelRef.child("\(self.chatSlug)-\(uid)")
        }
        
        //        messageRef = channelRef.child("Message")
        let messageQuery = messageRef.queryLimited(toLast:25)
        
//        print(messageRef)
        
        DispatchQueue.main.asyncAfter(deadline: .now()) {
            messageQuery.observe(.childAdded, with: { (snapshot) -> Void in
//                print(snapshot.value!)
                let messageData : NSDictionary = snapshot.value as! NSDictionary
//                print(messageData)
                
                if let id = messageData["sender"] as! String?,
                    let photoURL = messageData["photoURL"] as! String?,
                    let videoURL = messageData["videoURL"] as! String? { // 1
                    // 2
                    if let mediaItem = JSQVideoMediaItem(maskAsOutgoing: id == self.senderId) {
                        // 3
                        self.addVideoMessage(withId: id, key: snapshot.key, mediaItem: mediaItem)
                        // 4
                        if videoURL.hasPrefix("gs://") {
                            self.fetchVideoDataAtURL(photoURL, videoURL: videoURL , forMediaItem: mediaItem, clearsPhotoMessageMapOnSuccessForKey: nil)
                        }
                    }
                }
                    
                else if let id = messageData["sender"] as! String?,
                    let photoURL = messageData["photoURL"] as! String? { // 1
                    // 2
                    if let mediaItem = JSQPhotoMediaItem(maskAsOutgoing: id == self.senderId) {
                        // 3
                        self.addPhotoMessage(withId: id, key: snapshot.key, mediaItem: mediaItem)
                        // 4
                        if photoURL.hasPrefix("gs://") {
                            self.fetchImageDataAtURL(photoURL, forMediaItem: mediaItem, clearsPhotoMessageMapOnSuccessForKey: nil)
                        }
                    }
                }
                    
                else{
                    let senderUid = messageData["sender"] as! String
                    
                    if let text = messageData["message"] as! String?{
                        if(senderUid == self.chatSlug){
                            self.addMessage(text: text, sender: senderUid , DisplayName: self.roomName)
                        }
                        else{
                            self.addMessage(text: text, sender: uid , DisplayName: "Me")
                        }
                        
                        SVProgressHUD.dismiss()
                        self.finishReceivingMessage()
                    }
                    else{
                        SVProgressHUD.dismiss()
                        //                    SVProgressHUD.showError(withStatus: "message empty")
                    }
                }
            })
        }
        
        updatedMessageRefHandle = messageRef.observe(.childChanged, with: { (snapshot) in
            let key = snapshot.key
            let messageData = snapshot.value as! Dictionary<String, String> // 1
            
            if let photoURL = messageData["photoURL"] as String? { // 2
                // The photo has been updated.
                if let mediaItem = self.photoMessageMap[key] { // 3
                    self.fetchImageDataAtURL(photoURL, forMediaItem: mediaItem, clearsPhotoMessageMapOnSuccessForKey: key) // 4
                }
            }
        })
    }
    
    
    override func didPressSend(_ button: UIButton!, withMessageText text: String!, senderId: String!, senderDisplayName: String!, date: Date!) {
        
        SVProgressHUD.show()
        
        if(!self.sendingState){
            self.sendingState = true
            let userDefault = UserDefaults.standard
            let uid = userDefault.value(forKey: "user_uid") as! String
            
            let params : [String : Any ] = [
                "senderName" : "\(self.firstName) \(self.lastName)",
                "reciverID" : self.chatSlug,
                "senderID" : uid,
                "message" : text!,
                "avatarURL" : self.imageURL
            ]
            
            Alamofire.request(URL(string: "https://us-central1-bookme-1515492592081.cloudfunctions.net/addMessage")!, method: .post, parameters: params, encoding: JSONEncoding.default).responseString(completionHandler: { (result) in

                SVProgressHUD.dismiss()
                self.sendingState = false
                JSQSystemSoundPlayer.jsq_playMessageSentSound()
                self.finishSendingMessage()
            })
        }
        
    }
    
    func sendPhotoMessage() -> String? {
        let itemRef = messageRef.childByAutoId()
        
        let userDefault = UserDefaults.standard
        let uid = userDefault.value(forKey: "user_uid") as! String
        
        let userID = uid
        
        let messageItem = [
            "photoURL": imageURLNotSetKey,
            "sender": userID,
            ]
        
        itemRef.setValue(messageItem)
        
        JSQSystemSoundPlayer.jsq_playMessageSentSound()
        
        finishSendingMessage()
        return itemRef.key
    }
    
    func sendVideoMessage() -> String? {
        let itemRef = messageRef.childByAutoId()
        
        let userDefault = UserDefaults.standard
        let uid = userDefault.value(forKey: "user_uid") as! String
        
        let userID = uid
        
        let messageItem = [
            "photoURL": imageURLNotSetKey,
            "videoURL" :imageURLNotSetKey,
            "sender": userID,
            ]
        
        itemRef.setValue(messageItem)
        
        JSQSystemSoundPlayer.jsq_playMessageSentSound()
        
        finishSendingMessage()
        return itemRef.key
    }
    
    // MARK: UI and User Interaction
    
    private func setupOutgoingBubble() -> JSQMessagesBubbleImage {
        let bubbleImageFactory = JSQMessagesBubbleImageFactory()
        return bubbleImageFactory!.outgoingMessagesBubbleImage(with: UIColor(red: 29.0/255.0, green: 176.0/255.0, blue: 237.0/255.0, alpha: 1.0))
    }
    
    private func setupOutgoingNonReadBubble() -> JSQMessagesBubbleImage {
        let bubbleImageFactory = JSQMessagesBubbleImageFactory()
        return bubbleImageFactory!.outgoingMessagesBubbleImage(with: UIColor.jsq_messageBubbleGreen())
    }
    
    private func setupIncomingBubble() -> JSQMessagesBubbleImage {
        let bubbleImageFactory = JSQMessagesBubbleImageFactory()
        return bubbleImageFactory!.incomingMessagesBubbleImage(with: UIColor.jsq_messageBubbleLightGray())
    }
    
    override func didPressAccessoryButton(_ sender: UIButton) {
        
//        var config = YPImagePickerConfiguration()
//        config.library.onlySquare = false
//        config.onlySquareImagesFromCamera = true
//        config.targetImageSize = .original
//        config.usesFrontCamera = true
//        config.showsPhotoFilters = false
//        config.shouldSaveNewPicturesToAlbum = true
//        config.albumName = "BookMe"
//        config.screens = [.library, .photo]
//        config.startOnScreen = .library
//        config.wordings.libraryTitle = "Gallery"
//        config.hidesStatusBar = false
//        
//        // Build a picker with your configuration
//        let picker = YPImagePicker(configuration: config)
//        
//        picker.didFinishPicking(completion: { [unowned picker] items , cancelled  in
//            // image picked
//            if cancelled {
//                
//                print("Did Cancel")
//                picker.dismiss(animated: true, completion: nil)
//            }
//            
//            if let photo = items.singlePhoto {
////                print(photo.fromCamera) // Image source (camera or library)
////                print(photo.image) // Final image selected by the user
////                print(photo.originalImage) // original image selected by the user, unfiltered
////                print(photo.modifiedImage) // Transformed image, can be nil
////                print(photo.exifMeta) // Print exif meta data of original image.
//                
//                print("Select Image")
//                
//                let image : UIImage = photo.image
//                self.uploadPhoto(imageToUpload: image)
//            }
//            
//            if let video = items.singleVideo{
//                print(video.thumbnail)
//                video.fetchData(completion: { (data) in
//                    self.uploadVideo(videoToUpload: data, thumbs: video.thumbnail)
//                    })
//            }
//            
//            picker.dismiss(animated: true, completion: nil)
//        })
//        
//         self.present(picker, animated: true, completion: nil)
    }
    
    private func addMessage(text: String , sender : String , DisplayName: String) {
        if let message = JSQMessage(senderId: sender , displayName :  DisplayName , text: text) {
            messages.append(message)
        }
    }
    
    private func addPhotoMessage(withId id: String, key: String, mediaItem: JSQPhotoMediaItem) {
        if let message = JSQMessage(senderId: id, displayName: "", media: mediaItem) {
            messages.append(message)
            
            if (mediaItem.image == nil) {
                photoMessageMap[key] = mediaItem
            }
            
            collectionView.reloadData()
        }
    }
    
    private func addVideoMessage(withId id: String, key: String, mediaItem: JSQVideoMediaItem) {
        if let message = JSQMessage(senderId: id, displayName: "", media: mediaItem) {
            messages.append(message)
            
            if (mediaItem.fileURL == nil) {
                videoMessageMap[key] = mediaItem
            }
            
            collectionView.reloadData()
        }
    }
    
    
    private func fetchImageDataAtURL(_ photoURL: String, forMediaItem mediaItem: JSQPhotoMediaItem, clearsPhotoMessageMapOnSuccessForKey key: String?) {
        // 1
        let storageRef = Storage.storage().reference(forURL: photoURL)
        
        // 2
        storageRef.getData(maxSize: INT64_MAX){ (data, error) in
            if let error = error {
                print("Error downloading image data: \(error)")
                return
            }
            
            // 3
            storageRef.getMetadata(completion: { (metadata, metadataErr) in
                if let error = metadataErr {
                    print("Error downloading metadata: \(error)")
                    return
                }
                
                // 4
                //                if (metadata?.contentType == "image/gif") {
                //                    mediaItem.image = UIImage.gifWithData(data!)
                //                } else {
                mediaItem.image = UIImage.init(data: data!)
                //                }
                self.collectionView.reloadData()
                
                // 5
                guard key != nil else {
                    return
                }
                self.photoMessageMap.removeValue(forKey: key!)
            })
        }
    }
    
    
    private func fetchVideoDataAtURL(_ photoURL: String, videoURL: String, forMediaItem mediaItem: JSQVideoMediaItem , clearsPhotoMessageMapOnSuccessForKey key: String?) {
        // 1
        let storageRef = Storage.storage().reference(forURL: videoURL)
        print(storageRef)
        print(videoURL)
        // 2
        storageRef.getData(maxSize: INT64_MAX){ (data, error) in
            if let error = error {
                print("Error downloading image data: \(error)")
                return
            }
            
            // 3
            storageRef.getMetadata(completion: { (metadata, metadataErr) in
                if let error = metadataErr {
                    print("Error downloading metadata: \(error)")
                    return
                }
                
                mediaItem.fileURL = URL(string: videoURL)
                self.collectionView.reloadData()
                
                // 5
                guard key != nil else {
                    return
                }
                self.photoMessageMap.removeValue(forKey: key!)
            })
        }
    }
    
    // MARK: UITextViewDelegate methods
    
    override func textViewDidChange(_ textView: UITextView) {
        super.textViewDidChange(textView)
        isTyping = textView.text != ""
    }
    
    func setImageURL(_ url: String, forPhotoMessageWithKey key: String) {
        let itemRef = messageRef.child(key)
        itemRef.updateChildValues(["photoURL": url])
    }
    
    func setVideoURL(_ url: String, forPhotoMessageWithKey key: String) {
        let itemRef = messageRef.child(key)
        itemRef.updateChildValues(["videoURL": url])
    }
    
    func uploadPhoto(imageToUpload : UIImage){
        
        let userDefault = UserDefaults.standard
        let uid = userDefault.value(forKey: "user_uid") as! String
        
        let image = imageToUpload
        // 2
        if let key = sendPhotoMessage() {
            // 3
            let imageData = image.jpegData(compressionQuality: commonValue.imageCompress)
//            let imageData = UIImageJPEGRepresentation(image, 0.8)
            // 4
            let imagePath = uid + "/\(Int(Date.timeIntervalSinceReferenceDate * 1000)).jpg"
            // 5
            let metadata = StorageMetadata()
            metadata.contentType = "image/jpeg"
            // 6
            storageRef.child(imagePath).putData(imageData!, metadata: metadata) { (metadata, error) in
                if let error = error {
                    print("Error uploading photo: \(error)")
                    return
                }
                // 7
                self.setImageURL(self.storageRef.child((metadata?.path)!).description, forPhotoMessageWithKey: key)
            }
        }
    }
    
    func uploadVideo(videoToUpload : Data , thumbs : UIImage){
        
        let userDefault = UserDefaults.standard
        let uid = userDefault.value(forKey: "user_uid") as! String
        
        let image = thumbs
        // 2
        if let key = sendVideoMessage()  {
            // 3
            let imageData = image.jpegData(compressionQuality: commonValue.imageCompress)
//            let imageData = UIImageJPEGRepresentation(image, 0.8)
            // 4
            let fileName = "\(Int(Date.timeIntervalSinceReferenceDate * 1000))"
            let imagePath = uid + "/\(fileName).jpg"
            let videoPath = uid + "/\(fileName).mov"
            
            //5
            let metadata = StorageMetadata()
            metadata.contentType = "image/jpeg"
            
            let videoMetadata = StorageMetadata()
            metadata.contentType = "video/quicktime"
            
            
            storageRef.child(imagePath).putData(imageData!, metadata: metadata) { (metadata, error) in
                if let error = error {
                    print("Error uploading photo: \(error)")
                    return
                }
                
                self.setImageURL(self.storageRef.child((metadata?.path)!).description, forPhotoMessageWithKey: key)
                
                // 7
                self.storageRef.child(videoPath).putData(videoToUpload, metadata: videoMetadata) { (metadata, error) in
                    if let error = error {
                        print("Error uploading photo: \(error)")
                        return
                    }
                    
                    self.setVideoURL(self.storageRef.child((metadata?.path)!).description, forPhotoMessageWithKey: key)
                    print("Video Upload done")
                }
            }
            
        }
    }
    
}
