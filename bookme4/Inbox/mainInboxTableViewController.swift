//
//  mainInboxTableViewController.swift
//  BookMe
//
//  Created by Naphat Sottiyaphai on 7/12/2560 BE.
//  Copyright © 2560 Naphat Sottiyaphai. All rights reserved.
//

import UIKit
import SVProgressHUD
import CRRefresh
import SwiftDate
import FirebaseDatabase

class mainInboxTableViewController: UITableViewController {
    
    class func instantiateFromStoryboard() -> mainInboxTableViewController {
        let storyboard = UIStoryboard(name: "Inbox", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: String(describing: self)) as! mainInboxTableViewController
    }
    
    private lazy var channelRef: DatabaseReference = Database.database().reference().child("room")
    var roomData = [NSDictionary]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Inbox"
        
//        let btn2 = UIButton(type: .custom)
//        btn2.setImage(UIImage(named: "icon_search"), for: .normal)
//        btn2.frame = CGRect(x: 0, y: 0, width: 40, height: 18)
//        btn2.addTarget(self, action: #selector(self.go2search), for: .touchUpInside)
//        let item2 = UIBarButtonItem(customView: btn2)
//        self.navigationItem.setRightBarButtonItems([item2], animated: true)
        
        
        // Do any additional setup after loading the view.
//        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5){
//            self.observeRoom()
//        }
        
        self.setBackgroundColor()
        self.setClearNavBar()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        if #available(iOS 11.0, *) {
            navigationController?.navigationBar.prefersLargeTitles = true
        } else {
            print("It's down of iOS 11")
        }
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5){
            self.observeRoom()
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        URLCache.shared.removeAllCachedResponses()
    }
    
    @objc func go2search() {
        let vc = searchViewController.instantiateFromStoryboard()
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    private func observeRoom() {
        self.roomData.removeAll()
        
        let userDefault = UserDefaults.standard
        let uid = userDefault.value(forKey: "user_uid") as! String
        
        let itemRef = channelRef.child(uid)
        
        itemRef.observe( .childAdded , with: {(snapshot) -> Void in
            let roomDatas : NSDictionary = snapshot.value as! NSDictionary
            self.roomData.append(roomDatas)
            self.roomData.sort(by: {
                (($0.value(forKey: "modifedDate") as! String).toDate()!.date) > (($1.value(forKey: "modifedDate") as! String).toDate()!.date)
            })
            
            DispatchQueue.main.async {
                self.tableView.reloadData()
                SVProgressHUD.dismiss()
            }
        })
        
        itemRef.observe(.childChanged) { (snapshot) in
            let roomDatas : NSDictionary = snapshot.value as! NSDictionary
            let chatSlug = roomDatas.value(forKey: "chatSlug") as! String
            
            for allRoom in self.roomData{
                let newChatSlug = allRoom.value(forKey: "chatSlug") as! String
                if(chatSlug == newChatSlug){
                    let indexRoom = self.roomData.firstIndex(of: allRoom)
                     self.roomData[indexRoom!] = roomDatas
                }
            }
            
            self.roomData.sort(by: {
                (($0.value(forKey: "modifedDate") as! String).toDate()!.date) > (($1.value(forKey: "modifedDate") as! String).toDate()!.date)
            })
            
            DispatchQueue.main.async {
                self.tableView.reloadData()
                SVProgressHUD.dismiss()
            }
        }
    }
    
    // MARK: - Table view data source
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 95
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if(roomData.count == 0){
            self.tableView.setEmptyMessage("let's chat someone.")
        }
        else{
            self.tableView.restore()
        }
        
        return self.roomData.count
    }
    
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let inboxCell = tableView.dequeueReusableCell(withIdentifier: "inboxCell", for: indexPath) as! inboxCell

        var rowData = NSDictionary()
        
        if(self.roomData.count >= indexPath.row){
            rowData = self.roomData[indexPath.row]
            
            if(rowData.object(forKey: "avatarURL") != nil){
                let avatarURL = rowData.value(forKey: "avatarURL") as! String
                let profileName = rowData.value(forKey: "roomName") as! String
                let lastMessage = rowData.value(forKey: "lastMessage") as! String
                
                inboxCell.profileImage.sd_setImage(with: URL(string: avatarURL), placeholderImage: UIImage(named: "bookme_loading"))
                inboxCell.profileName.text = profileName
                inboxCell.lastMessage.text = lastMessage
            }

            
            inboxCell.notiCount.setRounded()
            inboxCell.notiCount.isHidden = true
            
            inboxCell.profileImage.setRounded()
            inboxCell.bgView.setRoundWithR(r: commonValue.viewRadius)
            
            
        }else{
            print("over row")
        }
        
        return inboxCell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if(self.roomData.count >= indexPath.row){
            let rowData = self.roomData[indexPath.row]
            
            let avatarURL = rowData.value(forKey: "avatarURL") as! String
            let profileName = rowData.value(forKey: "roomName") as! String
            let chatSlug = rowData.value(forKey: "chatSlug") as! String
            
            let vc = chatViewController.instantiateFromStoryboard()
            vc.chatSlug = chatSlug
            vc.roomName = profileName
            vc.avatarURL = avatarURL
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
//    @available(iOS 11.0, *)
//    override func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
//        let askAction = UIContextualAction(style: .normal, title: nil) { action, view, complete in
//            print("Ask!")
//            complete(true)
//        }
//
//        askAction.image = UIImage(named: "icon_deleteTableview")
//        askAction.backgroundColor = UIColor(red: 242.0/255.0, green: 242.0/255.0, blue: 242.0/255.0, alpha: 1.0)
//
//        return UISwipeActionsConfiguration(actions: [askAction])
//    }
    
    func tableView(tableView: UITableView, willDisplayCell cell: UITableViewCell, forRowAtIndexPath indexPath: NSIndexPath) {
        cell.backgroundColor = UIColor(red: 242.0/255.0, green: 242.0/255.0, blue: 242.0/255.0, alpha: 1.0)
    }
    
    override func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        let deleteAction = UITableViewRowAction(style: .normal, title:"       ") { (rowAction, indexPath) in
            print("delete clicked")

        }

        deleteAction.backgroundColor = UIColor(patternImage:UIImage(named: "icon_deleteTableview")!)
    
        return [deleteAction]
    }
}


