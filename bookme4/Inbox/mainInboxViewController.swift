//
//  mainInboxViewController.swift
//  bookme
//
//  Created by Naphat Sottiyaphai on 10/7/2562 BE.
//  Copyright © 2562 Naphat Sottiyaphai. All rights reserved.
//

import UIKit
import SVProgressHUD
import CRRefresh
import SwiftDate
import FirebaseDatabase
import Alamofire

class mainInboxViewController: UIViewController , UITableViewDataSource , UITableViewDelegate{

    class func instantiateFromStoryboard() -> mainInboxViewController {
        let storyboard = UIStoryboard(name: "Inbox", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: String(describing: self)) as! mainInboxViewController
    }
    
    private lazy var channelRef: DatabaseReference = Database.database().reference().child("room")
    
    @IBOutlet weak var tableView: UITableView!
    var roomData = [NSDictionary]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Inbox"
        
        let btn2 = UIButton(type: .custom)
        btn2.setImage(UIImage(named: "icon_search"), for: .normal)
        btn2.frame = CGRect(x: 0, y: 0, width: 40, height: 18)
        btn2.addTarget(self, action: #selector(self.go2search), for: .touchUpInside)
        let item2 = UIBarButtonItem(customView: btn2)
        self.navigationItem.setRightBarButtonItems([item2], animated: true)
        
        
        // Do any additional setup after loading the view.
        //        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5){
        //            self.observeRoom()
        //        }
        
        self.tableView.backgroundColor = UIColor.clear
        self.tableView.rowHeight = UITableView.automaticDimension;
        self.tableView.cr.addHeadRefresh(animator: NormalHeaderAnimator()) { [weak self] in
            
            self!.roomData.removeAll()
            self?.tableView.reloadData()
            
            DispatchQueue.main.async {
                self!.observeRoom()
            }
            
            DispatchQueue.main.asyncAfter(deadline: .now() + 1, execute: {
                self?.tableView.cr.endHeaderRefresh()
            })
            
        }
        self.tableView.cr.beginHeaderRefresh()
        
        
        self.setBackgroundColor()
        self.setClearNavBar()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        if #available(iOS 11.0, *) {
            navigationController?.navigationBar.prefersLargeTitles = true
        } else {
            print("It's down of iOS 11")
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    
    @objc func go2search() {
        let vc = searchViewController.instantiateFromStoryboard()
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    private func observeRoom() {
        self.roomData.removeAll()
        
        let userDefault = UserDefaults.standard
        let uid = userDefault.value(forKey: "user_uid") as! String
        
        let itemRef = channelRef.child(uid)
        
        itemRef.observe( .childAdded , with: {(snapshot) -> Void in
            let roomDatas : NSDictionary = snapshot.value as! NSDictionary
            self.roomData.append(roomDatas)
            self.roomData.sort(by: {
                (($0.value(forKey: "modifedDate") as! String).toDate()!.date) > (($1.value(forKey: "modifedDate") as! String).toDate()!.date)
            })
            
            DispatchQueue.main.async {
                self.tableView.reloadData()
                SVProgressHUD.dismiss()
            }
        })
        
        itemRef.observe(.childChanged) { (snapshot) in
            let roomDatas : NSDictionary = snapshot.value as! NSDictionary
            let chatSlug = roomDatas.value(forKey: "chatSlug") as! String
            
            for allRoom in self.roomData{
                if let newChatSlug = allRoom.value(forKey: "chatSlug"){
                    if(chatSlug == newChatSlug as! String){
                        let indexRoom = self.roomData.firstIndex(of: allRoom)
                        self.roomData[indexRoom!] = roomDatas
                    }
                }
                else{
                    
                }
            }
            
            self.roomData.sort(by: {
                (($0.value(forKey: "modifedDate") as! String).toDate()!.date) > (($1.value(forKey: "modifedDate") as! String).toDate()!.date)
            })
            
            DispatchQueue.main.async {
                self.tableView.reloadData()
                SVProgressHUD.dismiss()
            }
        }
    }
    
    // MARK: - Table view data source
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 95
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if(roomData.count == 0){
            self.tableView.setEmptyMessage("let's chat someone.")
        }
        else{
            self.tableView.restore()
        }
        
        return self.roomData.count
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let inboxCell = tableView.dequeueReusableCell(withIdentifier: "inboxCell", for: indexPath) as! inboxCell
        
        var rowData = NSDictionary()
        
        if(self.roomData.count >= indexPath.row){
            rowData = self.roomData[indexPath.row]
            
            if(rowData.object(forKey: "avatarURL") != nil){
                let avatarURL = rowData.value(forKey: "avatarURL") as! String
                let profileName = rowData.value(forKey: "roomName") as! String
                let lastMessage = rowData.value(forKey: "lastMessage") as! String
                
                inboxCell.profileImage.sd_setImage(with: URL(string: avatarURL), placeholderImage: UIImage(named: "bookme_loading"))
                inboxCell.profileName.text = profileName
                inboxCell.lastMessage.text = lastMessage
            }
            
            
            inboxCell.notiCount.setRounded()
            inboxCell.notiCount.isHidden = true
            
            inboxCell.profileImage.setRounded()
            inboxCell.bgView.setRoundWithR(r: commonValue.viewRadius)
            
            
        }else{
            print("over row")
        }
        
        return inboxCell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if(self.roomData.count >= indexPath.row){
            let rowData = self.roomData[indexPath.row]
            
            let avatarURL = rowData.value(forKey: "avatarURL") as! String
            let profileName = rowData.value(forKey: "roomName") as! String
            let chatSlug = rowData.value(forKey: "chatSlug") as! String
            
            let vc = chatViewController.instantiateFromStoryboard()
            vc.chatSlug = chatSlug
            vc.roomName = profileName
            vc.avatarURL = avatarURL
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    
    private func tableView(tableView: UITableView, willDisplayCell cell: UITableViewCell, forRowAtIndexPath indexPath: NSIndexPath) {
        cell.backgroundColor = UIColor(red: 242.0/255.0, green: 242.0/255.0, blue: 242.0/255.0, alpha: 1.0)
    }
    
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        let deleteAction = UITableViewRowAction(style: .normal, title:"       ") { (rowAction, indexPath) in
            print("delete clicked")
            print("index \(indexPath.row)")
            print(self.roomData[indexPath.row])
            
            let rowData = self.roomData[indexPath.row]
            let chatSlug = rowData.value(forKey: "chatSlug") as! String
            
            let userDefault = UserDefaults.standard
            let uid = userDefault.value(forKey: "user_uid") as! String
            
            self.deleteByUserID(uId: uid, pId: chatSlug)
        }
        
        deleteAction.backgroundColor = UIColor(patternImage:UIImage(named: "icon_deleteTableview")!)
        
        return [deleteAction]
    }
    
    func deleteByUserID(uId : String , pId : String) {
        self.callGetRoomName(uId: uId, pId: pId)
    }

    func callGetRoomName(uId : String , pId : String) {
        let params : [String : Any] = [
            "senderID" : uId,
            "reciverID" : pId,
        ]
        
        Alamofire.request(URL(string : "https://us-central1-bookme-1515492592081.cloudfunctions.net/getRoomName")!, method: .post, parameters: params, encoding: JSONEncoding.default).responseString { (response) in
            print(response)
            
            if let result = response.result.value{
                let data = result as! String
                print(data)
                
                self.callDeleteChat(roomName: data, userID: uId)
            }
            else{
                SVProgressHUD.showError(withStatus: "Ops! Something wrong.\n Please Try again later.")
                DispatchQueue.main.asyncAfter(deadline: .now() + 2){
                    SVProgressHUD.dismiss()
                }
            }
        }
    }
    
    func callDeleteChat(roomName : String , userID : String) {
        let params : [String : Any] = [
            "roomName" : roomName,
            "userID" : userID,
        ]
        
        Alamofire.request(URL(string : "https://us-central1-bookme-1515492592081.cloudfunctions.net/deleteChatRoom")!, method: .delete, parameters: params, encoding: JSONEncoding.default).responseJSON(completionHandler: { (response) in
            
            if let result = response.result.value{
                let data = result as! NSDictionary
                print(data)
            }
            else{
                SVProgressHUD.showError(withStatus: "Ops! Something wrong.\n Please Try again later.")
                DispatchQueue.main.asyncAfter(deadline: .now() + 2){
                    SVProgressHUD.dismiss()
                }
            }
        })
    }
}

class inboxCell: UITableViewCell {
    
    @IBOutlet var profileImage : UIImageView!
    @IBOutlet var profileName : UILabel!
    @IBOutlet var lastMessage : UILabel!
    @IBOutlet var notiCount : UILabel!
    @IBOutlet var bgView: UIView!
    @IBOutlet var CaseMode: UILabel!
    @IBOutlet var headerType: UIView!
    
}
