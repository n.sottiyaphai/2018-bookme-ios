//
//  tabBarViewController.swift
//  bookme
//
//  Created by Naphat Sottiyaphai on 22/6/2561 BE.
//  Copyright © 2561 Naphat Sottiyaphai. All rights reserved.
//

import UIKit
import UIKit
//import TransitionableTab

class tabBarViewController: UITabBarController {

    @IBOutlet weak var tabbar: UITabBar!
    
    class func instantiateFromStoryboard() -> tabBarViewController {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: String(describing: self)) as! tabBarViewController
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
//        self.delegate = self
        
        self.tabBar.layer.masksToBounds = true
        self.tabBar.isTranslucent = true
        self.tabBar.barStyle = .blackOpaque
        self.tabBar.layer.cornerRadius = 10
        self.tabBar.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner]
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        print("tabbar call")
        self.tabBarController?.selectedIndex = 0
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}
//
//extension tabBarViewController: TransitionableTab {
//
//    func tabBarController(_ tabBarController: UITabBarController, shouldSelect viewController: UIViewController) -> Bool {
//        return animateTransition(tabBarController, shouldSelect: viewController)
//    }
//}

