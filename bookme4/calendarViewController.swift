//
//  calendarViewController.swift
//  bookme
//
//  Created by Naphat Sottiyaphai on 16/5/2561 BE.
//  Copyright © 2561 Naphat Sottiyaphai. All rights reserved.
//

import UIKit
import Crashlytics

class calendarViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func crashButtonTapped(_ sender: AnyObject) {
        Crashlytics.sharedInstance().crash()
    }
    
}
