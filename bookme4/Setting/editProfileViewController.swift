//
//  editProfileViewController.swift
//  bookme4
//
//  Created by Naphat Sottiyaphai on 24/4/2561 BE.
//  Copyright © 2561 Naphat Sottiyaphai. All rights reserved.
//

import UIKit
import SDWebImage
import Alamofire
import ActionSheetPicker_3_0
import SVProgressHUD
import SwiftDate
import SwiftHash

class editProfileViewController: UIViewController {
    
    @IBOutlet weak var editButtonOutlet: UIButton!
    @IBOutlet weak var saveButton: UIButton!
    @IBOutlet weak var profileImage: UIImageView!
    
    @IBOutlet weak var fname: UITextField!
    @IBOutlet weak var lname: UITextField!
    @IBOutlet weak var titleText: UITextField!
    @IBOutlet weak var addressTF: UITextField!
    @IBOutlet weak var emailTF: UITextField!
    @IBOutlet weak var mobileTF: UITextField!
    @IBOutlet weak var bdTF: UITextField!
    @IBOutlet weak var bgView: UIView!
    
    var firstName = String()
    var lastName = String()
    var imageURL = String()
    var email = String()
    var titles = String()
    var bdText = String()
    var address = String()
    var mobile = String()
    var id = String()
    var bd = String()
    var selectImage = UIImage()
    var userProfileData = NSDictionary()
    
    var RefreshCount = 0
    
    class func instantiateFromStoryboard() -> editProfileViewController {
        let storyboard = UIStoryboard(name: "profile", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: String(describing: self)) as! editProfileViewController
    }
    
    //    MARK:- LIFE CYCLE
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "Edit Profile"
        
        self.mobileTF.delegate = self
        self.bdTF.delegate = self
        
        saveButton.setRoundedwithR(r: commonValue.viewRadius)
        editButtonOutlet.setRoundWithShadow(r: 16.5)
        profileImage.setRounded()
        
        self.callBluweoProfile()
        setBackgroundColor()
        bgView.setRoundWithR(r: commonValue.viewRadius)
        
        fname.setDefaultTextFieldLayout()
        lname.setDefaultTextFieldLayout()
        titleText.setDefaultTextFieldLayout()
        addressTF.setDefaultTextFieldLayout()
        emailTF.setDefaultTextFieldLayout()
        mobileTF.setDefaultTextFieldLayout()
        bdTF.setDefaultTextFieldLayout()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.navigationController?.navigationBar.isHidden = false
    
        UINavigationBar.appearance().prefersLargeTitles = true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        UINavigationBar.appearance().prefersLargeTitles = false
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //    MARK:- Bluweo Profile Functions Call
    func callBluweoProfile() {
        
        print("Call Bluweo profile")
        
        let userDefaults = UserDefaults.standard
        let accessToken = userDefaults.value(forKey: "bluweoAccessToken") as! String
        
        let url = "\(baseURL.bluweoProfile)/get?access_token=\(accessToken)"
        
        let headers = [
            "client_id" : "1001",
            "secret_key" : "bookme",
            "Content-Type": "application/x-www-form-urlencoded"
        ]
        
        Alamofire.request(URL(string: url)!, method: .get , encoding: URLEncoding(destination: .httpBody) , headers: headers).responseJSON(completionHandler: { (respond) in
            
            if let result = respond.result.value{
                let responsed = result as! NSDictionary
                let response = responsed.value(forKey: "response") as! Bool
                
                if(response){
                    let data = responsed.value(forKey: "data") as! NSDictionary
                    let access_token = data.value(forKey: "access_token") as! String
                    
                    let userDefaults = UserDefaults.standard
                    userDefaults.set(access_token, forKey: "bluweoAccessToken")
                    
                    var birthDate = String()
                    self.userProfileData = data
                    
                    self.firstName = data.value(forKey: "first_name") as! String
                    self.lastName = data.value(forKey: "last_name") as! String
                    self.imageURL = data.value(forKey: "profile_photo") as! String
                    self.titles = data.value(forKey: "title") as! String
                    self.email = data.value(forKey: "email") as! String
                    self.address = data.value(forKey: "address") as! String
                    self.mobile = data.value(forKey: "mobile") as! String
                    
                    if(data.value(forKey: "birth_date") is NSNull){
                        print("birthDate is NULL")
                        birthDate = ""
                    }
                    else{
                        birthDate = data.value(forKey: "birth_date") as! String
                    }
                    
                    
                    self.fname.text = self.firstName
                    self.lname.text = self.lastName
                    self.profileImage.sd_setImage(with: URL(string: self.imageURL), placeholderImage: UIImage(named: "1-05.jpg"))
                    self.titleText.text = self.titles
                    self.emailTF.text = self.email
                    self.mobileTF.text = self.mobile
                    
                    if let address = data.value(forKey: "address"){
                        self.addressTF.text = address as! String
                    }
                    
                    self.bd = birthDate
                    let dateBD = birthDate.toDate()
                    self.bdTF.text = dateBD?.toFormat("dd MMMM YYYY")
                }
                else{
                    let data = responsed.value(forKey: "data") as! NSDictionary
                    let message = data.value(forKey: "message") as! NSArray
                    let msg = message[0] as! NSDictionary
                    let errMsg = msg.value(forKey: "message") as! String
                    
                    self.RefreshCount+=1
                    
                    if(errMsg == "access token not valid or expired"){
                        self.refreshBluweoToken()
                    }
                    
                    SVProgressHUD.show()
                    DispatchQueue.main.asyncAfter(deadline: .now() + 3){
                        
                        if(self.RefreshCount < 2){
                            self.callBluweoProfile()
                        }
                        else{
                            SVProgressHUD.showError(withStatus: "Something wrong.\nTry again later")
                            self.RefreshCount = 0
                        }
                    }
                }
            }
                
            else{
                print(respond.error?.localizedDescription ?? "")
                SVProgressHUD.dismiss()
            }
        })
    }
    
    
    func updateBluweoProfile(data : NSDictionary) {
        
        SVProgressHUD.show()
        
        let ud = UserDefaults.standard
        let uid = ud.value(forKey: "user_uid") as! String
        let accessToken = ud.value(forKey: "bluweoAccessToken") as! String
        print("uid : \(uid)")
        
        let titles = self.titleText.text
        let firstName = self.fname.text
        let lastName = self.lname.text
        let birthDate = self.bd
        let profilePhoto = self.imageURL
        let mobile = self.mobileTF.text
        let address = self.addressTF.text
        let email = data.value(forKey: "email") as! String
        
        let url = "\(baseURL.bluweoProfile)?access_token=\(accessToken)"
        
        let headers = [
            "client_id" : "1001",
            "secret_key" : "bookme",
            "Content-Type": "application/x-www-form-urlencoded"
        ]
        let params = [
            "title" : titles!,
            "first_name" : firstName!,
            "last_name" : lastName!,
            "birth_date" : birthDate,
            "profile_photo" : URL(string: profilePhoto)!,
            "mobile" : mobile!,
            "email" : email,
            "address" : address!
            ] as [String : Any]
        
        
        Alamofire.request(URL(string: url)!, method: .put , parameters: params, encoding: URLEncoding(destination: .httpBody) , headers: headers).responseJSON(completionHandler: { (respond) in
            
            if let result = respond.result.value{
                let responsed = result as! NSDictionary
                print(responsed)
                
                let response = responsed.value(forKey: "response") as! Bool
                if(response){
                    let data = responsed.value(forKey: "data") as! NSDictionary
                    let access_token = data.value(forKey: "access_token") as! String
                    let refresh_token = data.value(forKey: "refresh_token") as! String
                    
                    let userDefaults = UserDefaults.standard
                    userDefaults.set(access_token, forKey: "bluweoAccessToken")
                    userDefaults.set(refresh_token, forKey: "bluweoRefreshToken")
                    
                    SVProgressHUD.dismiss()
                    print("!! Update Success")
                    
                    SVProgressHUD.showSuccess(withStatus: "Update Success")
                    DispatchQueue.main.asyncAfter(deadline: .now() + 2){
                        self.callBluweoProfile()
                        SVProgressHUD.dismiss()
                    }
                    //                    self.performSegue(withIdentifier: "authSuccess", sender: self)
                }
                else{
                    let data = responsed.value(forKey: "data") as! NSDictionary
                    let message = data.value(forKey: "message") as! NSArray
                    let msg = message[0] as! NSDictionary
                    let errMsg = msg.value(forKey: "message") as! String
                    
                    self.RefreshCount+=1
                    if(errMsg == "access token not valid or expired"){
                        self.refreshBluweoToken()
                        
                    }
                    
                    SVProgressHUD.show()
                    DispatchQueue.main.asyncAfter(deadline: .now() + 3){
                        
                        if(self.RefreshCount < 2){
                            self.updateBluweoProfile(data: self.userProfileData)
                        }
                        else{
                            SVProgressHUD.showError(withStatus: "Something wrong.\nTry again later")
                            self.RefreshCount = 0
                        }
                    }
                }
            }
            else{
                print(respond.error?.localizedDescription ?? "")
                SVProgressHUD.dismiss()
            }
        })
    }
    
    // MARK:- UIButton Action
    @IBAction func editPhoto(_ sender: Any) {
        
//        var config = YPImagePickerConfiguration()
//        
//        config.library.onlySquare = false
//        config.onlySquareImagesFromCamera = true
//        config.targetImageSize = .original
//        config.usesFrontCamera = true
//        config.showsPhotoFilters = true
//        config.shouldSaveNewPicturesToAlbum = true
//        config.albumName = "BookMe"
//        config.screens = [.library, .photo]
//        config.startOnScreen = .library
//        config.wordings.libraryTitle = "Gallery"
//        config.hidesStatusBar = false
//        
//        // Build a picker with your configuration
//        let picker = YPImagePicker(configuration: config)
//        
//        picker.didFinishPicking(completion: { [unowned picker] items ,  cancelled  in
//            if let photo = items.singlePhoto {
//                let image = photo.image.jpegData(compressionQuality: commonValue.imageCompress)
//                self.ImageUploading(data: image!)
//            }
//            if cancelled {
//                print("Picker was canceled")
//            }
//            picker.dismiss(animated: true, completion: nil)
//        })
////        self.present(picker, animated: true, completion: nil)
//        self.navigationController?.present(picker, animated: true, completion: nil)
    }
    
    
    @IBAction func save(_ sender: Any) {
        self.updateBluweoProfile(data: self.userProfileData)
        print("Save Update")
    }
    
    //    MARK:- Bluweo Media Uploading
    func ImageUploading(data : Data) {
        
        
        let ud = UserDefaults.standard
        let uid = ud.value(forKey: "user_uid") as! String
        let accessToken = ud.value(forKey: "bluweoAccessToken") as! String
        
        let url = URL(string: "\(baseURL.bluweoMedia)upload")
        
        let headers = [
            "access_token" : accessToken,
            "accept": "application/x-www-form-urlencoded",
            "Content-Type" : "multipart/form-data"
        ]
        
        let parameters = [
            "api": "profile",
            "path" : "profileImage"
        ]
        
        print(url)
        print(headers)
        print(parameters)
        
        Alamofire.upload(multipartFormData: { (multipartFormData) in

            multipartFormData.append(data, withName: "file", fileName: "\(uid)_\(Date().timeIntervalSince1970)_profileImage.jpg", mimeType: "image/jpeg")
            
            for (key, value) in parameters {
                multipartFormData.append(value.data(using: String.Encoding.utf8)!, withName: key)
            }
            
        }, to: url!, method: .post, headers: headers) { (result) in
            
            switch result {
            case .success(let upload, _, _):
                
                upload.uploadProgress(closure: { (progress) in
                    SVProgressHUD.showProgress(Float(progress.fractionCompleted), status: "Image Uploading")
                })
                
                upload.responseJSON { response in
                    
                    if let result = response.result.value {
                        
                        let results = result as! NSDictionary
                        let ArryResult = results.value(forKey: "data") as! NSDictionary
                        
                        if(ArryResult.object(forKey: "file") != nil){
                            let filePath = ArryResult.value(forKey: "file") as! String
                            
                            self.imageURL = "\(baseURL.bluweoMedia)\(filePath)"
                            print("\(baseURL.bluweoMedia)\(filePath)")
                            
                            self.updateBluweoProfile(data: self.userProfileData)
                            SVProgressHUD.dismiss()
                        }
                        else{
                            print(ArryResult)
                            
                            if let message = ArryResult.value(forKey: "message"){
                                if(message is NSDictionary){
                                    if let errMsg = (message as! NSDictionary).value(forKey: "message"){
                                        print(errMsg as! Any)
                                    }
                                }
                                else{
                                    let message = ArryResult.value(forKey: "message") as! String
                                    print(message)
                                }
                            }
                            
                            self.RefreshCount+=1
                            self.refreshBluweoToken()
                            
                            SVProgressHUD.show()
                            DispatchQueue.main.asyncAfter(deadline: .now() + 3){
                                
                                if(self.RefreshCount < 2){
                                    self.ImageUploading(data: data)
                                    SVProgressHUD.dismiss()
                                }
                                else{
                                    SVProgressHUD.showError(withStatus: "Something wrong.\nTry again later")
                                    self.RefreshCount = 0
                                }
                            }
                            
                        }
                    }
                }
            case .failure(let encodingError):
                print("ERROR : \(encodingError)")
                SVProgressHUD.showError(withStatus: "Media Connection Error : \(encodingError)")
                break
            }
        }
    }
    
    
    //     MARK:-
    func updateProfile(data : NSDictionary , back : Bool) {
        
        let ud = UserDefaults.standard
        var accesstoken = String()
        
        if(ud.object(forKey: "accessToken") != nil){
            accesstoken = ud.value(forKey: "accessToken") as! String
        }
        
        let header : HTTPHeaders = [
            "accessToken" : accesstoken
        ]
        
        let uid = ud.value(forKey: "user_uid") as! String
        
        print("uid : \(uid)")
        
        let titles = self.titleText.text
        let firstName = self.fname.text
        let lastName = self.lname.text
        //        let birthDate = self.bd.toDate()?.toISO()
        let profilePhoto = self.imageURL
        let backgroundPhoto = data.value(forKey: "backgroundPhoto") as! String
        let address = self.addressTF.text
        let city = data.value(forKey: "city") as! String
        let country = data.value(forKey: "country") as! String
        let mobile = self.mobileTF.text
        let email = data.value(forKey: "email") as! String
        
        let parameters: [String: Any] = [
            "title" : titles!,
            "firstName" : firstName!,
            "lastName" : lastName!,
            "profilePhoto" : profilePhoto,
            "backgroundPhoto" : backgroundPhoto,
            "address" : address!,
            "city" : city,
            "country" : country,
            "mobile" : mobile!,
            "email" : email,
        ]
        
        
        print(parameters)
        
        Alamofire.request(baseURL.url + "profile/\(uid)", method: .put, parameters: parameters, encoding: JSONEncoding.default , headers : header)
            .responseJSON(completionHandler: { response in
                
                if let result = response.result.value {
                    _ = result as! NSDictionary
                    
                    SVProgressHUD.showSuccess(withStatus: "Update done")
                    
                    DispatchQueue.main.asyncAfter(deadline: .now()+1){
                        SVProgressHUD.dismiss()
                        let ud = UserDefaults.standard
                        
                        let uid = ud.value(forKey: "user_uid") as! String
                        
                        print("uid : \(uid)")
                        //                        self.callProfile(uid: uid)
                        
                        if(back){
                            self.navigationController?.popViewController(animated: true)
                        }
                        
                    }
                }
                else{
                    SVProgressHUD.showError(withStatus: "Network Error")
                }
            })
    }
    
    
}

extension editProfileViewController : UITextFieldDelegate {
    func textFieldDidBeginEditing(_ textField: UITextField) {
        switch textField {
        case bdTF:
            print("birthDate Select")
            self.view.endEditing(true)
            
            let datePicker = ActionSheetDatePicker(title: " Select Birthdate", datePickerMode: UIDatePicker.Mode.date, selectedDate: Date()-20.years , doneBlock: {
                picker, value, index in
                
                let dateValue = value as! Date
                let isoDateValue = (dateValue + 7.hours).toFormat("yyyy-MM-dd")
                print(isoDateValue)
                self.bd = isoDateValue
                
                textField.text = "\((dateValue + 7.hours).toFormat("dd MMMM YYYY"))"
                textField.resignFirstResponder()
                
                return
            }, cancel: { ActionStringCancelBlock in return }, origin: (textField as AnyObject).superview!?.superview)
            
            datePicker?.maximumDate = Date()
            datePicker?.show()
            
            break
        default:
            break
        }
    }
}

