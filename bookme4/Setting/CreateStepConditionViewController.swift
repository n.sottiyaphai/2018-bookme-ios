//
//  CreateStepConditionViewController.swift
//  bookme
//
//  Created by Naphat Sottiyaphai on 24/2/2562 BE.
//  Copyright © 2562 Naphat Sottiyaphai. All rights reserved.
//

import UIKit
import Alamofire
import SVProgressHUD

class CreateStepConditionViewController: UIViewController {
    
    var professional = NSDictionary()
    var professID = String()
    
    @IBOutlet weak var nextButt: UIButton!
    @IBOutlet weak var tableView: UITableView!
    
    // conditions Declaration
    var conditions = [""]
    
    class func instantiateFromStoryboard() -> CreateStepConditionViewController {
        let storyboard = UIStoryboard(name: "profile", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: String(describing: self)) as! CreateStepConditionViewController
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.nextButt.setRoundedwithR(r: commonValue.subTadius)
        
        
        self.getProfess()             // <- Open this for real testing
        
        self.setClearNavBar()
        self.setBackgroundColor()
    }
    
    
    @IBAction func nextStep(_ sender: Any) {
        self.updateProfess()
    }
    
    func getProfess() {
        
        let ud = UserDefaults.standard
        var accesstoken = String()
        
        if(ud.object(forKey: "accessToken") != nil){
            accesstoken = ud.value(forKey: "accessToken") as! String
        }
        
        let header : HTTPHeaders = [
            "accessToken" : accesstoken
        ]
        
        Alamofire.request(baseURL.url + "professional/\(self.professID)", method: .get, encoding: JSONEncoding.default , headers : header)
            .responseJSON(completionHandler: { response in
                
                if let responses = response.result.value {
                    let responseData = responses as! NSDictionary
                    let data = responseData.value(forKey: "data") as! NSDictionary
                    
                    self.professional = data
                    print("collect profess complete")
                }
                else{
                    SVProgressHUD.showError(withStatus: response.error?.localizedDescription)
                }
            })
    }
    
    func updateProfess() {
        
        print("Updateing profess")
        let portfolio = professional.value(forKey: "portfolio") as! NSDictionary
        
        let ud = UserDefaults.standard
        var accesstoken = String()
        
        if(ud.object(forKey: "accessToken") != nil){
            accesstoken = ud.value(forKey: "accessToken") as! String
        }
        
        let header : HTTPHeaders = [
            "accessToken" : accesstoken
        ]
        
        let parameters: [String: Any] = [
            "activeStatus" : true,
            "portfolio" : [
                "gallery" : portfolio.value(forKey: "gallery") as! Array<String>,
                "portfolioBio" : portfolio.value(forKey: "portfolioBio") as! String,
                "instagram" : "",
                "conditions" : self.conditions,
                "calendar" : []
            ]
        ]
        
        print(parameters)
        
        Alamofire.request(baseURL.url + "professional/\(self.professID)", method: .put, parameters: parameters, encoding: JSONEncoding.default , headers: header).responseJSON { (response) in
            
            if let result = response.result.value {
                print(result)
                let vc = ThankyouViewController.instantiateFromStoryboard()
                self.navigationController?.pushViewController(vc, animated: true)
            }
            else{
                SVProgressHUD.showError(withStatus: response.error?.localizedDescription)
            }
        }
    }
    
}

extension CreateStepConditionViewController : UITableViewDataSource , UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return conditions.count + 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if(indexPath.row < conditions.count){
            let cell = tableView.dequeueReusableCell(withIdentifier: "CreateConditionCell", for: indexPath) as! createConditionCell
            
            cell.condition.delegate = self
            cell.condition.tag = indexPath.row
            
            cell.point.setRounded()
            cell.bgView.setRoundWithCommonR()
            
            
            return cell
        }
        else{
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "addObject", for: indexPath) as! addOCreatebjectCell
            
            cell.titleTet.text = "Add a Condition"
            cell.bgView.setRoundWithCommonR()
            
            return cell
        }
        
        return UITableViewCell()
        
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if(indexPath.row < conditions.count){
            return UITableView.automaticDimension
        }
        else{
            return 60
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if(indexPath.row >= conditions.count){
            self.conditions.append("")
            
            DispatchQueue.main.async {
                self.tableView.reloadData()
            }
        }
    }
    
    
}

extension CreateStepConditionViewController : UITextViewDelegate {
    
    func textViewDidChange(_ textView: UITextView) {
        DispatchQueue.main.async {
            self.tableView?.beginUpdates()
            self.tableView?.endUpdates()
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        print(textView.tag)
        self.conditions[textView.tag] = textView.text
    }
    
}



class createConditionCell: UITableViewCell {
    @IBOutlet weak var point: UIView!
    @IBOutlet weak var condition : UITextView!
    @IBOutlet weak var lineView: UIView!
    @IBOutlet weak var bgView : UIView!
    
}

class addOCreatebjectCell : UITableViewCell {
    @IBOutlet weak var titleTet: UILabel!
    @IBOutlet weak var bgView: UIView!
}
