//
//  IDCardVerifyViewController.swift
//  bookme
//
//  Created by Naphat Sottiyaphai on 30/5/2561 BE.
//  Copyright © 2561 Naphat Sottiyaphai. All rights reserved.
//

import UIKit
import ACFloatingTextfield_Swift
import SwiftHash
import Alamofire
import SVProgressHUD

class IDCardVerifyViewController: UIViewController {
    
    @IBOutlet weak var IDimageView: UIImageView!
    @IBOutlet weak var editButton: UIButton!
    @IBOutlet weak var saveButton: UIButton!
    @IBOutlet weak var idCardNumber: UITextField!
    @IBOutlet weak var bgView: UIView!
    
    var imageURL = String()
    var imageData = Data()
    
    
    class func instantiateFromStoryboard() -> IDCardVerifyViewController {
        let storyboard = UIStoryboard(name: "profile", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: String(describing: self)) as! IDCardVerifyViewController
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        idCardNumber.setDefaultTextFieldLayout()
        self.saveButton.setRoundedwithR(r: commonValue.viewRadius)
//        IDimageView.setRoundWithR(r: commonValue.viewRadius)
        
        self.title = "ID Card"
        self.idCardNumber.delegate = self
        
        self.setBackgroundColor()
        self.IDimageView.setRoundWithR(r: commonValue.viewRadius)
        self.bgView.setRoundWithR(r: commonValue.viewRadius)
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.navigationController?.navigationBar.isHidden = false
        UINavigationBar.appearance().prefersLargeTitles = true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        UINavigationBar.appearance().prefersLargeTitles = false
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func takeAPicture(_ sender: Any) {
        
//        var config = YPImagePickerConfiguration()
//        config.onlySquareImagesFromCamera = true
//        config.targetImageSize = .original
//        config.usesFrontCamera = false
//        config.showsPhotoFilters = false
//        config.shouldSaveNewPicturesToAlbum = true
//        config.albumName = "BookMe"
//        config.screens = [.photo]
//        config.wordings.libraryTitle = "Gallery"
//        config.hidesStatusBar = false
//        config.library.maxNumberOfItems = 1
//        config.library.minNumberOfItems = 1
//        
//        // Build a picker with your configuration
//        let picker = YPImagePicker(configuration: config)
//        
//        
//        // unowned is Mandatory since it would create a retain cycle otherwise :)
//        picker.didFinishPicking(completion: { [unowned picker] items , cancelled  in
//            // image picked
//            if cancelled {
//                
//                print("Did Cancel")
//                picker.dismiss(animated: true, completion: nil)
//            }
//            
//            if let photo = items.singlePhoto {
////                print(photo.fromCamera) // Image source (camera or library)
////                print(photo.image) // Final image selected by the user
////                print(photo.originalImage) // original image selected by the user, unfiltered
////                print(photo.modifiedImage) // Transformed image, can be nil
////                print(photo.exifMeta) // Print exif meta data of original image.
//                
//                self.IDimageView.image = photo.image
//                let image = photo.image.jpegData(compressionQuality: commonValue.imageCompress)
//                self.editButton.setTitle("", for: .normal)
//                self.imageData = image!
//            }
//            picker.dismiss(animated: true, completion: nil)
//        })
//        self.present(picker, animated: true, completion: nil)
        
    }
    
    @IBAction func submitData(_ sender: Any) {
        if (idCardNumber.text != "" && self.imageData != Data()){
            self.ImageUploading(data: self.imageData)
        }
        else{
            var title = String()
            var description = String()
            var actionTitle = String()
            
            let userDefault = UserDefaults.standard
            let lang = userDefault.value(forKey: "langDevice") as! String
            
            if(lang == "th-TH"){
                title = thai.someThingWrong
                description = thai.fillNotValidate
                actionTitle = thai.ok
            }
            else{
                title = english.someThingWrong
                description = english.fillNotValidate
                actionTitle = english.ok
            }
            
            
            let alert = UIAlertController.init(title: title, message: description, preferredStyle: .alert)
            
            let ok = UIAlertAction.init(title: actionTitle, style: .default) { (alert) in
                print("OK")
            }
            
            alert.addAction(ok)
            
            self.present(alert, animated: true, completion: nil)
            
        }
    }
    
    func ImageUploading(data : Data) {
        
        let ud = UserDefaults.standard
        let uid = ud.value(forKey: "user_uid") as! String
        let md5_uid = MD5(uid)
        let url = URL(string: "\(baseURL.media)api/uploads/")
        
        
        Alamofire.upload(multipartFormData: { (multipartFormData) in
            
            multipartFormData.append("\(uid)".data(using: String.Encoding.utf8)!, withName: "uId" as String)
            multipartFormData.append(data, withName: "imgFiles", fileName: "\(uid)_swift_file.jpeg", mimeType: "image/jpeg")
            
            
        },to: url! )
        { (result) in
            
            switch result {
            case .success(let upload, _, _):
                
                upload.uploadProgress(closure: { (progress) in
                    SVProgressHUD.showProgress(Float(progress.fractionCompleted), status: "Image Uploading")
                })
                
                upload.responseJSON { response in
                    
                    if let result = response.result.value {
                        let results = result as! NSDictionary
                        let data = results.value(forKey: "data") as! NSArray
                        
                        self.imageURL = "\(baseURL.photoURL)\(md5_uid.lowercased())/storages/\(data[0])"
                        
                        self.updateProfileDocument()
                        
                        SVProgressHUD.dismiss()
                        
                    }
                }
                
            case .failure(let encodingError):
                print("ERROR : \(encodingError)")
                SVProgressHUD.showError(withStatus: "Media Connection Error : \(encodingError)")
                break
            }
        }
    }
    
    func updateProfileDocument() {
        
        let ud = UserDefaults.standard
        let uid = ud.value(forKey: "user_uid") as! String
        
        print("uid : \(uid)")
        
        let parameters: [String: Any] = [
            "idCardNumber" : self.idCardNumber.text!,
            "idCardImage" : self.imageURL
        ]
        
        var accesstoken = String()
        
        if(ud.object(forKey: "accessToken") != nil){
            accesstoken = ud.value(forKey: "accessToken") as! String
        }
        
        let header : HTTPHeaders = [
            "accessToken" : accesstoken
        ]
        
        
        
        print(parameters)
        
        Alamofire.request(baseURL.url + "profile/\(uid)/document/update-id-card", method: .put, parameters: parameters, encoding: JSONEncoding.default , headers : header)
            .responseJSON(completionHandler: { response in
                
                if let result = response.result.value {
                    _ = result as! NSDictionary
                    
                    SVProgressHUD.showSuccess(withStatus: "Update done")
                    
                    DispatchQueue.main.asyncAfter(deadline: .now()+1){
                        SVProgressHUD.dismiss()
                        self.navigationController?.popViewController(animated: true)
                    }
                }
                else{
                    SVProgressHUD.showError(withStatus: "Network Error")
                }
            })
    }
    
}

extension IDCardVerifyViewController : UITextFieldDelegate {
    func textFieldDidEndEditing(_ textField: UITextField) {
        let string = textField.text!
        
        if(textField == self.idCardNumber){
            if(textField.text?.count == 13){
                
                textField.textColor = UIColor.darkText
                
                let index0 = string.index(string.startIndex, offsetBy: 1)
                let index1 = string.index(index0, offsetBy: 4)
                let index2 = string.index(index1, offsetBy: 5)
                let index3 = string.index(index2, offsetBy: 2)
                let index4 = string.index(index3, offsetBy: 1)
                
                let mySubstring = string[..<index0]
                let mySubstring1 = string[index0..<index1]
                let mySubstring2 = string[index1..<index2]
                let mySubstring3 = string[index2..<index3]
                let mySubstring4 = string[index3..<index4]
                
                let formatString = "\(mySubstring)-\(mySubstring1)-\(mySubstring2)-\(mySubstring3)-\(mySubstring4)"
                textField.text = formatString
            }
            else{
                textField.textColor = UIColor.red
            }
        }
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        textField.text = ""
        textField.textColor = UIColor.darkText
        return true
    }
}
