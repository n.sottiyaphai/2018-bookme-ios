//
//  ThankyouViewController.swift
//  bookme4
//
//  Created by Naphat Sottiyaphai on 17/4/2561 BE.
//  Copyright © 2561 Naphat Sottiyaphai. All rights reserved.
//

import UIKit
import BEMCheckBox

class ThankyouViewController: UIViewController {

    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var textLabel: UILabel!
    
    @IBOutlet weak var profileCheckBox: BEMCheckBox!
    @IBOutlet weak var portCheck: BEMCheckBox!
    @IBOutlet weak var packCheck: BEMCheckBox!
    @IBOutlet weak var carlendarCheck: BEMCheckBox!
    
    @IBOutlet weak var profileText: UILabel!
    @IBOutlet weak var portText: UILabel!
    @IBOutlet weak var packText: UILabel!
    @IBOutlet weak var carlendarText: UILabel!
    
    @IBOutlet weak var back: UIButton!
    @IBOutlet weak var done: UIButton!
    
    class func instantiateFromStoryboard() -> ThankyouViewController {
        let storyboard = UIStoryboard(name: "profile", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: String(describing: self)) as! ThankyouViewController
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.setClearNavBar()
//        self.setBackgroundColor()
        
        self.profileCheckBox.setOn(true, animated: true)
        self.portCheck.setOn(true, animated: true)
        self.packCheck.setOn(true, animated: true)
        
        self.back.setRoundWithCommonRWithBorder()
        self.done.setRoundWithCommonRWithBorder()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.navigationBar.isHidden = true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.navigationController?.navigationBar.isHidden = false
    }
    
    @IBAction func back(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func done(_ sender: Any) {
        self.navigationController?.popToRootViewController(animated: true)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
