//
//  selectSubcatNoExpandViewController.swift
//  bookme
//
//  Created by Naphat Sottiyaphai on 7/12/2561 BE.
//  Copyright © 2561 Naphat Sottiyaphai. All rights reserved.
//

import UIKit
import SVProgressHUD
import Alamofire
import BWWalkthrough

class selectSubcatNoExpandViewController: UIViewController , BWWalkthroughViewControllerDelegate {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var nextButton: UIButton!
    
    var mode = String()
    var cat = [String]()
    var catID = [String]()
    var subcat = [[Any]]()
    
    var SegueName = String()
    var SegueimageProfileURL = String()
    
    var selectCatIndex = Int()
    var selectCat = String()
    var selectCatId = String()
    var checkboxState = [Bool]()
    
    
    
    class func instantiateFromStoryboard() -> selectSubcatNoExpandViewController {
        let storyboard = UIStoryboard(name: "profile", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: String(describing: self)) as! selectSubcatNoExpandViewController
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.nextButton.setRoundedwithR(r: commonValue.viewRadius)
        DispatchQueue.main.async {
            self.callCat()
        }
        
        let stb = UIStoryboard(name: "Walkthrough", bundle: nil)
        let walkthrough = stb.instantiateViewController(withIdentifier: "walk") as! BWWalkthroughViewController
        let page_zero = stb.instantiateViewController(withIdentifier: "create0")
        let page_one = stb.instantiateViewController(withIdentifier: "create1")
        let page_two = stb.instantiateViewController(withIdentifier: "create2")
        let page_three = stb.instantiateViewController(withIdentifier: "create3")
        
        // Attach the pages to the master
        walkthrough.delegate = self
        walkthrough.add(viewController:page_zero)
        walkthrough.add(viewController:page_one)
        walkthrough.add(viewController:page_two)
        walkthrough.add(viewController:page_three)
        
        self.present(walkthrough, animated: true, completion: nil)
        
        setBackgroundColor()
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.navigationController?.navigationBar.isHidden = false
        
        if #available(iOS 11.0, *) {
            navigationController?.navigationBar.prefersLargeTitles = false
        } else {
            print("It's down of iOS 11")
        }
    }
    
    @IBAction func nextToSubct(_ sender: Any) {
        
        if(self.selectCat == "" || self.selectCatId == ""){
            SVProgressHUD.showInfo(withStatus: "Please select one of category.")
            
            DispatchQueue.main.asyncAfter(deadline: .now() + 2){
                SVProgressHUD.dismiss()
            }
        }
        else{
            let vc = selectSubCatViewController.instantiateFromStoryboard()
            
            vc.selectCat = self.selectCat
            vc.mode = self.mode
            vc.subCat = self.subcat[self.selectCatIndex] as! [String]
            vc.selectCatId = self.selectCatId
            vc.SegueName = self.SegueName
            vc.SegueimageProfileURL = self.SegueimageProfileURL
            
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    @IBAction func selectCat(_ sender: UIButton) {
  
        if( self.checkboxState[sender.tag] == false){
            
            self.checkboxState = self.checkboxState.map{_ in false}
            self.checkboxState[sender.tag] = true
            
            self.selectCatIndex = sender.tag
            self.selectCat = self.cat[sender.tag]
            self.selectCatId = self.catID[sender.tag]
            
        }
        else{
            self.checkboxState[sender.tag] = false
        }
    
        let filted = self.checkboxState.filter{$0 == true}
        if(filted.isEmpty){
            self.selectCat = ""
            self.selectCatId = ""
        }
    
        print(self.selectCat)
        print(self.selectCatId)
        
        DispatchQueue.main.async {
            self.tableView.reloadData()
        }
    }
    
    
    func callCat() {
        
        let ud = UserDefaults.standard
        var accesstoken = String()
        
        if(ud.object(forKey: "accessToken") != nil){
            accesstoken = ud.value(forKey: "accessToken") as! String
        }
        
        let header : HTTPHeaders = [
            "accessToken" : accesstoken
        ]
        
        self.cat.removeAll()
        self.subcat.removeAll()
        
        Alamofire.request(baseURL.url + "categories", method: .get, encoding: JSONEncoding.default , headers : header)
            .responseJSON(completionHandler: { response in
                
                if let result = response.result.value {
                    
                    let JSON = result as! NSDictionary
                    
                    let JsonResult = JSON.value(forKey: "response") as! Bool
                    if(JsonResult){
                        let data = JSON.value(forKey: "data") as! NSArray
                        
                        for dataInArr in data {
                            let dictData = dataInArr as! NSDictionary
                            let catName = dictData.value(forKey: "catName") as! String
                            let subcat = dictData.value(forKey: "subCat") as! Array<Any>
                            let catID = dictData.value(forKey: "_id") as! String
                            
                            self.subcat.append(subcat)
                            self.cat.append(catName)
                            self.catID.append(catID)
                        }
                        
                        for _ in self.cat {
                            self.checkboxState.append(false)
                        }
                    }
                    DispatchQueue.main.async {
                        SVProgressHUD.dismiss()
                        self.tableView.reloadData()
                    }
                }
                else{
                    SVProgressHUD.showError(withStatus: "Network Error")
                }
            })
    }
    
    func walkthroughPageDidChange(_ pageNumber: Int) {
        print("Current Page \(pageNumber)")
    }
    
    func walkthroughCloseButtonPressed() {
        self.dismiss(animated: true, completion: nil)
    }
    
}

extension selectSubcatNoExpandViewController : UITableViewDelegate , UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.cat.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cat", for: indexPath) as! subCatCell
        
        cell.titleLable.text = self.cat[indexPath.row]
        cell.Button.tag = indexPath.row

        if(self.checkboxState[indexPath.row] == true){
            cell.checkBox.setOn(true, animated: true)
        }else{
            cell.checkBox.setOn(false, animated: true)
        }
        
        return cell
        
    }
}
