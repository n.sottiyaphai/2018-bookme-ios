//
//  selectSubCatViewController.swift
//  bookme
//
//  Created by Naphat Sottiyaphai on 6/12/2561 BE.
//  Copyright © 2561 Naphat Sottiyaphai. All rights reserved.
//

import UIKit
import BEMCheckBox
import SVProgressHUD
import Alamofire

class selectSubCatViewController: UIViewController {
    
    class func instantiateFromStoryboard() -> selectSubCatViewController {
        let storyboard = UIStoryboard(name: "profile", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: String(describing: self)) as! selectSubCatViewController
    }
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var nextButton: UIButton!
    @IBOutlet weak var tableView: UITableView!
    
    var selectCat = String()
    var selectCatId = String()
    
    var subCat = [String]()
    var selectSubcat = [String]()
    var checkboxState = [Bool]()
    
    var mode = String()
    var SegueName = String()
    var SegueimageProfileURL = String()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setClearNavBar()
        self.setBackgroundColor()
        
        self.nextButton.setRoundedwithR(r: commonValue.viewRadius)
        for _ in self.subCat {
            self.checkboxState.append(false)
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.navigationController?.navigationBar.isHidden = false
        
        if #available(iOS 11.0, *) {
            navigationController?.navigationBar.prefersLargeTitles = false
        } else {
            print("It's down of iOS 11")
        }
    }
    
    @IBAction func nextAction(_ sender: Any) {
        
                if(self.selectSubcat.count == 0){
                    SVProgressHUD.showInfo(withStatus: "Please select one of skill.")
        
                    DispatchQueue.main.asyncAfter(deadline: .now() + 2){
                        SVProgressHUD.dismiss()
                    }
                }else{
        //            let vc = professRegisViewController.instantiateFromStoryboard()
        //
        //            vc.catName = self.selectCat
        //            vc.subCatName = self.selectSubcat
        //            vc.mode = self.mode
        //            vc.catID = self.selectCatId
        //            vc.SegueName = self.SegueName
        //            vc.SegueimageProfileURL = self.SegueimageProfileURL
        //
        //            self.navigationController?.pushViewController(vc, animated: true)
        //        }
        
                    
                    self.createProfess()
        
//                    let vc = CreateStepBioViewController.instantiateFromStoryboard()
//                    self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    @IBAction func selectCat(_ sender: UIButton) {
        print(sender.tag)
        if( self.checkboxState[sender.tag] == false){
            self.checkboxState[sender.tag] = true
            self.selectSubcat.append(self.subCat[sender.tag])
        }
        else{
            self.checkboxState[sender.tag] = false
            
            let index = self.selectSubcat.lastIndex(of: self.subCat[sender.tag])
            self.selectSubcat.remove(at: index!)
        }
        
        DispatchQueue.main.async {
            self.tableView.reloadData()
        }
    }
    
    func createProfess() {
        
        let ud = UserDefaults.standard
        let uid = ud.value(forKey: "user_uid") as! String
        var accesstoken = String()
        
        if(ud.object(forKey: "accessToken") != nil){
            accesstoken = ud.value(forKey: "accessToken") as! String
        }
        
        let parameters : [String : Any] = [
            "profileId" : uid,
            "portfolio" : [
                "gallery" : [],
                "portfolioBio" : "",
                "instagram" : ""
            ],
            "packages" : [],
            "categories" : [
                "catId" : self.selectCatId,                  //_id ของ cat
                "subCat" : self.selectSubcat,
                "catName" : self.selectCat
            ],
            "conditions" : [],
            "calendar" : []
        ]
        
        let header : HTTPHeaders = [
            "accessToken" : accesstoken
        ]
        
        Alamofire.request(baseURL.url + "professional", method: .post , parameters: parameters, encoding: JSONEncoding.default , headers : header)
            .responseJSON(completionHandler: { response in
                
                if let result = response.result.value{
                    print(result)
                    
                    if let data = (result as! NSDictionary).value(forKey: "data"){
                        if((data as! NSDictionary).object(forKey: "_id") != nil){
                            let professUpdateID = (data as! NSDictionary).value(forKey: "_id") as! String
                            ud.set(professUpdateID, forKey: "profess_id_in_process")
                            print("collect id complete")
                            
                            
                            let vc = CreateStepBioViewController.instantiateFromStoryboard()
                            self.navigationController?.pushViewController(vc, animated: true)
                        }
                        else{
                            let err = (result as! NSDictionary).value(forKey: "err") as! NSDictionary
                            
                            
                            
                            SVProgressHUD.showError(withStatus: err.value(forKey: "message") as? String)
                            SVProgressHUD.setBackgroundColor(.white)
                            SVProgressHUD.setForegroundColor(.black)
                            
                            DispatchQueue.main.asyncAfter(deadline: .now()+5){
                                SVProgressHUD.dismiss()
                                SVProgressHUD.setBackgroundColor(.clear)
                                SVProgressHUD.setForegroundColor(.white)
                                self.navigationController?.popToRootViewController(animated: true)
                            }
                        }
                    }
                }
                else{
                    SVProgressHUD.showError(withStatus: response.error?.localizedDescription)
                }
        })
    }
}

extension selectSubCatViewController : UITableViewDelegate , UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return self.subCat.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "subcat", for: indexPath) as! subCatCell
        
        cell.titleLable.text = self.subCat[indexPath.row]
        cell.Button.tag = indexPath.row
        
        if(self.checkboxState[indexPath.row] == true){
            cell.checkBox.setOn(true, animated: false)
        }else{
            cell.checkBox.setOn(false, animated: false)
        }
        
        return cell
    }
    
}

class subCatCell : UITableViewCell{
    @IBOutlet weak var titleLable : UILabel!
    @IBOutlet weak var Button : UIButton!
    @IBOutlet weak var checkBox : BEMCheckBox!
}
