//
//  professRegisViewController.swift
//  bookme4
//
//  Created by Naphat Sottiyaphai on 24/3/2561 BE.
//  Copyright © 2561 Naphat Sottiyaphai. All rights reserved.
//

import UIKit
import ACFloatingTextfield_Swift
import Alamofire
import SVProgressHUD
import Koyomi
import ActionSheetPicker_3_0
import OpalImagePicker
import Kingfisher
import SwiftHash

class professRegisViewController: UIViewController , EnterTextViewDelegate {
    
    
    
    var mode = String()
    var UpdateUID = String()
    
    var SegueName = String()
    var SegueimageProfileURL = String()
    
    var aboutHeight = 0
    var conditionHeight = 0
    var projectRefSection = 0
    
    var WherePickerDrop = "workImage"
    let headerText = ["","Your Bio","","Work Experiences","Package Rate","Conditions",""]
    
    //Instagram Declaration
    var igUsername = String()
    var igId = String()
    
    // About Declaration
    var aboutText = String()
    
    // tag for segues
    var catName = String()
    var subCatName = [String]()
    var catID = String()
    var catsName = String()
    
    // Project Referance Declaration
    var projectDateTime = [""]
    var projectRefName = [""]
    var projectRefDetail = [""]
    var projectRefImage = [[UIImage]]()
    var projectRefIndexRow = 0
    var urlprojectRef = [[String]]()
    
    // package Declaration
    var packageName = [""]
    var packageHours = [""]
    var packagePrices = [""]
    var packUnit = [0]
    var packageType = [0]
    
    // conditions Declaration
    //    var conditions = [""]
    
    // Pro Regisation data Declaration
    var RegProData = [String : Any]()
    
    // Work Declaration
    var workImage = [UIImage]()
    var preloadGallery = [Data]()
    var urlGallery = [String]()
    
    
    // Update Work image Declare
    var imageBFU = [UIImage]()
    var nameImageBFU = [String]()
    var urlImageBFU = [Any]()
    var new_workImageUpdate = [UIImage]()
    
    // Update Project image Declare
    var nameProjectRef = [[String]]()
    var urlProjectRefBFU = [[Any]]()
    var new_ProjectRefUpdate = [[UIImage]]()
    var updateProjectRefURL = [[String]]()
    
    // Image Upload Controller
    var imageCoverUploaded = false
    var imageRefUploaded = false
    var imageRefUploadCount = 0
    
    // New Declare for UpdateOnly
    var portfolios = NSDictionary()
    var projects = [NSDictionary]()
    var packages = [NSDictionary]()
    var conditions = [String]()
    
    // State Checker
    var uploadImageProject = false
    
    @IBOutlet var tableView: UITableView!
    @IBOutlet var profileImage: UIImageView!
    @IBOutlet weak var profileName: UILabel!
    //    @IBOutlet weak var tagList: TagListView!
    //    @IBOutlet weak var titleTextField: UITextField!
    @IBOutlet weak var headerTableView: UIView!
    
    class func instantiateFromStoryboard() -> professRegisViewController {
        let storyboard = UIStoryboard(name: "profile", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: String(describing: self)) as! professRegisViewController
    }
    
    func sendBackText(title: String, text: String , conditionIndex : Int) {
        print(title)
        print(text)
        print(conditionIndex)
        
        if(title == "Your Bio"){
            self.aboutText = text
            DispatchQueue.main.async {
                self.tableView.reloadData()
            }
        }
        else if ( title == "Conditions"){
            //            let indexRow = conditionIndex
            self.conditions[conditionIndex] = text
            DispatchQueue.main.async {
                self.tableView.reloadData()
            }
        }
    }
    
    // MARK: - Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if(mode == "Update"){          // Update Mode
            
            callProfess()
            
            //            tagList.isHidden = true
            
            saveProfile.setTitle("Update", for: .normal)
            saveProfile.backgroundColor = commonColor.mainYellow
            saveProfile.clipsToBounds = true
            
            headerTableView.clipsToBounds = true
            headerTableView.sizeToFit()
            headerTableView.updateConstraints()
            
        }else{                          // New Create Mode
            self.profileName.text = self.SegueName
            self.catsName = self.catName
            self.profileImage.sd_setImage(with: URL(string: self.SegueimageProfileURL), placeholderImage: UIImage(named: "bookme_loading"))
            
        }
        
        self.saveProfile.setRoundedwithR(r: commonValue.viewRadius)
        
        projectRefImage.append(Array<UIImage>())
        
        profileName.sizeToFit()
        profileImage.setRounded()
        let NameH = profileName.frame.height
        
        tableView.delegate = self
        let nib = UINib(nibName: "TableSectionHeader", bundle: nil)
        self.tableView.register(nib, forHeaderFooterViewReuseIdentifier: "TableSectionHeader")
        
        let nib2 = UINib(nibName: "ProfileTableSelectionHeader", bundle: nil)
        self.tableView.register(nib2, forHeaderFooterViewReuseIdentifier: "ProfileTableSelectionHeader")
        
        setBackgroundColor()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        self.navigationController?.navigationBar.isHidden = false
        navigationController?.interactivePopGestureRecognizer?.isEnabled = false
        
        
        if #available(iOS 11.0, *) {
            navigationController?.navigationBar.prefersLargeTitles = false
        } else {
            print("It's down of iOS 11")
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        sizeHeaderToFit()
    }
    
    func sizeHeaderToFit() {
        let headerView = tableView.tableHeaderView!
        
        headerView.setNeedsLayout()
        headerView.layoutIfNeeded()
        
        let height = headerView.systemLayoutSizeFitting(UIView.layoutFittingCompressedSize).height
        var frame = headerView.frame
        frame.size.height = height
        headerView.frame = frame
        
        tableView.tableHeaderView = headerView
    }
    
    //MARK: - Call Bookme API
    func callProfess() {
        
        let ud = UserDefaults.standard
        var accesstoken = String()
        
        if(ud.object(forKey: "accessToken") != nil){
            accesstoken = ud.value(forKey: "accessToken") as! String
        }
        
        let header : HTTPHeaders = [
            "accessToken" : accesstoken
        ]
        
        self.projects.removeAll()
        self.packages.removeAll()
        self.conditions.removeAll()
        
        Alamofire.request(baseURL.url + "professional/\(self.UpdateUID)", method: .get, encoding: JSONEncoding.default , headers : header)
            .responseJSON(completionHandler: { response in
                
                if(response.error != nil){
                    return
                }
                
                if let result = response.result.value {
                    let data = result as! NSDictionary
                    let JSON = data.value(forKey: "data") as! NSDictionary
                    
                    
                    let categories = JSON.value(forKey: "categories") as! NSDictionary
                    
                    //Conditions Field Complete
                    let condition = JSON.value(forKey: "conditions") as! Array<String>
                    self.conditions = condition
                    
                    
                    var portfolio = NSDictionary()
                    
                    if(JSON.value(forKey: "portfolio") is NSDictionary){
                        portfolio = JSON.value(forKey: "portfolio") as! NSDictionary
                    }
                    
                    var profile = NSDictionary()
                    if(JSON.value(forKey: "profile") is NSDictionary){
                        profile = JSON.value(forKey: "profile") as! NSDictionary
                    }
                    else{
                        let arrayProfile = JSON.value(forKey: "profile") as! NSArray
                        profile = arrayProfile[0] as! NSDictionary
                    }
                    
                    self.catID = categories.value(forKey: "catId") as! String
                    self.subCatName = categories.value(forKey: "subCat") as! Array<String>
                    self.catsName = categories.value(forKey: "catName") as! String
                    
                    let GalleryURL = portfolio.value(forKey: "gallery") as! Array<String>
                    
                    let aboutTexts = portfolio.value(forKey: "portfolioBio") as! String
                    let instagram = portfolio.value(forKey: "instagram") as! String
                    let firstName = profile.value(forKey: "firstName") as! String
                    let lastName = profile.value(forKey: "lastName") as! String
                    let imageURL = profile.value(forKey: "profilePhoto") as! String
                    
                    self.profileName.text = "\(firstName) \(lastName)"
                    self.profileImage.sd_setImage(with: URL(string: imageURL))
                    self.conditions = condition
                    self.aboutText = aboutTexts
                    self.igId = instagram
                    self.urlImageBFU = GalleryURL.map{$0}
                    self.nameImageBFU = GalleryURL.map{$0}
                    
                    // Complete Package
                    let packages = JSON.value(forKey: "packages") as! NSArray
                    for pack in packages{
                        
                        let dictPack = pack as! NSDictionary
                        var package = Dictionary<String,Any>()
                        
                        package["packageTime"] = dictPack.value(forKey: "packageTime") as! String
                        package["packageName"] = dictPack.value(forKey: "packageName") as! String
                        package["packagePrice"] = dictPack.value(forKey: "packagePrice") as! String
                        package["packageTimeUnit"] = dictPack.value(forKey: "packageTimeUnit") as! Int
                        package["packageType"] = dictPack.value(forKey: "packageType") as! Int
                        
                        self.packages.append(package as NSDictionary)
                    }
                    
                    // Complete Project
                    let projects = JSON.value(forKey: "projects") as! NSArray
                    for pro in projects{
                        let dictPro = pro as! NSDictionary
                        var project = Dictionary<String,Any>()
                        
                        project["projectDate"] = dictPro.value(forKey: "projectDate") as! String
                        project["projectName"] = dictPro.value(forKey: "projectName") as! String
                        project["projectDetail"] = dictPro.value(forKey: "projectDetail") as! String
                        
                        // ถ้ามีรูป project Image
                        if(dictPro.object(forKey: "projectImage") != nil){
                            // มีรูปโปรเจค
                            let projectImage = dictPro.value(forKey: "projectImage") as! Array<String>
                            if(projectImage.count > 0){
                                // มีรูปมากกว่า 0 รูป
                                let projectImage = dictPro.value(forKey: "projectImage") as! Array<String>
                                project["projectImage"] = projectImage
                            }
                            else{
                                // ไม่มีรูป
                                project["projectImage"] = []
                            }
                            
                        }
                        else{
                            // ไม่มี projectImage
                            project["projectImage"] = []
                        }
                        
                        self.projects.append(project as NSDictionary)
                    }
                    self.tableView.reloadData()
                }
                else{
                    SVProgressHUD.showError(withStatus: "Network Error")
                }
            })
    }
    
    
    @IBOutlet var saveProfile: UIButton!
    
    @IBAction func save(_ sender: Any) {
        //Process Showing
        SVProgressHUD.show()
        
        let about = tableView.dequeueReusableCell(withIdentifier: "about") as! aboutTextTableCell
        let instragamText = tableView.dequeueReusableCell(withIdentifier: "connectInstragram") as! connectInstragram
        
        if(aboutText != ""){
            
            var i = 0
            for conCheck in conditions{
                
                if(conCheck == ""){
                    SVProgressHUD.dismiss()
                    self.AlertShow(title: "Empty Field", message: "Please enter Condition or Delete empty one.")
                    return
                }
                i = i + 1
            }
            
            
            let aboutText  = about.aboutText.text
            let ig = instragamText.igNameText.text
            
            
            self.RegProData["about"] = aboutText
            self.RegProData["ig"] = ig
            self.RegProData["conditions"] = self.conditions
            
            //Work Uploading
            if(mode == "Update"){
                // MARK:-
                //                if(new_workImageUpdate.isEmpty){
                //                    self.imageCoverUploaded = true
                //                }
                //                if(!new_workImageUpdate.isEmpty){
                //                    self.imageRefUploaded = true
                //                    self.preloadGallery = new_workImageUpdate.map{ $0.jpegData(compressionQuality: commonValue.imageCompress)!}
                //                    self.uploadImage(data: self.preloadGallery)
                //                }
                //                else if(uploadImageProject){
                if(uploadImageProject){
                    
                    var imageForUpload = [[UIImage]]()
                    
                    for project in self.projects {
                        let proDoct = project
                        var image = [UIImage]()

                        if(proDoct.object(forKey: "projectImage") != nil){
                            let projectImage = proDoct.value(forKey: "projectImage")
                            if(projectImage is [UIImage]){
                                image = projectImage as! [UIImage]
                            }
                            else if (projectImage is [String]){
                                image = []
                            }
                        }
                        else{
                            image = []
                        }

                        imageForUpload.append(image)
                    }

                    self.workExpuploadImage(data: imageForUpload)
                }
                else{
                    SVProgressHUD.dismiss()
                    print("No work image update")
                    self.postReg(data: self.RegProData as NSDictionary)
                    
                }
            }
        }
        else{
            SVProgressHUD.dismiss()
            self.AlertShow(title: "Empty Field", message: "Please enter About text.")
        }
        
    }
    
    func AlertShow(title : String , message : String){
        let alarm = UIAlertController.init(title: title, message: message, preferredStyle: .alert)
        
        let ok = UIAlertAction.init(title: "Ok", style: .default, handler: {(UIAlertAction) in
            
        })
        
        alarm.addAction(ok)
        
        self.present(alarm, animated: true, completion: nil)
    }
    
    
    func ProjectRefImageUploading(data : [[UIImage]] ) {
        
        let ud = UserDefaults.standard
        let uid = ud.value(forKey: "user_uid") as! String
        let md5_uid = MD5(uid)
        let url = URL(string: "\(baseURL.media)api/uploads/bookme")
        
        var count = 0
        
        if(data.isEmpty){
            self.postReg(data: self.RegProData as NSDictionary)
            SVProgressHUD.dismiss()
        }
            
        else{
            for arrayImage in data{
                
                var proloadProjectRef = [Data]()
                
                for proload in arrayImage {
                    let image = proload.jpegData(compressionQuality: commonValue.imageCompress)
                    if(image != nil){
                        proloadProjectRef.append(image!)
                    }
                }
                
                if(proloadProjectRef.isEmpty){
                    print("it's Empty")
                    count += 1
                }
                else{
                    print("Not Empty")
                }
                
                Alamofire.upload(multipartFormData: {(multipartFormData) in
                    
                    multipartFormData.append("\(uid)".data(using: String.Encoding.utf8)!, withName: "uId" as String)
                    
                    for images in proloadProjectRef {
                        let data = images
                        multipartFormData.append(data, withName: "imgFiles", fileName: "\(md5_uid)_swift_file.jpeg", mimeType: "image/jpeg")
                    }
                    
                    print(multipartFormData)
                    
                }, usingThreshold: UInt64.init() , to: url!, method: .post , encodingCompletion: { (result) in
                    
                    switch result {
                    case .success(let upload, _, _):
                        upload.uploadProgress(closure: { (progress) in
                            SVProgressHUD.showProgress(Float(progress.fractionCompleted), status: "Uploading")
                        })
                        
                        upload.responseJSON { response in
                            
                            if let result = response.result.value {
                                
                                let filted = data.filter{!$0.isEmpty}
                                let dataCount = filted.count
                                
                                
                                let results = result as! NSDictionary
                                let ArryResult = results.value(forKey: "data") as! Array<String>
                                
                                if(self.mode == "Update"){
                                    for imageName in ArryResult{
                                        print(count)
                                        print(self.nameProjectRef)
                                        self.nameProjectRef[count].append(imageName)
                                    }
                                }
                                else{
                                    self.urlprojectRef.append(ArryResult)
                                }
                                
                                count += 1
                                
                                if( count >= dataCount ){
                                    self.postReg(data: self.RegProData as NSDictionary)
                                    SVProgressHUD.dismiss()
                                }
                            }
                            else{
                                print("Error")
                                print(response.error?.localizedDescription ?? "")
                                SVProgressHUD.dismiss()
                                SVProgressHUD.showError(withStatus: response.error?.localizedDescription ?? "")
                            }
                        }
                        
                    case .failure(let encodingError):
                        print("ERROR : \(encodingError)")
                        break
                    }
                })
            }
        }
    }
    
    @IBAction func deleteImageCover(_ sender: UIButton) {
        
    }
    
    // MARK: - Image Uploading
    func uploadImage (data : Array<Data>) {
        
        let preloadGallery = data
        var data = [Data]()
        
        for imgData in preloadGallery{
            data.append(imgData)
        }
        
        let ud = UserDefaults.standard
        let uid = ud.value(forKey: "user_uid") as! String
        let accessToken = ud.value(forKey: "bluweoAccessToken") as! String
        
        let url = URL(string: "\(baseURL.bluweoMedia)upload/multiple")
        
        let headers = [
            "access_token" : accessToken,
            "accept": "application/x-www-form-urlencoded",
            "Content-Type" : "multipart/form-data"
        ]
        
        let parameters = [
            "api": "professional",
            "path" : "cover"
        ]
        
        print(url!)
        print(headers)
        print(parameters)
        
        Alamofire.upload(multipartFormData: { (multipartFormData) in
            
            var index = 0
            
            for img in data {
                multipartFormData.append(img, withName: "files[\(index)]", fileName: "\(uid)_\(Date().timeIntervalSince1970)_Image.jpg", mimeType: "image/jpeg")
                index+=1
            }
            
            for (key, value) in parameters {
                multipartFormData.append(value.data(using: String.Encoding.utf8)!, withName: key)
            }
            
            print(multipartFormData)
            
        }, to: url!, method: .post, headers: headers) { (result) in
            
            switch result {
            case .success(let upload, _, _):
                
                upload.uploadProgress(closure: { (progress) in
                    SVProgressHUD.showProgress(Float(progress.fractionCompleted), status: "Image Uploading")
                })
                
                upload.responseJSON { response in
                    
                    if let result = response.result.value {
                        
                        let results = result as! NSDictionary
                        let ArryResult = results.value(forKey: "data") as! NSDictionary
                        
                        if(ArryResult.object(forKey: "files") != nil){
                            let filePath = ArryResult.value(forKey: "files") as! NSArray
                            var imageGalleryURL = [String]()
                            
                            for path in filePath{
                                let imgPath = (path as! NSDictionary).value(forKey: "file")
                                
                                let fullURL = "\(baseURL.bluweoMedia)\(imgPath as! String)"
                                imageGalleryURL.append(fullURL)
                            }
                            
                            if(self.mode == "Update"){
                                for nameImage in imageGalleryURL{
                                    self.nameImageBFU.append(nameImage)
                                }
                                print(self.nameImageBFU)
                                self.RegProData["workImage"] = self.nameImageBFU
                            }
                            else{
                                self.RegProData["workImage"] = imageGalleryURL
                            }
                            
                            self.imageCoverUploaded = true
                            
                            DispatchQueue.main.async {
                                SVProgressHUD.dismiss()
                                
                                if(self.imageRefUploaded){
                                    self.postReg(data: self.RegProData as NSDictionary)
                                }
                                else{
                                    print("continueUploading")
                                    let result = self.workExpuploadImage(data: self.new_ProjectRefUpdate)
                                    if(result){
                                        self.postReg(data: self.RegProData as NSDictionary)
                                    }
                                }
                            }
                            
                        }
                        else{
                            if let message = ArryResult.value(forKey: "message"){
                                if(message is NSDictionary){
                                    if let errMsg = (message as! NSDictionary).value(forKey: "message"){
                                        print(errMsg)
                                    }
                                }
                                else{
                                    let message = ArryResult.value(forKey: "message") as! String
                                    print(message)
                                }
                            }
                            
                            self.refreshBluweoToken()
                            
                            SVProgressHUD.show()
                            DispatchQueue.main.asyncAfter(deadline: .now() + 3){
                                SVProgressHUD.dismiss()
                            }
                            
                        }
                    }
                }
                
            case .failure(let encodingError):
                print("ERROR : \(encodingError)")
                SVProgressHUD.showError(withStatus: "Media Connection Error : \(encodingError)")
                break
            }
        }
    }
    
    
    func workExpuploadImage (data : [[UIImage]]) -> Bool{
        
        let lastCount = data.count
        let preloadGallery = data

        for imgArr in preloadGallery{
            
            var img = [Data]()

            if(imgArr.count <= 0){
                // เมื่อไม่มีรูปที่ต้อง Upload
                print("noting for uploading")
                SVProgressHUD.dismiss()
            }
            else {
                // เมื่อมีรูปที่ยังไม่ได้ upload
                print("Preparing for uploading")
                for imgs in imgArr {
                    let imageData = imgs.jpegData(compressionQuality: commonValue.imageCompress)!
                    img.append(imageData)
                }
                
                let ud = UserDefaults.standard
                let uid = ud.value(forKey: "user_uid") as! String
                let accessToken = ud.value(forKey: "bluweoAccessToken") as! String
                
                let url = URL(string: "\(baseURL.bluweoMedia)upload/multiple")
                
                let headers = [
                    "access_token" : accessToken,
                    "accept": "application/x-www-form-urlencoded",
                    "Content-Type" : "multipart/form-data"
                ]
                
                let parameters = [
                    "api": "professional",
                    "path" : "workExp"
                ]
                
                Alamofire.upload(multipartFormData: { (multipartFormData) in
                    
                    var index = 0
                    
                    print(img.count)
                    
                    for img in img {
                        multipartFormData.append(img, withName: "files[\(index)]", fileName: "\(uid)_\(Date().timeIntervalSince1970)_Image.jpg", mimeType: "image/jpeg")
                        index+=1
                    }
                    
                    for (key, value) in parameters {
                        multipartFormData.append(value.data(using: String.Encoding.utf8)!, withName: key)
                    }
                    
                    
                }, to: url!, method: .post, headers: headers) { (result) in
                    
                    switch result {
                    case .success(let upload, _, _):
                        
                        upload.uploadProgress(closure: { (progress) in
                            SVProgressHUD.showProgress(Float(progress.fractionCompleted), status: "Image Uploading")
                        })
                        
                        upload.responseJSON { response in
                            
                            if (response.error != nil){
                                print("ERROR : \(response.error?.localizedDescription)")
                                SVProgressHUD.showError(withStatus: "Media Connection Error : \(response.error?.localizedDescription)")
                                return
                            }
                            
                            if let result = response.result.value {
                                
                                let results = result as! NSDictionary
                                let ArryResult = results.value(forKey: "data") as! NSDictionary
                                
                                if(ArryResult.object(forKey: "files") != nil){
                                    let filePath = ArryResult.value(forKey: "files") as! NSArray
                                    var imageGalleryURL = [String]()
                                    
                                    for path in filePath{
                                        let imgPath = (path as! NSDictionary).value(forKey: "file")
                                        
                                        let fullURL = "\(baseURL.bluweoMedia)\(imgPath as! String)"
                                        imageGalleryURL.append(fullURL)
                                    }
                                    
                                    print(imageGalleryURL)
                                    
                                    DispatchQueue.main.async {
                                        SVProgressHUD.dismiss()
                                        
                                        let indexs = preloadGallery.firstIndex(of: imgArr)!
                                        print(indexs)
                                        
                                        let dictData = self.projects[indexs]
                                        var project = dictData as! Dictionary<String, Any>
                                        
                                        project["projectName"] = dictData.value(forKey: "projectName") as! String
                                        project["projectDate"] = dictData.value(forKey: "projectDate") as! String
                                        project["projectDetail"] = dictData.value(forKey: "projectDetail") as! String
                                        project["projectImage"] = imageGalleryURL
                                        
                                        self.projects[indexs] = project as NSDictionary
                                        
                                        if(lastCount == (indexs+1)){
                                            self.postReg(data: self.RegProData as NSDictionary)
                                        }
                                        else{
                                            print("continueUploading")
                                        }
                                    }
                                    
                                }
                                else{
                                    if let message = ArryResult.value(forKey: "message"){
                                        if(message is NSDictionary){
                                            if let errMsg = (message as! NSDictionary).value(forKey: "message"){
                                                print(errMsg)
                                            }
                                        }
                                        else{
                                            let message = ArryResult.value(forKey: "message") as! String
                                            print(message)
                                        }
                                    }
                                    
                                    self.refreshBluweoToken()
                                    
                                    SVProgressHUD.show()
                                    DispatchQueue.main.asyncAfter(deadline: .now() + 3){
                                        SVProgressHUD.dismiss()
                                    }
                                    
                                }
                                
                                let indexCount : Int = preloadGallery.firstIndex(of: imgArr)!
                                if(lastCount == indexCount){
                                    print("lastComplete")
                                }
                            }
                        }
                        
                    case .failure(let encodingError):
                        print("ERROR : \(encodingError)")
                        SVProgressHUD.showError(withStatus: "Media Connection Error : \(encodingError)")
                        break
                    }
                }
            }
        }
        
        return true
    }
    
    
    // MARK: - Old Image Uploading
    func workImageUploading(data : Array<Data>) {
        
        let ud = UserDefaults.standard
        let uid = ud.value(forKey: "user_uid") as! String
        let md5_uid = MD5(uid)
        let url = URL(string: "\(baseURL.media)api/uploads/bookme")
        
        let headers: HTTPHeaders = [
            "Content-Type": "application/json",
            "appId": apiKey.appId,
            "clientId" : apiKey.clientId,
            "secretId" : apiKey.secretId
        ]
        
        
        Alamofire.upload(multipartFormData: { (multipartFormData) in
            
            multipartFormData.append("\(uid)".data(using: String.Encoding.utf8)!, withName: "uId" as String)
            
            for images in data {
                let data = images
                multipartFormData.append(data, withName: "imgFiles", fileName: "\(md5_uid)_swift_file.jpeg", mimeType: "image/jpeg")
            }
            
            print(multipartFormData)
            
        }, usingThreshold: UInt64.init() , to: url!, method: .post , headers: headers  , encodingCompletion: { (result) in
            print(result)
            
            switch result {
            case .success(let upload, _, _):
                
                upload.uploadProgress(closure: { (progress) in
                    SVProgressHUD.showProgress(Float(progress.fractionCompleted), status: "Uploading")
                })
                
                upload.responseJSON { response in
                    print(response)
                    if let result = response.result.value {
                        let results = result as! NSDictionary
                        if(results.value(forKey: "data") is Array<String>){
                            let ArryResult = results.value(forKey: "data") as! Array<String>
                            
                            if(self.mode == "Update"){
                                for nameImage in ArryResult{
                                    self.nameImageBFU.append(nameImage)
                                }
                                print(self.nameImageBFU)
                                self.RegProData["workImage"] = self.nameImageBFU
                            }
                            else{
                                self.RegProData["workImage"] = ArryResult
                            }
                            
                            if(self.mode == "Update"){
                                self.ProjectRefImageUploading(data: self.new_ProjectRefUpdate)
                            }
                            else {
                                self.ProjectRefImageUploading(data: self.projectRefImage)
                            }
                            
                        }
                        SVProgressHUD.dismiss()
                        
                    }
                    else{
                        print("Error")
                        print(response.error?.localizedDescription ?? "")
                        SVProgressHUD.dismiss()
                        SVProgressHUD.showError(withStatus: response.error?.localizedDescription ?? "")
                    }
                }
                
            case .failure(let encodingError):
                print("ERROR : \(encodingError)")
                SVProgressHUD.showError(withStatus: "Media Connection Error : \(encodingError)")
                break
            }
        })
    }
    
    
    func postReg( data : NSDictionary)  {
        
        // Data intial before uploading
        
        let ud = UserDefaults.standard
        var accesstoken = String()
        
        if(ud.object(forKey: "accessToken") != nil){
            accesstoken = ud.value(forKey: "accessToken") as! String
        }
        
        let header : HTTPHeaders = [
            "accessToken" : accesstoken
        ]
        
        let uid = ud.value(forKey: "user_uid") as! String
        _ = MD5(uid)
        
        var imageName = [String]()
        
        if(mode == "Update"){
            imageName = self.nameImageBFU
        }
    
        
        
        let about = self.aboutText
        let ig = self.igId
        
        var projectRefImageFile = [[String]]()
        if(mode == "Update"){
            projectRefImageFile = self.nameProjectRef
        }
        
        var workURL = [String]()
        
        for fileName in imageName{
            workURL.append(fileName)
        }
        
//        var proRef = 0
//        for i in projectRefName{
//
//            print(self.updateProjectRefURL)
//
//            let iIndex = projectRefName.firstIndex(of: i)
//            var dict = Dictionary<String,Any>()
//
//            dict["dateTime"] = self.projectDateTime[proRef]
//            dict["projectName"] = self.projectRefName[proRef]
//            dict["projectDetail"] = self.projectRefDetail[proRef]
//
//            if(self.mode == "Update"){
//                let imageSet = self.updateProjectRefURL[iIndex!]
//                dict["projectImage"] = imageSet
//            }
//            //            if(projectRefImageFile.count > 0){
//            //                let imageSet = self.updateProjectRefURL[iIndex!]
//            //                dict["projectImage"] = imageSet
//            //                dict["projectImage"] = projectRefImageFile[proRef].filter{$0 != ""}
//            //            }
//            project.append(dict)
//            proRef = proRef + 1
//        }
//
//        project = project.compactMap{$0}
//        print(project)
//
        
        
        var conditionRow = 0
        for _ in conditions{
            
            let con = conditions[conditionRow] as String
            
            if(con.count == 0){
                conditions.remove(at: conditionRow)
            }
            conditionRow = conditionRow + 1
        }
        
        
        
        // Params declaration
        
        let parameters: [String: Any] = [
            "activeStatus" : true,
            "portfolio" : [
                "gallery" : workURL,
                "portfolioBio" : about,
                "instagram" : ig,
                "calendar" : [],
                "conditions" : self.conditions
            ],
            "projects" : self.projects ,
            "packages" : self.packages,
            "categories" : [
                "catId" : self.catID,                  //_id ของ cat
                "subCat" : self.subCatName,
                "catName" : self.catsName
            ],
            "conditions" : self.conditions,
            "profileId" : uid
        ]
        
        
        print(parameters)
        var path = String()
        
        var methods = HTTPMethod.post
        
        if(mode == "Update"){
            print("mode \(mode)")
            methods = HTTPMethod.put
            path = "professional/\(self.UpdateUID)"
        }else{
            print("mode New Regis")
            methods = HTTPMethod.post
            path = "professional"
        }
        
        Alamofire.request(baseURL.url + path, method: methods , parameters: parameters, encoding: JSONEncoding.default , headers : header)
            .responseJSON(completionHandler: { response in
                
                if let result = response.result.value {
                    let JSON = result as! NSDictionary
                    print(JSON)
                    
                    let response = JSON.value(forKey: "response") as! Bool
                    if (JSON.object(forKey: "err") != nil) {
                        
                        if(response){
                            SVProgressHUD.showSuccess(withStatus: "Create Done")
                            DispatchQueue.main.asyncAfter(deadline: .now() + 1.0){
                                SVProgressHUD.dismiss()
                                
                                let vc = ThankyouViewController.instantiateFromStoryboard()
                                self.navigationController?.pushViewController(vc, animated: true)
                                
                            }
                        }
                    }
                        
                    else{
                        
                        let response = JSON.value(forKey: "err") as! NSDictionary
                        let errMessage = response.value(forKey: "message") as! String
                        SVProgressHUD.showError(withStatus: errMessage)
                        DispatchQueue.main.asyncAfter(deadline: .now() + 2.0){
                            SVProgressHUD.dismiss()
                            
                        }
                    }
                }
            })
    }
    
    @IBAction func connectIG(_ sender: UIButton) {
    }
    
    @IBAction func deletePRFtableview(_ sender: UIButton) {
        
        print(sender.tag)
        
        let alert = UIAlertController(title: "Are you Sure ?", message: "Do you want to delete this Work Experiences ?", preferredStyle: .alert)
        let ok = UIAlertAction(title: "Sure", style: .default) { (action) in
            
            SVProgressHUD.show()
            DispatchQueue.main.asyncAfter(deadline: .now() + 1){
                SVProgressHUD.dismiss()
            }
            
            DispatchQueue.main.async {
                self.tableView.reloadData()
            }
            
        }
        let cancel = UIAlertAction(title: "Cancel", style: .cancel) { (action) in
            
        }
        alert.addAction(ok)
        alert.addAction(cancel)
        
        self.present(alert, animated: true, completion: nil)
    }
    
    func connectingIG(){
    }
    
    func deleteProfess() {
        let ud = UserDefaults.standard
        var accesstoken = String()
        
        if(ud.object(forKey: "accessToken") != nil){
            accesstoken = ud.value(forKey: "accessToken") as! String
        }
        
        let header : HTTPHeaders = [
            "accessToken" : accesstoken
        ]
        
        Alamofire.request(baseURL.url + "professional/\(self.UpdateUID)", method: .delete, encoding: JSONEncoding.default , headers : header)
            .responseJSON(completionHandler: { response in
                if let result = response.result.value{
                    print(result)
                    self.navigationController?.popViewController(animated: true)
                }
                else{
                    SVProgressHUD.showError(withStatus: "Something Wrong")
                    DispatchQueue.main.asyncAfter(deadline: .now() + 2){
                        SVProgressHUD.dismiss()
                    }
                }
            })
        
    }
    
    @IBAction func deleteImage(_ sender: UIButton) {
        
        print(sender.tag)
        if(sender.tag >= 0 && sender.tag <= 99){
            if(mode == "Update"){
                self.nameImageBFU.remove(at: sender.tag)
                self.urlImageBFU.remove(at: sender.tag)
                self.tableView.reloadData()
            }
            else{
                print("Create Not USE")
            }
        }
        
        
        
    }
    
}

// MARK: - TagListView Delegate
//extension professRegisViewController : TagListViewDelegate{
//
//}

// MARK: - UITextField Delegate
extension professRegisViewController : UITextFieldDelegate {
    
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        let newString = NSString(string: textField.text!).replacingCharacters(in: range, with: string)
        
        if(textField.tag >= 3200 && textField.tag <= 3299){
            let indexPart = textField.tag - 3200
            
            let dictData = self.projects[indexPart]
            var project = dictData as! Dictionary<String, Any>
            
            project["projectName"] = newString
            project["projectDate"] = dictData.value(forKey: "projectDate") as! String
            project["projectDetail"] = dictData.value(forKey: "projectDetail") as! String
            project["projectImage"] = dictData.value(forKey: "projectImage") as! NSArray
            
            self.projects[indexPart] = project as NSDictionary
            
        }
        else if(textField.tag >= 3300 && textField.tag <= 3399){
            
            let indexPart = textField.tag - 3300
            
            let dictData = self.projects[indexPart]
            var project = dictData as! Dictionary<String, Any>
            
            project["projectName"] = dictData.value(forKey: "projectName") as! String
            project["projectDate"] = dictData.value(forKey: "projectDate") as! String
            project["projectDetail"] = newString
            project["projectImage"] = dictData.value(forKey: "projectImage") as! NSArray
            
            self.projects[indexPart] = project as NSDictionary
        }
            
            // Tag CASE 4
        else if(textField.tag >= 4100 && textField.tag <= 4199){
            let indexPart = textField.tag - 4100
            
            let dictPack = self.packages[indexPart]
            var package = Dictionary<String,Any>()
            
            package["packageTime"] = newString
            package["packageName"] = dictPack.value(forKey: "packageName") as! String
            package["packagePrice"] = dictPack.value(forKey: "packagePrice") as! String
            package["packageTimeUnit"] = dictPack.value(forKey: "packageTimeUnit") as! Int
            package["packageType"] = dictPack.value(forKey: "packageType") as! Int
            
            self.packages[indexPart] = package as NSDictionary
        }
        else if(textField.tag >= 4200 && textField.tag <= 4299){
            
            let indexPart = textField.tag - 4200
            
            let dictPack = self.packages[indexPart]
            var package = Dictionary<String,Any>()
            
            package["packageTime"] = dictPack.value(forKey: "packageTime") as! String
            package["packageName"] = newString
            package["packagePrice"] = dictPack.value(forKey: "packagePrice") as! String
            package["packageTimeUnit"] = dictPack.value(forKey: "packageTimeUnit") as! Int
            package["packageType"] = dictPack.value(forKey: "packageType") as! Int
            
            self.packages[indexPart] = package as NSDictionary
        }
            
        else if(textField.tag >= 4300 && textField.tag <= 4399){
            let indexPart = textField.tag - 4300
            let dictPack = self.packages[indexPart]
            var package = Dictionary<String,Any>()
            
            package["packageTime"] = dictPack.value(forKey: "packageTime") as! String
            package["packageName"] = dictPack.value(forKey: "packageName") as! String
            package["packagePrice"] = newString
            package["packageTimeUnit"] = dictPack.value(forKey: "packageTimeUnit") as! Int
            package["packageType"] = dictPack.value(forKey: "packageType") as! Int
            
            self.packages[indexPart] = package as NSDictionary
        }
            
        else if(textField.tag >= 4400 && textField.tag <= 4499){
            let _ = textField.tag - 4400
            //            packagePrices[indexPart] = newString
        }
        
        return true
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        if(textField.tag >= 3100 && textField.tag <= 3199){
            let indexPart = textField.tag - 3100
            
            self.view.endEditing(true)
            
            let datePicker = ActionSheetDatePicker(title: "Project Date", datePickerMode: UIDatePicker.Mode.date, selectedDate: Date(), doneBlock: {
                picker, value, index in
                
                let dateValue = value as! Date
                let isoDateFormated = dateValue.toISO()
                let showDate = dateValue.toFormat("MMM yyyy")
                
                let dictData = self.projects[indexPart]
                var project = dictData as! Dictionary<String, Any>
                
                project["projectName"] = dictData.value(forKey: "projectName") as! String
                project["projectDate"] = isoDateFormated
                project["projectDetail"] = dictData.value(forKey: "projectDetail") as! String
                project["projectImage"] = dictData.value(forKey: "projectImage") as! NSArray
                
                self.projects[indexPart] = project as NSDictionary
                
                textField.text = showDate
                textField.resignFirstResponder()
                
                return
            }, cancel: { ActionStringCancelBlock in return }, origin: (textField as AnyObject).superview!?.superview)
            
            datePicker?.maximumDate = Date()
            datePicker?.show()
        }
        else if(textField.tag >= 4400 && textField.tag <= 4499){
            let indexPart = textField.tag - 4400
            
            textField.resignFirstResponder()
            self.view.endEditing(true)
            
            let unitPicker = ActionSheetStringPicker.init(title: "Select Time Unit", rows: ["Days" , "Hours" , "Project"], initialSelection: 1, doneBlock: {
                (picker, index, value) in
                
                if let type = value{
                    textField.text = "\(type)"
                }
                
                let dictPack = self.packages[indexPart]
                var package = Dictionary<String,Any>()
                
                package["packageTime"] = dictPack.value(forKey: "packageTime") as! Int
                package["packageName"] = dictPack.value(forKey: "packageName") as! String
                package["packagePrice"] = dictPack.value(forKey: "packagePrice") as! String
                package["packageTimeUnit"] = index + 1
                package["packageType"] = dictPack.value(forKey: "packageType") as! Int
                
                self.packages.append(package as NSDictionary)
                
            }, cancel: { (picker) in
                print("cancel")
            }, origin: (textField as AnyObject).superview!?.superview)
            
            unitPicker?.show()
        }
        else if (textField.tag ==  200){
            self.connectingIG()
            textField.resignFirstResponder()
        }
    }
}

// MARK: - TextView Delegate
extension professRegisViewController : UITextViewDelegate {
    
    func textViewDidChange(_ textView: UITextView) {
        
        if(textView.tag == 100){
            
            self.aboutText = textView.text
        }
        else if(textView.tag >= 500 && textView.tag <= 599){
            
            let indexPart = textView.tag - 500
            
            conditions[indexPart] = textView.text
            
        }
    }
    
    
    func textViewShouldBeginEditing(_ textView: UITextView) -> Bool {
        print("Should begin First")
        
        if(textView.tag == 100){
            let vc = EnterTextViewController.instantiateFromStoryboard()
            vc.titleText = "Your Bio"
            vc.oldText = self.aboutText
            vc.delegate = self
            textView.resignFirstResponder()
            self.present(vc, animated: true, completion: nil)
        }
            
        else if(textView.tag >= 500 && textView.tag <= 599){
            let vc = EnterTextViewController.instantiateFromStoryboard()
            vc.titleText = "Conditions"
            vc.oldText = self.conditions[textView.tag - 500]
            vc.conditionIndexs = textView.tag - 500
            vc.delegate = self
            textView.resignFirstResponder()
            self.present(vc, animated: true, completion: nil)
        }
        
        return true
    }
}


// MARK: - Table view data source
extension professRegisViewController :  UITableViewDelegate , UITableViewDataSource{
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 7
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        switch section {
        case 3 :
            return projects.count + 1
        case 4:
            return packages.count + 1
        case 5:
            return conditions.count + 1
        default:
            return 1
        }
        
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch indexPath.section {
            
        case 0:
            return 150
        case 1:
            return UITableView.automaticDimension
        //            return CGFloat(aboutHeight)
        case 2:
            return 0
        case 3:
            if(indexPath.row < projects.count){
                return 325
            }
            else{
                return 60
            }
        case 4:
            if(indexPath.row < packages.count){
                return 151
            }
            else{
                return 60
            }
            
        case 5:
            if(indexPath.row < conditions.count){
                return UITableView.automaticDimension
            }
            else{
                return 60
            }
        case 6:
            return 40
        default:
            return 65
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        switch indexPath.section {
        case 0:
            let cell = tableView.dequeueReusableCell(withIdentifier: "image", for: indexPath) as! imageCollectionTableviewCell
            
            cell.collectionView.delegate = self
            cell.collectionView.tag = 2001
            
            return cell
            
        case 1 :
            let cell = tableView.dequeueReusableCell(withIdentifier: "about", for: indexPath) as! aboutTextTableCell
            
            cell.aboutText.delegate = self
            cell.aboutText.tag = 100
            cell.aboutText.text = self.aboutText
            cell.aboutText.setRoundWithCommonRwithBGC_f8()
            
            cell.bgView.setRoundWithCommonR()
            
            cell.aboutText.textContainerInset = UIEdgeInsets(top: 15, left: 4, bottom: 15, right: 4)
            return cell
            
        case 2 :
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "connectInstragram", for: indexPath) as! connectInstragram
            
            cell.igNameText.delegate = self
            cell.igNameText.tag = 200
            cell.igNameText.text = self.igUsername
            cell.bgView.setRoundWithCommonR()
            cell.igNameText.setRoundWithCommonRwithBGC_f8()
            
            if(!self.igUsername.isEmpty){
                cell.connectButton.setTitle("X", for: .normal)
                cell.connectButton.setTitleColor(UIColor.red , for: .normal)
                
            }
            else{
                cell.connectButton.setTitle("Connect", for: .normal)
                cell.connectButton.setTitleColor(commonColor.mainBlueColor , for: .normal)
            }
            
            return cell
            
        case 3 :
            
            if(indexPath.row < projects.count){
                let cell = tableView.dequeueReusableCell(withIdentifier: "projectRefAdd", for: indexPath) as! projectRefAdd
                
                let projectDetail = self.projects[indexPath.row] as! NSDictionary
                
                
                cell.imageCollection.delegate = self
                cell.imageCollection.tag = 300 + indexPath.row
                
                cell.dateTime.delegate = self
                cell.projectName.delegate = self
                cell.projectDetail.delegate = self
                
                cell.dateTime.setLeftPaddingPoints(15)
                cell.projectName.setLeftPaddingPoints(15)
                cell.projectDetail.setLeftPaddingPoints(15)
                
                cell.dateTime.setRoundWithCommonRwithBGC_f8()
                cell.projectName.setRoundWithCommonRwithBGC_f8()
                cell.projectDetail.setRoundWithCommonRwithBGC_f8()
                
                cell.dateTime.tag = 3100 + indexPath.row
                cell.projectName.tag = 3200 + indexPath.row
                cell.projectDetail.tag = 3300 + indexPath.row
                
                let dateISO =  (projectDetail.value(forKey: "projectDate") as! String)
                let toDate = dateISO.toDate()
                cell.dateTime.text = toDate?.toFormat("MMM yyyy")
                
                cell.projectName.text = (projectDetail.value(forKey: "projectName") as! String)
                cell.projectDetail.text = (projectDetail.value(forKey: "projectDetail") as! String)
                
                cell.deleteButton.tag = indexPath.row
                cell.bgView.setRoundWithCommonR()
                
                return cell
            }
            else{
                let cell = tableView.dequeueReusableCell(withIdentifier: "addObject", for: indexPath) as! addObjectCell
                
                cell.titleTet.text = "Add more Experience"
                cell.bgView.setRoundWithCommonR()
                
                return cell
            }
            
            
        case 4 :
            if(indexPath.row < self.packages.count){
                let cell = tableView.dequeueReusableCell(withIdentifier: "packageCell", for: indexPath) as! packageAddCell
                
                let pack = self.packages[indexPath.row]
                
                cell.packHours.tag = 4100 + indexPath.row
                cell.packName.tag = 4200 + indexPath.row
                cell.packPrice.tag = 4300 + indexPath.row
                cell.packageUnit.tag = 4400 + indexPath.row
                
                cell.bgView.setRoundWithCommonR()
                
                cell.packHours.setRoundWithCommonRwithBGC_f8()
                cell.packName.setRoundWithCommonRwithBGC_f8()
                cell.packPrice.setRoundWithCommonRwithBGC_f8()
                cell.packageUnit.setRoundWithCommonRwithBGC_f8()
                
                cell.packHours.setLeftPaddingPoints(15)
                cell.packName.setLeftPaddingPoints(15)
                cell.packPrice.setLeftPaddingPoints(15)
                cell.packageUnit.setLeftPaddingPoints(15)
                
                cell.packHours.delegate = self
                cell.packName.delegate = self
                cell.packPrice.delegate = self
                cell.packageUnit.delegate = self
                
                cell.packHours.text = (pack.value(forKey: "packageTime") as! String)
                cell.packName.text = (pack.value(forKey: "packageName") as! String)
                cell.packPrice.text = (pack.value(forKey: "packagePrice") as! String)
                let packUnit = (pack.value(forKey: "packageTimeUnit") as! Int)
                
                if(mode=="Update"){
                    print(packUnit)
                    switch packUnit{
                    case 1 :
                        cell.packageUnit.text = "Days"
                        break
                    case 2 :
                        cell.packageUnit.text = "Hours"
                        break
                    case 3 :
                        cell.packageUnit.text = "Project"
                        break
                    default :
                        cell.packageUnit.text = "Days"
                        break
                    }
                }
                
                return cell
            }
            else{
                let cell = tableView.dequeueReusableCell(withIdentifier: "addObject", for: indexPath) as! addObjectCell
                
                cell.titleTet.text = "Add a package"
                cell.bgView.setRoundWithCommonR()
                
                return cell
            }
            
            
        case 5 :
            if (indexPath.row < self.conditions.count){
                let cell = tableView.dequeueReusableCell(withIdentifier: "ConditionCell", for: indexPath) as! ConditionCell
                
                cell.point.setRounded()
                cell.condition.tag = 500 + indexPath.row
                cell.condition.delegate = self
                cell.condition.setRoundWithCommonRwithBGC_f8()
                cell.condition.textContainerInset = UIEdgeInsets(top: 4, left: 4, bottom: 10, right: 4)
                
                cell.condition.sizeToFit()
                conditionHeight = Int(cell.condition.bounds.height + 15)
                
                cell.condition.text = conditions[indexPath.row]
                cell.bgView.setRoundWithCommonR()
                
                return cell
            }
            else{
                let cell = tableView.dequeueReusableCell(withIdentifier: "addObject", for: indexPath) as! addObjectCell
                
                cell.titleTet.text = "Add a Condition"
                cell.bgView.setRoundWithCommonR()
                
                return cell
            }
            
        case 6:
            let cell = tableView.dequeueReusableCell(withIdentifier: "DeleteTalent", for: indexPath) as! DeleteTalent
            cell.deleteText.text = "Delete Talent"
            return cell
            
        default:
            let cell = tableView.dequeueReusableCell(withIdentifier: "image", for: indexPath) as! imageCollectionTableviewCell
            
            cell.collectionView.delegate = self
            
            return cell
            
        }
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch indexPath.section {
            
        case 3 :
            
            if(self.mode=="Update"){
                
                // Add new Project into Array
                if(indexPath.row >= self.projects.count){
                    
                    var project = Dictionary<String,Any>()
                    
                    project["projectDate"] = ""
                    project["projectName"] = ""
                    project["projectDetail"] = ""
                    project["projectImage"] = []
                    
                    self.projects.append(project as NSDictionary)
                    self.tableView.reloadSections(IndexSet(integer: indexPath.section), with: .none)
                }
            }
            
            

        case 4 :
            
            if(indexPath.row >= packages.count){
                
                var package = Dictionary<String,Any>()
                
                package["packageTime"] = ""
                package["packageName"] = ""
                package["packagePrice"] = ""
                package["packageTimeUnit"] = 0
                package["packageType"] = 0
                
                self.packages.append(package as NSDictionary)
                self.tableView.reloadSections(IndexSet(integer: indexPath.section), with: .none)
            }
            
        case 5 :
            if(indexPath.row >= conditions.count){
                self.conditions.append("")
                self.tableView.reloadSections(IndexSet(integer: indexPath.section), with: .none)
            }
        case 6:
            print("deleted")
            
            let alert = UIAlertController.init(title: "Are you sure ?", message: "Do you want to delete your talent ?", preferredStyle: .alert)
            let yes = UIAlertAction.init(title: "Yes", style: .default , handler: {(alert: UIAlertAction!) in
                self.deleteProfess()
            })
            
            let No = UIAlertAction.init(title: "No", style: .cancel, handler: nil)
            
            alert.addAction(yes)
            alert.addAction(No)
            
            self.present(alert, animated: true, completion: nil)
            
            
        // Delete Talent Call
        default:
            break
            
        }
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        switch indexPath.section {
        case 0:
            guard let tableViewCell = cell as? imageCollectionTableviewCell else { return }
            tableViewCell.setCollectionViewDataSourceDelegate(self, forRow: indexPath.row)
        case 3 :
            guard let tableViewCell = cell as? projectRefAdd else { return }
            tableViewCell.setCollectionViewDataSourceDelegate(self, forRow: indexPath.row)
            
        default:
            break
        }
    }
    
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let cell = self.tableView.dequeueReusableHeaderFooterView(withIdentifier: "ProfileTableSelectionHeader") as! ProfileTableSelectionHeader
        
        let header = cell
        header.titleLabel.text = headerText[section]
        header.bgView.backgroundColor = UIColor.clear
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        
        switch section {
        case 0:
            return 0
        case 2:
            return 0
        case 6:
            return 0
        default:
            return CGFloat(tableViewValue.profileHeaderViewSize)
        }
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        
        switch indexPath.section {
        case 3:
            if(indexPath.row < projectRefName.count){
                return true
            }
            else{
                return false
            }
        case 4:
            if(indexPath.row < packageName.count){
                return true
            }
            else{
                return false
            }
        case 5:
            if(indexPath.row < conditions.count){
                return true
            }
            else{
                return false
            }
            
        default:
            return false
        }
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        
        switch indexPath.section {
        case 3:
            if(indexPath.row < projectRefName.count){
                if editingStyle == .delete {
                    print("Deleted")
                    
                    projectRefDetail.remove(at: indexPath.row)
                    projectRefName.remove(at: indexPath.row)
                    projectDateTime.remove(at: indexPath.row)
                    urlProjectRefBFU[indexPath.row] = []
                    
                    self.tableView.deleteRows(at: [indexPath], with: .automatic)
                    break
                }
            }
        case 4:
            
            if(indexPath.row < packageName.count){
                if editingStyle == .delete {
                    print("Deleted")
                    
                    
                    packageName.remove(at: indexPath.row)
                    packageHours.remove(at: indexPath.row)
                    packagePrices.remove(at: indexPath.row)
                    packUnit.remove(at: indexPath.row)
                    packageType.remove(at: indexPath.row)
                    
                    
                    self.tableView.deleteRows(at: [indexPath], with: .automatic)
                    break
                }
            }
            
        case 5:
            if(indexPath.row < conditions.count){
                if editingStyle == .delete {
                    print("Deleted")
                    
                    conditions.remove(at: indexPath.row)
                    
                    self.tableView.deleteRows(at: [indexPath], with: .automatic)
                    break
                }
            }
        default:
            break
        }
        
    }
}

// MARK: - Collection view data source
extension professRegisViewController : UICollectionViewDataSource , UICollectionViewDelegate , UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        let indexPathFromTag = collectionView.tag
        
        if(collectionView.tag == 2001){
            if(self.mode == "Update"){
                return urlImageBFU.count + 1
            }else{
                return workImage.count + 1
            }
        }
        
        if(collectionView.tag >= 300 && collectionView.tag <= 399){
            
            if(self.mode == "Update"){
                let ProRefIndexFromTag : Int = indexPathFromTag - 300
                
                let projectDetail = self.projects[ProRefIndexFromTag]
                let projectImage = projectDetail.value(forKey: "projectImage") as! NSArray
                return projectImage.count + 1
            }
            else{
                let ProRefIndexFromTag : Int = indexPathFromTag - 300
                if(projectRefImage.isEmpty){
                    return 1
                }
                else if (!projectRefImage[ProRefIndexFromTag].isEmpty){
                    return projectRefImage[ProRefIndexFromTag].count + 1
                }
                else{
                    return 1
                }
            }
        }
        else{
            return 0
        }
    }
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if(collectionView.tag == 2001){
            if(self.mode == "Update"){
                if(indexPath.row < urlImageBFU.count){
                    
                    let item = collectionView.dequeueReusableCell(withReuseIdentifier: "image", for: indexPath) as! imageCollectionItem
                    
                    item.deleteButton.tag = indexPath.row
                    let urlImage = self.urlImageBFU[indexPath.row]
                    if(urlImage is String){
                        item.image.sd_setImage(with: URL(string: urlImage as! String)!, placeholderImage: UIImage(named: "bookme_loading"))
                    }
                    else if (urlImage is UIImage){
                        item.image.image = (urlImage as! UIImage)
                    }
                    
                    item.image.setRoundWithR(r: commonValue.subTadius)
                    
                    return item
                }
                else{
                    let item = collectionView.dequeueReusableCell(withReuseIdentifier: "add", for: indexPath) as! addCollectionItem
                    
                    item.bgView.setRoundWithR(r: commonValue.subTadius)
                    
                    return item
                }
            }
            else{
                if(indexPath.row < workImage.count){
                    let item = collectionView.dequeueReusableCell(withReuseIdentifier: "image", for: indexPath) as! imageCollectionItem
                    
                    item.image.image = workImage[indexPath.row]
                    item.image.setRoundWithR(r: commonValue.subTadius)
                    
                    return item
                }
                else{
                    let item = collectionView.dequeueReusableCell(withReuseIdentifier: "add", for: indexPath) as! addCollectionItem
                    
                    item.bgView.setRoundWithR(r: commonValue.subTadius)
                    
                    return item
                }
            }
        }
        else if(collectionView.tag >= 300 && collectionView.tag <= 399){
            
            // Collection Tag
            let projectImage_tag = collectionView.tag - 300
            
            if(self.mode == "Update"){
                let projectDetail = self.projects[projectImage_tag]
                let projectImage = projectDetail.value(forKey: "projectImage") as! NSArray
                
                if(indexPath.row < projectImage.count){
                    let items = collectionView.dequeueReusableCell(withReuseIdentifier: "projectImage", for: indexPath) as! projectimageCollectionItem
                    
                    let url = projectImage[indexPath.row]
                    
                    if(url is String){
                        items.image.sd_setImage(with: URL(string: url as! String)!, placeholderImage: UIImage(named: "bookme_loading"))
                    }
                    else if (url is UIImage){
                        items.image.image = (url as! UIImage)
                    }
                    
                    items.image.setRoundWithR(r: commonValue.subTadius)
                    return items
                }
                else {
                    let item = collectionView.dequeueReusableCell(withReuseIdentifier: "addProject", for: indexPath) as! addCollectionItem
                    item.bgView.setRoundWithR(r: commonValue.subTadius)
                    return item
                }
            }
        }
        
        
        let item = collectionView.dequeueReusableCell(withReuseIdentifier: "addProject", for: indexPath) as! addCollectionItem
        return item
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        
        switch collectionView.tag {
        case 2001:
            return CGSize(width: collectionView.bounds.height*1.25-10 , height: collectionView.bounds.height-10 )
        default:
            return CGSize(width: collectionView.bounds.height-10 , height: collectionView.bounds.height-10 )
            
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        let projectImage_tag = collectionView.tag - 300
        
        switch collectionView.tag {
        case 2001:
            if(indexPath.row >= workImage.count){
                print("Add Image")
                
                self.WherePickerDrop = "workImage"
                let imagePicker = OpalImagePickerController()
                imagePicker.imagePickerDelegate = self
                imagePicker.maximumSelectionsAllowed = 15
                present(imagePicker, animated: true, completion: nil)
            }
            else{
                print("image touch")
            }
            
            break
        default :
            if(self.mode == "Update"){
                let projectDetail = self.projects[projectImage_tag]
                let projectImage = projectDetail.value(forKey: "projectImage") as! NSArray
                
                if(indexPath.row >= projectImage.count){
                    print("Add Image")
                    
                    self.WherePickerDrop = "projectRef"
                    self.projectRefIndexRow = projectImage_tag
                    
                    let imagePicker = OpalImagePickerController()
                    imagePicker.maximumSelectionsAllowed = 15
                    imagePicker.imagePickerDelegate = self
                    present(imagePicker, animated: true, completion: nil)
                }
                else{
                    print("out of index")
                }
            }
            else{
                if(projectRefImage.isEmpty){
                    
                    projectRefImage.append(Array<UIImage>())
                    
                    print("Add Image")
                    
                    self.WherePickerDrop = "projectRef"
                    self.projectRefIndexRow = projectImage_tag
                    
                    let imagePicker = OpalImagePickerController()
                    imagePicker.maximumSelectionsAllowed = 15
                    imagePicker.imagePickerDelegate = self
                    present(imagePicker, animated: true, completion: nil)
                }
                
                if(indexPath.row >= projectRefImage[projectImage_tag].count){
                    print("Add Image")
                    
                    self.WherePickerDrop = "projectRef"
                    self.projectRefIndexRow = projectImage_tag
                    
                    let imagePicker = OpalImagePickerController()
                    imagePicker.imagePickerDelegate = self
                    imagePicker.maximumSelectionsAllowed = 15
                    present(imagePicker, animated: true, completion: nil)
                }
                else{
                    print("image touch")
                }
            }
            break
        }
    }
}

// MARK: - ImagePicker Delegate
extension professRegisViewController : OpalImagePickerControllerDelegate {
    
    func imagePicker(_ picker: OpalImagePickerController, didFinishPickingImages images: [UIImage]) {
        print("did done")
        
        if (self.WherePickerDrop == "workImage"){
            
            if(mode == "Update"){
                new_workImageUpdate = images
                
                for image in images{
                    self.urlImageBFU.append(image)
                }
            }
            else{
                for image in images{
                    self.workImage.append(image)
                }
            }
            
            
        }
        else if (self.WherePickerDrop == "projectRef"){
            
            if (self.mode == "Update"){
                
                let dictData = self.projects[projectRefIndexRow]
                var project = dictData as! Dictionary<String, Any>
                
                project["projectName"] = dictData.value(forKey: "projectName") as! String
                project["projectDate"] = dictData.value(forKey: "projectDate") as! String
                project["projectDetail"] = dictData.value(forKey: "projectDetail") as! String
                
                var imageArray = [UIImage]()
                
                for image in images{
                    imageArray.append(image)
                }
                project["projectImage"] =  imageArray
                
                self.uploadImageProject = true
                self.projects[projectRefIndexRow] = project as NSDictionary
                
            }
            else{
                var imageArray = projectRefImage[projectRefIndexRow]
                
                for image in images{
                    imageArray.append(image)
                }
                
                if(projectRefImage.isEmpty){
                    self.projectRefImage.append(imageArray)
                }else{
                    self.projectRefImage[projectRefIndexRow] = imageArray
                }
            }
        }
        
        self.tableView.reloadData()
        self.dismiss(animated: true, completion: nil)
    }
    
    func imagePickerDidCancel(_ picker: OpalImagePickerController){
        self.dismiss(animated: true, completion: nil)
    }
}


// MARK: - Class Declareation
class addCollectionItem : UICollectionViewCell {
    @IBOutlet var bgView: UIView!
    @IBOutlet weak var textLabel: UILabel!
}

class connectInstragram : UITableViewCell {
    @IBOutlet weak var bgView: UIView!
    @IBOutlet weak var igNameText: UITextField!
    @IBOutlet weak var connectButton: UIButton!
}

class addObjectCell : UITableViewCell {
    @IBOutlet weak var bgView: UIView!
    @IBOutlet var titleTet: UILabel!
}

class packageAddCell : UITableViewCell {
    @IBOutlet var packName: UITextField!
    @IBOutlet var packPrice: UITextField!
    @IBOutlet var packHours: UITextField!
    @IBOutlet weak var packageUnit: UITextField!
    @IBOutlet weak var bgView: UIView!
    
}

class projectRefAdd : UITableViewCell {
    
    @IBOutlet var dateTime: UITextField!
    @IBOutlet var projectName: UITextField!
    @IBOutlet var projectDetail: UITextField!
    @IBOutlet var imageCollection: UICollectionView!
    @IBOutlet weak var deleteButton: UIButton!
    @IBOutlet weak var bgView: UIView!
}

class projectimageCollectionItem : UICollectionViewCell {
    @IBOutlet var image : UIImageView!
}

extension projectRefAdd {
    
    func setCollectionViewDataSourceDelegate<D: UICollectionViewDataSource & UICollectionViewDelegate>(_ dataSourceDelegate: D, forRow row: Int) {
        
        imageCollection.delegate = dataSourceDelegate
        imageCollection.dataSource = dataSourceDelegate
        imageCollection.setContentOffset(imageCollection.contentOffset, animated:false) //
        imageCollection.reloadData()
    }
    
    var collectionViewOffset: CGFloat {
        set { imageCollection.contentOffset.x = newValue }
        get { return imageCollection.contentOffset.x }
    }
}


class DeleteTalent: UITableViewCell {
    @IBOutlet weak var deleteText : UILabel!
}
