//
//  bankVerifyViewController.swift
//  bookme
//
//  Created by Naphat Sottiyaphai on 31/5/2561 BE.
//  Copyright © 2561 Naphat Sottiyaphai. All rights reserved.
//

import UIKit
import ACFloatingTextfield_Swift
import Alamofire
import SVProgressHUD
import SwiftHash
import ActionSheetPicker_3_0
import OpalImagePicker

class bankVerifyViewController: UIViewController {
    
    @IBOutlet weak var save: UIButton!
    @IBOutlet weak var bankImage: UIImageView!
    @IBOutlet weak var bankName: UITextField!
    @IBOutlet weak var bankAccountName: UITextField!
    @IBOutlet weak var bankAccountNumber: UITextField!
    @IBOutlet weak var edit: UIButton!
    @IBOutlet weak var bgView: UIView!
    
    var imageData = Data()
    var imageURL = String()
    
    private let bankNames = ["ธนาคารกรุงเทพ" , "ธนาคารกสิกรไทย" , "ธนาคารกรุงไทย" , "ธนาคารทหารไทย" , "ธนาคารไทยพาณิชย์" , "ธนาคารกรุงศรีอยุธยา" , "ธนาคารเกียรตินาคิน" , "ธนาคารซีไอเอ็มบีไทย" , "ธนาคารทิสโก้" , "ธนาคารธนชาต" , "ธนาคารยูโอบี" , "ธนาคารแลนด์ แอนด์ เฮาส์" , "ธนาคารเพื่อการเกษตรและสหกรณ์การเกษตร" , "ธนาคารออมสิน" , "ธนาคารอาคารสงเคราะห์" , "ธนาคารอิสลามแห่งประเทศไทย"]
    
    class func instantiateFromStoryboard() -> bankVerifyViewController {
        let storyboard = UIStoryboard(name: "profile", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: String(describing: self)) as! bankVerifyViewController
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.bankName.delegate = self
        self.bankAccountName.delegate = self
        self.bankAccountNumber.delegate = self
        
        self.bankImage.setRoundWithR(r: commonValue.viewRadius)
        self.bankName.setDefaultTextFieldLayout()
        self.bankAccountName.setDefaultTextFieldLayout()
        self.bankAccountNumber.setDefaultTextFieldLayout()
        
        self.save.setRoundedwithR(r: commonValue.viewRadius)
        self.title = "Bank Account"
        
        setBackgroundColor()
        bgView.setRoundWithR(r: commonValue.viewRadius)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.navigationController?.navigationBar.isHidden = false
        UINavigationBar.appearance().prefersLargeTitles = true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        UINavigationBar.appearance().prefersLargeTitles = false
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
    
    func ImageUploading(data : Data) {
        
        let ud = UserDefaults.standard
        let uid = ud.value(forKey: "user_uid") as! String
        let md5_uid = MD5(uid)
        let url = URL(string: "\(baseURL.media)api/uploads/")
        
        Alamofire.upload(multipartFormData: { (multipartFormData) in
            
            multipartFormData.append("\(uid)".data(using: String.Encoding.utf8)!, withName: "uId" as String)
            multipartFormData.append(data, withName: "imgFiles", fileName: "\(uid)_swift_file.jpeg", mimeType: "image/jpeg")
            
            
        },to: url! )
        { (result) in
            
            switch result {
            case .success(let upload, _, _):
                
                upload.uploadProgress(closure: { (progress) in
                    SVProgressHUD.showProgress(Float(progress.fractionCompleted), status: "Image Uploading")
                })
                
                upload.responseJSON { response in
                    
                    if let result = response.result.value {
                        let results = result as! NSDictionary
                        let data = results.value(forKey: "data") as! NSArray
                        
                        self.imageURL = "\(baseURL.photoURL)\(md5_uid.lowercased())/storages/\(data[0])"
                        
                        self.updateProfileDocument()
                        
                        SVProgressHUD.dismiss()
                        
                    }
                }
                
            case .failure(let encodingError):
                print("ERROR : \(encodingError)")
                SVProgressHUD.showError(withStatus: "Media Connection Error : \(encodingError)")
                break
            }
        }
    }
    
    func updateProfileDocument() {
        
        let ud = UserDefaults.standard
        let uid = ud.value(forKey: "user_uid") as! String
        
        var accesstoken = String()
        
        if(ud.object(forKey: "accessToken") != nil){
            accesstoken = ud.value(forKey: "accessToken") as! String
        }
        
        let header : HTTPHeaders = [
            "accessToken" : accesstoken
        ]
        
        print("uid : \(uid)")
        
        let parameters: [String: Any] = [
            "bankAccountName" : self.bankAccountName.text ?? "",
            "bankAccountNumber" : self.bankAccountNumber.text ?? "",
            "bankName" : self.bankName.text ?? "",
            "bankBookImage" : self.imageURL
        ]
        
        
        print(parameters)
        
        Alamofire.request(baseURL.url + "profile/\(uid)/document/update-bank-account", method: .put, parameters: parameters, encoding: JSONEncoding.default , headers : header)
            .responseJSON(completionHandler: { response in
                
                if let result = response.result.value {
                    _ = result as! NSDictionary
                    
                    SVProgressHUD.showSuccess(withStatus: "Update done")
                    
                    DispatchQueue.main.asyncAfter(deadline: .now()+1){
                        SVProgressHUD.dismiss()
                        self.navigationController?.popViewController(animated: true)
                    }
                }
                else{
                    SVProgressHUD.showError(withStatus: "Network Error")
                }
            })
    }
    
    
    @IBAction func takePhoto(_ sender: Any) {
        
        let imagePicker = OpalImagePickerController()
        imagePicker.imagePickerDelegate = self
        imagePicker.maximumSelectionsAllowed = 1
        
        present(imagePicker, animated: true, completion: nil)
        
//        var config = YPImagePickerConfiguration()
//        config.onlySquareImagesFromCamera = true
//        config.targetImageSize = .original
//        config.usesFrontCamera = false
//        config.showsPhotoFilters = false
//        config.shouldSaveNewPicturesToAlbum = true
//        config.albumName = "BookMe"
//        config.screens = [.photo]
//        config.wordings.libraryTitle = "Gallery"
//        config.hidesStatusBar = false
//        
//        // Build a picker with your configuration
//        let picker = YPImagePicker(configuration: config)
//        
//        picker.didFinishPicking(completion: { [unowned picker] items , cancelled  in
//            // image picked
//            if cancelled {
//                
//                print("Did Cancel")
//                picker.dismiss(animated: true, completion: nil)
//            }
//            
//            if let photo = items.singlePhoto {
////                print(photo.fromCamera) // Image source (camera or library)
////                print(photo.image) // Final image selected by the user
////                print(photo.originalImage) // original image selected by the user, unfiltered
////                print(photo.modifiedImage) // Transformed image, can be nil
////                print(photo.exifMeta) // Print exif meta data of original image.
//                
//                self.bankImage.image = photo.image
//                let image = photo.image.jpegData(compressionQuality: commonValue.imageCompress)
//                self.edit.setTitle("", for: .normal)
//                self.imageData = image!
//            }
//            picker.dismiss(animated: true, completion: nil)
//        })
//        self.present(picker, animated: true, completion: nil)
    }
    
    func ConvertImageToBase64String (img: UIImage) -> String {
        return img.jpegData(compressionQuality: 1)?.base64EncodedString() ?? ""
    }
    
    
    @IBAction func submitData(_ sender: Any) {
        if (bankName.text != "" && bankAccountName.text != "" && bankAccountNumber.text != ""  && self.imageData != Data()){
//            self.ImageUploading(data: self.imageData)
            let imageString = ConvertImageToBase64String(img : self.bankImage.image!)
            self.imageURL = imageString
            
            self.updateProfileDocument()
        }
        else{
            var title = String()
            var description = String()
            var actionTitle = String()
            
            let userDefault = UserDefaults.standard
            let lang = userDefault.value(forKey: "langDevice") as! String
            
            if(lang == "th-TH"){
                title = thai.someThingWrong
                description = thai.fillNotValidate
                actionTitle = thai.ok
            }
            else{
                title = english.someThingWrong
                description = english.fillNotValidate
                actionTitle = english.ok
            }
            
            let alert = UIAlertController(title: title, message: description, preferredStyle: .alert)
            
            let ok = UIAlertAction(title: actionTitle, style: .default) { (action) in
                self.dismiss(animated: false, completion: nil)
            }
            
            alert.addAction(ok)
            
            self.present(alert, animated: true, completion: nil)
        }
        
    }
}

extension bankVerifyViewController : UITextFieldDelegate {
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        if(textField == bankName){
            
            textField.resignFirstResponder()
            self.view.endEditing(true)
            
            let picker = ActionSheetStringPicker(title: "Select your Bank Name", rows: bankNames , initialSelection: 0, doneBlock: { (picker, index, value) in
                
                textField.text = value as! String
                
                return
                
            }, cancel: { (picker) in
                
                return
            }, origin: (textField as AnyObject).superview!?.superview )
            
            picker?.show()
        }
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if(textField == bankName){
            self.view.endEditing(true)
        }
        
        if(textField == bankAccountNumber){
            textField.text = ""
        }
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        let string = textField.text!
        
        if(textField == bankAccountNumber){
            if(textField.text?.count != 10){
                textField.textColor = UIColor.red
                
                SVProgressHUD.showError(withStatus: "Invalid bank account number.")
                DispatchQueue.main.asyncAfter(deadline: .now() + 2){
                    SVProgressHUD.dismiss()
                }
            }
            else{
                textField.textColor = UIColor.darkText
                
                let index0 = string.index(string.startIndex, offsetBy: 3)
                let index1 = string.index(index0, offsetBy: 1)
                let index2 = string.index(index1, offsetBy: 5)
                let index3 = string.index(index2, offsetBy: 1)
                
                let mySubstring = string[..<index0]
                let mySubstring1 = string[index0..<index1]
                let mySubstring2 = string[index1..<index2]
                let mySubstring3 = string[index2..<index3]
                
                let formatString = "\(mySubstring)-\(mySubstring1)-\(mySubstring2)-\(mySubstring3)"
                textField.text = formatString
            }
        }
    }
}

extension bankVerifyViewController : OpalImagePickerControllerDelegate {
    
    func imagePicker(_ picker: OpalImagePickerController, didFinishPickingImages images: [UIImage]) {
        print("did done")
        
        self.bankImage.image = images[0]
        let image = images[0].jpegData(compressionQuality: commonValue.imageCompress)
        self.edit.setTitle("", for: .normal)
        self.imageData = image!
        
        self.dismiss(animated: true, completion: nil)
    }
    
    func imagePickerDidCancel(_ picker: OpalImagePickerController){
        //        self.dismiss(animated: true, completion: nil)
    }
}
