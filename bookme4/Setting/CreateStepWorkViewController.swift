//
//  CreateStepWorkViewController.swift
//  bookme
//
//  Created by Naphat Sottiyaphai on 24/2/2562 BE.
//  Copyright © 2562 Naphat Sottiyaphai. All rights reserved.
//

import UIKit
import Alamofire
import SVProgressHUD
import SwiftDate
import SwiftHash
import OpalImagePicker
import ACFloatingTextfield_Swift
import ActionSheetPicker_3_0

class CreateStepWorkViewController: UIViewController{
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var nextButt: UIButton!
    
    private var workExp = [Dictionary<String, Any>]()
    private var workImage = [[UIImage]]()
    private var selectProjectImageRow = Int()
    
    // Project Referance Declaration
    var projectDateTime = [""]
    var projectRefName = [""]
    var projectRefDetail = [""]
    var projectRefImage = [[UIImage]]()
    var projectRefIndexRow = 0
    var urlprojectRef = [[String]]()
    
    var professional = NSDictionary()
    var professID = String()
    
    var loop = Int()
    var looCount = Int()
    
    
    class func instantiateFromStoryboard() -> CreateStepWorkViewController {
        let storyboard = UIStoryboard(name: "profile", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: String(describing: self)) as! CreateStepWorkViewController
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let userDefaults = UserDefaults.standard
        self.professID = userDefaults.value(forKey: "profess_id_in_process") as! String
        
        self.projectRefImage.append(Array<UIImage>())
        
        self.getProfess()
        self.nextButt.setRoundedwithR(r: commonValue.subTadius)
        
        self.setClearNavBar()
        self.setBackgroundColor()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.navigationController?.navigationBar.isHidden = false
        
        if #available(iOS 11.0, *) {
            navigationController?.navigationBar.prefersLargeTitles = false
        } else {
            print("It's down of iOS 11")
        }
    }
    
    @IBAction func nextStep(_ sender: Any) {
        //        let vc = CreateStepPackViewController.instantiateFromStoryboard()
        //        vc.professID = self.professID
        //        self.navigationController?.pushViewController(vc, animated: true)
        
        //        self.updateProfess()
        //        self.ProjectRefImageUploading(data: self.projectRefImage)
        self.loop = 0
        self.looCount = self.projectRefImage.count
        
        for setImage in self.projectRefImage{
            self.ImageUploading(data: setImage)
        }
    }
    
    func dataInit() {
        
        let project : [String : Any] = [
            "projectDetail": "",
            "projectName": "",
            "projectImage": [],
            "dateTime": ""
        ]
        
        self.workImage.append([])
        self.workExp.append(project)
        
        DispatchQueue.main.async {
            self.tableView.reloadData()
        }
    }
    
    func getProfess() {
        
        let ud = UserDefaults.standard
        var accesstoken = String()
        
        if(ud.object(forKey: "accessToken") != nil){
            accesstoken = ud.value(forKey: "accessToken") as! String
        }
        
        let header : HTTPHeaders = [
            "accessToken" : accesstoken
        ]
        
        Alamofire.request(baseURL.url + "professional/\(self.professID)", method: .get, encoding: JSONEncoding.default , headers : header)
            .responseJSON(completionHandler: { response in
                
                if let responses = response.result.value {
                    let responseData = responses as! NSDictionary
                    let data = responseData.value(forKey: "data") as! NSDictionary
                    
                    self.professional = data
                    print("collect profess complete")
                    
                }
                else{
                    SVProgressHUD.showError(withStatus: response.error?.localizedDescription)
                }
            })
    }
    
    func updateProfess() {
        print("Updateing profess")

        var project = [Dictionary<String,Any>]()
        var projectRefImageFile = [[String]]()

        for fileName in self.urlprojectRef{
            var arrayString = [String]()
            for name in fileName{
                arrayString.append(name)
            }
            projectRefImageFile.append(arrayString)
        }

        var proRef = 0
        for _ in projectRefName{
            
            var dict = Dictionary<String,Any>()
            
            dict["projectDate"] = self.projectDateTime[proRef]
            dict["projectName"] = self.projectRefName[proRef]
            dict["projectDetail"] = self.projectRefDetail[proRef]

            if(projectRefImageFile.count > 0){
                dict["projectImage"] = projectRefImageFile[proRef].filter{$0 != ""}
            }
            else{
                dict["projectImage"] = []
            }
            project.append(dict)
            proRef = proRef + 1
        }
        
        project = project.compactMap{$0}
        print(project)
        
        
        let ud = UserDefaults.standard
        var accesstoken = String()
        
        if(ud.object(forKey: "accessToken") != nil){
            accesstoken = ud.value(forKey: "accessToken") as! String
        }
        
        let header : HTTPHeaders = [
            "accessToken" : accesstoken
        ]
        
        let parameters: [String: Any] = [
            "projects" : project
        ]
        
        print(parameters)
        
        Alamofire.request(baseURL.url + "professional/\(self.professID)", method: .put, parameters: parameters, encoding: JSONEncoding.default , headers: header).responseJSON { (response) in
            
            if let result = response.result.value {
                print(result)
                let vc = CreateStepPackViewController.instantiateFromStoryboard()
                vc.professID = self.professID
                self.navigationController?.pushViewController(vc, animated: true)
                SVProgressHUD.dismiss()
            }
            else{
                SVProgressHUD.showError(withStatus: response.error?.localizedDescription)
                SVProgressHUD.dismiss()
            }
        }
    }
    
    func ProjectRefImageUploading(data : [[UIImage]] ) {
        
        let ud = UserDefaults.standard
        let uid = ud.value(forKey: "user_uid") as! String
        let md5_uid = MD5(uid)
        let url = URL(string: "\(baseURL.media)api/uploads/bookme")
        
        var count = 0
        
        if(data.isEmpty){
            self.updateProfess()
            SVProgressHUD.dismiss()
        }
            
        else{
            for arrayImage in data{
                
                var proloadProjectRef = [Data]()
                
                for proload in arrayImage {
                    let image = proload.jpegData(compressionQuality: commonValue.imageCompress)
                    if(image != nil){
                        proloadProjectRef.append(image!)
                    }
                }
                
                if(proloadProjectRef.isEmpty){
                    print("it's Empty")
                    count += 1
                    self.urlprojectRef.append([])
                    
                    SVProgressHUD.dismiss()
                    
                    if( count >= data.count){
                        self.updateProfess()
                        //                                    self.postReg(data: self.RegProData as NSDictionary)
                        SVProgressHUD.dismiss()
                    }
                    
                }
                else{
                    print("Not Empty")
                }
                
                Alamofire.upload(multipartFormData: {(multipartFormData) in
                    
                    multipartFormData.append("\(uid)".data(using: String.Encoding.utf8)!, withName: "uId" as String)
                    
                    for images in proloadProjectRef {
                        let data = images
                        multipartFormData.append(data, withName: "imgFiles", fileName: "\(md5_uid)_swift_file.jpeg", mimeType: "image/jpeg")
                    }
                    
                    print(multipartFormData)
                    
                }, usingThreshold: UInt64.init() , to: url!, method: .post , encodingCompletion: { (result) in
                    
                    switch result {
                    case .success(let upload, _, _):
                        upload.uploadProgress(closure: { (progress) in
                            SVProgressHUD.showProgress(Float(progress.fractionCompleted), status: "Uploading")
                        })
                        
                        upload.responseJSON { response in
                            
                            if let result = response.result.value {
                                
                                let filted = data.filter{!$0.isEmpty}
                                let dataCount = filted.count
                                
                                
                                let results = result as! NSDictionary
                                let ArryResult = results.value(forKey: "data") as! Array<String>
                                
                                //                                if(self.mode == "Update"){
                                //                                    for imageName in ArryResult{
                                //                                        print(count)
                                //                                        print(self.nameProjectRef)
                                //                                        self.nameProjectRef[count].append(imageName)
                                //                                    }
                                //                                }
                                //                                else{
                                self.urlprojectRef.append(ArryResult)
                                //                                }
                                
                                count += 1
                                
                                if( count >= dataCount ){
                                    self.updateProfess()
                                    //                                    self.postReg(data: self.RegProData as NSDictionary)
                                    SVProgressHUD.dismiss()
                                }
                            }
                            else{
                                print("Error")
                                print(response.error?.localizedDescription ?? "")
                                SVProgressHUD.dismiss()
                                SVProgressHUD.showError(withStatus: response.error?.localizedDescription ?? "")
                            }
                        }
                        
                    case .failure(let encodingError):
                        print("ERROR : \(encodingError)")
                        break
                    }
                })
            }
        }
    }
    
    func ImageUploading(data : [UIImage]) {
        
        var preloadGallery = data.map{ $0.jpegData(compressionQuality: commonValue.imageCompress)!}
        
            SVProgressHUD.show()
        
            let ud = UserDefaults.standard
            let uid = ud.value(forKey: "user_uid") as! String
            let accessToken = ud.value(forKey: "bluweoAccessToken") as! String
            
            let url = URL(string: "\(baseURL.bluweoMedia)upload/multiple")
            
            let headers = [
                "access_token" : accessToken,
                "accept": "application/x-www-form-urlencoded",
                "Content-Type" : "multipart/form-data"
            ]
            
            let parameters = [
                "api": "professional",
                "path" : "workExp"
            ]
            
            Alamofire.upload(multipartFormData: { (multipartFormData) in
                
                var index = 0
                
                for img in preloadGallery{
                    multipartFormData.append(img, withName: "files[\(index)]", fileName: "\(uid)_\(Date().timeIntervalSince1970)_Image.jpg", mimeType: "image/jpeg")
                    index+=1
                }
                
                for (key, value) in parameters {
                    multipartFormData.append(value.data(using: String.Encoding.utf8)!, withName: key)
                }
                
                
            }, to: url!, method: .post, headers: headers) { (result) in
                
                switch result {
                case .success(let upload, _, _):
                    
                    upload.uploadProgress(closure: { (progress) in
                        SVProgressHUD.showProgress(Float(progress.fractionCompleted), status: "Image Uploading")
                    })
                    
                    upload.responseJSON { response in
                        
                        if let result = response.result.value {
                            
                            let results = result as! NSDictionary
                            let ArryResult = results.value(forKey: "data") as! NSDictionary
                            
                            if(ArryResult.object(forKey: "files") != nil){
                                let filePath = ArryResult.value(forKey: "files") as! NSArray
                                
                                
                                var collectURLImage = [String]()
                                
                                for path in filePath{
                                    let imgPath = (path as! NSDictionary).value(forKey: "file")
                                    
                                    let fullURL = "\(baseURL.bluweoMedia)\(imgPath as! String)"
                                    collectURLImage.append(fullURL)
                                }
                                
                                self.urlprojectRef.append(collectURLImage)
                                
                                
                            }
                            else{
                                print(ArryResult)
                                
                                if let message = ArryResult.value(forKey: "message"){
                                    if(message is NSDictionary){
                                        if let errMsg = (message as! NSDictionary).value(forKey: "message"){
                                            print(errMsg as! Any)
                                        }
                                    }
                                    else{
                                        let message = ArryResult.value(forKey: "message") as! String
                                        print(message)
                                    }
                                }
                                
                                self.refreshBluweoToken()
                                
                                SVProgressHUD.show()
                                DispatchQueue.main.asyncAfter(deadline: .now() + 3){
                                    SVProgressHUD.dismiss()
                                }
                                
                            }
                            
                            if(self.looCount-1 == self.loop){
                                print("Done")
                                SVProgressHUD.dismiss()
                                print(self.urlprojectRef)
                                self.updateProfess()
                            }
                            
                            self.loop += 1
                        }
                    }
                case .failure(let encodingError):
                    print("ERROR : \(encodingError)")
                    SVProgressHUD.showError(withStatus: "Media Connection Error : \(encodingError)")
                    break
                }
            }
        }
}

extension CreateStepWorkViewController : UITableViewDelegate , UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return projectRefName.count + 1
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if(indexPath.row < projectRefName.count){
            return 325
        }
        else{
            return 60
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if(indexPath.row < projectRefName.count){
            let cell = tableView.dequeueReusableCell(withIdentifier: "projectRefAdd", for: indexPath) as! projectRefAddStep
            
            cell.imageCollection.delegate = self
            cell.imageCollection.tag = 300 + indexPath.row
            
            cell.dateText.setRoundWithCommonRwithBGC_f8()
            cell.titleText.setRoundWithCommonRwithBGC_f8()
            cell.detailText.setRoundWithCommonRwithBGC_f8()
            
            cell.dateText.setText()
            cell.titleText.setTextBold()
            cell.detailText.setText()
            
            cell.dateText.setLeftPaddingPoints(15)
            cell.titleText.setLeftPaddingPoints(15)
            cell.detailText.setLeftPaddingPoints(15)
            
            cell.dateText.delegate = self
            cell.titleText.delegate = self
            cell.detailText.delegate = self
            
            cell.dateText.tag = 3100 + indexPath.row
            cell.titleText.tag = 3200 + indexPath.row
            cell.detailText.tag = 3300 + indexPath.row
            
            let dateISO =  projectDateTime[indexPath.row]
            let toDate = dateISO.toDate()
            cell.dateText.text = toDate?.toFormat("MMM yyyy")
            
            cell.titleText.text = projectRefName[indexPath.row]
            cell.detailText.text = projectRefDetail[indexPath.row]
            cell.bgView.setRoundWithCommonR()
            
            return cell
        }
        else{
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "addObject", for: indexPath) as! addOCreatebjectCell
            
            cell.titleTet.text =  "Add more Experience"
            cell.bgView.setRoundWithCommonR()
            
            return cell
            
        }
        
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        guard let tableViewCell = cell as? projectRefAddStep else { return }
        tableViewCell.setCollectionViewDataSourceDelegate(self, forRow: indexPath.row)
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if(indexPath.row >= self.projectRefDetail.count){
            
            self.projectRefDetail.append("")
            self.projectRefName.append("")
            self.projectDateTime.append("")
            self.projectRefImage.append(Array<UIImage>())
            
            self.tableView.reloadSections(IndexSet(integer: indexPath.section), with: .none)
        }
    }
}

extension CreateStepWorkViewController : UICollectionViewDelegate , UICollectionViewDataSource , UICollectionViewDelegateFlowLayout {
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        let indexPathFromTag = collectionView.tag
        let ProRefIndexFromTag : Int = indexPathFromTag - 300
        print(projectRefImage)
        
        if(projectRefImage.isEmpty){
            return 1
        }
        else if (!projectRefImage[ProRefIndexFromTag].isEmpty){
            return projectRefImage[ProRefIndexFromTag].count + 1
        }
        else{
            return 1
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        print(collectionView.tag)
        let projectImage_tag = collectionView.tag - 300
        
        if(projectRefImage.isEmpty){
            let item = collectionView.dequeueReusableCell(withReuseIdentifier: "addProject", for: indexPath) as! addCollectionItem
            
            item.bgView.setRoundWithCommonR()
            
            return item
        }
        else if(indexPath.row < projectRefImage[projectImage_tag].count){
            let items = collectionView.dequeueReusableCell(withReuseIdentifier: "projectImage", for: indexPath) as! projectimageCollectionItem
            
            let Dictimage =  projectRefImage[projectImage_tag][indexPath.row]
            items.image.image = Dictimage
            items.image.setRoundWithCommonR()
            
            return items
        }
        else {
            let item = collectionView.dequeueReusableCell(withReuseIdentifier: "addProject", for: indexPath) as! addCollectionItem
            
            item.bgView.setRoundWithCommonR()
            
            return item
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.bounds.height-10 , height: collectionView.bounds.height-10 )
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        let projectImage_tag = collectionView.tag - 300
        
        if(projectRefImage.isEmpty){
            
            projectRefImage.append(Array<UIImage>())
            
            //            self.WherePickerDrop = "projectRef"
            self.projectRefIndexRow = projectImage_tag
            
            let imagePicker = OpalImagePickerController()
            imagePicker.maximumSelectionsAllowed = 15
            imagePicker.imagePickerDelegate = self
            present(imagePicker, animated: true, completion: nil)
        }
        
        if(indexPath.row >= projectRefImage[projectImage_tag].count){
            
            //            self.WherePickerDrop = "projectRef"
            self.projectRefIndexRow = projectImage_tag
            
            let imagePicker = OpalImagePickerController()
            imagePicker.imagePickerDelegate = self
            imagePicker.maximumSelectionsAllowed = 15
            present(imagePicker, animated: true, completion: nil)
        }
        else{
            
        }
    }
}

extension CreateStepWorkViewController : OpalImagePickerControllerDelegate {
    
    func imagePicker(_ picker: OpalImagePickerController, didFinishPickingImages images: [UIImage]) {
        
        var imageArray = projectRefImage[projectRefIndexRow]
        
        for image in images{
            imageArray.append(image)
        }
        
        if(projectRefImage.isEmpty){
            self.projectRefImage.append(imageArray)
        }else{
            self.projectRefImage[projectRefIndexRow] = imageArray
        }
        
        self.tableView.reloadData()
        self.dismiss(animated: true, completion: nil)
    }
    
    func imagePickerDidCancel(_ picker: OpalImagePickerController){
        self.dismiss(animated: true, completion: nil)
    }
}

class projectRefAddStep : UITableViewCell {
    @IBOutlet weak var titleText: UITextField!
    @IBOutlet weak var dateText: UITextField!
    @IBOutlet weak var detailText: UITextField!
    @IBOutlet weak var imageCollection: UICollectionView!
    @IBOutlet weak var bgView: UIView!
}

extension projectRefAddStep {
    func setCollectionViewDataSourceDelegate<D: UICollectionViewDataSource & UICollectionViewDelegate>(_ dataSourceDelegate: D, forRow row: Int) {
        
        imageCollection.delegate = dataSourceDelegate
        imageCollection.dataSource = dataSourceDelegate
        imageCollection.setContentOffset(imageCollection.contentOffset, animated:false)
        imageCollection.reloadData()
    }
    
    var collectionViewOffset: CGFloat {
        set { imageCollection.contentOffset.x = newValue }
        get { return imageCollection.contentOffset.x }
    }
}

extension CreateStepWorkViewController : UITextFieldDelegate {
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        
        if(textField.tag >= 3200 && textField.tag <= 3299){
            let indexPart = textField.tag - 3200
            projectRefName[indexPart] = textField.text!
        }
        else if(textField.tag >= 3300 && textField.tag <= 3399){
            
            let indexPart = textField.tag - 3300
            projectRefDetail[indexPart] = textField.text!
        }
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if(textField.tag >= 3100 && textField.tag <= 3199){
            textField.resignFirstResponder()
            self.view.endEditing(true)
            
        }
        
        return true
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if(textField.tag >= 3100 && textField.tag <= 3199){
            let indexPart = textField.tag - 3100
            
            textField.resignFirstResponder()
            self.view.endEditing(true)
            
            let datePicker = ActionSheetDatePicker(title: "Project Date", datePickerMode: UIDatePicker.Mode.date, selectedDate: Date(), doneBlock: {
                picker, value, index in
                
                let dateValue = value as! Date
                let isoDateFormated = dateValue.toISO()
                let showDate = dateValue.toFormat("MMM yyyy")
                
                self.projectDateTime[indexPart] = isoDateFormated
                textField.text = showDate
                textField.resignFirstResponder()
                
                return
            }, cancel: { ActionStringCancelBlock in return }, origin: (textField as AnyObject).superview!?.superview)
            
            datePicker?.maximumDate = Date()
            datePicker?.show()
        }
    }
}
