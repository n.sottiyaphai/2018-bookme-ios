//
//  CreateStepPackViewController.swift
//  bookme
//
//  Created by Naphat Sottiyaphai on 24/2/2562 BE.
//  Copyright © 2562 Naphat Sottiyaphai. All rights reserved.
//

import UIKit
import Alamofire
import SVProgressHUD
import ActionSheetPicker_3_0

class CreateStepPackViewController: UIViewController {
    
    @IBOutlet weak var nextButt: UIButton!
    @IBOutlet weak var tableView: UITableView!
    
    // package Declaration
    var packageName = [""]
    var packageHours = [""]
    var packagePrices = [""]
    var packUnit = [0]
    var packageType = [0]
    
    var professional = NSDictionary()
    var professID = String()
    
    class func instantiateFromStoryboard() -> CreateStepPackViewController {
        let storyboard = UIStoryboard(name: "profile", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: String(describing: self)) as! CreateStepPackViewController
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.nextButt.setRoundedwithR(r: commonValue.subTadius)
        self.getProfess()             // <- Open this for real testing
        
        self.setClearNavBar()
        self.setBackgroundColor()
    }
    
    @IBAction func nextStep(_ sender: Any) {
        
        print(self.packageType)
        print(self.packageName)
        print(self.packageHours)
        print(self.packagePrices)
        print(self.packUnit)
        
        self.updateProfess()
        
        //        let vc = CreateStepConditionViewController.instantiateFromStoryboard()
        //        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
    func getProfess() {
        
        let ud = UserDefaults.standard
        var accesstoken = String()
        
        if(ud.object(forKey: "accessToken") != nil){
            accesstoken = ud.value(forKey: "accessToken") as! String
        }
        
        let header : HTTPHeaders = [
            "accessToken" : accesstoken
        ]
        
        print(self.professID)
        Alamofire.request(baseURL.url + "professional/\(self.professID)", method: .get, encoding: JSONEncoding.default , headers : header)
            .responseJSON(completionHandler: { response in
                
                if let responses = response.result.value {
                    let responseData = responses as! NSDictionary
                    let data = responseData.value(forKey: "data") as! NSDictionary
                    
                    self.professional = data
                    print("collect profess complete")
                }
                else{
                    SVProgressHUD.showError(withStatus: response.error?.localizedDescription)
                }
            })
    }
    
    func updateProfess() {
        
        print("Updateing profess")
        _ = professional.value(forKey: "portfolio") as! NSDictionary
        
        let ud = UserDefaults.standard
        var accesstoken = String()
        
        if(ud.object(forKey: "accessToken") != nil){
            accesstoken = ud.value(forKey: "accessToken") as! String
        }
        
        let header : HTTPHeaders = [
            "accessToken" : accesstoken
        ]
        
        var package = [Dictionary<String,Any>]()
        var packRef = 0
        for _ in packageName{
            
            var dict = Dictionary<String,Any>()
            
            //packageBody Release 1.0
            dict["packageType"] = self.packageType[packRef]
            dict["packageName"] = self.packageName[packRef]
            dict["packageTime"] = self.packageHours[packRef]
            dict["packagePrice"] = self.packagePrices[packRef]
            dict["packageTimeUnit"] = self.packUnit[packRef]
            
            // Feature Func
            dict["packageDetail"] = ""
            dict["packageImages"] = ""
            dict["packageConditions"] = ""
            
            package.append(dict)
            packRef = packRef + 1
        }
        
        package = package.flatMap{$0}
         print(package)
        
        let parameters: [String: Any] = [
            "packages" : package
        ]
        
        print(parameters)
        
        Alamofire.request(baseURL.url + "professional/\(self.professID)", method: .put, parameters: parameters, encoding: JSONEncoding.default , headers: header).responseJSON { (response) in
            
            if let result = response.result.value {
                print(result)
                let vc = CreateStepConditionViewController.instantiateFromStoryboard()
                vc.professID = self.professID
                self.navigationController?.pushViewController(vc, animated: true)
            }
            else{
                SVProgressHUD.showError(withStatus: response.error?.localizedDescription)
            }
        }
    }
}

extension CreateStepPackViewController : UITableViewDataSource , UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return packageName.count + 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if(indexPath.row < packageName.count){
            let cell = tableView.dequeueReusableCell(withIdentifier: "packageCell", for: indexPath) as! createPackageAddCell
            
            cell.packHours.setRoundWithCommonRwithBGC_f8()
            cell.packName.setRoundWithCommonRwithBGC_f8()
            cell.packPrice.setRoundWithCommonRwithBGC_f8()
            cell.packageUnit.setRoundWithCommonRwithBGC_f8()
            
            cell.packHours.setLeftPaddingPoints(15)
            cell.packName.setLeftPaddingPoints(15)
            cell.packPrice.setLeftPaddingPoints(15)
            cell.packageUnit.setLeftPaddingPoints(15)
            
            cell.packHours.setText()
            cell.packName.setText()
            cell.packPrice.setText()
            cell.packageUnit.setText()
            
            cell.packHours.delegate = self
            cell.packName.delegate = self
            cell.packPrice.delegate = self
            cell.packageUnit.delegate = self
            
            cell.packHours.tag = 4100 + indexPath.row
            cell.packName.tag = 4200 + indexPath.row
            cell.packPrice.tag = 4300 + indexPath.row
            cell.packageUnit.tag = 4400 + indexPath.row
            
            cell.packHours.text = packageHours[indexPath.row]
            cell.packName.text = packageName[indexPath.row]
            cell.packPrice.text = packagePrices[indexPath.row]
            
            switch packUnit[indexPath.row]{
            case 1 :
                cell.packageUnit.text = "Days"
                break
            case 2 :
                cell.packageUnit.text = "Hours"
                break
            case 3 :
                cell.packageUnit.text = "Project"
                break
            default :
                cell.packageUnit.text = "Days"
                break
            }
            
            cell.bgView.setRoundWithCommonR()
            
            return cell
        }
        else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "addObject", for: indexPath) as! addOCreatebjectCell
            
            cell.titleTet.text = "Add a Package"
            cell.bgView.setRoundWithCommonR()
            
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if(indexPath.row < self.packageName.count){
            return 151
        }
        else{
            return 60
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if(indexPath.row >= self.packageName.count){
            packageName.append("")
            packageHours.append("")
            packagePrices.append("")
            packUnit.append(0)
            packageType.append(0)
            
            self.tableView.reloadSections(IndexSet(integer: indexPath.section), with: .none)
        }
    }
}

extension CreateStepPackViewController : UITextFieldDelegate {
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        // Tag CASE 4
        if(textField.tag >= 4100 && textField.tag <= 4199){
            let indexPart = textField.tag - 4100
            packageHours[indexPart] = textField.text!
        }
        else if(textField.tag >= 4200 && textField.tag <= 4299){
            
            let indexPart = textField.tag - 4200
            packageName[indexPart] = textField.text!
        }
            
        else if(textField.tag >= 4300 && textField.tag <= 4399){
            let indexPart = textField.tag - 4300
            packagePrices[indexPart] = textField.text!
        }
        else if(textField.tag >= 4400 && textField.tag <= 4499){
            let indexPart = textField.tag - 4400
            
            textField.resignFirstResponder()
            //            packagePrices[indexPart] = newString
        }
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if(textField.tag >= 4400 && textField.tag <= 4499){
            let indexPart = textField.tag - 4400
            
            textField.resignFirstResponder()
            self.view.endEditing(true)
            
            let unitPicker = ActionSheetStringPicker.init(title: "Select Time Unit", rows: ["Days" , "Hours" , "Project"], initialSelection: 1, doneBlock: {
                (picker, index, value) in
                
                if let type = value{
                    textField.text = "\(type)"
                }
                
                if(index == 2){
                    self.packUnit[indexPart] = 2
                }
                else{
                    self.packUnit[indexPart] = 1
                }
                
                self.packUnit[indexPart] = index + 1
                
                textField.resignFirstResponder()
                self.view.endEditing(true)
                
            }, cancel: { (picker) in
                print("cancel")
            }, origin: (textField as AnyObject).superview!?.superview)
            
            unitPicker?.show()
        }
    }
}

class createPackageAddCell : UITableViewCell {
    

    @IBOutlet weak var packName: UITextField!
    @IBOutlet weak var packHours: UITextField!
    @IBOutlet weak var packageUnit: UITextField!
    @IBOutlet weak var packPrice: UITextField!
    @IBOutlet weak var bgView: UIView!
    
    //    @IBOutlet var packPrice: UITextField!
//    @IBOutlet var packHours: UITextField!
//    @IBOutlet weak var packageUnit: UITextField!
    
}
