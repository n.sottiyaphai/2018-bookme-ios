//
//  AppSettingTableViewController.swift
//  bookme
//
//  Created by Naphat Sottiyaphai on 10/6/2561 BE.
//  Copyright © 2561 Naphat Sottiyaphai. All rights reserved.
//

import UIKit
import ActionSheetPicker_3_0
import UIKit
import SDWebImage
import SVProgressHUD
import Alamofire
import GoogleSignIn
import Firebase
import SwiftHash

class AppSettingTableViewController: UITableViewController {

    let lang = ["ภาษาไทย" , "English"]
    
    @IBOutlet weak var langBg: UIView!
    @IBOutlet weak var lang_content: UILabel!
    @IBOutlet weak var lang_label: UILabel!
    
    @IBOutlet weak var logout_label: UILabel!
    @IBOutlet weak var logoutBG: UIView!
    
    @IBOutlet weak var notification_label: UILabel!
    @IBOutlet weak var notification_Switch: UISwitch!
    @IBOutlet weak var notiBG: UIView!
    
    class func instantiateFromStoryboard() -> AppSettingTableViewController {
        let storyboard = UIStoryboard(name: "profile", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: String(describing: self)) as! AppSettingTableViewController
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.initData()
        self.title = "Setting"
        
        self.setBackgroundColor()
        
        langBg.setRoundOnlyTop(r: commonValue.viewRadius)
        notiBG.setRoundOnlyDown(r: commonValue.viewRadius)
        
        logoutBG.setRoundWithCommonR()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.navigationController?.navigationBar.isHidden = false
        
        
        if #available(iOS 11.0, *) {
            UINavigationBar.appearance().prefersLargeTitles = true
        } else {
            print("It's down of iOS 11")
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        UINavigationBar.appearance().prefersLargeTitles = false
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    //    MARK:- Tableview Delegate
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 85
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if(indexPath.row == 0){
            ActionSheetStringPicker.show(withTitle: "", rows: lang, initialSelection: 0, doneBlock: {
                picker, index, value in
             
                let userDefault = UserDefaults.standard
                
                if(index == 0){
                    userDefault.set("th-TH" , forKey: "langDevice")
                }else if (index == 1){
                    userDefault.set("en-EN" , forKey: "langDevice")
                }
                
                self.initData()
                
                
                return
            }, cancel: { ActionStringCancelBlock in return }, origin: tableView)
        }
        
        else if (indexPath.row == 2){
            print("logout")
            
            let alert = UIAlertController.init(title: "Are you sure ?", message: "Do you want to log out ?", preferredStyle: .alert)
            let yes = UIAlertAction.init(title: "Yes", style: .default , handler: {(alert: UIAlertAction!) in
                
//                let api = Instagram.shared
//                api.logout()
                
                GIDSignIn.sharedInstance().signOut()
                
                let domain = Bundle.main.bundleIdentifier!
                UserDefaults.standard.removePersistentDomain(forName: domain)
                UserDefaults.standard.synchronize()
                
                let vc = LoginViewController.instantiateFromStoryboard()
                vc.modalPresentationStyle = .fullScreen
                self.navigationController?.present(vc, animated: true, completion: nil)
                
            })
            
            let No = UIAlertAction.init(title: "No", style: .cancel, handler: nil)
            
            alert.addAction(yes)
            alert.addAction(No)
            
            self.present(alert, animated: true, completion: nil)
        }     
    }
    
    override func scrollViewDidScroll(_ scrollView: UIScrollView) {
        var offest = scrollView.contentOffset.y / 150
        if(offest > 1){
            offest = 1
            self.navigationController?.navigationBar.backgroundColor = UIColor(red: 1, green: 1, blue: 1, alpha: offest)
//            UIApplication.shared.statusBar?.backgroundColor = UIColor(red: 1, green: 1, blue: 1, alpha: offest)
        }
        else{
            self.navigationController?.navigationBar.backgroundColor = UIColor(red: 1, green: 1, blue: 1, alpha: offest)
//            UIApplication.shared.statusBar?.backgroundColor = UIColor(red: 1, green: 1, blue: 1, alpha: offest)
        }
    }
    
    func initData() {
        let userDefault = UserDefaults.standard
        let lang = userDefault.value(forKey: "langDevice") as! String
        
        if(lang == "th-TH"){
            self.lang_label.text = thai.language
            self.notification_label.text = thai.notification
            self.logout_label.text = thai.logout
            
            self.lang_content.text = "ภาษาไทย"
        }
        else{
            self.lang_label.text = english.language
            self.notification_label.text = english.notification
            self.logout_label.text = english.logout
            
            self.lang_content.text = "English"
        }
    }
    
    func firebaseLogout(uid : String) {
        let body : [String : Any] = [
            "userID" : uid
        ]
        
        Alamofire.request("https://us-central1-bookme-1515492592081.cloudfunctions.net/logoutNoti", method: .post, parameters: body, encoding: JSONEncoding.default).responseJSON { (result) in
            if let resposne = result.result.value{
                print("logout success")
            }
            else{
                print("unsuccess")
            }
        }
    }

}
