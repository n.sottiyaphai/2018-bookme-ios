//
//  EnterTextViewController.swift
//  bookme4
//
//  Created by Naphat Sottiyaphai on 19/4/2561 BE.
//  Copyright © 2561 Naphat Sottiyaphai. All rights reserved.
//

import UIKit
import KMPlaceholderTextView

protocol EnterTextViewDelegate
{
    func sendBackText(title : String,text : String , conditionIndex : Int)
}

class EnterTextViewController: UIViewController {

    class func instantiateFromStoryboard() -> EnterTextViewController {
        let storyboard = UIStoryboard(name: "profile", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: String(describing: self)) as! EnterTextViewController
    }
    
    var conditionIndexs = Int()
    var oldText = String()
    var delegate: EnterTextViewDelegate?
    var titleText = String()
    
    @IBOutlet weak var svae: UIButton!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var textfield: KMPlaceholderTextView!
    
    @IBAction func SaveAction(_ sender: Any) {
        self.view.endEditing(true)

        let about = self.textfield.text!
        self.delegate?.sendBackText(title : titleText ,text : about, conditionIndex: conditionIndexs)
        
        self.dismiss(animated: true, completion: nil)
        self.navigationController?.popViewController(animated: true)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        textfield.becomeFirstResponder()
        textfield.clipsToBounds = true
        self.titleLabel.text = titleText
        self.textfield.text = self.oldText
        svae.setRoundWithR(r: commonValue.viewRadius)
        
        let tab : UITapGestureRecognizer = UITapGestureRecognizer.init(target: self, action: #selector(self.dismissKeyboard))
        view.addGestureRecognizer(tab)
        
        let pan : UIPanGestureRecognizer = UIPanGestureRecognizer.init(target: self, action: #selector(self.dismissKeyboard))
        view.addGestureRecognizer(pan)

    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        if #available(iOS 11.0, *) {
            self.automaticallyAdjustsScrollViewInsets = true
            self.navigationController?.navigationBar.isHidden = false
        } else {
            self.automaticallyAdjustsScrollViewInsets = false
        }
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }

}

