//
//  changePasswordViewController.swift
//  bookme
//
//  Created by Naphat Sottiyaphai on 23/9/2561 BE.
//  Copyright © 2561 Naphat Sottiyaphai. All rights reserved.
//

import UIKit
import ACFloatingTextfield_Swift
import SVProgressHUD
import Alamofire

class changePasswordViewController: UIViewController {
    
    @IBOutlet weak var mainButton: UIButton!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var confirmPassword: UITextField!
    @IBOutlet weak var newPassword: UITextField!
    @IBOutlet weak var oldPassword: UITextField!
    @IBOutlet weak var bgView: UIView!
    
    var matchPassword = false
    
    class func instantiateFromStoryboard() -> changePasswordViewController {
        let storyboard = UIStoryboard(name: "profile", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: String(describing: self)) as! changePasswordViewController
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        confirmPassword.delegate = self
        newPassword.delegate = self
        
        confirmPassword.setLeftPaddingPoints(10)
        confirmPassword.setRoundWithR(r: commonValue.viewRadius)
        
        newPassword.setLeftPaddingPoints(10)
        newPassword.setRoundWithR(r: commonValue.viewRadius)
        
        oldPassword.setLeftPaddingPoints(10)
        oldPassword.setRoundWithR(r: commonValue.viewRadius)
        
        mainButton.setRoundWithR(r: commonValue.viewRadius)
        
        setBackgroundColor()
        bgView.setRoundWithR(r: commonValue.viewRadius)
        self.title = "Change Password"
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.navigationController?.navigationBar.isHidden = false
    
        if #available(iOS 11.0, *) {
            navigationController?.navigationBar.prefersLargeTitles = true
        } else {
            print("It's down of iOS 11")
        }
        
        scrollView.contentInsetAdjustmentBehavior = .automatic
    }
    
    func callBluweoCahngePassword(old_password : String , new_password : String) {
        
        let url = "\(baseURL.bluweoConnectPassword)change"
        
        let userDefaults = UserDefaults.standard
        let bluweoAccessToken = userDefaults.value(forKey: "bluweoAccessToken") as! String
        
        let headers = [
            "client_id" : "1001",
            "secret_key" : "bookme",
            "access_token" : bluweoAccessToken,
            "Content-Type": "application/x-www-form-urlencoded"
        ]
        let params = [
            "old_password" : old_password,
            "new_password" : new_password
        ]
        
        Alamofire.request(URL(string: url)!, method: .put , parameters: params, encoding: URLEncoding(destination: .httpBody) , headers: headers).responseJSON(completionHandler: { (respond) in
            
            if let result = respond.result.value{
                let responsed = result as! NSDictionary
                print(responsed)
                
                let response = responsed.value(forKey: "response") as! Bool
                if(response){
                    
                    SVProgressHUD.showSuccess(withStatus: "Changed password success")
                    DispatchQueue.main.asyncAfter(deadline: .now() + 2){
                        SVProgressHUD.dismiss()
                        self.navigationController?.popViewController(animated: true)
                    }
                }
                else{
                    let data = responsed.value(forKey: "data") as! NSDictionary
                    let message = data.value(forKey: "message") as! NSArray
                    let msg = message[0] as! NSDictionary
                    let errMsg = msg.value(forKey: "message") as! String
                    print(errMsg)
                    
                    SVProgressHUD.showError(withStatus: errMsg)
                    DispatchQueue.main.asyncAfter(deadline: .now() + 2){
                        SVProgressHUD.dismiss()
                    }
                }
            }
            else{
                print(respond.error?.localizedDescription)
                SVProgressHUD.dismiss()
            }
        })
    }
    
    @IBAction func changePassword(_ sender: Any) {
        if(self.matchPassword){
            self.callBluweoCahngePassword(old_password: self.oldPassword.text!, new_password: self.newPassword.text!)
        }
        else{
            print("password not match")
        }
        
    }
    
}
extension changePasswordViewController : UITextFieldDelegate {
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if(textField == confirmPassword){
            
            if(self.newPassword.text == textField.text){
                print("Correct confirm")
                self.matchPassword = true
                
            }
            else{
                print("Confirm not match")
                
                SVProgressHUD.showError(withStatus: "Confirm not match")
                DispatchQueue.main.asyncAfter(deadline: .now() + 2){
                    SVProgressHUD.dismiss()
                }
                
//                self.newPassword.showErrorWithText(errorText: "Password not match")
//                self.confirmPassword.shakeLineWithError = true
//                self.confirmPassword.showErrorWithText(errorText: "Password not match")
            }
        }
    }
}
