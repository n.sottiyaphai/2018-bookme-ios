//
//  CreateStepBioViewController.swift
//  bookme
//
//  Created by Naphat Sottiyaphai on 24/2/2562 BE.
//  Copyright © 2562 Naphat Sottiyaphai. All rights reserved.
//

import UIKit
import Alamofire
import SVProgressHUD
import OpalImagePicker
import SwiftHash

class CreateStepBioViewController: UIViewController {
    
    var professional = NSDictionary()
    var professID = String()
    var imageGallery = [UIImage]()
    var imageGalleryName = [String]()
    var imageUploadStatus = false
    
    @IBOutlet weak var bioBgView: UIView!
    @IBOutlet weak var bioText: UITextField!
    //    @IBOutlet weak var bioText: UITextField!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var nextButt: UIButton!
    
    class func instantiateFromStoryboard() -> CreateStepBioViewController {
        let storyboard = UIStoryboard(name: "profile", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: String(describing: self)) as! CreateStepBioViewController
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let userDefaults = UserDefaults.standard
        professID = userDefaults.value(forKey: "profess_id_in_process") as! String
        
        self.nextButt.setRoundedwithR(r: commonValue.subTadius)
        
        DispatchQueue.main.async {
            self.getProfess()
        }
        // Do any additional setup after loading the view.
        
        self.bioBgView.setRoundWithCommonR()
        self.setClearNavBar()
        self.setBackgroundColor()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.navigationController?.navigationBar.isHidden = false
        
        if #available(iOS 11.0, *) {
            navigationController?.navigationBar.prefersLargeTitles = false
        } else {
            print("It's down of iOS 11")
        }
    }
    
    
    @IBAction func nextStep(_ sender: Any) {
        if(imageGallery.count > 0){
            self.ImageUploading()
        }
        else{
            print("Update with out image Cover")
            self.updateProfess()
        }
    }
    
    func getProfess() {
        
        let ud = UserDefaults.standard
        var accesstoken = String()
        
        if(ud.object(forKey: "accessToken") != nil){
            accesstoken = ud.value(forKey: "accessToken") as! String
        }
        
        let header : HTTPHeaders = [
            "accessToken" : accesstoken
        ]
        
        Alamofire.request(baseURL.url + "professional/\(self.professID)", method: .get, encoding: JSONEncoding.default , headers : header)
            .responseJSON(completionHandler: { response in
                
                if let responses = response.result.value {
                    let responseData = responses as! NSDictionary
                    let data = responseData.value(forKey: "data") as! NSDictionary
                    
                    self.professional = data
                    print("collect profess complete")
                }
                else{
                    SVProgressHUD.showError(withStatus: response.error?.localizedDescription)
                }
            })
    }
    
    //    MARK:- Bluweo Media Uploading
    func ImageUploading() {
        
        let preloadGallery = imageGallery.map{ $0.jpegData(compressionQuality: commonValue.imageCompress)!}
        var data = [Data]()
        
        for imgData in preloadGallery{
            data.append(imgData)
        }
        
        let ud = UserDefaults.standard
        let uid = ud.value(forKey: "user_uid") as! String
        let accessToken = ud.value(forKey: "bluweoAccessToken") as! String
        
        let url = URL(string: "\(baseURL.bluweoMedia)upload/multiple")
        
        let headers = [
            "access_token" : accessToken,
            "accept": "application/x-www-form-urlencoded",
            "Content-Type" : "multipart/form-data"
        ]
        
        let parameters = [
            "api": "professional",
            "path" : "cover"
        ]
        
        print(url)
        print(headers)
        print(parameters)
        
        Alamofire.upload(multipartFormData: { (multipartFormData) in
            
            var index = 0
            
            for img in data {
                multipartFormData.append(img, withName: "files[\(index)]", fileName: "\(uid)_\(Date().timeIntervalSince1970)_Image.jpg", mimeType: "image/jpeg")
                index+=1
            }
            
            for (key, value) in parameters {
                multipartFormData.append(value.data(using: String.Encoding.utf8)!, withName: key)
            }
            
            print(multipartFormData)
            
        }, to: url!, method: .post, headers: headers) { (result) in
            
            switch result {
            case .success(let upload, _, _):
                
                upload.uploadProgress(closure: { (progress) in
                    SVProgressHUD.showProgress(Float(progress.fractionCompleted), status: "Image Uploading")
                })
                
                upload.responseJSON { response in
                    
                    if let result = response.result.value {
                        
                        let results = result as! NSDictionary
                        let ArryResult = results.value(forKey: "data") as! NSDictionary
                        
                        if(ArryResult.object(forKey: "files") != nil){
                            let filePath = ArryResult.value(forKey: "files") as! NSArray
                            
                            for path in filePath{
                                let imgPath = (path as! NSDictionary).value(forKey: "file")
                                print(imgPath)
                                
                                let fullURL = "\(baseURL.bluweoMedia)\(imgPath as! String)"
                                self.imageGalleryName.append(fullURL)
                            }
                            
                            self.imageUploadStatus = true
                            
                            DispatchQueue.main.async {
                                 SVProgressHUD.dismiss()
                                self.updateProfess()
                            }
                            
                            
                            //                            self.imageURL = "\(baseURL.bluweoMedia)\(filePath)"
//                            print("\(baseURL.bluweoMedia)\(filePath)")
                            
//
                        }
                        else{
                            print(ArryResult)
                            
                            if let message = ArryResult.value(forKey: "message"){
                                if(message is NSDictionary){
                                    if let errMsg = (message as! NSDictionary).value(forKey: "message"){
                                        print(errMsg as! Any)
                                    }
                                }
                                else{
                                    let message = ArryResult.value(forKey: "message") as! String
                                    print(message)
                                }
                            }
                            
                            self.refreshBluweoToken()
                            
                            SVProgressHUD.show()
                            DispatchQueue.main.asyncAfter(deadline: .now() + 3){
                                SVProgressHUD.dismiss()
                            }
                            
                        }
                    }
                }
            case .failure(let encodingError):
                print("ERROR : \(encodingError)")
                SVProgressHUD.showError(withStatus: "Media Connection Error : \(encodingError)")
                break
            }
        }
    }
    
    //    MARK :- Old Media
    
    func uploadImage () {
        
        let ud = UserDefaults.standard
        let uid = ud.value(forKey: "user_uid") as! String
        let md5_uid = MD5(uid)
        let url = URL(string: "\(baseURL.media)api/uploads/bookme")
        
        let headers: HTTPHeaders = [
            "Content-Type": "application/json",
            "appId": apiKey.appId,
            "clientId" : apiKey.clientId,
            "secretId" : apiKey.secretId
        ]
        
        let preloadGallery = imageGallery.map{ $0.jpegData(compressionQuality: commonValue.imageCompress)!}
        
        Alamofire.upload(multipartFormData: { (multipartFormData) in
            
            multipartFormData.append("\(uid)".data(using: String.Encoding.utf8)!, withName: "uId" as String)
            
            for images in preloadGallery {
                multipartFormData.append(images, withName: "imgFiles", fileName: "\(md5_uid)_swift_file.jpeg", mimeType: "image/jpeg")
            }
            
            print(multipartFormData)
            
        }, usingThreshold: UInt64.init() , to: url!, method: .post , headers: headers  , encodingCompletion: { (result) in
            print(result)
            
            switch result {
            case .success(let upload, _, _):
                
                upload.uploadProgress(closure: { (progress) in
                    SVProgressHUD.showProgress(Float(progress.fractionCompleted), status: "Uploading")
                })
                
                upload.responseJSON { response in
                    print(response)
                    if let result = response.result.value {
                        let results = result as! NSDictionary
                        if(results.value(forKey: "data") is Array<String>){
                            let ArryResult = results.value(forKey: "data") as! Array<String>
                            self.imageGalleryName = ArryResult
                            self.imageUploadStatus = true
                            
                            DispatchQueue.main.async {
                                self.updateProfess()
                            }
                        }
                        
                        SVProgressHUD.dismiss()
                        
                    }
                    else{
                        print("Error")
                        print(response.error?.localizedDescription ?? "")
                        SVProgressHUD.dismiss()
                        SVProgressHUD.showError(withStatus: response.error?.localizedDescription ?? "")
                    }
                }
                
            case .failure(let encodingError):
                print("ERROR : \(encodingError)")
                SVProgressHUD.showError(withStatus: "Media Connection Error : \(encodingError)")
                break
            }
        })
    }
    
    
    func updateProfess() {
        print("Updateing profess")
        
        let about = self.bioText.text!
        let portfolio = professional.value(forKey: "portfolio") as! NSDictionary
        
        let ud = UserDefaults.standard
        var accesstoken = String()
        
        if(ud.object(forKey: "accessToken") != nil){
            accesstoken = ud.value(forKey: "accessToken") as! String
        }
        
        let header : HTTPHeaders = [
            "accessToken" : accesstoken
        ]
        
        let parameters: [String: Any] = [
            "portfolio" : [
                "gallery" : self.imageGalleryName,
                "portfolioBio" : about,
                "instagram" : "",
                "conditions" : [],
                "calendar" : []
            ]
        ]
        
        print(parameters)
        
        Alamofire.request(baseURL.url + "professional/\(self.professID)", method: .put, parameters: parameters, encoding: JSONEncoding.default , headers: header).responseJSON { (response) in
            
            if let result = response.result.value {
                print(result)
                let vc = CreateStepWorkViewController.instantiateFromStoryboard()
                vc.professID = self.professID
                self.navigationController?.pushViewController(vc, animated: true)
            }
            else{
                SVProgressHUD.showError(withStatus: response.error?.localizedDescription)
            }
        }
    }
}

extension CreateStepBioViewController : UICollectionViewDelegate , UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return imageGallery.count + 1
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if(indexPath.row < imageGallery.count){
            let item = collectionView.dequeueReusableCell(withReuseIdentifier: "image", for: indexPath) as! imageCollectionItem
            
            item.image.image = imageGallery[indexPath.row]
            item.image.setRoundWithR(r: commonValue.subTadius)
            
            return item
        }
        else{
            let item = collectionView.dequeueReusableCell(withReuseIdentifier: "add", for: indexPath) as! addCollectionItem
            item.bgView.setRoundWithR(r: commonValue.subTadius)
            return item
        }
        
        return UICollectionViewCell()
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if(indexPath.row < imageGallery.count){
            // Delect old image
            imageGallery.remove(at: indexPath.row)
            self.collectionView.reloadData()
            print("delete done")
        }
        else{
            // Add image
            let imagePicker = OpalImagePickerController()
            imagePicker.imagePickerDelegate = self
            imagePicker.maximumSelectionsAllowed = 10
            
            present(imagePicker, animated: true, completion: nil)
        }
    }
}

extension CreateStepBioViewController : OpalImagePickerControllerDelegate {
    
    func imagePicker(_ picker: OpalImagePickerController, didFinishPickingImages images: [UIImage]) {
        print("did done")
        
        for img in images {
            self.imageGallery.append(img)
        }
        
        self.collectionView.reloadData()
        self.dismiss(animated: true, completion: nil)
    }
    
    func imagePickerDidCancel(_ picker: OpalImagePickerController){
        //        self.dismiss(animated: true, completion: nil)
    }
}
