////
////  selectCatViewController.swift
////  bookme4
////
////  Created by Naphat Sottiyaphai on 9/4/2561 BE.
////  Copyright © 2561 Naphat Sottiyaphai. All rights reserved.
////
//
//import UIKit
////import ExpandableCell
//import SVProgressHUD
//import Alamofire
//
////    ************************
////    ****** NOT IN USE ******
////    ************************
//
//class selectCatViewController: UIViewController  {
//    
//    var mode = String()
//    var cat = [String]()
//    var catID = [String]()
//    var subcat = [[Any]]()
//    
//    var catSelect = String()
//    var catIDSelect = String()
//    var subcatSelect = [String]()
//    
//    var SegueName = String()
//    var SegueimageProfileURL = String()
//    
//    private var lastCatIndex = IndexPath()
//    private var lastSection = Int()
//    private var isExpand = false
//    
//    var cell: UITableViewCell {
//        return tableView.dequeueReusableCell(withIdentifier: CatExpandCell.ID)!
//    }
//    
//    @IBOutlet weak var titleText: UILabel!
//    @IBOutlet weak var tableView: ExpandableTableView!
//    @IBOutlet weak var nextButton: UIButton!
//    
//    
//    class func instantiateFromStoryboard() -> selectCatViewController {
//        let storyboard = UIStoryboard(name: "profile", bundle: nil)
//        return storyboard.instantiateViewController(withIdentifier: String(describing: self)) as! selectCatViewController
//    }
//    
//    override func viewDidLoad() {
//        super.viewDidLoad()
//        
//        tableView.expandableDelegate = self
//        tableView.animation = .automatic
//        tableView.reloadData()
//        
//        tableView.register(UINib(nibName: "CatExpandCell", bundle: nil), forCellReuseIdentifier: CatExpandCell.ID)
//        tableView.register(UINib(nibName: "subCatExpand", bundle: nil), forCellReuseIdentifier: subCatExpand.ID)
//        
//        nextButton.setRoundedwithR(r: commonValue.subTadius)
//    }
//    
//    override func viewWillAppear(_ animated: Bool) {
//        super.viewWillAppear(true)
//        
//        navigationController?.interactivePopGestureRecognizer?.isEnabled = false
//        
//        self.subcatSelect.removeAll()
//        self.catIDSelect.removeAll()
//        self.catSelect.removeAll()
//        
//        DispatchQueue.main.async {
//            self.callCat()
//        }
//        
//        self.navigationController?.navigationBar.isHidden = false
//        
//    }
//    
//    override func didReceiveMemoryWarning() {
//        super.didReceiveMemoryWarning()
//    }
//    
//    func callCat() {
//        
//        let ud = UserDefaults.standard
//        var accesstoken = String()
//        
//        if(ud.object(forKey: "accessToken") != nil){
//            accesstoken = ud.value(forKey: "accessToken") as! String
//        }
//        
//        let header : HTTPHeaders = [
//            "accessToken" : accesstoken
//        ]
//        
//        self.cat.removeAll()
//        self.subcat.removeAll()
//        
//        Alamofire.request(baseURL.url + "categories", method: .get, encoding: JSONEncoding.default , headers : header)
//            .responseJSON(completionHandler: { response in
//                
//                if let result = response.result.value {
//                    
//                    let JSON = result as! NSDictionary
//                    
//                    let JsonResult = JSON.value(forKey: "response") as! Bool
//                    if(JsonResult){
//                        let data = JSON.value(forKey: "data") as! NSArray
//                        
//                        for dataInArr in data {
//                            let dictData = dataInArr as! NSDictionary
//                            let catName = dictData.value(forKey: "catName") as! String
//                            let subcat = dictData.value(forKey: "subCat") as! Array<Any>
//                            let catID = dictData.value(forKey: "_id") as! String
//                            
//                            self.subcat.append(subcat)
//                            self.cat.append(catName)
//                            self.catID.append(catID)
//                        }
//                    }
//                    DispatchQueue.main.async {
//                        SVProgressHUD.dismiss()
//                        self.tableView.reloadData()
//                    }
//                }
//                else{
//                    SVProgressHUD.showError(withStatus: "Network Error")
//                }
//            })
//    }
//    
//    @IBAction func go2regis(_ sender: Any) {
//        
//        if(catIDSelect.isEmpty || subcatSelect.isEmpty){
//            SVProgressHUD.showInfo(withStatus: "Please select one of category")
//        }
//        else{
//            
////            let vc = professRegisViewController.instantiateFromStoryboard()
////
////            vc.catName = self.catSelect
////            vc.subCatName = self.subcatSelect
////            vc.mode = self.mode
////            vc.catID = self.catIDSelect
////            vc.SegueName = self.SegueName
////            vc.SegueimageProfileURL = self.SegueimageProfileURL
////
////            self.navigationController?.pushViewController(vc, animated: true)
//            
//            let vc = selectSubCatViewController.instantiateFromStoryboard()
//            vc.subCat = subcat[0] as! [String]
//            self.navigationController?.pushViewController(vc, animated: true)
//            
//        }
//        
//    }
//}
//
//extension selectCatViewController: ExpandableDelegate {
//    
//    //Expanding Cell Init
//    func expandableTableView(_ expandableTableView: ExpandableTableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
//        print("Expand will display")
//    }
//    
//    func expandableTableView(_ expandableTableView: UITableView, didCloseRowAt indexPath: IndexPath) {
//        print("Close at \(indexPath.row)")
//    }
//    
//    
//    func expandableTableView(_ expandableTableView: ExpandableTableView, heightsForExpandedRowAt indexPath: IndexPath) -> [CGFloat]? {
//        let subcarArray = self.subcat[indexPath.row] as NSArray
//        var returnArray = [CGFloat]()
//        
//        if(subcarArray.count > 0){
//            for _ in subcarArray {
//                returnArray.append(44)
//            }
//            
//            return returnArray
//        }
//        return [CGFloat]()
//    }
//    
//    func expandableTableView(_ expandableTableView: ExpandableTableView, expandedCellsForRowAt indexPath: IndexPath) -> [UITableViewCell]? {
//        let subcarArray = self.subcat[indexPath.section] as NSArray
//        var returnArray = [UITableViewCell]()
//        
//        if(subcarArray.count > 0){
//            for subcat in subcarArray {
//                let string = subcat as! String
//                let cell = tableView.dequeueReusableCell(withIdentifier: subCatExpand.ID) as! subCatExpand
//                cell.titleLabel.text = string
//                
//                cell.BEMCheck.tag = indexPath.row
//                cell.BEMCheck.setOn(false, animated: false)
//                
//                returnArray.append(cell)
//            }
//            
//            return returnArray
//        }
//        
//        return [UITableViewCell]()
//    }
//    
//    func expandableTableView(_ expandableTableView: ExpandableTableView, didSelectExpandedRowAt indexPath: IndexPath) {
//        let cell = expandableTableView.cellForRow(at: indexPath) as! subCatExpand
//        let selectSub = cell.titleLabel.text
//        
//        cell.BEMCheck.isHidden = false
//        
//        if(cell.BEMCheck.on){
//            
//            if let index = subcatSelect.index(of: selectSub!) {
//                subcatSelect.remove(at: index)
//            }
//            cell.BEMCheck.setOn(false, animated: true)
//        }
//        else{
//            self.subcatSelect.append(selectSub!)
//            cell.BEMCheck.setOn(true, animated: true)
//        }
//        
//    }
//    
//    func expandableTableView(_ expandableTableView: UITableView, shouldHighlightRowAt indexPath: IndexPath) -> Bool {
//        return true
//    }
//    
//    
//    
//    
//    // Normal Cell Init
//    func numberOfSections(in expandableTableView: ExpandableTableView) -> Int {
//        return self.cat.count
//    }
//    
//    func expandableTableView(_ expandableTableView: ExpandableTableView, numberOfRowsInSection section: Int) -> Int {
//        return 1
//    }
//    
//    func expandableTableView(_ expandableTableView: ExpandableTableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
//        return 50
//    }
//    
//    func expandableTableView(_ expandableTableView: ExpandableTableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
//        
//        let cell = expandableTableView.dequeueReusableCell(withIdentifier: CatExpandCell.ID)
//        
//        cell?.textLabel?.text = self.cat[indexPath.section]
//        cell?.textLabel?.font = UIFont.boldSystemFont(ofSize: (cell?.textLabel?.font.pointSize)!)
//        
//        return cell!
//    }
//    
//    func expandableTableView(_ expandableTableView: ExpandableTableView, didSelectRowAt indexPath: IndexPath) {
//        if(lastSection == Int()){
//            self.tableView.closeAll()
//            self.catSelect = self.cat[indexPath.section]
//            self.catIDSelect = self.catID[indexPath.section]
//        }
//        else{
//            self.catSelect = self.cat[indexPath.section]
//            self.catIDSelect = self.catID[indexPath.section]
//            self.tableView.close(at: IndexPath.init(row: 0 , section: lastSection))
//        }
//        
//        lastSection = indexPath.section
//    }
//}
//
