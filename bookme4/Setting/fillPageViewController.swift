//
//  fillPageViewController.swift
//  bookme
//
//  Created by Naphat Sottiyaphai on 26/9/2561 BE.
//  Copyright © 2561 Naphat Sottiyaphai. All rights reserved.
//

import UIKit

protocol fillPageViewControllerProtocal {
    func viewDidLoad()
    func prepare(for segue: UIStoryboardSegue, sender: Any?)
}

class fillPageViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
}
