//
//  balanceViewController.swift
//  bookme
//
//  Created by Naphat Sottiyaphai on 14/11/2561 BE.
//  Copyright © 2561 Naphat Sottiyaphai. All rights reserved.
//

import UIKit
import Alamofire

class balanceViewController: UIViewController {

    class func instantiateFromStoryboard() -> balanceViewController {
        let storyboard = UIStoryboard(name: "profile", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: String(describing: self)) as! balanceViewController
    }
    
    
    @IBOutlet weak var mainButton: UIButton!
    @IBOutlet weak var drawBG: UIView!
    @IBOutlet weak var pendingBG: UIView!
    @IBOutlet weak var allBalanceBG: UIView!
    @IBOutlet weak var bgView: UIView!
    @IBOutlet weak var paidBalance: UIView!
    @IBOutlet weak var paidText: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        mainButton.setRoundWithCommonR()
        
        allBalanceBG.setRoundWithCommonR()
        drawBG.setRoundWithCommonR()
        paidBalance.setRoundWithCommonR()
        pendingBG.setRoundWithCommonR()
        
        allBalanceBG.setRoundWithCommonR()
        drawBG.setRoundWithCommonR()
        paidBalance.setRoundWithCommonR()
        pendingBG.setRoundWithCommonR()
        
        drawBG.backgroundColor = UIColor(red: 45.0/255.0, green: 191.0/255.0, blue: 255.0/255.0, alpha: 0.6)
        paidBalance.backgroundColor = UIColor(red: 45.0/255.0, green: 191.0/255.0, blue: 255.0/255.0, alpha: 0.6)
        pendingBG.backgroundColor = UIColor(red: 45.0/255.0, green: 191.0/255.0, blue: 255.0/255.0, alpha: 0.6)
        
        
        //        drawBG.backgroundColor = commonColor.onlineGreen
//        pendingBG.backgroundColor = commonColor.mainYellow
        
        
        // Do any additional setup after loading the view.
        
        DispatchQueue.main.async {
            self.callMybalance()
        }
    
        self.title = "My Balance"
        setBackgroundColor()
        bgView.setRoundWithR(r: commonValue.viewRadius)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.navigationController?.navigationBar.isHidden = false
        UINavigationBar.appearance().prefersLargeTitles = true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        UINavigationBar.appearance().prefersLargeTitles = false
    }
    
    func callMybalance() {
        let ud = UserDefaults.standard
        let accesstoken = ud.value(forKey: "accessToken") as! String
        let uid = ud.value(forKey: "user_uid") as! String
        
        let header : HTTPHeaders = [
            "accessToken" : accesstoken
        ]
        
        let url = "\(baseURL.url)balance/uid/\(uid)"
        
        Alamofire.request(URL(string: url)!, method: .get, encoding: JSONEncoding.default, headers: header).responseJSON { (response) in
            
            if let result = response.result.value{
                let responses = result as! NSDictionary
                print(responses)
            
                
            }
            
        }
    }

}
