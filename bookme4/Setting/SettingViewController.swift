//
//  SettingViewController.swift
//  bookme4
//
//  Created by Naphat Sottiyaphai on 24/3/2561 BE.
//  Copyright © 2561 Naphat Sottiyaphai. All rights reserved.
//

import UIKit
import SDWebImage
import SVProgressHUD
import Alamofire
import GoogleSignIn
import Firebase
import SwiftHash

class SettingViewController: UIViewController {
    
    @IBOutlet var aboutButton: UIButton!
    @IBOutlet var collectionView : UICollectionView!
    @IBOutlet var feedbackButton: UIButton!
    @IBOutlet var helpButton: UIButton!
    @IBOutlet var imageProfile: UIImageView!
    @IBOutlet var mainButton: UIButton!
    @IBOutlet var profileDest: UILabel!
    @IBOutlet var profileName: UILabel!
    @IBOutlet var tableView : UITableView!
    @IBOutlet weak var editProfileButton: UIButton!
    @IBOutlet weak var editProfilePic: UIButton!
    
    var Profess_menu         = [String]()
    var RefreshCount         = 0
    var SegueName            = String()
    var SegueimageProfileURL = String()
    var imageURL             = String()
    var indexCount           = 2
    var menu                 = [String]()
    var mode                 = 1
    var portId               = [NSDictionary]()
    var profileDocument      = NSDictionary()
    var userProfileData      = NSDictionary()
    var userUID              = String()
    
    let Profess_iconMenu     = ["icon_profile" , "icon_payment" , "icon_bank" , "icon_idcard" , "icon_lock" , "icon_setting" ]
    let Profess_menu_en      = ["My Account" , "Payment" , "Bank Account" , "ID Card"  , "Change Password" , "Setting"]
    let Profess_menu_th      = ["บัญชีของฉัน" , "การชำระเงิน" , "ยืนยันบัญชีธนาคาร" , "ยืนยันบัตรประชาชน" ,"เปลี่ยนรหัสผ่าน" , "ตั้งค่า" ]
    let iconMenu             = ["icon_profile" , "icon_payment" , "icon_lock" , "icon_setting"]
    let menu_en              = ["My Account" , "Payment" , "Change Password" , "Setting" ]
    let menu_th              = ["ตัวฉัน" , "การชำระเงิน" , "เปลี่ยนรหัสผ่าน" , "ตั้งค่า"]
    
    //    MARK:- LIFE CYCLE
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let userDefault = UserDefaults.standard
        let uid = userDefault.value(forKey: "user_uid") as! String
        
        self.userUID = uid
        self.checkUser()
        
        
        helpButton.setRoundedwithR(r: commonValue.viewRadius)
        feedbackButton.setRoundedwithR(r: commonValue.viewRadius)
        aboutButton.setRoundedwithR(r: commonValue.viewRadius)
        
        setBackgroundColor()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        DispatchQueue.main.async {
            self.initData()
            
            let userdefaults = UserDefaults.standard
            let isNonlogin = userdefaults.value(forKey: "isNonLogin") as! Bool
            if(isNonlogin){
                //prepare for login
                let vc = LoginViewController.instantiateFromStoryboard()
                self.present(vc, animated: true, completion: nil)
            }
            else{
                //set profile logined
                self.callBluweoProfile()
            }
            
            
        }
        
        if #available(iOS 11.0, *) {
            UINavigationBar.appearance().prefersLargeTitles = true
            self.navigationController?.navigationBar.isHidden = true
        } else {
            self.automaticallyAdjustsScrollViewInsets = false
        }
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        self.navigationController?.navigationBar.isHidden = false
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        sizeHeaderToFit()
    }
    
    func sizeHeaderToFit() {
        let headerView = tableView.tableHeaderView!
        
        headerView.setNeedsLayout()
        headerView.layoutIfNeeded()
        
        let height = headerView.systemLayoutSizeFitting(UIView.layoutFittingCompressedSize).height
        var frame = headerView.frame
        frame.size.height = height
        headerView.frame = frame
        
        tableView.tableHeaderView = headerView
    }
    
    //    MARK: -
    
    func initData() {
        let userDefault = UserDefaults.standard
        let lang = userDefault.value(forKey: "langDevice") as! String
        
        if(lang == "th-TH"){
            self.Profess_menu = self.Profess_menu_th
            self.menu = self.menu_th
            
            self.helpButton.setTitle(thai.help, for: .normal)
            self.feedbackButton.setTitle(thai.feedback, for: .normal)
            self.aboutButton.setTitle(thai.about, for: .normal)
            
            self.helpButton.setTitle(thai.help, for: .normal)
            self.feedbackButton.setTitle(thai.feedback, for: .normal)
            self.aboutButton.setTitle(thai.about, for: .normal)
            
        }
        else{
            self.Profess_menu = self.Profess_menu_en
            self.menu = self.menu_en
            
            self.helpButton.setTitle(english.help, for: .normal)
            self.feedbackButton.setTitle(english.feedback, for: .normal)
            self.aboutButton.setTitle(english.about, for: .normal)
            
        }
        
        self.tableView.reloadData()
        self.callProfile()
    }
    
    
    
    func callProfile() {
        
        let ud = UserDefaults.standard
        var accesstoken = String()
        
        if(ud.object(forKey: "accessToken") != nil){
            accesstoken = ud.value(forKey: "accessToken") as! String
        }
        
        let header : HTTPHeaders = [
            "accessToken" : accesstoken
        ]
        
        Alamofire.request(baseURL.url + "profile/\(self.userUID)", method: .get, encoding: JSONEncoding.default , headers : header)
            .responseJSON(completionHandler: { response in
                
                if let result = response.result.value {
                    let data = result as! NSDictionary
                    
                    var firstName = String()
                    var lastName = String()
                    var profilePhoto = String()
                    //                    var titles = String()
                    var document = NSDictionary()
                    
                    if(data.object(forKey: "data") != nil){
                        if(data.value(forKey: "data") is NSDictionary){
                            let JSON = data.value(forKey: "data") as! NSDictionary
                            firstName = JSON.value(forKey: "firstName") as! String
                            lastName = JSON.value(forKey: "lastName") as! String
                            profilePhoto = JSON.value(forKey: "profilePhoto") as! String
                            //                            titles = JSON.value(forKey: "title") as! String
                            document = JSON.value(forKey: "documents") as! NSDictionary
                            
                            self.userProfileData = JSON
                        }
                    }
                    else{
                        firstName = data.value(forKey: "firstName") as! String
                        lastName = data.value(forKey: "lastName") as! String
                        profilePhoto = data.value(forKey: "profilePhoto") as! String
                        //                        titles = data.value(forKey: "tittle") as! String
                        document = data.value(forKey: "documents") as! NSDictionary
                        
                        self.userProfileData = data
                        
                    }
                    
                    //                    let profileID = self.userProfileData.value(forKey: "profile_id") as! String
                    
                    // UI Init
                    self.profileName.text = "\(firstName) \(lastName)"
                    self.profileDest.text = "Edit Profile"
                    self.imageProfile.sd_setImage(with: URL(string: profilePhoto), placeholderImage: UIImage(named: "bookme_loading"))
                    self.imageProfile.setRounded()
                    
                    // Collect Data
                    
                    self.SegueName = "\(firstName) \(lastName)"
                    self.SegueimageProfileURL = profilePhoto
                    self.profileDocument = document
                    self.checkUser()
                    
                }
                else{
                    print("Request profile by uId : Fail")
                    SVProgressHUD.showError(withStatus: "Network Error")
                }
            })
    }
    
    
    func checkUser() {
        
        let ud = UserDefaults.standard
        var accesstoken = String()
        
        if(ud.object(forKey: "accessToken") != nil){
            accesstoken = ud.value(forKey: "accessToken") as! String
        }
        
        let header : HTTPHeaders = [
            "accessToken" : accesstoken
        ]
        
        //        SVProgressHUD.show(withStatus: "Profile Checking")
        self.portId.removeAll()
        
        let userDefault = UserDefaults.standard
        let uid = userDefault.value(forKey: "user_uid") as! String
        let lang = userDefault.value(forKey: "langDevice") as! String
        
        Alamofire.request(baseURL.url + "profile/\(uid)/professional", method: .get, encoding: JSONEncoding.default , headers : header)
            .responseJSON(completionHandler: { response in
                
                if let result = response.result.value {
                    
                    let JSON = result as! NSDictionary
                    
                    let JsonResult = JSON.value(forKey: "response") as! Bool
                    if(JsonResult){
                        let data = JSON.value(forKey: "data") as! NSArray
                        for dataInArr in data {
                            let data = dataInArr as! NSDictionary
                            self.portId.append(data)
                        }
                        
                        self.collectionView.reloadData()
                        
                        if(self.portId.count > 0){
                            
                            self.profileDest.text = "Edit Profile"
                            //                            self.profileDest.text = "You have \(self.portId.count) Portfolio."
                            self.mode = 2
                            self.tableView.reloadData()
                        }
                        else{
                            self.mode = 1
                            
                            self.profileDest.text = "Edit Profile"
                            //                            self.profileDest.text = "You don't have a Portfolio."
                            self.tableView.reloadData()
                        }
                    }
                    
                    SVProgressHUD.dismiss()
                }
                else{
                    self.mode = 1
                    SVProgressHUD.dismiss()
                }
            })
        
    }
    
    func callBluweoProfile() {
        
        print("Call Bluweo profile")
        
        let userDefaults = UserDefaults.standard
        let accessToken = userDefaults.value(forKey: "bluweoAccessToken") as! String
        
        let url = "\(baseURL.bluweoProfile)/get?access_token=\(accessToken)"
        
        let headers = [
            "client_id" : "1001",
            "secret_key" : "bookme",
            "Content-Type": "application/x-www-form-urlencoded"
        ]
        
        Alamofire.request(URL(string: url)!, method: .get , encoding: URLEncoding(destination: .httpBody) , headers: headers).responseJSON(completionHandler: { (respond) in
            
            if let result = respond.result.value{
                let responsed = result as! NSDictionary
                let response = responsed.value(forKey: "response") as! Bool
                
                if(response){
                    let data = responsed.value(forKey: "data") as! NSDictionary
                    let access_token = data.value(forKey: "access_token") as! String
                    
                    let userDefaults = UserDefaults.standard
                    userDefaults.set(access_token, forKey: "bluweoAccessToken")
                    
                }
                else{
                    let data = responsed.value(forKey: "data") as! NSDictionary
                    let message = data.value(forKey: "message") as! NSArray
                    let msg = message[0] as! NSDictionary
                    let errMsg = msg.value(forKey: "message") as! String
                    
                    if(errMsg == "access token not valid or expired"){
                        self.refreshBluweoToken()
                    }
                }
            }
            else{
                print(respond.error?.localizedDescription ?? "")
                SVProgressHUD.dismiss()
            }
        })
    }
    
    @IBAction func editProfilePicture(_ sender: Any) {
        
        //        var title = String()
        //        var yes = String()
        //        var no = String()
        //
        //        let userDefault = UserDefaults.standard
        //        let lang = userDefault.value(forKey: "langDevice") as! String
        //
        //        if(lang == "th-TH"){
        //            title = thai.imageProfileSetUp
        //            yes = thai.yes
        //            no = thai.no
        //        }
        //        else{
        //            title = english.imageProfileSetUp
        //            yes = english.yes
        //            no = english.no
        //        }
        //
        //        let alert = UIAlertController.init(title: title, message: nil , preferredStyle: .alert)
        //        let yesAction = UIAlertAction.init(title: yes, style: .default , handler: {(alert: UIAlertAction!) in
        //
        //            var config = YPImagePickerConfiguration()
        //            config.library.onlySquare = false
        //            config.onlySquareImagesFromCamera = true
        //            config.targetImageSize = .original
        //            config.usesFrontCamera = true
        //            config.showsPhotoFilters = true
        //            config.shouldSaveNewPicturesToAlbum = true
        //            config.albumName = "BookMe"
        //            config.screens = [.library, .photo]
        //            config.startOnScreen = .library
        //            config.wordings.libraryTitle = "Gallery"
        //            config.hidesStatusBar = false
        //
        //            // Build a picker with your configuration
        //            let picker = YPImagePicker(configuration: config)
        //
        //            picker.didFinishPicking(completion: { [unowned picker] items ,  cancelled  in
        //
        //                if let photo = items.singlePhoto {
        //                    let image = photo.image.jpegData(compressionQuality: commonValue.imageCompress)
        //                    //                    let image = UIImageJPEGRepresentation(photo.image, 0.75)
        //                    self.ImageUploading(data: image!)
        //                }
        //
        //                if cancelled {
        //                    print("Picker was canceled")
        //                }
        //
        //                picker.dismiss(animated: true, completion: nil)
        //
        //            })
        //
        //            self.present(picker, animated: true, completion: nil)
        //
        //
        //
        //        })
        //
        //        let noAction = UIAlertAction.init(title: no , style: .cancel, handler: nil)
        //
        //        alert.addAction(yesAction)
        //        alert.addAction(noAction)
        //
        //        self.present(alert, animated: true, completion: nil)
        
    }
    
    func updateImageProfile(data : NSDictionary) {
        
        let parameters: [String: Any] = [
            "profilePhoto" : self.imageURL
        ]
        let ud = UserDefaults.standard
        var accesstoken = String()
        
        if(ud.object(forKey: "accessToken") != nil){
            accesstoken = ud.value(forKey: "accessToken") as! String
        }
        
        let header : HTTPHeaders = [
            "accessToken" : accesstoken
        ]
        
        
        Alamofire.request(baseURL.url + "profile/profile-image/\(self.userUID)", method: .put, parameters: parameters, encoding: JSONEncoding.default , headers : header)
            .responseJSON(completionHandler: { response in
                
                if let result = response.result.value {
                    _ = result as! NSDictionary
                    self.callProfile()
                    
                    SVProgressHUD.dismiss()
                    
                }
                else{
                    SVProgressHUD.showError(withStatus: "Network Error")
                }
            })
    }
    
    func ImageUploading(data : Data) {
        
        
        let ud = UserDefaults.standard
        let uid = ud.value(forKey: "user_uid") as! String
        let md5_uid = MD5(uid)
        let url = URL(string: "\(baseURL.media)api/uploads/")
        
        Alamofire.upload(multipartFormData: { (multipartFormData) in
            
            multipartFormData.append("\(uid)".data(using: String.Encoding.utf8)!, withName: "uId" as String)
            multipartFormData.append(data, withName: "imgFiles", fileName: "\(uid)_swift_file.jpeg", mimeType: "image/jpeg")
            
            
        },to: url! )
        { (result) in
            
            switch result {
            case .success(let upload, _, _):
                
                upload.uploadProgress(closure: { (progress) in
                    SVProgressHUD.showProgress(Float(progress.fractionCompleted), status: "Image Uploading")
                })
                
                upload.responseJSON { response in
                    
                    if let result = response.result.value {
                        let results = result as! NSDictionary
                        let data = results.value(forKey: "data") as! NSArray
                        
                        self.imageURL = "\(baseURL.photoURL)\(md5_uid.lowercased())/thumbs/\(data[0])"
                        self.updateImageProfile(data : self.userProfileData)
                        
                        SVProgressHUD.dismiss()
                        
                    }
                }
                
            case .failure(let encodingError):
                print("ERROR : \(encodingError)")
                SVProgressHUD.showError(withStatus: "Media Connection Error : \(encodingError)")
                break
            }
        }
    }
    
    @IBAction func go2EditPage(_ sender: Any) {
        self.performSegue(withIdentifier: "editProfile", sender: self)
    }
    
    
    
    @IBAction func mainAction(_ sender: Any) {
        
        if(self.mode != 1){
            let vc = professRegisViewController.instantiateFromStoryboard()
            
            let profressUID = self.portId[0].value(forKey: "_id") as! String
            
            vc.mode = "Update"
            vc.UpdateUID = profressUID
            vc.SegueName = self.SegueName
            vc.SegueimageProfileURL = self.SegueimageProfileURL
            
            self.navigationController?.pushViewController(vc, animated: true)
        }
        else{
            //            let vc = selectCatViewController.instantiateFromStoryboard()
            //
            //            vc.mode = "Create"
            //            vc.SegueName = self.SegueName
            //            vc.SegueimageProfileURL = self .SegueimageProfileURL
            //
            //            self.navigationController?.pushViewController(vc, animated: true)
            
            let vc = selectSubcatNoExpandViewController.instantiateFromStoryboard()
            
            vc.mode = "Create"
            vc.SegueName = self.SegueName
            vc.SegueimageProfileURL = self .SegueimageProfileURL
            
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    @objc func logout () {
        
        print("logout")
        
        let alert = UIAlertController.init(title: "Are you sure ?", message: "Do you want to log out ?", preferredStyle: .alert)
        let yes = UIAlertAction.init(title: "Yes", style: .default , handler: {(alert: UIAlertAction!) in
            
            //            let api = Instagram.shared
            //            api.logout()
            
            GIDSignIn.sharedInstance().signOut()
            
            let domain = Bundle.main.bundleIdentifier!
            UserDefaults.standard.removePersistentDomain(forName: domain)
            UserDefaults.standard.synchronize()
            
            let vc = LoginViewController.instantiateFromStoryboard()
            self.navigationController?.present(vc, animated: true, completion: nil)
            
        })
        
        let No = UIAlertAction.init(title: "No", style: .cancel, handler: nil)
        
        alert.addAction(yes)
        alert.addAction(No)
        
        self.present(alert, animated: true, completion: nil)
    }
    
    @IBAction func helpAction(_ sender: Any) {
        guard let url = URL(string: "https://cms.bluweo.com/help") else {
            return //be safe
        }
        
        if #available(iOS 10.0, *) {
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        } else {
            UIApplication.shared.openURL(url)
        }
    }
    
    @IBAction func feedBackAction(_ sender: Any) {
        guard let url = URL(string: "https://cms.bluweo.com/feedback") else {
            return //be safe
        }
        
        if #available(iOS 10.0, *) {
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        } else {
            UIApplication.shared.openURL(url)
        }
    }
    
    @IBAction func aboutAction(_ sender: Any) {
        guard let url = URL(string: "https://cms.bluweo.com/about") else {
            return //be safe
        }
        
        if #available(iOS 10.0, *) {
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        } else {
            UIApplication.shared.openURL(url)
        }
    }
    
    
}

extension SettingViewController : UITableViewDelegate , UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        switch self.mode {
        case 2:
            return Profess_menu.count
        default:
            return menu.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch self.mode {
        case 2:
            let cell = tableView.dequeueReusableCell(withIdentifier: "setting", for: indexPath) as! settingTableCell
            
            let userDefault = UserDefaults.standard
            let lang = userDefault.value(forKey: "langDevice") as! String
            
            if(lang == "th-TH"){
                cell.label.text = Profess_menu[indexPath.row]
            }
            else{
                cell.label.text = Profess_menu[indexPath.row]
            }
            cell.label.setText()
            
            cell.images.image = UIImage(named : "\(Profess_iconMenu[indexPath.row])")
            
            if(Profess_menu[indexPath.row] == "Bank Account"){
                cell.statusImage.isHidden = false
                if(profileDocument != NSDictionary()){
                    let idData = profileDocument.value(forKey: "bankAccount") as! NSDictionary
                    let idNumber = idData.value(forKey: "bankAccountName") as! String
                    let verify = idData.value(forKey: "verified") as! Bool
                    
                    if (!verify){
                        if(idNumber == ""){
                            cell.statusImage.image = UIImage(named: "icon_notice")
                        }
                        else{
                            cell.statusImage.image = UIImage(named: "icon_processing")
                        }
                    }
                    else{
                        print("verify icon init")
                        cell.statusImage.image = UIImage(named: "icon_verify")
                    }
                }
                
            }
            else if (Profess_menu[indexPath.row] == "ID Card" ){
                cell.statusImage.isHidden = false
                if(profileDocument != NSDictionary()){
                    let idData = profileDocument.value(forKey: "idCard") as! NSDictionary
                    let idNumber = idData.value(forKey: "idCardNumber") as! String
                    let verify = idData.value(forKey: "verified") as! Bool
                    
                    
                    if (!verify){
                        if(idNumber == ""){
                            cell.statusImage.image = UIImage(named: "icon_notice")
                        }
                        else{
                            cell.statusImage.image = UIImage(named: "icon_processing")
                        }
                    }
                    else{
                        print("verify icon init")
                        cell.statusImage.image = UIImage(named: "icon_verify")
                    }
                }
            }
            else{
                cell.statusImage.isHidden = true
                cell.bgView.setRoundWithR(r: commonValue.viewRadius)
            }
            
            return cell
            
        default:
            let cell = tableView.dequeueReusableCell(withIdentifier: "setting", for: indexPath) as! settingTableCell
            
            let userDefault = UserDefaults.standard
            let lang = userDefault.value(forKey: "langDevice") as! String
            
            if(lang == "th-TH"){
                cell.label.text = menu[indexPath.row]
            }
            else{
                cell.label.text = menu[indexPath.row]
            }
            
            cell.label.setText()
            cell.bgView.setRoundWithR(r: commonValue.viewRadius)
            cell.images.image = UIImage(named : "\(iconMenu[indexPath.row])")
            
            return cell
        }
        
    }
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 70
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        switch self.mode {
        case 2:
            if(indexPath.row == 0){
                print("Me : Edit personal info")
                
                self.performSegue(withIdentifier: "editProfile", sender: self)
                //                let vc = profileViewController.instantiateFromStoryboard()
                //                vc.uid =  self.portId[0].value(forKey: "_id") as! String
                //                self.navigationController?.pushViewController(vc, animated: true)
            }
                
            else if (indexPath.row == 1){
                let vc = balanceViewController.instantiateFromStoryboard()
                self.navigationController?.pushViewController(vc, animated: true)
            }
                
            else if(indexPath.row == 2){
                if(profileDocument != NSDictionary()){
                    let idData = profileDocument.value(forKey: "bankAccount") as! NSDictionary
                    let idCardNumber = idData.value(forKey: "bankAccountName") as! String
                    
                    if (idCardNumber != ""){
                        print("Waiting Verify")
                        SVProgressHUD.showInfo(withStatus: "Waiting Verify Infomation")
                        
                        DispatchQueue.main.asyncAfter(deadline: .now() + 2){
                            SVProgressHUD.dismiss()
                        }
                    }
                    else{
                        let vc = bankVerifyViewController.instantiateFromStoryboard()
                        self.navigationController?.pushViewController(vc, animated: true)
                    }
                }
                else{
                    print("Object Error")
                }
                
                
            }
                
            else if(indexPath.row == 3){
                
                if(profileDocument != NSDictionary()){
                    let idData = profileDocument.value(forKey: "idCard") as! NSDictionary
                    
                    let idCardNumber = idData.value(forKey: "idCardNumber") as! String
                    
                    if (idCardNumber != ""){
                        print("Waiting Verify")
                        
                        SVProgressHUD.showInfo(withStatus: "Waiting Verify Infomation")
                        
                        DispatchQueue.main.asyncAfter(deadline: .now() + 2){
                            SVProgressHUD.dismiss()
                        }
                    }
                    else{
                        let vc = IDCardVerifyViewController.instantiateFromStoryboard()
                        self.navigationController?.pushViewController(vc, animated: true)
                    }
                }
                else{
                    print("Object Error")
                }
            }
            else if (indexPath.row == 4){
                let vc = changePasswordViewController.instantiateFromStoryboard()
                self.navigationController?.pushViewController(vc, animated: true)
            }
            else if(indexPath.row == 5){
                let vc = AppSettingTableViewController.instantiateFromStoryboard()
                self.navigationController?.pushViewController(vc, animated: true)
            }
            
            
        default:
            if(indexPath.row == 0){
                print("Me : Edit personal info")
                self.performSegue(withIdentifier: "editProfile", sender: self)
            }
            else if (indexPath.row == 1){
                let vc = balanceViewController.instantiateFromStoryboard()
                self.navigationController?.pushViewController(vc, animated: true)
            }
            else if (indexPath.row == 2){
                let vc = changePasswordViewController.instantiateFromStoryboard()
                self.navigationController?.pushViewController(vc, animated: true)
            }
            else if(indexPath.row == 3){
                let vc = AppSettingTableViewController.instantiateFromStoryboard()
                self.navigationController?.pushViewController(vc, animated: true)
            }
            
            break
        }
    }
}

extension SettingViewController : UICollectionViewDelegate , UICollectionViewDataSource, UICollectionViewDelegateFlowLayout{
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.portId.count + 1
    }
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        
        
        if(indexPath.row <= self.portId.count - 1){
            let portData = self.portId[indexPath.row]
            let catName = portData.value(forKey: "catName") as! String
            let item = collectionView.dequeueReusableCell(withReuseIdentifier: "catagory", for: indexPath) as! catCollectionCell
            
            item.bgView.setRoundWithR(r: commonValue.viewRadius)
            
            if(portData.object(forKey: "catURL") != nil){
                item.imageCat.sd_setImage(with: URL(string: portData.value(forKey: "catURL") as! String))
            }
            else{
                print("not catURL")
            }
            
            item.nameLabel.text = catName
            item.numberLabel.isHidden = true
            
            return item
            
            //            let item = collectionView.dequeueReusableCell(withReuseIdentifier: "talent", for: indexPath) as! talentCell
            //
            //
            //            if let images = portData.value(forKey: "Gallery"){
            //                if(images is NSArray){
            //                    let Image = images as! NSArray
            //                    if(Image.count > 0){
            //                        let firstImage = Image[0] as! String
            //
            //                        let userDefault = UserDefaults.standard
            //                        let uid = userDefault.value(forKey: "user_uid") as! String
            //
            //                        let md5 = MD5(uid)
            //                        let urls = "\(baseURL.photoURL)user/\(md5.lowercased())/bookme/storages/\(firstImage)"
            //                        let urlString = URL(string: urls)
            //
            //
            //                        item.talentName.text = catName
            //                        item.imageBG.sd_setImage(with: urlString)
            ////                        item.imageBG.setRoundOnlyTop(r: commonValue.subTadius)
            //                        item.imageBG.setRoundWithR(r: commonValue.subTadius)
            ////                        item.bgView.setRoundWithR(r: commonValue.viewRadius)
            //                    }
            //                    else {
            //                        item.talentName.text = catName
            //                        item.imageBG.image = UIImage(named: "cover.jpg")
            ////                        item.imageBG.setRoundOnlyTop(r: commonValue.subTadius)
            //                        item.imageBG.setRoundWithR(r: commonValue.subTadius)
            ////                        item.bgView.setRoundWithR(r: commonValue.viewRadius)
            //                    }
            //                }
            //            }
            //
            //            else {
            //
            //                item.talentName.text = catName
            //                item.imageBG.image = UIImage(named: "cover.jpg")
            ////                item.imageBG.setRoundOnlyTop(r: commonValue.subTadius)
            //                item.imageBG.setRoundWithR(r: commonValue.subTadius)
            ////                item.bgView.setRoundWithR(r: commonValue.viewRadius)
            //            }
            //
            //
            //            return item
        }
        else{
            let item = collectionView.dequeueReusableCell(withReuseIdentifier: "addTalent", for: indexPath) as! addTalent
            
            item.bgView.setRoundWithR(r: commonValue.subTadius)
            
            return item
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if(indexPath.row <= self.portId.count - 1){
            let portData = self.portId[indexPath.row]
            let profressUID = portData.value(forKey: "professId") as! String
            
            let vc = professRegisViewController.instantiateFromStoryboard()
            
            vc.mode = "Update"
            vc.UpdateUID = profressUID
            vc.SegueName = self.SegueName
            vc.SegueimageProfileURL = self.SegueimageProfileURL
            
            self.navigationController?.pushViewController(vc, animated: true)
        }
        else{
            let vc = selectSubcatNoExpandViewController.instantiateFromStoryboard()
            
            vc.mode = "Create"
            vc.SegueName = self.SegueName
            vc.SegueimageProfileURL = self .SegueimageProfileURL
            
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        //        return CGSize(width: collectionView.frame.height - 4, height: collectionView.frame.height - 4)
        return CGSize(width: collectionView.bounds.height-20, height: collectionView.bounds.height-20)
    }
}

class settingTableCell:  UITableViewCell {
    @IBOutlet var images: UIImageView!
    @IBOutlet var label: UILabel!
    @IBOutlet weak var statusImage: UIImageView!
    @IBOutlet weak var bgView: UIView!
}

class talentCell: UICollectionViewCell {
    @IBOutlet weak var imageBG : UIImageView!
    @IBOutlet weak var imageStatus : UIImageView!
    @IBOutlet weak var talentName : UILabel!
    
}

class addTalent : UICollectionViewCell{
    @IBOutlet weak var bgView : UIView!
}
