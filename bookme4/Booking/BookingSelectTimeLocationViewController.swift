//
//  BookingSelectTimeLocationViewController.swift
//  bookme
//
//  Created by Naphat Sottiyaphai on 21/12/2561 BE.
//  Copyright © 2561 Naphat Sottiyaphai. All rights reserved.
//

import UIKit
import SwiftDate
import MapKit
import CoreLocation
import GoogleMaps
import GooglePlaces
import ACFloatingTextfield_Swift
import ActionSheetPicker_3_0
import SVProgressHUD

class BookingSelectTimeLocationViewController: UIViewController {
    
    var package = [NSDictionary]()
    var professID = String()
    var profess_uid = String()
    var profileImageURL = String()
    var profileNames = String()
    var profileTitles = String()
    
    var packNames = String()
    var packProce = String()
    var dateTimes = String()
    var locations = String()
    
    var jobName = String()
    var jobText = String()
    
    var locationName = String()
    var location_lat = Double()
    var location_lng = Double()
    var dateTimeFull = DateInRegion()
    
    var imageName = [String]()
    var imageData = [UIImage]()
    var imageDataJPGE = [Data]()
    
    @IBOutlet weak var locationTF: UITextField!
    @IBOutlet weak var dateTF: UITextField!
    @IBOutlet weak var nextBtn: UIButton!
    
    // Passing Data
    var packageSelected = NSDictionary()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        locationTF.delegate = self
        dateTF.delegate = self
        
        locationTF.setRoundWithCommonR()
        dateTF.setRoundWithCommonR()
        
        locationTF.setLeftPaddingPoints(15)
        dateTF.setLeftPaddingPoints(15)
        
        nextBtn.setRoundedwithR(r: commonValue.viewRadius)
        self.setClearNavBar()
        self.setBackgroundColor()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if(segue.identifier == "selectDateLo"){
            let vc = segue.destination as! BookingSelectTitleJobViewController
            
            vc.packageSelected = self.packageSelected
            vc.package = self.package
            vc.professID = self.professID
            vc.profess_uid = self.profess_uid
            vc.profileImageURL = self.profileImageURL
            vc.profileNames = self.profileNames
            vc.profileTitles = self.profileTitles
            
            vc.imageData = self.imageData
            vc.locationName = self.locationName
            vc.location_lat = self.location_lat
            vc.location_lng = self.location_lng
            
            vc.dateTimes = self.dateTimes
            vc.dateTimeFull = self.dateTimeFull.toISO().toDate()!
            
            vc.packNames = self.packNames
            vc.packProce = self.packProce
        }
    }
    
    @IBAction func mainAction(_ sender: Any) {
        if(locationTF.text == "" || dateTF.text == ""){
            print("Enter fill")
            SVProgressHUD.showError(withStatus: "Sowething Empty")
            
            DispatchQueue.main.asyncAfter(deadline: .now() + 2){
                SVProgressHUD.dismiss()
            }
            
        }
        else{
            self.performSegue(withIdentifier: "selectDateLo", sender: self)
        }
    }
}


extension BookingSelectTimeLocationViewController :  UITextFieldDelegate {
    func textFieldDidBeginEditing(_ textField: UITextField) {
        print("edit begin")
        if(textField == dateTF){
            DispatchQueue.main.async {
                textField.endEditing(true)
            }
            
            
            let datePicker = ActionSheetDatePicker(title: "Date", datePickerMode: UIDatePicker.Mode.dateAndTime, selectedDate: NSDate() as Date?, doneBlock: {
                picker, value, index in
                
                
                let selectDate = value as! Date
                let myFullSting = selectDate.toFormat("yyyy-MM-dd HH:mm:ss")
                let mySting = selectDate.toFormat("dd MMM yyyy")
                
                self.dateTimeFull = myFullSting.toDate()!
                self.dateTimes = mySting
                
                
                print(self.dateTimeFull)
                print(self.dateTimes)
                
                textField.text = "\(mySting)"
                textField.resignFirstResponder()
                
                return
            }, cancel: { ActionStringCancelBlock in return }, origin: (textField as AnyObject).superview!?.superview)
            
            datePicker?.minimumDate = Date()
            datePicker?.minuteInterval = 30
            datePicker?.show()
        }
        else if (textField == locationTF){
            DispatchQueue.main.async {
                textField.endEditing(true)
            }
            DispatchQueue.main.async {
                self.autocompleteClicked()
            }
        }
    }
    
    
}


extension BookingSelectTimeLocationViewController : GMSAutocompleteViewControllerDelegate  {
    func autocompleteClicked() {
        
        let autocompletecontroller = GMSAutocompleteViewController()
        autocompletecontroller.delegate = self
        let filter = GMSAutocompleteFilter()
        filter.type = .establishment  //suitable filter type
        filter.country = "TH"  //appropriate country code
        autocompletecontroller.autocompleteFilter = filter
        
        present(autocompletecontroller, animated: true, completion: nil)
    }
    
    // Handle the user's selection.
    func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {
        print("Place name: \(place.name ?? "")")
        print("Place address: \(String(describing: place.formattedAddress))")
        print("Place attributions: \(String(describing: place.attributions))")
        
        self.location_lat = place.coordinate.latitude
        self.location_lng = place.coordinate.longitude
        self.locationName = "\(place.name ?? "")"
        
        self.locationTF.text = "\(place.name ?? "")"
        
        dismiss(animated: true, completion: nil)
    }
    
    func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
        // TODO: handle the error.
        print("Error: ", error.localizedDescription)
    }
    
    // User canceled the operation.
    func wasCancelled(_ viewController: GMSAutocompleteViewController) {
        dismiss(animated: true, completion: nil)
    }
    
    // Turn the network activity indicator on and off again.
    func didRequestAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
    }
    
    func didUpdateAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
    }
    
}
