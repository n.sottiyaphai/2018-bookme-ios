//
//  thankYouBookingViewController.swift
//  bookme
//
//  Created by Naphat Sottiyaphai on 17/6/2561 BE.
//  Copyright © 2561 Naphat Sottiyaphai. All rights reserved.
//

import UIKit
import BEMCheckBox

class thankYouBookingViewController: UIViewController {
    
    // Image Status set
    // ---------------------
    // 0 : preCancel
    // 1 : Canceled
    // 2 : new booking
    // 3 : confirm booking
    // 4 : make payment
    // 5 : confirm payment
    // 6 : work complete
    // 7 : approve payment
    // 8 : transfer payment
    // 9 : work done
    // 10 : complete review
    // 11 : Create Profile Success
    // ----------------------
    
    var statusImageName = ["status_preCancel" , "status_cancel" , "status_newBooking" ,"status_confirm", "status_makePayment" , "status_confirmPayment" , "status_workComplete" , "status_approvePayment" , "status_transferPayment" , "status_workDone" , "status_completeReview" , "status_updateProfile" , "status_updateProfile"]
    
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var textLabel: UILabel!
    
    @IBOutlet weak var profileCheckBox: BEMCheckBox!
    @IBOutlet weak var portCheck: BEMCheckBox!
    @IBOutlet weak var packCheck: BEMCheckBox!

    
    @IBOutlet weak var profileText: UILabel!
    @IBOutlet weak var portText: UILabel!
    @IBOutlet weak var packText: UILabel!

    @IBOutlet weak var done: UIButton!
    
    
    var imageName = String()
    var titleStr = String()
    var textString = String()
    
    var formOtherSegue : Bool = false
    
    var makeBooking : Bool = true
    var comfirmation : Bool = false
    var makePayment : Bool = false
    
    var stateIndex = 2
    
    class func instantiateFromStoryboard() -> thankYouBookingViewController {
        let storyboard = UIStoryboard(name: "booking", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: String(describing: self)) as! thankYouBookingViewController
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        done.setRoundedwithR(r: commonValue.viewRadius)
        
        if(formOtherSegue){
            imageView.image = UIImage(named: imageName)
            titleLabel.text = titleStr
            textLabel.text = textString
        }
        
        if(makeBooking){
            profileCheckBox.setOn(true, animated: true)
        }
        if(comfirmation){
            portCheck.setOn(true, animated: true)
        }
        if(makePayment){
            packCheck.setOn(true, animated: true)
        }
        
        switch stateIndex {
        case 0:
            imageView.image = UIImage(named: statusImageName[stateIndex])
            break
        case 1:
            imageView.image = UIImage(named: statusImageName[stateIndex])
            break
        case 2:
            imageView.image = UIImage(named: statusImageName[stateIndex])
            break
        case 3:
            imageView.image = UIImage(named: statusImageName[stateIndex])
            break
        case 4:
            imageView.image = UIImage(named: statusImageName[stateIndex])
            break
        case 5:
            imageView.image = UIImage(named: statusImageName[stateIndex])
            break
        case 6:
            imageView.image = UIImage(named: statusImageName[stateIndex])
            break
        case 7:
            imageView.image = UIImage(named: statusImageName[stateIndex])
            break
        case 8:
            imageView.image = UIImage(named: statusImageName[stateIndex])
            break
        case 9:
            imageView.image = UIImage(named: statusImageName[stateIndex])
            break
        case 10:
            imageView.image = UIImage(named: statusImageName[stateIndex])
            break
        case 11:
            imageView.image = UIImage(named: statusImageName[stateIndex])
            break
        default:
            imageView.image = UIImage(named: statusImageName[11])
            break
        }
        
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.navigationBar.isHidden = true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.navigationController?.navigationBar.isHidden = false
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    
    @IBAction func done(_ sender: Any) {
        self.navigationController?.popToRootViewController(animated: true)
    }

}
