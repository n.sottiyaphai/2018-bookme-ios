//
//  profileViewController.swift
//  bookme4
//
//  Created by Naphat Sottiyaphai on 20/3/2561 BE.
//  Copyright © 2561 Naphat Sottiyaphai. All rights reserved.
//

import UIKit
import Koyomi
import Alamofire
import SVProgressHUD
import SDWebImage
import Kingfisher
import FirebaseDatabase
import SwiftHash
import GoogleSignIn
import StatusAlert
import SwiftDate
import ImageSlideshow
import Agrume

class profileViewController: UIViewController , UITableViewDelegate , UITableViewDataSource{
    
    private lazy var userRef: DatabaseReference = Database.database().reference().child("user")
    
    var BusyDate        = [DateInRegion]()
    var Gallery         = [String]()
    var aboutHeight     = 0
    var aboutText       = String()
    var catNameTag      = String()
    var conditionHeight = 0
    var conditons       = [String]()
    var currentMounth   = String()
    var dateSelect      = [Date]()
    var img             = [SDWebImageSource]()
    var indexPage       = Int()
    var instagrams      = String()
    var instagramsMedia = [URL]()
    var lastSection     = 0
    var ownedProfess    = Bool()
    var package         = [NSDictionary]()
    var professID       = String()
    var profess_uid     = String()
    var profileImageURL = String()
    var profileNames    = String()
    var profileTitle    = String()
    var project         = [NSDictionary]()
    var subCatArrayTag  = [String()]
    var uid             = String()
    
    @IBOutlet var bookButton: UIButton!
    @IBOutlet var onlineIndecator: UIView!
    @IBOutlet var profileImage: UIImageView!
    @IBOutlet var profileName : UILabel!
    @IBOutlet var tableView: UITableView!
    @IBOutlet weak var MonthText: UILabel!
    @IBOutlet weak var bgView: UIView!
    @IBOutlet weak var chatButton: UIButton!
    @IBOutlet weak var favCountLabel: UILabel!
    @IBOutlet weak var favImage: UIImageView!
    @IBOutlet weak var imageSlide: ImageSlideshow!
    @IBOutlet weak var koyomiBgView: UIView!
    @IBOutlet weak var loveButton: UIButton!
    @IBOutlet weak var projectCountLabel: UILabel!
    @IBOutlet weak var ratingCountLabel: UILabel!
    @IBOutlet weak var ratingIcon: UIImageView!
    @IBOutlet weak var tagCollection: UICollectionView!
    @IBOutlet weak var titleText: UILabel!
    
    @IBOutlet fileprivate weak var koyomi: Koyomi!{
        didSet {
            koyomi.circularViewDiameter = 0.7
            
            koyomi.calendarDelegate = self
            koyomi.inset = .zero
            koyomi.weeks = ("SU", "MO", "TU", "WE", "TH", "FR", "SA")
            koyomi.style = .standard
            koyomi.dayPosition = .center
            koyomi.selectionMode = .multiple(style: .circle)
            
            koyomi.selectedStyleColor = UIColor(red: 254.0/255.0, green: 210.0/255.0, blue: 0.0/255.0, alpha: 1.0)
            koyomi
                .setDayFont(size: 14)
                .setWeekFont(size: 14)
        }
    }
    
    private lazy var channelRef: DatabaseReference = Database.database().reference().child("room")
    
    class func instantiateFromStoryboard() -> profileViewController {
        let storyboard = UIStoryboard(name: "booking", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: String(describing: self)) as! profileViewController
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = true
        
        DispatchQueue.main.async {
            //            self.callProfess()
            
            let userDefaults = UserDefaults.standard
            let pids = userDefaults.value(forKey: "userData_FavoritePID") as! Array<String>
            
            let alreadyLove = pids.filter{ $0 == self.uid}
            //            print(alreadyLove)
            
            if(!alreadyLove.isEmpty){
                self.favImage.image = UIImage(named: "icon_fav_red")
            }
            else{
                self.favImage.image = UIImage(named: "icon_fav")
            }
        }
        
        DispatchQueue.main.async {
            
            self.onlineIndecator.setRounded()
            self.bookButton.setRoundedwithR(r: commonValue.viewRadius)
            self.MonthText.text = self.koyomi.currentDateString(withFormat: "MMM yyyy")
            self.currentMounth = self.koyomi.currentDateString(withFormat: "MM")
            self.koyomi.calendarDelegate = self
            
            self.koyomiBgView.setRoundWithCommonR()
            self.bgView.setRoundWithR(r: commonValue.viewRadius)
            self.setBackgroundColor()
        }
        
        let oldNib = UINib(nibName: "TableSectionHeader", bundle: nil)
        self.tableView.register(oldNib, forHeaderFooterViewReuseIdentifier: "TableSectionHeader")
        
        let nib = UINib(nibName: "ProfileTableSelectionHeader", bundle: nil)
        self.tableView.register(nib, forHeaderFooterViewReuseIdentifier: "ProfileTableSelectionHeader")
        
        let nibFooter = UINib(nibName: "Emptyfootertableivew", bundle: nil)
        self.tableView.register(nibFooter, forHeaderFooterViewReuseIdentifier: "Emptyfootertableivew")
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        //        self.clearAllDeclare()
        self.callProfess()
        
        if #available(iOS 11.0, *) {
            self.navigationController?.navigationBar.isHidden = false
            navigationController?.navigationBar.prefersLargeTitles = false
        } else {}
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        
        self.setClearNavBar()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        URLCache.shared.removeAllCachedResponses()
        
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        sizeHeaderToFit()
    }
    
    
    func sizeHeaderToFit() {
        let headerView = tableView.tableHeaderView!
        
        headerView.setNeedsLayout()
        headerView.layoutIfNeeded()
        
        let height = headerView.systemLayoutSizeFitting(UIView.layoutFittingCompressedSize).height
        var frame = headerView.frame
        frame.size.height = height
        headerView.frame = frame
        
        tableView.tableHeaderView = headerView
        self.profileImage.setRounded()
        
    }
    
    @IBAction func showImage(_ sender: Any) {
        SVProgressHUD.show()
        var url = [URL]()
        
        for URLs in self.Gallery{
            
            //            let md5 = MD5(self.profess_uid)
            //            let urls = "\(baseURL.photoURL)user/\(md5.lowercased())/bookme/storages/\(URLs)"
            
            url.append(URL(string: URLs)!)
        }
        
        //        let imageDownloader = AlamofireImageDownloader()
        //        let vc = Optik.imageViewer(withURLs: url, initialImageDisplayIndex: 0, imageDownloader: imageDownloader)
        
        SVProgressHUD.dismiss()
        //        present(vc, animated: true, completion: nil)
        
    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if(segue.identifier == "booking"){
            print("booking")
            let vc = segue.destination as? BookingSelectPackViewController
            //            let vc = segue.destination as? selectPackageTableViewController
            vc?.package = self.package
            vc?.professID = self.professID
            vc?.profess_uid = self.profess_uid
            vc?.profileImageURL = self.profileImageURL
            vc?.profileNames = self.profileNames
            vc?.profileTitles = self.profileTitle
            vc?.dateSelect = self.dateSelect
        }
        else if (segue.identifier == "review"){
            let vc  = segue.destination as? reviewTableViewController
            vc?.pid = self.professID
            vc?.owner = self.ownedProfess
        }
    }
    
    func callProfile() {
        
        let userDefaults = UserDefaults.standard
        let uid = userDefaults.value(forKey: "user_uid") as! String
        
        let ud = UserDefaults.standard
        var accesstoken = String()
        
        if(ud.object(forKey: "accessToken") != nil){
            accesstoken = ud.value(forKey: "accessToken") as! String
        }
        
        let header : HTTPHeaders = [
            "accessToken" : accesstoken
        ]
        
        Alamofire.request(baseURL.url + "profile/\(uid)", method: .get, encoding: JSONEncoding.default , headers : header)
            .responseJSON(completionHandler: { response in
                
                
                if let result = response.result.value {
                    let data = result as! NSDictionary
                    
                    if(data.value(forKey: "data") is NSDictionary){
                        let JSON = data.value(forKey: "data") as! NSDictionary
                        if let fav = JSON.value(forKey: "favorite"){
                            var favUserPID = [String]()
                            let favArray = fav as! NSArray
                            
                            for favData in favArray{
                                if(favData is NSDictionary){
                                    let dataDict = favData as! NSDictionary
                                    let pid = dataDict.value(forKey: "_id") as! String
                                    favUserPID.append(pid)
                                }
                                else{
                                    print("fav err")
                                }
                            }
                            userDefaults.set(favUserPID, forKey: "userData_FavoritePID")
                        }
                    }
                    
                }
                else if (response.result.error != nil){
                    SVProgressHUD.showError(withStatus: "Cannot Fetch your profile Please Login again.")
                    
                    GIDSignIn.sharedInstance().signOut()
                    
                    let domain = Bundle.main.bundleIdentifier!
                    UserDefaults.standard.removePersistentDomain(forName: domain)
                    UserDefaults.standard.synchronize()
                    
                    let vc = LoginViewController.instantiateFromStoryboard()
                    self.navigationController?.present(vc, animated: true, completion: nil)
                }
            })
    }
    
    func onlineCheck() {
        
        let userDefaults = UserDefaults.standard
        let uid = userDefaults.value(forKey: "user_uid") as! String
        userRef.child(self.profess_uid).child("isOnline").observe(.value) { (snapshot) in
            
            print(snapshot.value)
            if(snapshot.value is Bool){
                let userOnlineBool = snapshot.value as! Bool
                if(userOnlineBool){
                    self.onlineIndecator.backgroundColor = UIColor(red: 114.0/255.0, green: 178.0/255.0, blue: 84.0/255.0, alpha: 1.0)
                }else{
                    self.onlineIndecator.backgroundColor = UIColor.gray
                }
            }
        }
    }
    
    func callProfess() {
        self.Gallery.removeAll()
        self.project.removeAll()
        self.package.removeAll()
        self.img.removeAll()
        
        let ud = UserDefaults.standard
        var accesstoken = String()
        
        if(ud.object(forKey: "accessToken") != nil){
            accesstoken = ud.value(forKey: "accessToken") as! String
        }
        
        let header : HTTPHeaders = [
            "accessToken" : accesstoken
        ]
        
        Alamofire.request(baseURL.url + "professional/\(uid)", method: .get, encoding: JSONEncoding.default , headers : header)
            .responseJSON(completionHandler: { response in
                
                if let result = response.result.value {
                    let data = result as! NSDictionary
                    if(data.value(forKey: "data") is NSDictionary ){
                        let JSON = data.value(forKey: "data") as! NSDictionary
                        
                        var profile = NSDictionary()
                        if(JSON.value(forKey: "profile") is NSDictionary){
                            profile = JSON.value(forKey: "profile") as! NSDictionary
                        }
                        else{
                            let arrayProfile = JSON.value(forKey: "profile") as! NSArray
                            profile = arrayProfile[0] as! NSDictionary
                        }
                        
                        let packages = JSON.value(forKey: "packages") as! NSArray
                        let projects = JSON.value(forKey: "projects") as! NSArray
                        let portfolio = JSON.value(forKey: "portfolio") as! NSDictionary
                        let uid = JSON.value(forKey: "profileId") as! String
                        let professID = JSON.value(forKey: "_id") as! String
                        let reviewRate = JSON.value(forKey: "review") as! Double
                        let favCount = JSON.value(forKey: "favCount") as! Int
                        let category = JSON.value(forKey: "categories") as! NSDictionary
                        let subCat = category.value(forKey: "subCat") as! Array<String>
                        let catName = category.value(forKey: "catName") as! String
                        
                        let profileFirstName = profile.value(forKey: "firstName") as! String
                        let profileLastName = profile.value(forKey: "lastName") as! String
                        let profileImageURL = profile.value(forKey: "profilePhoto") as! String
                        let profileTitle = profile.value(forKey: "title") as! String
                        
                        
                        let GalleryURL = portfolio.value(forKey: "gallery") as! Array<String>
                        let condition = portfolio.value(forKey: "conditions") as! Array<String>
                        let aboutTexts = portfolio.value(forKey: "portfolioBio") as! String
                        let ig = portfolio.value(forKey: "instagram") as! String
                        
                        self.catNameTag = catName
                        self.subCatArrayTag = subCat
                        
                        self.professID = professID
                        self.profess_uid = uid
                        self.profileImageURL = profileImageURL
                        
                        self.conditons = condition
                        self.instagrams = ig
                        self.Gallery = GalleryURL
                        self.aboutText = aboutTexts
                        
                        for  imageURL in GalleryURL {
                            let urlString = URL(string: imageURL)
                            self.img.append(SDWebImageSource(url : urlString!))
                        }
                        
                        self.imageSlide.backgroundColor = UIColor.black
                        self.imageSlide.slideshowInterval = 10
                        self.imageSlide.contentScaleMode = UIView.ContentMode.scaleAspectFill
                        self.imageSlide.setImageInputs(self.img)
                        self.imageSlide.setCurrentPage(self.indexPage, animated: true)
                        self.imageSlide.activityIndicator = DefaultActivityIndicator()
                        self.imageSlide.pageIndicatorPosition = .init(horizontal: .right(padding: 10), vertical: .bottom)
                        
                        let recognizer = UITapGestureRecognizer(target: self, action: #selector(profileViewController.didTap))
                        self.imageSlide.addGestureRecognizer(recognizer)
                        
                        self.favCountLabel.text = "\(favCount)"
                        
                        if(reviewRate == 0){
                            self.ratingCountLabel.text = "-"
                            self.ratingIcon.image = UIImage(named: "icon_star")
                        }else{
                            self.ratingCountLabel.text = "\(reviewRate)"
                            self.ratingIcon.image = UIImage(named: "icon_star_fill_blue")
                        }
                        
                        self.projectCountLabel.text = "\(projects.count)"
                        
                        self.titleText.text = profileTitle
                        self.profileTitle = profileTitle
                        
                        self.profileImage.kf.setImage(with: URL(string: profileImageURL),
                                                      placeholder: UIImage(named: "bookme_loading"),
                                                      options: [
                                                        .scaleFactor(UIScreen.main.scale),
                                                        .transition(.fade(0.5)),
                                                        .cacheOriginalImage
                        ])
                        
                        //                        self.profileImage.sd_setImage(with: URL(string : profileImageURL), placeholderImage: UIImage(named : "appLogo.png"))
                        
                        self.profileName.text = "\(profileFirstName) \(profileLastName)"
                        self.profileNames = "\(profileFirstName) \(profileLastName)"
                        
                        for pack in packages{
                            self.package.append(pack as! NSDictionary)
                        }
                        
                        for pro in projects{
                            self.project.append(pro as! NSDictionary)
                        }
                        
                        let userDefaults = UserDefaults.standard
                        let user_uid = userDefaults.value(forKey: "user_uid") as! String
                        
                        if(user_uid == uid){
                            self.ownedProfess = true
                            self.bookButton.setTitle("Edit Profile", for: .normal)
                            self.bookButton.backgroundColor = commonColor.mainYellow
                        }
                        else{
                            self.ownedProfess = false
                        }
                        
                        self.onlineCheck()
                        self.fetchCalendarProfile()
                        SVProgressHUD.dismiss()
                        self.tableView.reloadData()
                        self.tagCollection.reloadData()
                    }
                    else{
                        SVProgressHUD.showError(withStatus: "Data not match. Please try again.")
                        self.navigationController?.popViewController(animated: true)
                    }
                }
                else{
                    SVProgressHUD.showError(withStatus: "Network Error")
                    self.navigationController?.popViewController(animated: true)
                }
            })
    }
    
    func getLanguageISO() -> String {
        let locale = Locale.current
        guard let languageCode = locale.languageCode,
            let regionCode = locale.regionCode else {
                return "de_DE"
        }
        return languageCode + "_" + regionCode
    }
    
    func setBusyBG() {
        
        for date in self.BusyDate{
            var dates = Date()
            let device_region = getLanguageISO()
            //            print(getLanguageISO())
            
            let thaiReg = Region(calendar: Calendars.buddhist, zone: Zones.asiaBangkok, locale: Locales.thai)
            
            if(device_region == "en_TH"){
                let jobs = date.convertTo(calendar: Calendars.buddhist, timezone: Zones.asiaBangkok, locale: Locales.thai).date
                dates = jobs + 543.years
            }
            else{
                let jobs = date.convertTo(calendar: Calendars.buddhist, timezone: Zones.asiaBangkok, locale: Locales.thai).date
                dates = jobs.date
            }
            
            self.koyomi.setDayColor(UIColor.lightGray, of: dates)
            self.koyomi.setDayBackgrondColor(UIColor.groupTableViewBackground, of: dates)
        }
        DispatchQueue.main.async {
            self.koyomi.reloadData()
        }
    }
    
    func fetchCalendarProfile(){
        BusyDate.removeAll()
        
        let ud = UserDefaults.standard
        var accesstoken = String()
        
        if(ud.object(forKey: "accessToken") != nil){
            accesstoken = ud.value(forKey: "accessToken") as! String
        }
        
        let ReqURL = "\(baseURL.url)calendar/byProfile/\(self.profess_uid)"
        let header : HTTPHeaders = [
            "month" : currentMounth,
            "accessToken" : accesstoken
        ]
        
        Alamofire.request(URL(string: ReqURL)!, method: .get, headers : header).responseJSON(completionHandler: { (Result) in
            
            if (Result.response!.statusCode >= 400){
                if (Result.response?.statusCode == 401){
                    self.bookmeTokenCreate()
                    self.callProfile()
                }
                    
                else if (Result.response?.statusCode == 404){
                    // Calendar No found
                    return
                }
                
                SVProgressHUD.showError(withStatus: Result.error?.localizedDescription)
                DispatchQueue.main.asyncAfter(deadline: .now() + 2){
                    SVProgressHUD.dismiss()
                }
                return
            }
            
            if let result = Result.result.value{
                let JSON = result as! NSDictionary
                let apiResponse = JSON.value(forKey: "response") as! Bool
                
                if(apiResponse){
                    let data = JSON.value(forKey: "data") as! NSArray
                    for Once in data{
                        let onceRow = Once as! NSDictionary
                        let calendar = onceRow.value(forKey: "calendar") as! NSDictionary
                        let busyDate = calendar.value(forKey: "busyDate") as! NSArray
                        
                        for busy in busyDate {
                            let date = busy as! String
                            let thaiRegions = Region(calendar: Calendars.gregorian, zone: Zones.americaNewYork, locale: Locales.english)
                            let dateArr = date.toDate()
                            let toRegionsDate = dateArr?.convertTo(region: thaiRegions)
                            self.BusyDate.append(toRegionsDate!)
                            
                        }
                    }
                    self.setBusyBG()
                }
                else{
                    let apiError = JSON.value(forKey: "err") as! NSDictionary
                    let errMessage = apiError.value(forKey: "message") as! String
                    print("Error : \(errMessage)")
                }
            }
        })
    }
    
    
    @IBAction func chatAction(_ sender: Any) {
        
        SVProgressHUD.show()
        print("Go 2 Chat")
        
        let userDefault = UserDefaults.standard
        let myUid = userDefault.value(forKey: "user_uid") as! String
        let myName = userDefault.value(forKey: "user_fullName") as! String
        let myAvatar = userDefault.value(forKey: "user_avatarURL") as! String
        
        let params : [String : Any] = [
            "senderID" : myUid,
            "senderName" : myName,
            "senderAvatar" : myAvatar,
            "reciverID" : self.profess_uid,
            "reciverName" : self.profileNames ,
            "reciverAvatar" : self.profileImageURL
        ]
        
        Alamofire.request(URL(string : "https://us-central1-bookme-1515492592081.cloudfunctions.net/createRoom")!, method: .post, parameters: params, encoding: JSONEncoding.default).responseJSON(completionHandler: { (response) in
            
            if let result = response.result.value{
                let data = result as! NSDictionary
                print(result)
                
                // Send to ChatRoom
                let vc = chatViewController.instantiateFromStoryboard()
                vc.chatSlug = self.profess_uid
                vc.roomName = self.profileName.text!
                vc.avatarURL = self.profileImageURL
                
                SVProgressHUD.dismiss()
                self.navigationController?.pushViewController(vc, animated: true)
                
            }
            else{
                SVProgressHUD.showError(withStatus: "Ops! Something wrong.\n Please Try again later.")
                DispatchQueue.main.asyncAfter(deadline: .now() + 2){
                    SVProgressHUD.dismiss()
                }
            }
        })
    }
    
    @IBAction func booking(_ sender: UIButton) {
        if(sender.title(for: .normal) == "Edit Profile"){
            
            let vc = professRegisViewController.instantiateFromStoryboard()
            let profressUID = self.professID
            
            vc.mode = "Update"
            vc.UpdateUID = profressUID
            vc.SegueName = self.profileName.text!
            vc.SegueimageProfileURL = self.profileImageURL
            
            self.navigationController?.pushViewController(vc, animated: true)
        }
        else{
            self.performSegue(withIdentifier: "booking", sender: self)
        }
        
    }
    
    @IBAction func loveAction(_ sender: Any) {
        
        let userDefaults = UserDefaults.standard
        //        print("index of id : \(index)")
        
        let pids = userDefaults.value(forKey: "userData_FavoritePID") as! Array<String>
        let uid = userDefaults.value(forKey: "user_uid") as! String
        
        let alreadyLove = pids.filter{ $0 == self.uid}
        
        
        if (!alreadyLove.isEmpty){
            // Unfavorited
            
            let alert = UIAlertController(title: "Are you sure ?", message: "Do you want to unfavorite this professional", preferredStyle: .alert)
            
            let ok = UIAlertAction(title: "OK", style: .default) { (alert) in
                print("Capture action OK")
                self.favoriteDelete(uid: uid, pid: self.uid)
            }
            
            let cancel = UIAlertAction(title: "Cancel", style: .default) { (alert) in
                print("Capture action Cancel")
                self.dismiss(animated: true, completion: nil)
            }
            
            alert.addAction(cancel)
            alert.addAction(ok)
            
            
            self.present(alert, animated: true, completion: nil)
            
        }
        else {
            // Add Favorite
            self.favoriteAdd(uid: uid, pid: self.uid)
        }
        
    }
    
    func favoriteAdd(uid : String , pid : String) {
        
        let ud = UserDefaults.standard
        var accesstoken = String()
        
        if(ud.object(forKey: "accessToken") != nil){
            accesstoken = ud.value(forKey: "accessToken") as! String
        }
        
        let header : HTTPHeaders = [
            "accessToken" : accesstoken
        ]
        
        let param : [String:Any] = [
            "professId" : pid
        ]
        
        Alamofire.request(baseURL.url + "favorite/" + "\(uid)", method: .post , parameters: param , encoding: JSONEncoding.default , headers : header)
            .responseJSON(completionHandler: { response in
                
                if let result = response.result.value {
                    let respond = result as! NSDictionary
                    
                    if(respond.value(forKey: "response") as! Bool){
                        
                        // Creating StatusAlert instance
                        let statusAlert = StatusAlert()
                        
                        statusAlert.image = commonImage.iconFavBig
                        statusAlert.title = "Favorited"
                        statusAlert.message =  "You can open favorites list in favorited tab."
                        statusAlert.canBePickedOrDismissed = true
                        
                        // Presenting created instance
                        statusAlert.showInKeyWindow()
                        
                        self.favImage.image = UIImage(named: "icon_fav_red")
                        
                        DispatchQueue.main.async {
                            self.callProfess()
                            self.callProfile()
                        }
                    }
                }
                else{
                    SVProgressHUD.showError(withStatus: "Error")
                }
            })
    }
    
    func favoriteDelete(uid : String , pid : String) {
        
        let ud = UserDefaults.standard
        var accesstoken = String()
        
        if(ud.object(forKey: "accessToken") != nil){
            accesstoken = ud.value(forKey: "accessToken") as! String
        }
        
        let header : HTTPHeaders = [
            "accessToken" : accesstoken
        ]
        
        
        let param : [String:Any] = [
            "professId" : pid
        ]
        
        Alamofire.request(baseURL.url + "favorite/delete/" + "\(uid)", method: .post , parameters: param , encoding: JSONEncoding.default , headers : header)
            .responseJSON(completionHandler: { response in
                
                if let result = response.result.value {
                    let respond = result as! NSDictionary
                    
                    if(respond.value(forKey: "response") as! Bool){
                        
                        // Creating StatusAlert instance
                        
                        let statusAlert = StatusAlert()
                        
                        statusAlert.image = commonImage.iconUnfavBig
                        statusAlert.title = "Unfavorite"
                        statusAlert.message =  "unfavorite complete."
                        statusAlert.canBePickedOrDismissed = true
                        
                        // Presenting created instance
                        statusAlert.showInKeyWindow()
                        
                    }
                    else{
                        print("ERROR : Data type not correct.")
                    }
                    
                    self.favImage.image = UIImage(named: "icon_fav")
                    
                    DispatchQueue.main.async {
                        self.callProfess()
                        self.callProfile()
                    }
                }
                else{
                    SVProgressHUD.showError(withStatus: "Error")
                }
            })
    }
    
    
    @IBAction func go2Review(_ sender: Any) {
        self.performSegue(withIdentifier: "review", sender: self)
    }
    
    
    @objc func didTap() {
        let fullScreenController = imageSlide.presentFullScreenController(from: self)
        fullScreenController.slideshow.activityIndicator = DefaultActivityIndicator(style: .white, color: nil)
    }
    
    func setGradient() {
        let view = UIView(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 50))
        let gradient = CAGradientLayer()
        
        gradient.frame = view.bounds
        gradient.colors = [UIColor.white.cgColor, UIColor.black.cgColor]
        
        view.layer.insertSublayer(gradient, at: 0)
    }
    
    
    
    // MARK: - Table view data source
    
    func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 5
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        switch section {
        case 2 :
            return self.project.count
        case 3:
            return self.package.count
        case 4:
            return self.conditons.count
        default:
            return 1
        }
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch indexPath.section {
            
            //        case 0:
        //            return 180
        case 0:
            return UITableView.automaticDimension
        case 1:
            if(self.instagrams == ""){
                return 0
            }
            else{
                return UIScreen.main.bounds.height/3
            }
        case 2:
            return 200
        case 3:
            return 105
        case 4:
            return UITableView.automaticDimension
        default:
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        self.profileImage.setRounded()
        
        self.lastSection = indexPath.section
        switch indexPath.section {
            
        case 0 :
            let cell = tableView.dequeueReusableCell(withIdentifier: "about", for: indexPath) as! aboutTextTableCell
            
            cell.aboutText.setRoundWithCommonR()
            cell.aboutText.text = self.aboutText
            cell.aboutText.sizeToFit()
            cell.aboutText.setRoundWithR(r: commonValue.viewRadius)
            cell.aboutText.contentInset = UIEdgeInsets(top: 8, left: 15, bottom: 8, right: 15);
            
            return cell
            
        case 1 :
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "IGimage", for: indexPath) as! imageFromIG
            
            cell.igCollection.delegate = self
            cell.igCollection.tag = 200
            
            return cell
            
        case 2 :
            let cell = tableView.dequeueReusableCell(withIdentifier: "ref", for: indexPath) as! projectRefTableViewCell
            
            if(indexPath.row  < self.project.count){
                let pro = self.project[indexPath.row]
                let projectDetail = pro.value(forKey: "projectDetail") as! String
                let projectName = pro.value(forKey: "projectName") as! String
                let dateTime = pro.value(forKey: "projectDate") as! String
                let dateFormated = dateTime.toDate()
                
                cell.projectImageColl.delegate = self
                cell.projectImageColl.tag = 300 + indexPath.row
                
                cell.projectName.text = projectName
                cell.projectDest.text = projectDetail
                cell.projectTime.text = dateFormated?.toFormat("MMM yyyy")
                cell.bullet.setRounded()
                cell.bgView.setRoundWithCommonR()
                
                return cell
            }
            else{
                return cell
            }
            
            
        case 3 :
            let cell = tableView.dequeueReusableCell(withIdentifier: "packageCell", for: indexPath) as! packageCell
            
            if(indexPath.row < self.package.count){
                let pack = self.package[indexPath.row]
                
                var packName = String()
                var packPrice = String()
                var packHours = String()
                var packUnit = Int()
                
                if(pack.object(forKey: "packageName") != nil){
                    packName = pack.value(forKey: "packageName") as! String
                }
                else{
                    packName = ""
                }
                
                if(pack.object(forKey: "packagePrice") != nil){
                    packPrice = pack.value(forKey: "packagePrice") as! String
                }
                else{
                    packPrice = ""
                }
                
                if(pack.object(forKey: "packageTime") != nil){
                    packHours = pack.value(forKey: "packageTime") as! String
                }
                else{
                    packHours = ""
                }
                if(pack.object(forKey: "packageTimeUnit") != nil){
                    packUnit = pack.value(forKey: "packageTimeUnit") as! Int
                }
                else{
                    packUnit = 0
                }
                
                if let myInteger = Int(packPrice) {
                    let myNumber = NSNumber(value:myInteger)
                    
                    let numberFormatter = NumberFormatter()
                    numberFormatter.numberStyle = .decimal
                    
                    if let prices = numberFormatter.string(from: myNumber){
                        cell.packagePrice.text = "฿\(prices)"
                    }
                    
                }
                
                switch packUnit{
                case 1 :
                    cell.timeUnit.text = "Days"
                    break
                case 2 :
                    cell.timeUnit.text = "Hours"
                    break
                case 3 :
                    cell.timeUnit.text = "Project"
                    break
                default :
                    cell.timeUnit.text = ""
                    break
                }
                
                cell.time.text = packHours
                cell.packageName.text = "\(packName)"
                cell.point.setRounded()
                cell.bgView.setRoundWithCommonR()
                
                return cell
            }
            else{
                return cell
            }
            
        case 4 :
            let cell = tableView.dequeueReusableCell(withIdentifier: "ConditionCell", for: indexPath) as! ConditionCell
            
            if(indexPath.row < self.conditons.count){
                cell.condition.text =  self.conditons[indexPath.row]
                cell.bgView.setRoundWithR(r: commonValue.viewRadius)
                
                cell.point.setRounded()
                
                return cell
            }
            else{
                return cell
            }
            
        default:
            let cell = tableView.dequeueReusableCell(withIdentifier: "image", for: indexPath) as! imageCollectionTableviewCell
            
            cell.collectionView.delegate = self
            cell.collectionView.tag = 2999
            
            return cell
        }
    }
    
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        switch indexPath.section {
            //        case 0:
            //            guard let tableViewCell = cell as? imageCollectionTableviewCell else { return }
        //            tableViewCell.setCollectionViewDataSourceDelegate(self, forRow: indexPath.row)
        case 1 :
            guard let tableViewCell = cell as? imageFromIG else { return }
            tableViewCell.setCollectionViewDataSourceDelegate(self, forRow: indexPath.row)
        case 2 :
            guard let tableViewCell = cell as? projectRefTableViewCell else { return }
            tableViewCell.setCollectionViewDataSourceDelegate(self, forRow: indexPath.row)
        default:
            break
        }
    }
    
    //    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
    //        switch section {
    //        case 0:
    //            return "About"
    //        case 1:
    //            return ""
    //        case 2:
    //            return "Project Reference"
    //        case 3 :
    //            return "Rate"
    //        case 4:
    //            return "Conditions"
    //        default:
    //            return nil
    //        }
    //    }
    
    let headerText = ["About Me","","My Experiences","My Package","My Conditions","My Calendar"]
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        switch section {
            //        case 0:
        //            return 0
        case 1:
            if(self.instagrams == ""){
                return UIView()
            }
            else{
                // Dequeue with the reuse identifier
                let cell = self.tableView.dequeueReusableHeaderFooterView(withIdentifier: "TableSectionHeader") as! TableSectionHeader
                
                let header = cell
                header.titleLabel.text = headerText[section]
                
                return cell
            }
        case 2:
            if(self.project.count > 0){
                // Dequeue with the reuse identifier
                let cell = self.tableView.dequeueReusableHeaderFooterView(withIdentifier: "TableSectionHeader") as! TableSectionHeader
                
                let header = cell
                header.titleLabel.text = headerText[section]
                
                return cell
            }
            else{
                return UIView()
            }
        case 3:
            if(self.package.count > 0){
                // Dequeue with the reuse identifier
                let cell = self.tableView.dequeueReusableHeaderFooterView(withIdentifier: "TableSectionHeader") as! TableSectionHeader
                
                let header = cell
                header.titleLabel.text = headerText[section]
                
                return cell
            }
            else{
                return UIView()
            }
        case 4:
            if(self.conditons.count > 0){
                // Dequeue with the reuse identifier
                let cell = self.tableView.dequeueReusableHeaderFooterView(withIdentifier: "TableSectionHeader") as! TableSectionHeader
                
                let header = cell
                header.titleLabel.text = headerText[section]
                
                return cell
            }
            else{
                return UIView()
            }
        default:
            // Dequeue with the reuse identifier
            let cell = self.tableView.dequeueReusableHeaderFooterView(withIdentifier: "TableSectionHeader") as! TableSectionHeader
            
            let header = cell
            header.titleLabel.text = headerText[section]
            
            return cell
        }
        
        
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        
        switch section {
            //        case 0:
        //            return 0
        case 1:
            if(self.instagrams == ""){
                return 0
            }
            else{
                return CGFloat(tableViewValue.profileHeaderViewSize)
            }
        case 2:
            if(self.project.count > 0){
                return CGFloat(tableViewValue.profileHeaderViewSize)
            }
            else{
                return 0
            }
        case 3:
            if(self.project.count > 0){
                return CGFloat(tableViewValue.profileHeaderViewSize)
            }
            else{
                return 0
            }
        case 4:
            if(self.project.count > 0){
                return CGFloat(tableViewValue.profileHeaderViewSize)
            }
            else{
                return 0
            }
            
        default:
            return CGFloat(tableViewValue.profileHeaderViewSize)
        }
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        var offest = scrollView.contentOffset.y / 150
        if(offest > 1){
            offest = 1
            //
            //            // Show white Color
            //
            //            if #available(iOS 13.0, *) {
            //                let navBarAppearance = UINavigationBarAppearance()
            //                navBarAppearance.configureWithOpaqueBackground()
            //                navBarAppearance.titleTextAttributes = [.foregroundColor: UIColor.white]
            //                navBarAppearance.largeTitleTextAttributes = [.foregroundColor: UIColor.white]
            //                navBarAppearance.backgroundColor = UIColor(red: 1, green: 1, blue: 1, alpha: offest)
            //                self.navigationController?.navigationBar.standardAppearance = navBarAppearance
            //                self.navigationController?.navigationBar.scrollEdgeAppearance = navBarAppearance
            //            }
            //            else{
            self.navigationController?.navigationBar.backgroundColor = UIColor(red: 1, green: 1, blue: 1, alpha: offest)
            //                UIApplication.shared.statusBar.backgroundColor = UIColor(red: 1, green: 1, blue: 1, alpha: offest)
            //            }
        }
        else{
            //
            //            if #available(iOS 13.0, *) {
            //                let navBarAppearance = UINavigationBarAppearance()
            //                navBarAppearance.configureWithOpaqueBackground()
            //                navBarAppearance.titleTextAttributes = [.foregroundColor: UIColor.white]
            //                navBarAppearance.largeTitleTextAttributes = [.foregroundColor: UIColor.white]
            //                navBarAppearance.backgroundColor = UIColor(red: 1, green: 1, blue: 1, alpha: offest)
            //                self.navigationController?.navigationBar.standardAppearance = navBarAppearance
            //                self.navigationController?.navigationBar.scrollEdgeAppearance = navBarAppearance
            //            }
            //            else{
            self.navigationController?.navigationBar.backgroundColor = UIColor(red: 1, green: 1, blue: 1, alpha: offest)
            //                //  UIApplication.shared.statusBar?.backgroundColor = UIColor(red: 1, green: 1, blue: 1, alpha: offest)
            //            }
        }
    }
}



extension profileViewController : UICollectionViewDataSource , UICollectionViewDelegate , UICollectionViewDelegateFlowLayout {
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        //        if(collectionView.tag >= 0 && collectionView.tag <= 99){
        //            return self.Gallery.count
        //        }
        
        switch collectionView {
        case tagCollection:
            return  self.subCatArrayTag.count
        default :
            if(collectionView.tag >= 200 && collectionView.tag <= 299){
                return self.instagramsMedia.count
            }
            if(collectionView.tag >= 300 && collectionView.tag <= 399){
                
                let index = collectionView.tag - 300
                if(index < self.project.count){
                    let pro = self.project[index]
                    
                    if(pro.object(forKey: "projectImage") != nil){
                        let image = pro.value(forKey: "projectImage") as! Array<String>
                        return image.count
                    }
                }
                
                return 0
                
            }
            else{
                return 1
            }
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        switch collectionView {
        case tagCollection:
            if(indexPath.row == 0){
                let item = collectionView.dequeueReusableCell(withReuseIdentifier: "sort", for: indexPath) as! sortCollectionCell
                
                item.bgView.setRoundWithShadow(r: Float(collectionView.bounds.height/2-11))
                item.bgView.clipsToBounds = true
                item.bgView.backgroundColor = UIColor(red: 84.0/255.0, green: 189.0/255.0, blue: 239.0/255.0, alpha: 1.0)
                item.textLabel.text = self.catNameTag
                
                
                return item
            }
                
            else {
                let item = collectionView.dequeueReusableCell(withReuseIdentifier: "sort2", for: indexPath) as! sortCollectionCell2
                
                item.bgView.setRoundWithR(r: Float(collectionView.bounds.height/2-11))
                item.bgView.clipsToBounds = true
                item.bgView.backgroundColor = UIColor(red: 151.0/255.0, green: 226.0/255.0, blue: 252.0/255.0, alpha: 1.0)
                item.textLabel.text = self.subCatArrayTag[indexPath.row-1]
                
                return item
            }
        default :
            
            if(collectionView.tag >= 200 && collectionView.tag <= 299){
                let item = collectionView.dequeueReusableCell(withReuseIdentifier: "IGimage", for: indexPath) as! IGimageItem
                
                let url = self.instagramsMedia[indexPath.row]
                item.IGimage.sd_setImage(with: url, placeholderImage: UIImage(named: "bookme_loading"))
                item.IGimage.setRoundWithR(r: commonValue.subTadius)
                
                return item
            }
            
            
            if(collectionView.tag >= 300 && collectionView.tag <= 399){
                let item = collectionView.dequeueReusableCell(withReuseIdentifier: "projectImage", for: indexPath) as! projectRefCollImage
                
                let index = collectionView.tag - 300
                if(index < self.project.count){
                    let pro = self.project[index]
                    
                    let image = pro.value(forKey: "projectImage") as! Array<String>
                    let imageURL = image[indexPath.row]
                    
                    item.refImage.kf.setImage(with: URL(string: imageURL),
                                              placeholder: UIImage(named: "bookme_loading"),
                                              options: [
                                                .scaleFactor(UIScreen.main.scale),
                                                .transition(.fade(0.5)),
                                                .cacheOriginalImage
                    ])
                    
                    item.refImage.setRoundWithR(r: commonValue.subTadius)
                    
                    return item
                }
                else{
                    return item
                }
            }
                
            else{
                let item = collectionView.dequeueReusableCell(withReuseIdentifier: "image", for: indexPath) as! imageCollectionItem
                item.image.setRoundWithR(r: commonValue.subTadius)
                return item
            }
        }
        
        //        return UICollectionViewCell()
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        //        if(collectionView.tag >= 0 && collectionView.tag <= 99){
        //            return CGSize(width: collectionView.bounds.height*1.25-10 , height: collectionView.bounds.height-10 )
        //        }
        switch collectionView {
        case tagCollection:
            if(indexPath.row == 0){
                return CGSize(width: collectionView.bounds.width/2.6, height: collectionView.bounds.height-15)
            }
            else{
                return CGSize(width: collectionView.bounds.width/2.6, height: collectionView.bounds.height-15)
            }
            
        default :
            if(collectionView.tag >= 200 && collectionView.tag <= 299){
                return CGSize(width: collectionView.bounds.height/2-7.5 , height: collectionView.bounds.height/2-7.5 )
            }
            if(collectionView.tag >= 300 && collectionView.tag <= 399){
                return CGSize(width: collectionView.bounds.height-10 , height: collectionView.bounds.height-10 )
            }
            else{
                return CGSize(width: collectionView.bounds.height, height: collectionView.bounds.height)
            }
        }
        
        //        return CGSize()
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        switch collectionView {
        case tagCollection:
            break
        default :
            if (collectionView.tag >= 300 && collectionView.tag <= 399){
                SVProgressHUD.show()
                
                let index = collectionView.tag - 300
                let pro = self.project[index]
                
                let image = pro.value(forKey: "projectImage") as! Array<String>
                
                var images = [URL]()
                
                for URLs in image{
                    images.append(URL(string: URLs)!)
                }
                
                let button = UIBarButtonItem(barButtonSystemItem: .stop, target: nil, action: nil)
                button.tintColor = .white
                
                let agrume = Agrume(urls: images, startIndex: indexPath.item, background: .blurred(.dark) , dismissal: .withButton(button))
                agrume.statusBarStyle = .lightContent
                agrume.show(from: self)
                
                SVProgressHUD.dismiss()
            }
            else{
                print("it's other")
            }
        }
    }
    
    
}

extension profileViewController : KoyomiDelegate {
    
    func koyomi(_ koyomi: Koyomi, didSelect date: Date?, forItemAt indexPath: IndexPath) {
        
        let compareDate = date?.toFormat("yyyy-MM-dd")
        let mapBusyDete = BusyDate.map{($0.date + 543.years).toFormat("yyyy-MM-dd") }
        
        let busyfilter = mapBusyDete.filter{$0 == compareDate}
        
        if(busyfilter.count == 0){
            let filterValue = dateSelect.filter{$0 == date}
            if(filterValue.isEmpty){
                self.dateSelect.append(date!)
            }
            else{
                self.dateSelect = dateSelect.filter{$0 != date}
            }
        }
        else{
            SVProgressHUD.showError(withStatus: "This date busy. Please select another date.")
            DispatchQueue.main.asyncAfter(deadline: .now()+1){
                self.koyomi.unselect(date: date!)
                self.koyomi.reloadData()
                SVProgressHUD.dismiss()
            }
        }
    }
    
    func koyomi(_ koyomi: Koyomi, currentDateString dateString: String) {
        koyomi.currentDateFormat = "MMMM yyyy"
        MonthText.text = dateString
        currentMounth = koyomi.currentDateString(withFormat: "MM")
    }
    
    func koyomi(_ koyomi: Koyomi, selectionColorForItemAt indexPath: IndexPath, date: Date) -> UIColor? {
        let bookmeYello = UIColor(red: 254.0/255.0, green: 210.0/255.0, blue: 0.0/255.0, alpha: 1.0)
        
        return bookmeYello
    }
    
    func splitDate(date : Date) -> String{
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        
        let myString = formatter.string(from: date)
        let yourDate = formatter.date(from: myString)
        formatter.dateFormat = "dd MMM yyyy"
        let myStringafd = formatter.string(from: yourDate!)
        
        return myStringafd
        
    }
    
    @IBAction func prev(_ sender: Any) {
        let month: MonthType = {
            .previous
        }()
        koyomi.currentDateFormat = "MMM yyyy"
        koyomi.display(in: month)
        
        DispatchQueue.main.async {
            self.fetchCalendarProfile()
        }
    }
    @IBAction func next(_ sender: Any) {
        let month: MonthType = {
            .next
        }()
        koyomi.currentDateFormat = "MMM yyyy"
        koyomi.display(in: month)
        
        DispatchQueue.main.async {
            self.fetchCalendarProfile()
        }
    }
    
    @objc(koyomi:shouldSelectDates:to:withPeriodLength:)
    
    func koyomi(_ koyomi: Koyomi, shouldSelectDates date: Date?, to toDate: Date?, withPeriodLength length: Int) -> Bool {
        //        print("you select : \(String(describing: date))")
        return true
    }
    
}




class aboutTextTableCell : UITableViewCell {
    @IBOutlet weak var bgView: UIView!
    @IBOutlet weak var aboutText: UITextView!
    
}

class imageCollectionTableviewCell : UITableViewCell {
    @IBOutlet weak var deleteButton: UIButton!
    @IBOutlet weak var collectionView: UICollectionView!
}

class imageFromIG : UITableViewCell {
    @IBOutlet weak var igCollection: UICollectionView!
}

class projectRefTableViewCell: UITableViewCell {
    
    @IBOutlet weak var bgView: UIView!
    @IBOutlet weak var projectName : UILabel!
    @IBOutlet weak var projectTime : UILabel!
    @IBOutlet weak var projectDest : UILabel!
    @IBOutlet weak var bullet: UIView!
    @IBOutlet weak var projectImageColl : UICollectionView!
}

class projectRefCollImage : UICollectionViewCell{
    @IBOutlet var refImage : UIImageView!
}

class packageCell: UITableViewCell {
    
    @IBOutlet weak var point: UIView!
    @IBOutlet weak var packageName : UILabel!
    @IBOutlet weak var packagePrice : UILabel!
    @IBOutlet weak var timeUnit: UILabel!
    @IBOutlet weak var time: UILabel!
    @IBOutlet weak var bgView: UIView!
    
}

class ConditionCell: UITableViewCell {
    @IBOutlet weak var point: UIView!
    @IBOutlet weak var condition : UITextView!
    @IBOutlet weak var bgView: UIView!
    
}

class IGimageItem: UICollectionViewCell {
    @IBOutlet var IGimage : UIImageView!
}

extension imageFromIG{
    
    func setCollectionViewDataSourceDelegate<D: UICollectionViewDataSource & UICollectionViewDelegate>(_ dataSourceDelegate: D, forRow row: Int) {
        
        igCollection.delegate = dataSourceDelegate
        igCollection.dataSource = dataSourceDelegate
        //        igCollection.tag = row
        igCollection.setContentOffset(igCollection.contentOffset, animated:false) // Stops collection view if it was scrolling.
        igCollection.reloadData()
    }
    
    var collectionViewOffset: CGFloat {
        set { igCollection.contentOffset.x = newValue }
        get { return igCollection.contentOffset.x }
    }
}

extension imageCollectionTableviewCell{
    
    func setCollectionViewDataSourceDelegate<D: UICollectionViewDataSource & UICollectionViewDelegate>(_ dataSourceDelegate: D, forRow row: Int) {
        
        collectionView.delegate = dataSourceDelegate
        collectionView.dataSource = dataSourceDelegate
        //        collectionView.tag = row
        collectionView.setContentOffset(collectionView.contentOffset, animated:false) // Stops collection view if it was scrolling.
        collectionView.reloadData()
    }
    
    var collectionViewOffset: CGFloat {
        set { collectionView.contentOffset.x = newValue }
        get { return collectionView.contentOffset.x }
    }
}

extension projectRefTableViewCell{
    
    func setCollectionViewDataSourceDelegate<D: UICollectionViewDataSource & UICollectionViewDelegate>(_ dataSourceDelegate: D, forRow row: Int) {
        
        projectImageColl.delegate = dataSourceDelegate
        projectImageColl.dataSource = dataSourceDelegate
        //        projectImageColl.tag = row
        projectImageColl.setContentOffset(projectImageColl.contentOffset, animated:false) // Stops collection view if it was scrolling.
        projectImageColl.reloadData()
    }
    
    var collectionViewOffset: CGFloat {
        set { projectImageColl.contentOffset.x = newValue }
        get { return projectImageColl.contentOffset.x }
    }
}


