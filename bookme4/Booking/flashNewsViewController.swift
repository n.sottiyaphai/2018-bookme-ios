//
//  flashNewsViewController.swift
//  ETNECALink
//
//  Created by Naphat Sottiyaphai on 9/4/2562 BE.
//  Copyright © 2562 Naphat Sottiyaphai. All rights reserved.
//

import UIKit

class flashNewsViewController: UIViewController , UIGestureRecognizerDelegate {
    
    @IBOutlet weak var bgView: UIView!
    @IBOutlet weak var blackBG: UIView!
    @IBOutlet weak var descDetail: UILabel!
    @IBOutlet weak var dismissButton: UIButton!
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var titleText: UILabel!
    
    var imageURL       = String()
    var descDetailText = String()
    var titleTextStr   = String()
    
    class func instantiateFromStoryboard() -> flashNewsViewController {
        let storyboard = UIStoryboard(name: "booking", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: String(describing: self)) as! flashNewsViewController
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(didTab))
        tap.delegate = self // This is not required
    
        blackBG.alpha = 0
        blackBG.isHidden = false
        
        UIApplication.shared.keyWindow!.bringSubviewToFront(blackBG!)

        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5 ){
            UIView.animate(withDuration: 0.5, animations: {
                self.blackBG.alpha = 1
            })
        }

        self.descDetail.text = descDetailText
        self.titleText.text = titleTextStr
        
        self.imageView.sd_setImage(with: URL(string: imageURL))
        self.imageView.setRoundOnlyTop(r: commonValue.viewRadius)
        self.bgView.setRoundWithShadow(r: commonValue.viewRadius)
        self.dismissButton.setRoundWithR(r: commonValue.viewRadius)
        
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.dismissButton.setTitle("Close", for: .normal)
    }
    
    
    @IBAction func dismiss(_ sender: Any) {
        self.blackBG.alpha = 0
        self.dismiss(animated: true, completion: nil)
    }
    
    @objc func didTab () {
        print("tap")
        self.blackBG.alpha = 0
        self.dismiss(animated: true, completion: nil)
    }
    
}
