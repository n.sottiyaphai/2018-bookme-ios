//
//  MainCatViewController.swift
//  bookme4
//
//  Created by Naphat Sottiyaphai on 4/5/2561 BE.
//  Copyright © 2561 Naphat Sottiyaphai. All rights reserved.
//

import UIKit
import Alamofire

class MainCatViewController: UIViewController {
    
    public var tag = String()
    public var catId = String()
    public var catIcon = String()
    public var dataMode = "normal"
    public var resultData = NSArray()
    
    private var mode = "table"
    public var header = HTTPHeaders()
    public var URLPath = String()
    
    class func instantiateFromStoryboard() -> MainCatViewController {
        let storyboard = UIStoryboard(name: "booking", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: String(describing: self)) as! MainCatViewController
    }
    
    @IBOutlet weak var containerView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        let btn1 = UIButton(type: .custom)
        btn1.setImage(UIImage(named: "icon_view"), for: .normal)
        btn1.frame = CGRect(x: 0, y: 0, width: 40  , height: 18)
        btn1.addTarget(self, action: #selector(self.switchView), for: .touchUpInside)
        let item1 = UIBarButtonItem(customView: btn1)
        
        let btn2 = UIButton(type: .custom)
        btn2.setImage(UIImage(named: "icon_search"), for: .normal)
        btn2.frame = CGRect(x: 0, y: 0, width: 40, height: 18)
        btn2.addTarget(self, action: #selector(self.go2search), for: .touchUpInside)
        let item2 = UIBarButtonItem(customView: btn2)
        
        self.navigationItem.setRightBarButtonItems([item2 , item1], animated: true)
        
        print(self.resultData)
        
        let userDefaults = UserDefaults.standard
        if(dataMode == "normal"){
            userDefaults.set(self.tag, forKey: "catagory_tag")
            userDefaults.set(self.catId, forKey: "catagory_id")
            userDefaults.set(self.catIcon, forKey: "catagory_icon")
            userDefaults.set(self.dataMode, forKey: "dataMode")
        }
        else{
            userDefaults.set(self.header, forKey: "header")
            userDefaults.set(self.URLPath, forKey: "URLPath")
//            userDefaults.set(self.resultData, forKey: "resultData")
            userDefaults.set(self.dataMode, forKey: "dataMode")
            userDefaults.set(self.tag, forKey: "catagory_tag")
        }
        
        
        
        catagoryListTableViewController.embed(
            withIdentifier: "catagoryListTableViewController", // Storyboard ID
            parent: self,
            container: self.containerView){ vc in
                
                print("Embed Complete")
                // do things when embed complete
        }
        
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        self.setBackgroundColor()
        
        self.navigationController?.navigationBar.isHidden = false
        if #available(iOS 11.0, *) {
            navigationController?.navigationBar.prefersLargeTitles = false
        } else {
            print("It's down of iOS 11")
        }
    }
    
    @objc func go2search() {
        let vc = searchViewController.instantiateFromStoryboard()
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @objc func switchView (){
        if(mode == "table"){
            mode = "collection"
            centerCardCatagoryViewController.embed(
                withIdentifier: "centerCardCatagoryViewController", // Storyboard ID
                parent: self,
                container: self.containerView){ vc in
                    
                    print("Embed Complete")
                    
                    let btn1 = UIButton(type: .custom)
                    btn1.setImage(UIImage(named: "icon_list"), for: .normal)
                    btn1.frame = CGRect(x: 0, y: 0, width: 40  , height: 18)
                    btn1.addTarget(self, action: #selector(self.switchView), for: .touchUpInside)
                    let item1 = UIBarButtonItem(customView: btn1)
                    
                    let btn2 = UIButton(type: .custom)
                    btn2.setImage(UIImage(named: "icon_search"), for: .normal)
                    btn2.frame = CGRect(x: 0, y: 0, width: 40, height: 18)
                    let item2 = UIBarButtonItem(customView: btn2)
                    
                    self.navigationItem.setRightBarButtonItems([item2 , item1], animated: true)
                    
                    // do things when embed complete
            }
        }
        else{
            mode = "table"
            catagoryListTableViewController.embed(
                withIdentifier: "catagoryListTableViewController", // Storyboard ID
                parent: self,
                container: self.containerView){ vc in

                    print("Embed Complete")
                    // do things when embed complete
                    
                    let btn1 = UIButton(type: .custom)
                    btn1.setImage(UIImage(named: "icon_view"), for: .normal)
                    btn1.frame = CGRect(x: 0, y: 0, width: 40  , height: 18)
                    btn1.addTarget(self, action: #selector(self.switchView), for: .touchUpInside)
                    let item1 = UIBarButtonItem(customView: btn1)
                    
                    let btn2 = UIButton(type: .custom)
                    btn2.setImage(UIImage(named: "icon_search"), for: .normal)
                    btn2.frame = CGRect(x: 0, y: 0, width: 40, height: 18)
                    let item2 = UIBarButtonItem(customView: btn2)
                    
                    self.navigationItem.setRightBarButtonItems([item2 , item1], animated: true)
            }
        }
    }
}
