//
//  selectPackageTableViewController.swift
//  bookme
//
//  Created by Naphat Sottiyaphai on 17/6/2561 BE.
//  Copyright © 2561 Naphat Sottiyaphai. All rights reserved.
//

import UIKit
import BEMCheckBox
import ACFloatingTextfield_Swift
import OpalImagePicker
import MapKit
import CoreLocation
import GoogleMaps
import GooglePlaces
import ActionSheetPicker_3_0
import SVProgressHUD
import SwiftDate

class selectPackageTableViewController: UIViewController , UITableViewDelegate , UITableViewDataSource , GMSMapViewDelegate{
    
    @IBOutlet weak var mainButton: UIButton!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var collectionView: UICollectionView!
    
    @IBOutlet weak var profileImage: UIImageView!
    @IBOutlet weak var profileName: UILabel!
    @IBOutlet weak var profileTitle: UILabel!
//    @IBOutlet weak var tagList: TagListView!
    @IBOutlet weak var destTextView: UITextView!
    @IBOutlet weak var jobTitle: UITextField!
    
    var imageData = [UIImage]()
    var package = [NSDictionary]()
    var professID = String()
    var profess_uid = String()
    var profileImageURL = String()
    var profileNames = String()
    var profileTitles = String()
    
    var packNames = String()
    var packProce = String()
    
    var locationName = String()
    var location_lat = Double()
    var location_lng = Double()
    
    var dateTime = String()
    var dateTimeFull = String()
    var selectPack = Bool()
    var dateSelect = [Date]()
    
    var packBool = [Bool]()
    var optionPackSelected = Bool()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationItem.hidesBackButton = true
        let newBackButton = UIBarButtonItem(title: "Cancel", style: UIBarButtonItem.Style.plain, target: self, action: #selector(selectPackageTableViewController.back(sender:)))
        
        self.navigationItem.leftBarButtonItem = newBackButton
        
        print("come from profile")
        print(package)
        
        for pack in package {
            packBool.append(false)
        }
        
        jobTitle.setLeftPaddingPoints(15)
        jobTitle.setRightPaddingPoints(15)
        jobTitle.setRoundWithR(r: commonValue.viewRadius)
//        jobTitle.layer.borderWidth = 0.5
//        jobTitle.layer.borderColor = UIColor.lightGray.cgColor
        
        destTextView.textContainerInset = .init(top: 15, left: 15, bottom: 15, right: 15)
        destTextView.setRoundWithR(r: commonValue.viewRadius)
//        destTextView.layer.borderWidth = 0.5
//        destTextView.layer.borderColor = UIColor.lightGray.cgColor
        
        profileImage.sd_setImage(with: URL(string: profileImageURL), placeholderImage: UIImage(named: "bookme_loading"))
        profileImage.setRounded()
        
        profileName.text = profileNames
        profileTitle.text = profileTitles
        
        mainButton.setRoundedwithR(r: commonValue.viewRadius)
        
    }
    
    
    @objc func back(sender: UIBarButtonItem) {
        print("back Detect")
        
        let alearView = UIAlertController(title: "Are you sure ?", message: "Are you sure to cancel this ?", preferredStyle: .alert)
        let ok = UIAlertAction(title: "Sure", style: .default, handler: { (alert) in
            self.dismiss(animated: true, completion: nil)
            self.navigationController?.popViewController(animated: true)
        })
        let cancel = UIAlertAction(title: "Cancel", style: .cancel, handler: { (alert) in
            self.dismiss(animated: true, completion: nil)
        })
        
        alearView.addAction(cancel)
        alearView.addAction(ok)
        
        self.present(alearView, animated: true, completion: nil)
        
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        if #available(iOS 11.0, *) {
            self.automaticallyAdjustsScrollViewInsets = true
        } else {
            self.automaticallyAdjustsScrollViewInsets = false
        }
        
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        sizeHeaderToFit()
    }
    
    func sizeHeaderToFit() {
        let headerView = tableView.tableHeaderView!
        
        headerView.setNeedsLayout()
        headerView.layoutIfNeeded()
        
        let height = headerView.systemLayoutSizeFitting(UIView.layoutFittingCompressedSize).height
        var frame = headerView.frame
        frame.size.height = height
        headerView.frame = frame
        
        tableView.tableHeaderView = headerView
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if(segue.identifier == "booking"){
            print("booking")
            
            let vc = segue.destination as? previewBookingViewController
            vc?.package = self.package
            vc?.professID = self.professID
            vc?.profess_uid = self.profess_uid
            vc?.profileImageURL = self.profileImageURL
            vc?.profileNames = self.profileNames
            vc?.profileTitles = self.profileTitles
            vc?.dateTimes = self.dateTime
            vc?.dateTimeFull = self.dateTimeFull.toISODate()!
            
            vc?.imageData = self.imageData
            vc?.locationName = self.locationName
            vc?.location_lat = self.location_lat
            vc?.location_lng = self.location_lng
            
            vc?.jobName = self.jobTitle.text!
            vc?.jobText = self.destTextView.text
            
            vc?.packNames = self.packNames
            vc?.packProce = self.packProce
        }
    }
    
    @IBAction func nextAction(_ sender: Any) {
        
        // Varidateion Data
        if((jobTitle.text?.isEmpty)! || dateTime.isEmpty || locationName.isEmpty){
            showEmptyFillAlert ()
        }
        else if (!selectPack){
            showEmptyFillAlert ()
        }
            
        else if (self.packNames.count <= 0 || self.packProce.count  <= 0){
            showEmptyFillAlert ()
        }
        else{
            self.performSegue(withIdentifier: "booking", sender: self)
        }
        
    }
    
    func showEmptyFillAlert () {
        let alearView = UIAlertController(title: "Empty fill", message: english.fillNotValidate, preferredStyle: .alert)
        let ok = UIAlertAction(title: "OK", style: .default, handler: { (alert) in
            self.dismiss(animated: true, completion: nil)
        })
        alearView.addAction(ok)
        self.present(alearView, animated: true, completion: nil)
    }
    
    @IBAction func selectPackAction(_ sender: UIButton) {
        
        for index in 0...packBool.count-1{
            packBool[index] = false
        }
        
        if(packBool[sender.tag] == false){
            self.packBool[sender.tag] = true
            
            let packDict = package[sender.tag]
            packNames = packDict.value(forKey: "packageName") as! String
            packProce = packDict.value(forKey: "packagePrice") as! String
            optionPackSelected = false
            selectPack = true
        }
        else{
            self.packBool[sender.tag] = false
            packNames = String()
            packProce = String()
            optionPackSelected = false
            selectPack = false
        }
        
        
        self.tableView.reloadData()
        
    }
    
    
    
    // MARK: - Table view data source
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        switch indexPath.section {
        case 1:
            return 152
        default:
            return 55
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        switch section {
        case 1:
            return 1
        default:
            return package.count + 1
        }
        
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.section {
        case 1:
            let cell = tableView.dequeueReusableCell(withIdentifier: "whenAndWhere") as! whenAndWhere
            
            cell.dateTF.delegate = self
            cell.dateTF.tag = 1
            
            if(!self.dateSelect.isEmpty){
                let cleanNil = self.dateSelect.compactMap{$0}
                let stringDate = cleanNil.map{"\($0.toFormat("dd MMM")), "}
                let reduceDate = stringDate.reduce("" , {$0 + $1})
                print(reduceDate)
                
                cell.dateTF.text = reduceDate
            }
            cell.locationTF.delegate = self
            cell.locationTF.tag = 2
            cell.locationTF.text = locationName
            
            return cell
        default:
            if(indexPath.row < package.count){
                let cell = tableView.dequeueReusableCell(withIdentifier: "selectPack") as! selectPack
                
                let pack = package[indexPath.row]
                
                cell.button.tag = indexPath.row
                cell.checkBox.tag = indexPath.row
                
                
                if(packBool[indexPath.row] == false){
                    cell.checkBox.setOn(false, animated: true)
                }
                else{
                    cell.checkBox.setOn(true, animated: true)
                }
                
                
                if let packName = pack.value(forKey: "packageName") as? String{
                    cell.packName.text = "\(packName)"
                }
                if let prices = pack.value(forKey: "packagePrice") as? String{
                    
                    if let intPrice = Int(prices){
                        let myNumber = NSNumber(value:intPrice)
                        let numberFormatter = NumberFormatter()
                        numberFormatter.numberStyle = .decimal
                        
                        if let prices = numberFormatter.string(from: myNumber){
                            cell.price.text  = "\(prices) Baht"
                        }
                    }
                    else{
                        SVProgressHUD.showError(withStatus: "Data Incomplete.")
                        self.navigationController?.popViewController(animated: true)
                    }
                }
                
                return cell
            }
            else{
                let cell = tableView.dequeueReusableCell(withIdentifier: "optionPack") as! optionPack
                
                cell.checkBox.tag = indexPath.row
                
                cell.packName.tag = 100
                cell.price.tag = 200
                
                cell.packName.delegate = self
                cell.price.delegate = self
                
                if(optionPackSelected){
                    cell.checkBox.setOn(true, animated: true)
                    let _ = cell.packName.becomeFirstResponder()
                }
                else{
                    cell.checkBox.setOn(false, animated: true)
                }
                return cell
            }
        }
    }
    
    
    
    
    
}

extension selectPackageTableViewController : UICollectionViewDelegate , UICollectionViewDataSource , UICollectionViewDelegateFlowLayout{
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return imageData.count + 1
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        
        
        if(indexPath.row < imageData.count){
            let item = collectionView.dequeueReusableCell(withReuseIdentifier: "image", for: indexPath) as! imageCollectionItem
            
            item.image.image = imageData[indexPath.row]
            
            return item
        }
        else{
            let item = collectionView.dequeueReusableCell(withReuseIdentifier: "addProject", for: indexPath) as! addCollectionItem
            
            return item
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        return CGSize(width: collectionView.bounds.height-10 , height: collectionView.bounds.height-10 )
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if(indexPath.row < imageData.count ){
            
        }
        else{
            print("Select Image")
            
            let imagePicker = OpalImagePickerController()
            imagePicker.imagePickerDelegate = self
            present(imagePicker, animated: true, completion: nil)
        }
        
    }
}

extension selectPackageTableViewController : UITextFieldDelegate{
    func textFieldDidBeginEditing(_ textField: UITextField) {
        print("edit begin")
        
        switch textField.tag {
        case 1:
            print("from dateTime")
            
            DispatchQueue.main.async {
                textField.endEditing(true)
            }
            
            let datePicker = ActionSheetDatePicker(title: "Date", datePickerMode: UIDatePicker.Mode.date, selectedDate: NSDate() as Date?, doneBlock: {
                picker, value, index in
                
                let formatter = DateFormatter()
                formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
                
                
                
                let myString = formatter.string(from: value as! Date)
                self.dateTimeFull = myString
                
                let yourDate = formatter.date(from: myString)
                formatter.dateFormat = "dd MMM yyyy"
                let myStringafd = formatter.string(from: yourDate!)
                
                print("select date : \(myStringafd)")
                
                //                self.projectDateTime[indexPart] = myStringafd
                self.dateTime = "\(myStringafd)"
                textField.text = "\(myStringafd)"
                textField.resignFirstResponder()
                
                return
            }, cancel: { ActionStringCancelBlock in return }, origin: (textField as AnyObject).superview!?.superview)
            
            datePicker?.minimumDate = Date()
            datePicker?.show()
            break
            
        case 2:
            DispatchQueue.main.async {
                textField.endEditing(true)
            }
            DispatchQueue.main.async {
                self.autocompleteClicked()
            }
            break
            
        case 100:
            print("packName")
        default:
            break
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        switch textField.tag {
        case 100:
            
            let text: NSString = (textField.text ?? "") as NSString
            let resultString = text.replacingCharacters(in: range, with: string)
            
            self.packNames = resultString
            print(resultString)
            break
        case 200:
            
            let text: NSString = (textField.text ?? "") as NSString
            let resultString = text.replacingCharacters(in: range, with: string)
            
            self.packProce = resultString
            print(resultString)
        default:
            break
        }
        
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField, reason: UITextField.DidEndEditingReason) {
        
        let optionCell = self.tableView.dequeueReusableCell(withIdentifier: "optionPack") as! optionPack
        
        switch textField.tag {
        case 100:
            
            for index in 0...self.packBool.count-1{
                self.packBool[index] = false
            }
            
            optionCell.checkBox.setOn(true, animated: true)
            
            self.selectPack = true
            self.optionPackSelected = true
            
            self.tableView.reloadData()
            
        default:
            break
        }
        
    }
}

// MARK: - ImagePicker Delegate
extension selectPackageTableViewController : OpalImagePickerControllerDelegate {
    
    func imagePicker(_ picker: OpalImagePickerController, didFinishPickingImages images: [UIImage]) {
        print("did done")
        
        for image in images{
            self.imageData.append(image)
        }
        
        self.collectionView.reloadData()
        self.dismiss(animated: true, completion: nil)
        
    }
    
    func imagePickerDidCancel(_ picker: OpalImagePickerController){
        print("cancel")
        self.dismiss(animated: true, completion: nil)
    }
}

extension selectPackageTableViewController : GMSAutocompleteViewControllerDelegate  {
    func autocompleteClicked() {
        
        let autocompletecontroller = GMSAutocompleteViewController()
        autocompletecontroller.delegate = self
        let filter = GMSAutocompleteFilter()
        filter.type = .establishment  //suitable filter type
        filter.country = "TH"  //appropriate country code
        autocompletecontroller.autocompleteFilter = filter
        
        present(autocompletecontroller, animated: true, completion: nil)
    }
    
    // Handle the user's selection.
    func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {
        print("Place name: \(place.name)")
        print("Place address: \(String(describing: place.formattedAddress))")
        print("Place attributions: \(String(describing: place.attributions))")
        
        self.location_lat = place.coordinate.latitude
        self.location_lng = place.coordinate.longitude
        self.locationName = "\(place.name)"
        self.tableView.reloadData()
        
        dismiss(animated: true, completion: nil)
    }
    
    func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
        // TODO: handle the error.
        print("Error: ", error.localizedDescription)
    }
    
    // User canceled the operation.
    func wasCancelled(_ viewController: GMSAutocompleteViewController) {
        dismiss(animated: true, completion: nil)
    }
    
    // Turn the network activity indicator on and off again.
    func didRequestAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
    }
    
    func didUpdateAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
    }
    
}



class selectPack : UITableViewCell {
    
    @IBOutlet weak var button: UIButton!
    @IBOutlet weak var checkBox: BEMCheckBox!
    @IBOutlet weak var packName: UILabel!
    @IBOutlet weak var price: UILabel!
    
    
}

class optionPack: UITableViewCell {
    @IBOutlet weak var checkBox: BEMCheckBox!
    @IBOutlet weak var packName: ACFloatingTextfield!
    @IBOutlet weak var price: ACFloatingTextfield!
}

class whenAndWhere : UITableViewCell{
    @IBOutlet weak var locationTF: ACFloatingTextfield!
    @IBOutlet weak var dateTF: ACFloatingTextfield!
}


