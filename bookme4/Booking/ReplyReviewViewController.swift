//
//  ReplyReviewViewController.swift
//  bookme
//
//  Created by Naphat Sottiyaphai on 8/11/2561 BE.
//  Copyright © 2561 Naphat Sottiyaphai. All rights reserved.
//

import UIKit
import Cosmos
import Alamofire
import SVProgressHUD

class ReplyReviewViewController: UIViewController , EnterTextViewDelegate {
    
    @IBOutlet weak var review: UITextView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var imageProfile: UIImageView!
    @IBOutlet weak var profileName: UILabel!
    @IBOutlet weak var reviewDateTime: UILabel!
    @IBOutlet weak var starRate: CosmosView!
    @IBOutlet weak var bgView: UIView!
    
    var owner = Bool()
    var reviewObject = NSDictionary()
    var oldText = String()
    var reviewID = String()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let profile = reviewObject.value(forKey: "profile") as! NSDictionary
        let firstName = profile.value(forKey: "firstName") as! String
        let lastName = profile.value(forKey: "lastName") as! String
        let profileImage = profile.value(forKey: "profilePhoto") as! String
        let ratings = reviewObject.value(forKey: "reviewRate") as! String
        let reviews = reviewObject.value(forKey: "reviewText") as? String
        reviewID = reviewObject.value(forKey: "_id") as! String
        
        DispatchQueue.main.async {
            self.profileName.text = " \(firstName) \(lastName)"
            self.imageProfile.setRounded()
            self.imageProfile.sd_setImage(with: URL(string: profileImage), placeholderImage: UIImage(named: "bookme_loading"))
            self.starRate.rating = Double(ratings)!
            self.review.text = reviews
            self.reviewDateTime.text = " \(Date().toFormat("dd MMM yyyy"))"
            
            self.setBackgroundColor()
            self.bgView.setRoundWithCommonR()
            
            self.tableView.reloadData()
            self.tableView.rowHeight = UITableView.automaticDimension
        }
    }
    
    func sendBackText(title: String, text: String , conditionIndex : Int) {
        print(title)
        print(text)
        print(conditionIndex)
        
        self.oldText = text
        DispatchQueue.main.async {
            self.tableView.reloadData()
        }
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        self.sizeHeaderToFit()
    }
    
    func sizeHeaderToFit() {
        let headerView = tableView.tableHeaderView!
        
        headerView.setNeedsLayout()
        headerView.layoutIfNeeded()
        
        let height = headerView.systemLayoutSizeFitting(UIView.layoutFittingCompressedSize).height
        var frame = headerView.frame
        frame.size.height = height
        headerView.frame = frame
        
        tableView.tableHeaderView = headerView
    }
    
    @IBAction func replyPost(_ sender: Any) {
        print("Put Reply")
        self.sendReply()
    }
    
    func sendReply() {
//        /review/reply/{id}
        let url = "\(baseURL.url)review/reply/\(self.reviewID)"
        
        let userDefault = UserDefaults.standard
        let uid = userDefault.value(forKey: "user_uid") as! String
        
        let ud = UserDefaults.standard
        var accesstoken = String()
        
        if(ud.object(forKey: "accessToken") != nil){
            accesstoken = ud.value(forKey: "accessToken") as! String
        }
        
        let header : HTTPHeaders = [
            "accessToken" : accesstoken
        ]
        
        let params : [String : Any] = [
            "profileId" : uid,
            "reviewReply" : [
                "replyReviewText" : self.oldText
            ]
        ]
        
        Alamofire.request(URL(string: url)!, method: .put, parameters: params, encoding: JSONEncoding.default , headers : header ).responseJSON { (response) in
            
            let result = response.result.value as! NSDictionary
//            print(result)
            
            let responseValue = result.value(forKey: "response") as! Bool
            if(responseValue){
                SVProgressHUD.showSuccess(withStatus: "Reply success")
                
                DispatchQueue.main.asyncAfter(deadline: .now() + 2){
                    SVProgressHUD.dismiss()
                    self.navigationController?.popViewController(animated: true)
                }
            }else{
                SVProgressHUD.showError(withStatus: "Can't reply review please try again later.")
                
                DispatchQueue.main.asyncAfter(deadline: .now() + 2){
                    SVProgressHUD.dismiss()
//                    self.navigationController?.popViewController(animated: true)
                }
            }
        }
    }
    
}

extension ReplyReviewViewController : UITextViewDelegate {
    func textViewShouldBeginEditing(_ textView: UITextView) -> Bool {
        
        textView.resignFirstResponder()
        self.view.endEditing(true)
        
        let vc = EnterTextViewController.instantiateFromStoryboard()
        vc.titleText = "Reply Review"
        vc.oldText = self.oldText
        vc.delegate = self
        
        self.present(vc, animated: true, completion: nil)
        
        return true
    }
}

extension ReplyReviewViewController : UITableViewDelegate , UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let arrayReply = reviewObject.value(forKey: "reviewReply") as! NSArray
        var returnValue = Int()
        
        if(owner){
            returnValue = arrayReply.count + 1
        }
        else{
            returnValue = arrayReply.count
        }
        
        return returnValue
        
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let arrayReply = reviewObject.value(forKey: "reviewReply") as! NSArray
        
        if(indexPath.row < arrayReply.count){
            let cell = tableView.dequeueReusableCell(withIdentifier: "reply", for: indexPath) as! replyReviewCell
            let objReply = reviewObject.value(forKey: "reviewReply") as! NSArray
            let arrayReply = objReply[indexPath.row] as! NSDictionary
            
            let replyReviewText = arrayReply.value(forKey: "replyReviewText") as! String
            let profile = arrayReply.value(forKey: "profile") as! NSDictionary
            let profileImageURL = profile.value(forKey: "profilePhoto") as! String
            let firstName = profile.value(forKey: "firstName") as! String
            let lastName = profile.value(forKey: "lastName") as! String
            
            cell.profileName.text = " \(firstName) \(lastName)"
            cell.profileImage.setRounded()
            cell.profileImage.sd_setImage(with: URL(string: profileImageURL), placeholderImage: UIImage(named: "bookme_loading"))
            cell.reviewText.text = replyReviewText
            cell.bgView.setRoundWithCommonR()
            
            return cell
        }
        else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "replyEdit", for: indexPath ) as! EnterReviewText
            
            cell.textView.delegate = self
//            cell.textView.layer.borderWidth = 0.1
//            cell.textView.layer.borderColor = UIColor.lightGray.cgColor
            cell.textView.setRoundWithR(r: commonValue.subTadius)
            cell.textView.text = self.oldText
            
            cell.sendButton.setRoundWithR(r: commonValue.subTadius)
            
            return cell
        }
    }
}
