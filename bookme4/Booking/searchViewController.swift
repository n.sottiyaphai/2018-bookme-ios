//
//  searchViewController.swift
//  bookme
//
//  Created by Naphat Sottiyaphai on 22/9/2561 BE.
//  Copyright © 2561 Naphat Sottiyaphai. All rights reserved.
//

import UIKit
import SVProgressHUD
import Alamofire
import BEMCheckBox
import GoogleMaps
import GooglePlaces
import ACFloatingTextfield_Swift
import SwiftDate
import ActionSheetPicker_3_0

class searchViewController: UIViewController {
    
    let sortingPostition     = 0
    let sortting             = ["New comer" , "Popular" , "Recommend" , "Best Review"]
    var SegueName            = String()
    var SegueimageProfileURL = String()
    var cat                  = [String]()
    var catID                = [String]()
    var catIDSelect          = String()
    var catSelect            = String()
    var dateSelected         = String()
    var filter               = 0
    var locationName         = String()
    var location_lat         = Double()
    var location_lng         = Double()
    var mode                 = String()
    var subcat               = [[Any]]()
    var subcatSelect         = [String]()
    
    private var isExpand     = false
    private var lastCatIndex = IndexPath()
    private var lastSection  = Int()
    
    @IBOutlet weak var bgView: UIView!
    @IBOutlet weak var dateTF: UITextField!
    @IBOutlet weak var dateTimeBGView: UIView!
    @IBOutlet weak var locationGB: UIView!
    @IBOutlet weak var locationTF: UITextField!
    @IBOutlet weak var mainButton: UIButton!
    @IBOutlet weak var searchBgView: UIView!
    @IBOutlet weak var searchingTF: UITextField!
    @IBOutlet weak var sortingCollectionView: UICollectionView!
    @IBOutlet weak var tableView: UITableView!
    
    class func instantiateFromStoryboard() -> searchViewController {
        let storyboard = UIStoryboard(name: "booking", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: String(describing: self)) as! searchViewController
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        dateTF.delegate = self
        locationTF.delegate = self
        sortingCollectionView.delegate = self
        
        bgView.setRoundWithR(r: commonValue.viewRadius)
        dateTimeBGView.setRoundWithR(r: commonValue.viewRadius)
        locationGB.setRoundWithR(r: commonValue.viewRadius)
        mainButton.setRoundedwithR(r: commonValue.subTadius)
        searchBgView.setRoundWithR(r: commonValue.viewRadius)
        searchingTF.setRoundWithR(r: commonValue.viewRadius)
        
        setBackgroundColor()
        
        self.title = "Search"
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        self.subcatSelect.removeAll()
        navigationController?.interactivePopGestureRecognizer?.isEnabled = false

        DispatchQueue.main.async {
            self.callCat()
        }
        
        if #available(iOS 11.0, *) {
            self.navigationController?.navigationBar.isHidden = false
            UINavigationBar.appearance().prefersLargeTitles = true
        }
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @IBAction func showResult(_ sender: Any) {
        SVProgressHUD.show()
        self.filterProfess()
    }
    
    func callCat() {
        
        self.cat.removeAll()
        self.subcat.removeAll()
        
        let ud = UserDefaults.standard
        var accesstoken = String()
        
        if(ud.object(forKey: "accessToken") != nil){
            accesstoken = ud.value(forKey: "accessToken") as! String
        }
        
        let header : HTTPHeaders = [
            "accessToken" : accesstoken
        ]
        
        
        Alamofire.request(baseURL.url + "categories", method: .get, encoding: JSONEncoding.default , headers : header)
            .responseJSON(completionHandler: { response in
                
                if(response.error == nil){
                if let result = response.result.value {
                    let JSON = result as! NSDictionary
                    
                    let JsonResult = JSON.value(forKey: "response") as! Bool
                    if(JsonResult){
                        let data = JSON.value(forKey: "data") as! NSArray
                        
                        for dataInArr in data {
                            let dictData = dataInArr as! NSDictionary
                            let catName = dictData.value(forKey: "catName") as! String
                            let subcat = dictData.value(forKey: "subCat") as! Array<Any>
                            let catID = dictData.value(forKey: "_id") as! String
                            
                            self.subcat.append(subcat)
                            self.cat.append(catName)
                            self.catID.append(catID)
                        }
                    }
                    DispatchQueue.main.async {
                        SVProgressHUD.dismiss()
                        self.tableView.reloadData()
                    }
                }
                else{
                    SVProgressHUD.showError(withStatus: "Network Error")
                }
                }
            })
    }
    
    func filterProfess() {
        
        let ud = UserDefaults.standard
        var accesstoken = String()
        
        if(ud.object(forKey: "accessToken") != nil){
            accesstoken = ud.value(forKey: "accessToken") as! String
        }
        
        let header : HTTPHeaders = [
            "sorting" : String(self.filter),
            "searchtext" : self.searchingTF.text!,
            "cetagoryId" : self.catIDSelect,
            "location_lat" : "\(self.location_lat)",
            "location_lng" : "\(self.location_lng)",
            "searchdate" : self.dateSelected,
            "accessToken" : accesstoken
        ]
        
        
        Alamofire.request("\(baseURL.url)professional", headers: header).responseJSON { response in
            
            if let results = response.result.value {
                let result = results as! NSDictionary
                
                if(result.object(forKey: "data") != nil){
                    if let datas = result.value(forKey: "data") {
                        if(datas is NSArray){
                            let data = datas as! NSArray
                            if(data.count >= 1){
                                
                                DispatchQueue.main.asyncAfter(deadline: .now()){
                                    SVProgressHUD.dismiss()
                                    
                                    let vc = MainCatViewController.instantiateFromStoryboard()
                                    
                                    vc.resultData = data
                                    vc.dataMode = "searching"
                                    vc.tag = self.catSelect
                                    vc.header = header
                                    vc.URLPath = "\(baseURL.url)professional"
                                    
                                    self.navigationController?.pushViewController(vc, animated: true)
                                    
                                }
                            }
                            else{
                                // Not found Data
                                self.showHUDError(string: "Sorry, We not found someone do you need.")
                            }
                        }
                        else{
                            // Not found Data
                            self.showHUDError(string: "Sorry, We not found someone do you need.")
                        }
                    }
                    else{
                        // Not found Data
                        self.showHUDError(string: "Sorry, We not found someone do you need.")
                    }
                }
                else{
                    // Data Error
                    self.showHUDError(string: "Data Incorrect")
                }
            }
            else{
                // Network Error
                self.showHUDError(string: "Network Error")
            }
        }
    }
    
    func showHUDError(string : String){
        SVProgressHUD.showError(withStatus: string)
        DispatchQueue.main.asyncAfter(deadline: .now() + 2){
            SVProgressHUD.dismiss()
        }
    }
    
    func getLanguageISO() -> String {
        let locale = Locale.current
        guard let languageCode = locale.languageCode,
            let regionCode = locale.regionCode else {
                return "de_DE"
        }
        return languageCode + "_" + regionCode
    }
    
    
}

extension searchViewController : UITableViewDataSource , UITableViewDelegate {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.cat.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cat", for: indexPath) as! categoryCell
        
        let cat = self.cat[indexPath.row]
        cell.catName.text = cat
        cell.checkBox.delegate = self
        cell.checkBox.tag = indexPath.row
        
        if(self.catIDSelect == self.catID[indexPath.row]){
            cell.checkBox.setOn(true, animated: true)
        }
        else{
            cell.checkBox.setOn(false, animated: false)
        }
        
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        print("select at : \(indexPath.row)")
        let cell = tableView.dequeueReusableCell(withIdentifier: "cat", for: indexPath) as! categoryCell
        
        cell.checkBox.isHidden = false
        self.catIDSelect = self.catID[indexPath.row]
        self.catSelect = self.cat[indexPath.row]
        
        DispatchQueue.main.async {
            self.tableView.reloadData()
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
}

extension searchViewController : UICollectionViewDelegate , UICollectionViewDataSource , UICollectionViewDelegateFlowLayout  {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return sortting.count
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        print(filter)
        
        if(filter == indexPath.row){
            let item = collectionView.dequeueReusableCell(withReuseIdentifier: "sort", for: indexPath) as! sortCollectionCell
            
            item.bgView.setRoundWithShadow(r: Float(collectionView.bounds.height/2-11))
            item.bgView.clipsToBounds = true
            
            item.textLabel.text = sortting[indexPath.row]
            
            // Lang Setup
            let userDefault = UserDefaults.standard
            let lang = userDefault.value(forKey: "langDevice") as! String
    
            return item
        }
            
        else {
            let item = collectionView.dequeueReusableCell(withReuseIdentifier: "sort2", for: indexPath) as! sortCollectionCell2
            
            item.bgView.setRoundWithR(r: Float(collectionView.bounds.height/2-11))
            item.bgView.clipsToBounds = true
            
            item.textLabel.text = sortting[indexPath.row]
            
            // Lang Setup
            let userDefault = UserDefaults.standard
            let lang = userDefault.value(forKey: "langDevice") as! String

            return item
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        return CGSize(width: collectionView.bounds.width/3, height: collectionView.bounds.height-15)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        //        SVProgressHUD.show()
        
        DispatchQueue.main.asyncAfter(deadline: .now()){
            self.filter = indexPath.row
            self.sortingCollectionView.reloadData()
        }
    }
    
}

extension searchViewController : BEMCheckBoxDelegate {
    func didTap(_ checkBox: BEMCheckBox) {
        
        print("tap at : \(checkBox.tag)")
        print("cat secect : \(self.catIDSelect)")
        self.catIDSelect = self.catID[checkBox.tag]
        self.catSelect = self.cat[checkBox.tag]
        
        DispatchQueue.main.async {
            self.tableView.reloadData()
        }
    }
}

extension searchViewController : GMSAutocompleteViewControllerDelegate  {
    func autocompleteClicked() {
        
        let autocompletecontroller = GMSAutocompleteViewController()
        autocompletecontroller.delegate = self
        let filter = GMSAutocompleteFilter()
        filter.type = .establishment  //suitable filter type
        filter.country = "TH"  //appropriate country code
        autocompletecontroller.autocompleteFilter = filter
        
        present(autocompletecontroller, animated: true, completion: nil)
    }
    
    // Handle the user's selection.
    func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {
        print("Place name: \(place.name)")
        print("Place address: \(String(describing: place.formattedAddress))")
        print("Place attributions: \(String(describing: place.attributions))")
        
        self.location_lat = place.coordinate.latitude
        self.location_lng = place.coordinate.longitude
        self.locationName = "\(place.name)"
        //        self.tableView.reloadData()
        self.locationTF.text = self.locationName
        
        dismiss(animated: true, completion: nil)
    }
    
    func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
        // TODO: handle the error.
        print("Error: ", error.localizedDescription)
    }
    
    // User canceled the operation.
    func wasCancelled(_ viewController: GMSAutocompleteViewController) {
        dismiss(animated: true, completion: nil)
    }
    
    // Turn the network activity indicator on and off again.
    func didRequestAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
    }
    
    func didUpdateAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
    }
    
}

extension searchViewController : UITextFieldDelegate {
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if(textField == locationTF){
            //            textField.endEditing(true)
            
            DispatchQueue.main.async {
                self.autocompleteClicked()
            }
        }
        else if (textField == dateTF){
            self.view.endEditing(true)
            textField.resignFirstResponder()
            
            print("Date picker Init")
            let action = ActionSheetDatePicker.init(title: "Select Date to Searching", datePickerMode: .date, selectedDate: Date(), doneBlock: { (picker, index, value) in
                let selectDate = index as! Date
                let deviceRegions = self.getLanguageISO()
                textField.text = selectDate.toFormat("dd MMM YYYY")
                
                if(deviceRegions == "en_TH"){
                    self.dateSelected = (selectDate - 543.years).toISO()
                }
                else{
                    self.dateSelected = selectDate.toISO()
                }
                
                textField.resignFirstResponder()
                
            }, cancel: { (picker) in
                print("cancel")
                self.dismiss(animated: true, completion: nil)
                
            }, origin: self.view.superview)
            
            action?.minimumDate = Date()
            action?.show()
        }
        
    }
    
//    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
//
//        if(textField == locationTF){
////            textField.endEditing(true)
//
//            DispatchQueue.main.async {
//                self.autocompleteClicked()
//            }
//        }
//        else if (textField == dateTF){
//            self.view.endEditing(true)
////            textField.resignFirstResponder()
//
//            print("Date picker Init")
//            let action = ActionSheetDatePicker.init(title: "Select Date to Searching", datePickerMode: .date, selectedDate: Date(), doneBlock: { (picker, index, value) in
//                let selectDate = index as! Date
//                let deviceRegions = self.getLanguageISO()
//                textField.text = selectDate.toFormat("dd MMM YYYY")
//
//                if(deviceRegions == "en_TH"){
//                    self.dateSelected = (selectDate - 543.years).toISO()
//                }
//                else{
//                    self.dateSelected = selectDate.toISO()
//                }
//
//                textField.resignFirstResponder()
//
//            }, cancel: { (picker) in
//                print("cancel")
//                self.dismiss(animated: true, completion: nil)
//
//            }, origin: self.view.superview)
//
//            action?.minimumDate = Date()
//            action?.show()
//        }
//
//        return true
//    }
}

class categoryCell: UITableViewCell {
    @IBOutlet var catName : UILabel!
    @IBOutlet weak var checkBox: BEMCheckBox!
}
