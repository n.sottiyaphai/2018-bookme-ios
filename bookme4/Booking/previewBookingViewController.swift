//
//  previewBookingViewController.swift
//  bookme
//
//  Created by Naphat Sottiyaphai on 17/6/2561 BE.
//  Copyright © 2561 Naphat Sottiyaphai. All rights reserved.
//

import UIKit

import ACFloatingTextfield_Swift
import BEMCheckBox
import Alamofire
import SVProgressHUD
import SwiftHash
import SwiftDate


class previewBookingViewController: UIViewController {
    
    @IBOutlet weak var mainButton: UIButton!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var profileImage: UIImageView!
    @IBOutlet weak var profileName: UILabel!
    @IBOutlet weak var profileTitle: UILabel!
    @IBOutlet weak var destTextView: UITextView!
    @IBOutlet weak var jobtitle: UILabel!
    @IBOutlet weak var packPrice: UILabel!
    @IBOutlet weak var packName: UILabel!
    @IBOutlet weak var packBGView: UIView!
    @IBOutlet weak var dateTime: UILabel!
    @IBOutlet weak var location: UILabel!
    @IBOutlet weak var wwbgView: UIView!
    
    @IBOutlet weak var collectionViewHight: NSLayoutConstraint!
    @IBOutlet weak var padingHeight: NSLayoutConstraint!
    
    var package = [NSDictionary]()
    var professID = String()
    var profess_uid = String()
    var profileImageURL = String()
    var profileNames = String()
    var profileTitles = String()
    
    var packNames = String()
    var packProce = String()
    var dateTimes = String()
    var locations = String()
    
    var jobName = String()
    var jobText = String()
    
    var locationName = String()
    var location_lat = Double()
    var location_lng = Double()
    var dateTimeFull = DateInRegion()
    
    var imageName = [String]()
    var imageData = [UIImage]()
    var imageDataJPGE = [Data]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        destTextView.textContainerInset = .zero
        profileImage.sd_setImage(with: URL(string: profileImageURL), placeholderImage: UIImage(named: "bookme_loading"))
        profileImage.setRounded()
        
        profileName.text = profileNames
        profileTitle.text = profileTitles
        
        jobtitle.text = " \(jobName)"
        jobtitle.setTextBold()
        jobtitle.setRoundWithCommonR()
        //        jobtitle.padding = UIEdgeInsets(top: 8, left: 15, bottom: 8, right: 15)
        
        destTextView.text = jobText
        destTextView.setSubText()
        destTextView.setRoundWithCommonR()
        destTextView.contentInset = UIEdgeInsets(top: 15, left: 15, bottom: 15, right: 15);
        
        packName.text = packNames
        packName.setSubTextSemiBold()
        
        dateTime.text = dateTimes
        dateTime.setSubText()
        
        location.text = locationName
        location.setSubText()
        
        packBGView.setRoundWithCommonR()
        wwbgView.setRoundWithCommonR()
        
        let intPrice = Int(packProce)
        let myNumber = NSNumber(value:intPrice!)
        
        let numberFormatter = NumberFormatter()
        numberFormatter.numberStyle = .decimal
        
        if let prices = numberFormatter.string(from: myNumber){
            packPrice.text  = "\(prices) Baht"
            packPrice.setDescTextLight()
        }
        
        mainButton.setRoundedwithR(r: commonValue.viewRadius)
        
        self.setClearNavBar()
        self.setBackgroundColor()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    
    @IBAction func next(_ sender: Any) {
        if(!imageData.isEmpty){
            for data in imageData{
                let image = data.jpegData(compressionQuality: commonValue.imageCompress)
                //                let image = UIImageJPEGRepresentation(data, commonValue.imageCompress)
                self.imageDataJPGE.append(image!)
            }
            imageUpload(data: self.imageDataJPGE)
        }else{
            self.postBooking()
        }
    }
    
    func imageUpload(data : Array<Data>) {
        
        let ud = UserDefaults.standard
        let uid = ud.value(forKey: "user_uid") as! String
        let md5_uid = MD5(uid)
        let url = URL(string: "\(baseURL.media)api/uploads/bookme")
        
        let headers: HTTPHeaders = [
            "Content-Type": "application/json",
            "appId": apiKey.appId,
            "clientId" : apiKey.clientId,
            "secretId" : apiKey.secretId
        ]
        
        Alamofire.upload(multipartFormData: { (multipartFormData) in
            
            multipartFormData.append("\(uid)".data(using: String.Encoding.utf8)!, withName: "uId" as String)
            
            for images in data {
                let data = images
                multipartFormData.append(data, withName: "imgFiles", fileName: "\(md5_uid)_swift_file.jpeg", mimeType: "image/jpeg")
            }
            
            
        }, usingThreshold: UInt64.init() , to: url!, method: .post , headers: headers  , encodingCompletion: { (result) in
            
            switch result {
            case .success(let upload, _, _):
                
                upload.uploadProgress(closure: { (progress) in
                    SVProgressHUD.showProgress(Float(progress.fractionCompleted), status: "Image Uploading")
                })
                
                upload.responseJSON { response in
                    if let result = response.result.value {
                        
                        let results = result as! NSDictionary
                        if(results.value(forKey: "data") is Array<String>){
                            let ArryResult = results.value(forKey: "data") as! Array<String>
                            
                            print("Upload Done")
                            
                            self.imageName = ArryResult
                            self.postBooking()
                        }
                        SVProgressHUD.dismiss()
                        
                    }
                    else{
                        print("Error")
                        print(response.error?.localizedDescription ?? "")
                        SVProgressHUD.dismiss()
                        SVProgressHUD.showError(withStatus: response.error?.localizedDescription ?? "")
                    }
                }
                
            case .failure(let encodingError):
                print("ERROR : \(encodingError)")
                SVProgressHUD.showError(withStatus: "Media Connection Error : \(encodingError)")
                break
            }
        })
    }
    
    func postBooking(){
        
        print(dateTimeFull.toISO())
        
        //Prepare Date for booking
        let isoDate = (dateTimeFull.date + 543.years).toISO()
        
        let userDefaults = UserDefaults.standard
        let uid = userDefaults.value(forKey: "user_uid") as! String
        let url = "\(baseURL.url)booking"
        
        let ud = UserDefaults.standard
        var accesstoken = String()
        
        if(ud.object(forKey: "accessToken") != nil){
            accesstoken = ud.value(forKey: "accessToken") as! String
        }
        
        let header : HTTPHeaders = [
            "accessToken" : accesstoken
        ]
        
        let params : [ String : Any ] = [
            "customerId" : uid,
            "profileId" : profess_uid,
            "professId" : professID,
            "jobTitle" : jobName,
            "jobDescription" : jobText,
            "bookingPackage" : professID,
            "bookingPackageName" : packNames,
            "bookingPackagePrice" : Double(packProce)!,
            "bookingPackageType" : 1,
            "bookingPackageUnit" : "",
            "bookingPackageTime" : "",
            "bookingPackageDetail" : "",
            "bookingServiceFee" : "",
            "bookingDate" : [isoDate],
            "bookingProcessStatus" : 1,
            "bookingCancelReason" : "",
            "bookingLocation" : [
                "latitude" : location_lat,
                "longitudes" : location_lng,
                "locationName" : locationName
            ],
            "bookingWorkUpload" : imageName
        ]
        
        print(url)
        print(params)
        
        Alamofire.request(url, method: .post , parameters: params, encoding: JSONEncoding.default , headers : header).responseJSON(completionHandler: {(respond) in
            let result = respond.result.value
            print(result!)
            
            if(result is NSDictionary){
                let dictData = result as! NSDictionary
                var API_Error = String()
                
                if(dictData.value(forKey: "err") is String){
                    API_Error = dictData.value(forKey: "err") as! String
                }
                else{
                    let error  = dictData.value(forKey: "err") as! NSDictionary
                    API_Error = error.value(forKey: "message") as! String
                }
                
                let API_Respond = dictData.value(forKey: "response") as! Bool
                if(API_Respond){
                    self.performSegue(withIdentifier: "booking", sender: self)
                }
                else{
                    SVProgressHUD.showError(withStatus: API_Error)
                    self.navigationController?.popViewController(animated: true)
                }
            }
            else{
                SVProgressHUD.showError(withStatus: "API Error")
                self.navigationController?.popViewController(animated: true)
            }
            
        })
    }
    
    @IBOutlet weak var editDate: UIButton!
    @IBAction func editDateAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBOutlet weak var editLocation: UIButton!
    @IBAction func editLocationAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
}

extension previewBookingViewController : UICollectionViewDelegate , UICollectionViewDataSource , UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        print(imageData.count)
        if( imageData.count == 0){
            collectionViewHight.constant = 0
            padingHeight.constant = 0
        }
        
        return imageData.count
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let item = collectionView.dequeueReusableCell(withReuseIdentifier: "image", for: indexPath) as! imageCollectionItem
        
        item.image.image = imageData[indexPath.row]
        item.image.setRoundWithR(r: commonValue.subTadius)
        
        return item
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        return CGSize(width: collectionView.bounds.height-10 , height: collectionView.bounds.height-10 )
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
    }
}
