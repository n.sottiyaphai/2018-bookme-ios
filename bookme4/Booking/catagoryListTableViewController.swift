//
//  catagoryListTableViewController.swift
//  bookme4
//
//  Created by Naphat Sottiyaphai on 24/3/2561 BE.
//  Copyright © 2561 Naphat Sottiyaphai. All rights reserved.
//

import UIKit
import Alamofire
import SVProgressHUD
import StatusAlert
import SwiftHash
import GoogleSignIn

class catagoryListTableViewController: UITableViewController  , back2profile {
    
    func go2profile(id: String) {
        let vc = profileViewController.instantiateFromStoryboard()
        vc.uid = id
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    var delegate : back2profile? = nil
    
//    @IBOutlet var tagList: TagListView!
    @IBOutlet var titleNam: UILabel!
    @IBOutlet var catImage: UIImageView!
    
    var proList = [Any]()
    var favCounts = [Any]()
    var tag = ""
    var icon = ""
    
    class func instantiateFromStoryboard() -> catagoryListTableViewController {
        let storyboard = UIStoryboard(name: "booking", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: String(describing: self)) as! catagoryListTableViewController
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
//        self.setClearNavBar()
//        self.setBackgroundColor()
        
        // Lang Setup
        let userDefault = UserDefaults.standard
        
        let dataMode = userDefault.value(forKey: "dataMode") as! String
        if(dataMode == "normal"){
            
            self.tag = userDefault.value(forKey: "catagory_tag") as! String
            self.icon = userDefault.value(forKey: "catagory_icon") as! String
            self.titleNam.text = tag
            
//            tagList.textFont = UIFont.systemFont(ofSize: 15)
//            tagList.alignment = .left
//            tagList.addTag(tag)
            
            self.catImage.sd_setImage(with: URL(string: self.icon))
            self.callProfess()
        }
        else{
            
            let header = userDefault.value(forKey: "header") as! HTTPHeaders
            let URLPath = userDefault.value(forKey: "URLPath") as! String
            
            self.getHttpRequest(header: header, url: URLPath)
            
            //            self.tag = userDefault.value(forKey: "catagory_tag") as! String
            //            titleNam.text = self.tag
            //            self.proList = proListData as! [Any]
            //
            //            for favCount in proList {
            //                let dict = favCount as! NSDictionary
            //                let favCount = dict.value(forKey: "favCount") as! Int64
            //                self.favCounts.append(favCount)
            //            }
            //
            //            DispatchQueue.main.async {
            //                self.tableView.reloadData()
            //            }
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        URLCache.shared.removeAllCachedResponses()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.navigationController?.navigationBar.isHidden = false
        
        if #available(iOS 11.0, *) {
            navigationController?.navigationBar.prefersLargeTitles = false
        } else {
            print("It's down of iOS 11")
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        self.setClearNavBar()
    }
    
    func getHttpRequest(header : HTTPHeaders , url : String) {
        
        print(header)
        
        Alamofire.request("\(url)", headers: header).responseJSON { response in
            
            if let results = response.result.value {
                let result = results as! NSDictionary
                
                if(result.object(forKey: "data") != nil){
                    if let datas = result.value(forKey: "data") {
                        if(datas is NSArray){
                            let data = datas as! NSArray
                            if(data.count >= 1){
                                
                                print(data)
                                
                                for dataInArr in data {
                                    let dict = dataInArr as! NSDictionary
                                    let favCount = dict.value(forKey: "favCount") as! Int64
                                    
                                    self.favCounts.append(favCount)
                                    self.proList.append(dataInArr)
                                }
                                
                                DispatchQueue.main.asyncAfter(deadline: .now()){
                                    SVProgressHUD.dismiss()
                                    self.tableView.reloadData()
                                }
                            }
                        }
                    }
                }
            }
        }
    }
    
    func callProfile() {
        
        let ud = UserDefaults.standard
        var accesstoken = String()
        
        if(ud.object(forKey: "accessToken") != nil){
            accesstoken = ud.value(forKey: "accessToken") as! String
        }
        
        let header : HTTPHeaders = [
            "accessToken" : accesstoken
        ]
        
        let userDefaults = UserDefaults.standard
        let uid = userDefaults.value(forKey: "user_uid") as! String
        
        Alamofire.request(baseURL.url + "profile/\(uid)", method: .get, encoding: JSONEncoding.default , headers : header)
            .responseJSON(completionHandler: { response in
                
                if let result = response.result.value {
                    let data = result as! NSDictionary
                    
                    if(data.value(forKey: "data") is NSDictionary){
                        let JSON = data.value(forKey: "data") as! NSDictionary
                        if let fav = JSON.value(forKey: "favorite"){
                            var favUserPID = [String]()
                            let favArray = fav as! NSArray
                            
                            for favData in favArray{
                                let dataDict = favData as! NSDictionary
                                let pid = dataDict.value(forKey: "_id") as! String
                                favUserPID.append(pid)
                            }
                            userDefaults.set(favUserPID, forKey: "userData_FavoritePID")
                        }
                    }
                    
                    self.callProfess()
                    
                }
                else{
                    SVProgressHUD.showError(withStatus: "Cannot Fetch your profile Please Login again.")
                    
//                    let api = Instagram.shared
//                    api.logout()
                    
                    GIDSignIn.sharedInstance().signOut()
                    
                    let domain = Bundle.main.bundleIdentifier!
                    UserDefaults.standard.removePersistentDomain(forName: domain)
                    UserDefaults.standard.synchronize()
                    
                    let vc = LoginViewController.instantiateFromStoryboard()
                    self.navigationController?.present(vc, animated: true, completion: nil)
                    
                }
            })
    }
    
    func callProfess() {
        
        proList.removeAll()
        favCounts.removeAll()
        
        let userDefault = UserDefaults.standard
        let catId = userDefault.value(forKey: "catagory_id") as! String
        
        let ud = UserDefaults.standard
        var accesstoken = String()
        
        if(ud.object(forKey: "accessToken") != nil){
            accesstoken = ud.value(forKey: "accessToken") as! String
        }
        
        let header : HTTPHeaders = [
            "accessToken" : accesstoken,
            "cetagoryId" : catId
        ]
        
        Alamofire.request(baseURL.url + "professional", method: .get, encoding: JSONEncoding.default , headers : header)
            .responseJSON(completionHandler: { response in
                
                if let result = response.result.value {
                    let JSON = result as! NSDictionary
                    
                    let JsonResult = JSON.value(forKey: "response") as! Bool
                    if(JsonResult){
                        let data = JSON.value(forKey: "data") as! NSArray
                        
                        for dataInArr in data {
                            let dict = dataInArr as! NSDictionary
                            let favCount = dict.value(forKey: "favCount") as! Int64
                            
                            self.favCounts.append(favCount)
                            self.proList.append(dataInArr)
                        }
                    }
                    
                    SVProgressHUD.dismiss()
                    
                    DispatchQueue.main.async {
                        self.tableView.reloadData()
                    }
                }
                else{
                    SVProgressHUD.showError(withStatus: "Network Error")
                }
            })
        
    }
    
    @IBAction func FavPush(_ sender: UIButton) {
        
        let userDefaults = UserDefaults.standard
        let index = sender.tag
        
        let data = proList[index] as! NSDictionary
        let pids = data.value(forKey: "_id") as! String
        
        // Data From UserDefault
        let uid = userDefaults.value(forKey: "user_uid") as! String
        
        if (sender.image(for: .normal) == UIImage(named: "icon_fav_red")){
            // Already Favorited
            
            let alert = UIAlertController(title: "Are you sure ?", message: "Do you want to unfavorite this professional", preferredStyle: .alert)
            
            let ok = UIAlertAction(title: "OK", style: .default) { (action) in
                print("Capture action OK")
                self.favoriteDelete(uid: uid, pid: pids)
            }
            
            let cancel = UIAlertAction(title: "Cancel", style: .cancel) { (action) in
                print("Capture action Cancel")
                self.dismiss(animated: false, completion: nil)
            }
            
            alert.addAction(ok)
            alert.addAction(cancel)
            
            self.present(alert, animated: true, completion: nil)
        }
        else {
            // NOT Favorite
            self.favoriteAdd(uid: uid, pid: pids)
        }
        
    }
    
    func favoriteAdd(uid : String , pid : String) {
        
        //        let userDefaults = UserDefaults.standard
        let param : [String:Any] = [
            "professId" : pid
        ]
        
        Alamofire.request(baseURL.url + "favorite/" + "\(uid)", method: .post , parameters: param , encoding: JSONEncoding.default)
            .responseJSON(completionHandler: { response in
                
                if let result = response.result.value {
                    let respond = result as! NSDictionary
                    
                    if(respond.value(forKey: "response") as! Bool){
                        
                        // Creating StatusAlert instance
                        let statusAlert = StatusAlert()
                        
                        statusAlert.image = commonImage.iconFavBig
                        statusAlert.title = "Favorited"
                        statusAlert.message =  "You can open favorites list in favorited tab."
                        statusAlert.canBePickedOrDismissed = true
                        
                        // Presenting created instance
                        statusAlert.showInKeyWindow()
                        
                        DispatchQueue.main.async {
                            self.callProfile()
                        }
                    }
                }
                else{
                    SVProgressHUD.showError(withStatus: "Error")
                }
            })
    }
    
    func favoriteDelete(uid : String , pid : String) {
        
        //        let userDefaults = UserDefaults.standard
        let param : [String:Any] = [
            "professId" : pid
        ]
        
        
        Alamofire.request(baseURL.url + "favorite/delete/" + "\(uid)", method: .post , parameters: param , encoding: JSONEncoding.default)
            .responseJSON(completionHandler: { response in
                
                if let result = response.result.value {
                    let respond = result as! NSDictionary
                    
                    if(respond.value(forKey: "response") as! Bool){
                        
                        // Creating StatusAlert instance
                        let statusAlert = StatusAlert()
                        
                        statusAlert.image = commonImage.iconUnfavBig
                        statusAlert.title = "Unfavorite"
                        statusAlert.message =  "unfavorite complete."
                        statusAlert.canBePickedOrDismissed = true
                        
                        // Presenting created instance
                        statusAlert.showInKeyWindow()
                    }
                    else{
                        print("ERROR : Data type not correct.")
                    }
                    
                    DispatchQueue.main.async {
                        self.callProfile()
                    }
                }
                else{
                    SVProgressHUD.showError(withStatus: "Error")
                }
            })
    }
    // MARK: - Table view data source
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if(proList.count == 0){
            self.tableView.setEmptyMessage("Be the first person in \(tag)")
        }
        else{
            self.tableView.restore()
        }
        
        // #warning Incomplete implementation, return the number of rows
        return proList.count
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return commonValue.profileCellHeight
        
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "profiles2", for: indexPath) as! profile2TableCell
        
        if (proList[indexPath.row] is NSDictionary){
            let data = proList[indexPath.row] as! NSDictionary
            
            if(data.value(forKey: "profile") is NSDictionary){
                let profile = data.value(forKey: "profile") as! NSDictionary
                let pids = data.value(forKey: "_id") as! String
                let profileImage = profile.value(forKey: "profilePhoto") as? String
                let firstName = profile.value(forKey: "firstName") as? String
                let lastName = profile.value(forKey: "lastName") as? String
                //                let title = profile.value(forKey: "tittle") as? String
                let category = data.value(forKey: "categories") as! NSDictionary
                let catName = category.value(forKey: "catName") as! String
                
                if let first = firstName , let last = lastName {
                    cell.profileName.text = "\(first) \(last)"
                }
                
//                cell.profileImage.sd_setImage(with: URL(string: profileImage!), placeholderImage: UIImage(named : "bookme_loading"))
                cell.profileImage.kf.setImage(with: URL(string: profileImage!),
                             placeholder: UIImage(named: "bookme_loading"),
                             options: [
                                .scaleFactor(UIScreen.main.scale),
                                .transition(.fade(0.5)),
                                .cacheOriginalImage
                    ])
                cell.profileImage.setRounded()
                
                //                cell.profileDest.text = title
                
                cell.profileDest.text = ""
                cell.catName.text = catName
                
                cell.onlineIndecator.setRounded()
                cell.onlineIndecator.layer.borderWidth = 2
                cell.onlineIndecator.layer.borderColor = UIColor.white.cgColor
                cell.onlineIndecator.isHidden = true
                
                cell.collectionView.delegate = self
                cell.collectionView.reloadData()
                
                let userDefaults = UserDefaults.standard
                
                if let fav = userDefaults.value(forKey: "userData_FavoritePID"){
                    let userFav = fav as! Array<String>
                    if(!userFav.filter{$0 == pids}.isEmpty){
                        cell.favButton.setImage(UIImage(named: "icon_fav_red"), for: .normal)
                    }
                    else{
                        cell.favButton.setImage(UIImage(named: "icon_fav"), for: .normal)
                    }
                    cell.favButton.tag = indexPath.row
                    cell.favCount.isHidden = false
                    cell.favCount.text = "\(self.favCounts[indexPath.row])"
                }
                else{
                    cell.favButton.tag = indexPath.row
                    cell.favCount.isHidden = false
                    cell.favCount.text = "\(self.favCounts[indexPath.row])"
                    print("Error : cannot load userData_FavoritePID")
                }
                
                // Lang Setup
                let userDefault = UserDefaults.standard
                let lang = userDefault.value(forKey: "langDevice") as! String
                
                cell.bgView.backgroundColor = UIColor.white
                cell.bgView.setRoundWithR(r: commonValue.viewRadius)
            }
        }
        else{
            print("Prolist Error : DATA Type not Correct.")
        }
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        guard let tableViewCell = cell as? profile2TableCell else { return }
        tableViewCell.setCollectionViewDataSourceDelegate(self, forRow: indexPath.row)
        
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let vc = profileViewController.instantiateFromStoryboard()
        
        let data = proList[indexPath.row] as! NSDictionary
        let uids = data.value(forKey: "_id") as! String
        
        vc.uid = uids
        
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    override func scrollViewDidScroll(_ scrollView: UIScrollView) {
//
//        var offest = scrollView.contentOffset.y / 150
//        if(offest > 1){
//            offest = 1
//            self.navigationController?.navigationBar.backgroundColor = UIColor(red: 1, green: 1, blue: 1, alpha: offest)
//           UIApplication.shared.statusBar?.backgroundColor = UIColor(red: 1, green: 1, blue: 1, alpha: offest)
//        }
//        else{
//            self.navigationController?.navigationBar.backgroundColor = UIColor(red: 1, green: 1, blue: 1, alpha: offest)
//           UIApplication.shared.statusBar?.backgroundColor = UIColor(red: 1, green: 1, blue: 1, alpha: offest)
//        }
    }
    
    
    class func embed(
        parent:UIViewController,
        container:UIView,
        child:UIViewController,
        previous:UIViewController?){
        
        if let previous = previous {
            removeFromParent(vc: previous)
        }
        child.willMove(toParent: parent)
        parent.addChild(child)
        container.addSubview(child.view)
        child.didMove(toParent: parent)
        let w = container.frame.size.width;
        let h = container.frame.size.height;
        child.view.frame = CGRect(x: 0, y: 0, width: w, height: h)
    }
    
    class func removeFromParent(vc:UIViewController){
        vc.willMove(toParent: nil)
        vc.view.removeFromSuperview()
        vc.removeFromParent()
    }
    
    class func embed(withIdentifier id:String, parent:UIViewController, container:UIView, completion:((UIViewController)->Void)? = nil ){
        let vc = parent.storyboard!.instantiateViewController(withIdentifier: id)
        embed(
            parent: parent,
            container: container,
            child: vc,
            previous: parent.children.first
        )
        completion?(vc)
    }
}

extension catagoryListTableViewController : UICollectionViewDelegate , UICollectionViewDataSource , UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        let data = proList[collectionView.tag] as! NSDictionary
        
        let portfolio = data.value(forKey: "portfolio") as! NSDictionary
        let Gallery = portfolio.value(forKey: "gallery") as? Array<Any>
        
        return Gallery!.count
        
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let item = collectionView.dequeueReusableCell(withReuseIdentifier: "image", for: indexPath) as! imageCollectionItem
        
        let data = proList[collectionView.tag] as! NSDictionary
        let portfolio = data.value(forKey: "portfolio") as! NSDictionary
        let Gallery = portfolio.value(forKey: "gallery") as? Array<String>
        //        let pro_uid = data.value(forKey: "uId") as! String
        
        //        let md5 = MD5(pro_uid)
        
        //        let url = "\(baseURL.photoURL)user/\(md5.lowercased())/bookme/thumbs/\(Gallery![indexPath.row])"
        
        item.image.kf.setImage(with: URL(string: Gallery![indexPath.row]),
                            placeholder: UIImage(named: "bookme_loading"),
                            options: [
                                //                                        .processor(DownsamplingImageProcessor(size: imageView.size))
                                .scaleFactor(UIScreen.main.scale),
                                .transition(.fade(0.2)),
            ])
        
        item.image.setRoundWithR(r: commonValue.subTadius)
        
        
        return item
        
        
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        return CGSize(width: collectionView.bounds.height, height: collectionView.bounds.height)
    }
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        SVProgressHUD.show()
        
        let data = proList[collectionView.tag] as! NSDictionary
        let portfolio = data.value(forKey: "portfolio") as! NSDictionary
        let Gallery = portfolio.value(forKey: "Gallery") as? Array<String>
        let uids = data.value(forKey: "_id") as! String
        let profress_uid  = data.value(forKey: "profileId") as! String
        let md5_uid = MD5(profress_uid)
        
        let profile = data.value(forKey: "profile") as! NSDictionary
        let profileImage = profile.value(forKey: "profilePhoto") as? String
        let firstName = profile.value(forKey: "firstName") as? String
        let lastName = profile.value(forKey: "lastName") as? String
        
        var url = [URL]()
        
        for URLs in Gallery!{
            //            let full_url = "\(baseURL.photoURL)user/\(md5_uid.lowercased())/bookme/storages/\(URLs)"
            url.append( URL(string: URLs)! )
        }
        
        let vc = PreviewViewController.instantiateFromStoryboard()
        
        vc.urlArray = url
        vc.profileID = uids
        vc.imageProfileURL = profileImage!
        vc.indexPage = indexPath.row
        
        if let first = firstName , let last = lastName {
            vc.profileNameString = "\(first) \(last)"
        }
        
        vc.delegate = self
        
        SVProgressHUD.dismiss()
        present(vc, animated: true, completion: nil)
    }
    
    
}
