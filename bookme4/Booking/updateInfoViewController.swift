//
//  updateInfoViewController.swift
//  bookme
//
//  Created by Naphat Sottiyaphai on 28/8/2561 BE.
//  Copyright © 2561 Naphat Sottiyaphai. All rights reserved.
//

import UIKit
import ACFloatingTextfield_Swift
import Alamofire
import SVProgressHUD
import SwiftHash
import SwiftDate
import ActionSheetPicker_3_0
import BEMCheckBox
import StatusAlert

class updateInfoViewController: UIViewController {
    
    @IBOutlet weak var bdTF: ACFloatingTextfield!
    @IBOutlet weak var bgView: UIView!
    @IBOutlet weak var checkBox: BEMCheckBox!
    @IBOutlet weak var firstNameTF: ACFloatingTextfield!
    @IBOutlet weak var imageProfile: UIImageView!
    @IBOutlet weak var lastNameTF: ACFloatingTextfield!
    @IBOutlet weak var mobileNumberTF: ACFloatingTextfield!
    @IBOutlet weak var titleTF: ACFloatingTextfield!
    @IBOutlet weak var updateButton: UIButton!
    
    var RefreshCount    = 0
    var acceptTerm      = false
    var address         = String()
    var bd              = String()
    var bdText          = String()
    var email           = String()
    var firstName       = String()
    var id              = String()
    var imageURL        = String()
    var lastName        = String()
    var mobile          = String()
    var selectImage     = UIImage()
    var titles          = String()
    var userProfileData = NSDictionary()
    
    class func instantiateFromStoryboard() -> updateInfoViewController {
        let storyboard = UIStoryboard(name: "booking", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: String(describing: self)) as! updateInfoViewController
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.callBluweoProfile()
        
        bdTF.delegate = self
        mobileNumberTF.delegate = self
        lastNameTF.delegate = self
        firstNameTF.delegate = self
        titleTF.delegate = self
        
        checkBox.delegate = self
        
        imageProfile.setRounded()
        bgView.setRoundWithShadow(r: commonValue.viewRadius)
        updateButton.setRoundWithR(r: commonValue.viewRadius)
    }
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func ImageUploading(data : Data) {
        
        let ud = UserDefaults.standard
        let uid = ud.value(forKey: "user_uid") as! String
        let md5_uid = MD5(uid)
        let url = URL(string: "\(baseURL.media)api/uploads/")
        
        Alamofire.upload(multipartFormData: { (multipartFormData) in
            
            multipartFormData.append("\(uid)".data(using: String.Encoding.utf8)!, withName: "uId" as String)
            multipartFormData.append(data, withName: "imgFiles", fileName: "\(uid)_swift_file.jpeg", mimeType: "image/jpeg")
            
            
        },to: url! )
        { (result) in
            
            switch result {
            case .success(let upload, _, _):
                
                upload.uploadProgress(closure: { (progress) in
                    SVProgressHUD.showProgress(Float(progress.fractionCompleted), status: "Image Uploading")
                })
                
                upload.responseJSON { response in
                    
                    if let result = response.result.value {
                        let results = result as! NSDictionary
                        let data = results.value(forKey: "data") as! NSArray
                        
                        self.imageURL = "\(baseURL.photoURL)\(md5_uid.lowercased())/storages/\(data[0])"
                        self.imageProfile.sd_setImage(with: URL(string: self.imageURL), placeholderImage: UIImage(named: "bookme_loading"))
                        
                        self.updateImageProfile(data : self.userProfileData)
                        
                        SVProgressHUD.dismiss()
                        
                    }
                }
                
            case .failure(let encodingError):
                print("ERROR : \(encodingError)")
                SVProgressHUD.showError(withStatus: "Media Connection Error : \(encodingError)")
                break
            }
        }
    }
    
    func updateProfile(data : NSDictionary , back : Bool) {
        
        let ud = UserDefaults.standard
        let uid = ud.value(forKey: "user_uid") as! String
        
        print("uid : \(uid)")
        
        let titles = self.titleTF.text
        let firstName = self.firstNameTF.text
        let lastName = self.lastNameTF.text
        let birthDate = self.bd
        let profilePhoto = self.imageURL
        let backgroundPhoto = data.value(forKey: "backgroundPhoto") as! String
        let address = self.address
        let city = ""
        let country = ""
        let mobile = self.mobileNumberTF.text
        let email = data.value(forKey: "email") as! String
        
        var accesstoken = String()
        
        if(ud.object(forKey: "accessToken") != nil){
            accesstoken = ud.value(forKey: "accessToken") as! String
        }
        
        let header : HTTPHeaders = [
            "accessToken" : accesstoken
        ]
        
        
        let parameters: [String: Any] = [
            "title" : titles!,
            "firstName" : firstName!,
            "lastName" : lastName!,
            "birthDate" : birthDate,
            "profilePhoto" : profilePhoto,
            "backgroundPhoto" : backgroundPhoto,
            "address" : address,
            "city" : city,
            "country" : country,
            "mobile" : mobile!,
            "email" : email,
        ]
        
        
        print(parameters)
        
        Alamofire.request(baseURL.url + "profile/\(uid)", method: .put, parameters: parameters, encoding: JSONEncoding.default , headers : header)
            .responseJSON(completionHandler: { response in
                
                if let result = response.result.value {
                    _ = result as! NSDictionary
                    
                    SVProgressHUD.showSuccess(withStatus: "Update done")
                    
                    DispatchQueue.main.asyncAfter(deadline: .now()+1){
                        SVProgressHUD.dismiss()
                        let ud = UserDefaults.standard
                        
                        let uid = ud.value(forKey: "user_uid") as! String
                        
                        print("uid : \(uid)")
                        self.callBluweoProfile()
                        
                        if(back){
                            self.navigationController?.popViewController(animated: true)
                            self.dismiss(animated: true, completion: nil)
                        }
                    }
                }
                else{
                    SVProgressHUD.showError(withStatus: "Network Error")
                }
            })
    }
    
    func updateBluweoProfile(data : NSDictionary) {
        
        SVProgressHUD.show()
        
        let ud = UserDefaults.standard
        let uid = ud.value(forKey: "user_uid") as! String
        let accessToken = ud.value(forKey: "bluweoAccessToken") as! String
        print("uid : \(uid)")
        
        let titles = self.titleTF.text
        let firstName = self.firstNameTF.text
        let lastName = self.lastNameTF.text
        let birthDate = self.bd
        let profilePhoto = self.imageURL
        let mobile = self.mobileNumberTF.text
        let email = data.value(forKey: "email") as! String
        
        let url = "\(baseURL.bluweoProfile)?access_token=\(accessToken)"
        
        let headers = [
            "client_id" : "1001",
            "secret_key" : "bookme",
            "Content-Type": "application/x-www-form-urlencoded"
        ]
        let params = [
            "title" : titles!,
            "first_name" : firstName!,
            "last_name" : lastName!,
            "birthDate" : birthDate,
            "profile_photo" : profilePhoto,
            "mobile" : mobile!,
            "email" : email,
        ]
        
        
        Alamofire.request(URL(string: url)!, method: .put , parameters: params, encoding: URLEncoding(destination: .httpBody) , headers: headers).responseJSON(completionHandler: { (respond) in
            
            if let result = respond.result.value{
                let responsed = result as! NSDictionary
                print(responsed)
                
                let response = responsed.value(forKey: "response") as! Bool
                if(response){
                    let data = responsed.value(forKey: "data") as! NSDictionary
                    let access_token = data.value(forKey: "access_token") as! String
                    let refresh_token = data.value(forKey: "refresh_token") as! String
                    
                    let userDefaults = UserDefaults.standard
                    userDefaults.set(access_token, forKey: "bluweoAccessToken")
                    userDefaults.set(refresh_token, forKey: "bluweoRefreshToken")
                    
                    SVProgressHUD.dismiss()
                    print("!! Update Success")
                    
                    //                    self.performSegue(withIdentifier: "authSuccess", sender: self)
                }
                else{
                    let data = responsed.value(forKey: "data") as! NSDictionary
                    let message = data.value(forKey: "message") as! NSArray
                    let msg = message[0] as! NSDictionary
                    let errMsg = msg.value(forKey: "message") as! String
                    
                    self.RefreshCount+=1
                    if(errMsg == "access token not valid or expired"){
                        self.refreshBluweoToken()
                        
                    }
                    
                    SVProgressHUD.show()
                    DispatchQueue.main.asyncAfter(deadline: .now() + 3){
                        
                        if(self.RefreshCount < 3){
                            self.updateBluweoProfile(data: self.userProfileData)
                        }
                        else{
                            SVProgressHUD.showError(withStatus: "Something wrong.\nTry again later")
                            self.RefreshCount = 0
                        }
                    }
                }
            }
            else{
                print(respond.error?.localizedDescription ?? "")
                SVProgressHUD.dismiss()
            }
        })
    }
    
    
    func callBluweoProfile() {
        
        let userDefaults = UserDefaults.standard
        let accessToken = userDefaults.value(forKey: "bluweoAccessToken") as! String
        print(accessToken)
        
        let url = "\(baseURL.bluweoProfile)/get?access_token=\(accessToken)"
        
        let headers = [
            "client_id" : "1001",
            "secret_key" : "bookme",
            "Content-Type": "application/x-www-form-urlencoded"
        ]
        
        Alamofire.request(URL(string: url)!, method: .get , encoding: URLEncoding(destination: .httpBody) , headers: headers).responseJSON(completionHandler: { (respond) in
            
            if let result = respond.result.value{
                let responsed = result as! NSDictionary
                let response = responsed.value(forKey: "response") as! Bool
                
                if(response){
                    let data = responsed.value(forKey: "data") as! NSDictionary
                    let access_token = data.value(forKey: "access_token") as! String
                    
                    let userDefaults = UserDefaults.standard
                    userDefaults.set(access_token, forKey: "bluweoAccessToken")
                    
                    self.userProfileData = data
                    
                    self.firstName = data.value(forKey: "first_name") as! String
                    self.lastName = data.value(forKey: "last_name") as! String
                    self.imageURL = data.value(forKey: "profile_photo") as! String
                    self.titles = data.value(forKey: "title") as! String
                    self.email = data.value(forKey: "email") as! String
                    self.address = data.value(forKey: "address") as! String
                    self.mobile = data.value(forKey: "mobile") as! String
                    
                    self.titleTF.text = self.titles
                    self.firstNameTF.text = self.firstName
                    self.lastNameTF.text = self.lastName
                    self.mobileNumberTF.text = self.mobile
                    
                    self.imageProfile.sd_setImage(with: URL(string: self.imageURL), placeholderImage: UIImage(named: "bookme_loading"))
                }
                else{
                    let data = responsed.value(forKey: "data") as! NSDictionary
                    let message = data.value(forKey: "message") as! NSArray
                    let msg = message[0] as! NSDictionary
                    let errMsg = msg.value(forKey: "message") as! String
                    
                    self.RefreshCount+=1
                    
                    if(errMsg == "access token not valid or expired"){
                        self.refreshBluweoToken()
                    }
                    
                    SVProgressHUD.show()
                    DispatchQueue.main.asyncAfter(deadline: .now() + 3){
                        
                        if(self.RefreshCount < 3){
                            self.callBluweoProfile()
                        }
                        else{
                            SVProgressHUD.showError(withStatus: "Something wrong.\nTry again later")
                            self.RefreshCount = 0
                        }
                    }
                }
            }
                
            else{
                print(respond.error?.localizedDescription ?? "")
                SVProgressHUD.dismiss()
            }
        })
    }
    
    func updateImageProfile(data : NSDictionary) {
        
        let userDefaults = UserDefaults.standard
        let userUID = userDefaults.value(forKey: "user_uid") as! String
        
        
        let parameters: [String: Any] = [
            "profilePhoto" : self.imageURL
        ]
        
        
        Alamofire.request(baseURL.url + "profile/profile-image/\(userUID)", method: .put, parameters: parameters, encoding: JSONEncoding.default)
            .responseJSON(completionHandler: { response in
                
                
                if let result = response.result.value {
                    SVProgressHUD.dismiss()
//                    self.callProfile(uid: userUID)
                }
                else{
                    SVProgressHUD.showError(withStatus: "Network Error")
                }
            })
    }
    
    @IBAction func editImageProfile(_ sender: Any) {
            
//            var config = YPImagePickerConfiguration()
//            config.library.onlySquare = false
//            config.onlySquareImagesFromCamera = true
//            config.targetImageSize = .original
//            config.usesFrontCamera = true
//            config.showsPhotoFilters = true
//            config.shouldSaveNewPicturesToAlbum = true
//            config.albumName = "BookMe"
//            config.screens = [.library, .photo]
//            config.startOnScreen = .library
//            config.wordings.libraryTitle = "Gallery"
//            config.hidesStatusBar = false
//            
//            // Build a picker with your configuration
//            let picker = YPImagePicker(configuration: config)
//            
//            picker.didFinishPicking(completion: { [unowned picker] items ,  cancelled  in
//                
//                if let photo = items.singlePhoto {
//                    let image = photo.image.jpegData(compressionQuality: commonValue.imageCompress)
////                    let image = UIImageJPEGRepresentation(photo.image, commonValue.imageCompress)
//                    self.ImageUploading(data: image!)
//                }
//                
//                if cancelled {
//                    print("Picker was canceled")
//                }
//                
//                picker.dismiss(animated: true, completion: nil)
//                
//            })
//            
//            self.present(picker, animated: true, completion: nil)
    }
    
    
    @IBAction func updateAction(_ sender: Any) {
        
        acceptTerm = self.checkBox.on
        
        if(acceptTerm && validateData()){
//            self.updateProfile(data: self.userProfileData, back: true)
            self.updateBluweoProfile(data: self.userProfileData)
        }
        else{
            // Creating StatusAlert instance
            let statusAlert = StatusAlert()
            
            statusAlert.image = UIImage(named: "image_notice")
            statusAlert.title = "Please accept our term"
            statusAlert.message =  "Please accept our user term and condition before update your infomation."
            statusAlert.canBePickedOrDismissed = true
            
            // Presenting created instance
            statusAlert.showInKeyWindow()
        }
    }
    
    func validateData() -> Bool {
        
        if(self.titleTF.text == "" || self.firstNameTF.text == "" || self.lastNameTF.text == ""){
            return false
        }
        else{
            // All fill validate
            return true
        }
    }
    
    @IBAction func backAction(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
}

// MARK:- UITextFieldDelegate

extension updateInfoViewController : UITextFieldDelegate {
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if(textField == bdTF){
            
            textField.resignFirstResponder()
            self.view.endEditing(true)
            
            let defaultDate =  Date().dateAt(.endOfDay) - 25.years
            
            let datePicker = ActionSheetDatePicker(title: "select you birthday", datePickerMode: UIDatePicker.Mode.date, selectedDate: defaultDate, doneBlock: {
                picker, value, index in
                
                let formatter = DateFormatter()
                formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
                
                let myString = formatter.string(from: value as! Date)
                let yourDate = formatter.date(from: myString)
                formatter.dateFormat = "dd MMM yyyy"
                let myStringafd = formatter.string(from: yourDate!)
                
                print("select date : \(myStringafd)")
                
                textField.text = "\(myStringafd)"
                textField.resignFirstResponder()
                
                return
            }, cancel: { ActionStringCancelBlock in return }, origin: (textField as AnyObject).superview!?.superview)
            
            datePicker?.maximumDate = Date()
            datePicker?.show()
        }
        
        else if (textField == titleTF){
            textField.resignFirstResponder()
            self.view.endEditing(true)
            
            let actionPicker = ActionSheetStringPicker.init(title: "Select you title", rows: ["Mr." , "Mrs." , "Miss" , "Other"], initialSelection: 0, doneBlock: { (picker, index, value) in
                
                textField.text = value as? String
                
            }, cancel: { (picker) in
                print("cancel")
            }, origin: self.view.superview)
            
            actionPicker?.show()
        }
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if(textField == bdTF){
            self.view.endEditing(true)
        }
        else if(textField == titleTF){
            self.view.endEditing(true)
        }
        
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        
        if (textField == titleTF){
            if(textField.text != ""){
                print("correct")
            }
            else{
                self.titleTF.showErrorWithText(errorText: "Please Select Title.")
            }
        }
        
        if (textField == mobileNumberTF){
            if(textField.text?.count == 10 || textField.text?.count == 12 || textField.text?.count == 13){
                print("correct")
            }
            else{
                self.mobileNumberTF.showErrorWithText(errorText: "Phone number is incorrect.")
            }
            
        }
    }
    
}


// MARK:- BEMCheckbox Delegate

extension updateInfoViewController : BEMCheckBoxDelegate {

    func didTap(_ checkBox: BEMCheckBox) {
        if(checkBox.on){
            acceptTerm = false
        }
        else{
            acceptTerm = true
        }
    }
}
