//
//  reviewTableViewController.swift
//  bookme
//
//  Created by Naphat Sottiyaphai on 8/9/2561 BE.
//  Copyright © 2561 Naphat Sottiyaphai. All rights reserved.
//

import UIKit
import Cosmos
import Alamofire

class reviewTableViewController: UIViewController , UITextViewDelegate , EnterTextViewDelegate {
    
    @IBOutlet weak var tableView: UITableView!
    
    var owner = Bool()
    public var pid = String()
    var reviews = [NSDictionary]()
    var prepareObject = NSDictionary()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setBackgroundColor()
        
        self.tableView.contentInset = UIEdgeInsets(top: 20.0, left: 0.0, bottom: 0.0, right: 0.0)
        self.tableView.rowHeight = UITableView.automaticDimension
        self.tableView.estimatedRowHeight = 44
        self.tableView.reloadData()
        
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        DispatchQueue.main.asyncAfter(deadline: .now()){
            self.callReview()
        }
    }
    
    
    
    func textViewShouldBeginEditing(_ textView: UITextView) -> Bool {
        
        print("It's begin")
        let vc = EnterTextViewController.instantiateFromStoryboard()
        vc.titleText = "Reply Review"
        vc.oldText = textView.text
        vc.delegate = self
        textView.resignFirstResponder()
        self.present(vc, animated: true, completion: nil)
        
        return true
    }
    
    func sendBackText(title: String, text: String , conditionIndex : Int) {
        print(title)
        print(text)
        print(conditionIndex)
    }
    
    @IBAction func goSeeReply(_ sender: UIButton) {
        
        print(sender.tag)
        self.prepareObject = reviews[sender.tag]
        self.performSegue(withIdentifier: "reply", sender: self)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if(segue.identifier == "reply"){
            let vc = segue.destination as! ReplyReviewViewController
            vc.reviewObject = self.prepareObject
            vc.owner = self.owner
        }
    }
}

extension reviewTableViewController : UITableViewDelegate , UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        if(reviews.count <= 0){
            DispatchQueue.main.asyncAfter(deadline: .now() + 2){
//                self.tableView.setEmptyMessage("No Review for this professional.\nLet's Be the first reviewer to this professional.")
            }
        }
        else{
            self.tableView.restore()
        }
        
        return reviews.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        let replyReview = self.reviews[section]
        let arrayReply = replyReview.value(forKey: "reviewReply") as! NSArray
        let counter = arrayReply.count
        
        if(counter == 1){
            return 2
        }
        else if (counter > 1){
            return 3
        }
        
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let dataReview = reviews[indexPath.section]
        let replyReview = self.reviews[indexPath.section]
        let arrayReply = replyReview.value(forKey: "reviewReply") as! NSArray
//        let replyCount = arrayReply.count
        
        if(indexPath.row == 0){
            let cell = tableView.dequeueReusableCell(withIdentifier: "review", for: indexPath) as! reviewCell
            
            let profile = dataReview.value(forKey: "profile") as! NSDictionary
            let firstName = profile.value(forKey: "firstName") as! String
            let lastName = profile.value(forKey: "lastName") as! String
            let profileImage = profile.value(forKey: "profilePhoto") as! String
            
            cell.profileImage.sd_setImage(with: URL(string: profileImage), placeholderImage: UIImage(named: "bookme_loading"))
            cell.profileImage.setRounded()
            
            cell.profileName.text = " \(firstName) \(lastName)"
            cell.dateTime.text = " \(Date().toFormat("dd MMM yyyy"))"
            
            let ratings = dataReview.value(forKey: "reviewRate") as! String
            cell.rating.rating = Double(ratings)!
            cell.reviewText.text = dataReview.value(forKey: "reviewText") as? String
            cell.button.tag = indexPath.section
            cell.bgView.setRoundWithCommonR()
            
            return cell
        }
        else if (indexPath.row == 1){
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "reply", for: indexPath) as! replyReviewCell
            let firstReply = arrayReply[indexPath.row-1] as! NSDictionary
            
            let replyReviewText = firstReply.value(forKey: "replyReviewText") as! String
            let profile = firstReply.value(forKey: "profile") as! NSDictionary
            let profileImageURL = profile.value(forKey: "profilePhoto") as! String
            let firstName = profile.value(forKey: "firstName") as! String
            let lastName = profile.value(forKey: "lastName") as! String
            
            cell.profileName.text = " \(firstName) \(lastName)"
            cell.profileImage.setRounded()
            cell.profileImage.sd_setImage(with: URL(string: profileImageURL), placeholderImage: UIImage(named: "bookme_loading"))
            cell.reviewText.text = replyReviewText
            cell.bgView.setRoundWithCommonR()
            
            return cell
        }
        else if (indexPath.row > 1){
            let cell = tableView.dequeueReusableCell(withIdentifier: "someReply", for: indexPath) as! moreReply
            cell.replyCount.text = "And more reply."
            
            return cell
        }
        
        return UITableViewCell()
    }
    
    func callReview() {
        
        self.reviews.removeAll()
        
        let ud = UserDefaults.standard
        var accesstoken = String()
        
        if(ud.object(forKey: "accessToken") != nil){
            accesstoken = ud.value(forKey: "accessToken") as! String
        }
        
        let header : HTTPHeaders = [
            "accessToken" : accesstoken
        ]
        
        let url = URL(string: "\(baseURL.url)review/profess/\(pid)")
        Alamofire.request(url! , headers : header).responseJSON(completionHandler: { (reponse) in
            let Result = reponse.result.value as! NSDictionary
            
            if(Result.value(forKey: "response") as! Bool){
                let data = Result.value(forKey: "data") as! Array<Any>
                
                for review in data {
                    let dictData = review as! NSDictionary
                    self.reviews.append(dictData)
                }
            }
            
            DispatchQueue.main.async {
                self.tableView.reloadData()
            }
        })
    }
    
    
}

class reviewCell: UITableViewCell {
    @IBOutlet weak var profileImage: UIImageView!
    @IBOutlet weak var profileName: UILabel!
    @IBOutlet weak var rating: CosmosView!
    @IBOutlet weak var reviewText: UITextView!
    @IBOutlet weak var dateTime: UILabel!
    @IBOutlet weak var button: UIButton!
    @IBOutlet weak var bgView: UIView!
}

class replyReviewCell
: UITableViewCell{
    @IBOutlet weak var profileImage : UIImageView!
    @IBOutlet weak var profileName: UILabel!
    @IBOutlet weak var reviewText: UITextView!
    @IBOutlet weak var bgView: UIView!
}

class EnterReviewText: UITableViewCell {
    @IBOutlet weak var textView: UITextView!
    @IBOutlet weak var sendButton: UIButton!
}

class moreReply : UITableViewCell {
    @IBOutlet var replyCount : UILabel!
}
