//
//  BookingSelectTitleJobViewController.swift
//  bookme
//
//  Created by Naphat Sottiyaphai on 21/12/2561 BE.
//  Copyright © 2561 Naphat Sottiyaphai. All rights reserved.
//

import UIKit
import SwiftDate
import OpalImagePicker

class BookingSelectTitleJobViewController: UIViewController {

    @IBOutlet weak var mainButton: UIButton!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var destTextView: UITextView!
    @IBOutlet weak var jobTitle: UITextField!
    
    var package = [NSDictionary]()
    var professID = String()
    var profess_uid = String()
    var profileImageURL = String()
    var profileNames = String()
    var profileTitles = String()
    
    var packNames = String()
    var packProce = String()
    var dateTimes = String()
    var locations = String()
    
    var jobName = String()
    var jobText = String()
    
    var locationName = String()
    var location_lat = Double()
    var location_lng = Double()
    var dateTimeFull = DateInRegion()
    
    var imageName = [String]()
    var imageData = [UIImage]()
    var imageDataJPGE = [Data]()
    
    // Passing Data
    var packageSelected = NSDictionary()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        jobTitle.setLeftPaddingPoints(15)
        jobTitle.setRightPaddingPoints(15)
        jobTitle.setRoundWithR(r: commonValue.viewRadius)

        
        destTextView.textContainerInset = .init(top: 15, left: 15, bottom: 15, right: 15)
        destTextView.setRoundWithR(r: commonValue.viewRadius)
        mainButton.setRoundWithR(r: commonValue.viewRadius)
        
        self.setClearNavBar()
        self.setBackgroundColor()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if(segue.identifier == "booking"){
            print("booking")
            
            let vc = segue.destination as? previewBookingViewController
            vc?.package = self.package
            vc?.professID = self.professID
            vc?.profess_uid = self.profess_uid
            vc?.profileImageURL = self.profileImageURL
            vc?.profileNames = self.profileNames
            vc?.profileTitles = self.profileTitles
            vc?.dateTimes = self.dateTimes
            vc?.dateTimeFull = self.dateTimeFull
            
            vc?.imageData = self.imageData
            vc?.locationName = self.locationName
            vc?.location_lat = self.location_lat
            vc?.location_lng = self.location_lng
            
            vc?.jobName = self.jobTitle.text!
            vc?.jobText = self.destTextView.text
            
            vc?.packNames = self.packNames
            vc?.packProce = self.packProce
        }
    }
    
    @IBAction func nextAction(_ sender: Any) {
        self.performSegue(withIdentifier: "booking", sender: self)
    }

}

extension BookingSelectTitleJobViewController : UICollectionViewDelegate , UICollectionViewDataSource , UICollectionViewDelegateFlowLayout{
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return imageData.count + 1
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        
        
        if(indexPath.row < imageData.count){
            let item = collectionView.dequeueReusableCell(withReuseIdentifier: "image", for: indexPath) as! imageCollectionItem
            
            item.image.image = imageData[indexPath.row]
            item.image.setRoundWithCommonRWithBorder()
            
            return item
        }
        else{
            let item = collectionView.dequeueReusableCell(withReuseIdentifier: "addProject", for: indexPath) as! addCollectionItem
            
            item.bgView.setRoundWithCommonR()
            
            return item
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        return CGSize(width: collectionView.bounds.height-10 , height: collectionView.bounds.height-10 )
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if(indexPath.row < imageData.count ){
            
        }
        else{
            print("Select Image")
            
            let imagePicker = OpalImagePickerController()
            imagePicker.imagePickerDelegate = self
            present(imagePicker, animated: true, completion: nil)
        }
        
    }
}

// MARK: - ImagePicker Delegate
extension BookingSelectTitleJobViewController : OpalImagePickerControllerDelegate {
    
    func imagePicker(_ picker: OpalImagePickerController, didFinishPickingImages images: [UIImage]) {
        print("did done")
        
        for image in images{
            self.imageData.append(image)
        }
        
        self.collectionView.reloadData()
        self.dismiss(animated: true, completion: nil)
        
    }
    
    func imagePickerDidCancel(_ picker: OpalImagePickerController){
        print("cancel")
        self.dismiss(animated: true, completion: nil)
    }
}
