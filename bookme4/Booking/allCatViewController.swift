//
//  allCatViewController.swift
//  bookme
//
//  Created by Naphat Sottiyaphai on 10/1/2562 BE.
//  Copyright © 2562 Naphat Sottiyaphai. All rights reserved.
//

import UIKit
import Alamofire
import SVProgressHUD

class allCatViewController: UIViewController {
    
    @IBOutlet weak var catCollection : UICollectionView!
    
    var selectCatagory = ""
    var catagory       = [Any]()
    
    class func instantiateFromStoryboard() -> allCatViewController {
        let storyboard = UIStoryboard(name: "booking", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: String(describing: self)) as! allCatViewController
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.callCat()
        self.setBackgroundColor()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.navigationController?.navigationBar.isHidden = false
        
        if #available(iOS 11.0, *) {
            navigationController?.navigationBar.prefersLargeTitles = false
        } else {
            print("It's down of iOS 11")
        }
    }
    
    func callCat() {
        
        let ud = UserDefaults.standard
        var accesstoken = String()
        
        if(ud.object(forKey: "accessToken") != nil){
            accesstoken = ud.value(forKey: "accessToken") as! String
        }
        
        let header : HTTPHeaders = [
            "accessToken" : accesstoken
        ]
        
        self.catagory.removeAll()
        
        Alamofire.request(baseURL.url + "categories", method: .get, encoding: JSONEncoding.default, headers : header)
            .responseJSON(completionHandler: { response in
                
                if let result = response.result.value {
                    
                    let JSON = result as! NSDictionary
                    let JsonResult = JSON.value(forKey: "response") as! Bool
                    
                    if(JsonResult){
                        let data = JSON.value(forKey: "data") as! NSArray
                        for dataInArr in data {
                            self.catagory.append(dataInArr)
                        }
                    }
                    self.catCollection.reloadData()
                }
                else{
                    SVProgressHUD.showError(withStatus: "Network Error")
                }
            })
    }
}

extension allCatViewController : UICollectionViewDataSource , UICollectionViewDelegate , UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return catagory.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let item = collectionView.dequeueReusableCell(withReuseIdentifier: "catagory", for: indexPath) as! catCollectionCell

        item.bgView.setRoundWithR(r: commonValue.viewRadius)
        item.imageCat.setRoundWithR(r: commonValue.viewRadius)
        item.numberLabel.setRoundWithR(r: commonValue.subTadius)
        
        let data = catagory[indexPath.row] as! NSDictionary
        let catName = data.value(forKey: "catName") as! String
        var catImage = String()
        if(data.object(forKey: "catUrl") != nil){
            catImage = data.value(forKey: "catUrl") as! String
        }
        else{
            catImage = data.value(forKey: "catURL") as! String
        }
        
        let catCount = data.value(forKey: "catProfessCount") as! Int
        
        item.nameLabel.text = catName
        
        item.imageCat.sd_setImage(with: URL(string: catImage)!, placeholderImage: UIImage(named : "bookme_loading"))
        
        if(catCount == 0){
            item.numberLabel.isHidden = true
        }else{
            item.numberLabel.isHidden = false
            item.numberLabel.text = "\(catCount)"
        }
        
        return item
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.bounds.width/2-15, height: collectionView.bounds.width/2-15)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        let data = catagory[indexPath.row] as! NSDictionary
        
        let catName = data.value(forKey: "catName") as! String
        let catId = data.value(forKey: "_id") as! String
        let catIcon = data.value(forKey: "catIconURL") as! String
        
        let vc = MainCatViewController.instantiateFromStoryboard()
        vc.tag = catName
        vc.catId = catId
        vc.catIcon = catIcon
        
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
}
