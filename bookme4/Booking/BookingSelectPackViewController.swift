//
//  BookingSelectPackViewController.swift
//  bookme
//
//  Created by Naphat Sottiyaphai on 21/12/2561 BE.
//  Copyright © 2561 Naphat Sottiyaphai. All rights reserved.
//

import UIKit
import SVProgressHUD
import BEMCheckBox


class BookingSelectPackViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var nextBtn: UIButton!
    
    var imageData = [UIImage]()
    var package = [NSDictionary]()
    var packageSelected = NSDictionary()
    var professID = String()
    var profess_uid = String()
    var profileImageURL = String()
    var profileNames = String()
    var profileTitles = String()
    
    var packNames = String()
    var packProce = String()
    
    var locationName = String()
    var location_lat = Double()
    var location_lng = Double()
    
    var dateTime = String()
    var dateTimeFull = String()
    var selectPack = Bool()
    var dateSelect = [Date]()
    
    var packBool = [Bool]()
    var optionPackSelected = Bool()
    
    
    class func instantiateFromStoryboard() -> BookingSelectPackViewController {
        let storyboard = UIStoryboard(name: "booking", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: String(describing: self)) as! BookingSelectPackViewController
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        DispatchQueue.main.async {
            self.nextBtn.setRoundWithR(r: commonValue.viewRadius)
            self.tableView.reloadData()
        }
        
        
        self.setClearNavBar()
        self.setBackgroundColor()
        
//        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
//        self.navigationController?.navigationBar.shadowImage = UIImage()
//        self.navigationController?.navigationBar.isTranslucent = true
//
//        navigationController?.navigationBar.backgroundColor = UIColor.clear
        // Do any additional setup after loading the view.
    }
    
    @IBAction func mainAction(_ sender: Any) {
        if(packageSelected != NSDictionary()){
            self.performSegue(withIdentifier: "selectDateLo", sender: self)
        }
        else{
            SVProgressHUD.showError(withStatus: "Select some package!")
            
            DispatchQueue.main.asyncAfter(deadline: .now() + 1){
                SVProgressHUD.dismiss()
            }
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if(segue.identifier == "selectDateLo"){
            let vc = segue.destination as! BookingSelectTimeLocationViewController
            vc.packageSelected = self.packageSelected
            vc.packNames = self.packNames
            vc.packProce = self.packProce
            
            vc.package = self.package
            vc.professID = self.professID
            vc.profess_uid = self.profess_uid
            vc.profileImageURL = self.profileImageURL
            vc.profileNames = self.profileNames
            vc.profileTitles = self.profileTitles
        }
    }
}

extension BookingSelectPackViewController : BEMCheckBoxDelegate {
    func didTap(_ checkBox: BEMCheckBox) {
        print(checkBox.tag)
        
        print("select at : \(checkBox.tag)")
//        let cell = tableView.dequeueReusableCell(withIdentifier: "pack", for: indexPath) as! packSelectCell
//
//        cell.checkbox.isHidden = false
        checkBox.isHidden = false
        self.packageSelected = self.package[checkBox.tag]
        
        if let packName = self.package[checkBox.tag].value(forKey: "packageName") as? String{
            if let prices = self.package[checkBox.tag].value(forKey: "packagePrice") as? String{
                self.packNames = packName
                self.packProce = prices
            }
        }
        
        DispatchQueue.main.async {
            self.tableView.reloadData()
        }
        
    }
}

extension BookingSelectPackViewController : UITableViewDataSource , UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 75
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return package.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "pack", for: indexPath) as! packSelectCell
        
        let pack = package[indexPath.row]
        cell.checkbox.tag = indexPath.row
        cell.checkbox.delegate = self
        cell.bgView.setRoundWithCommonR()
        
        if(self.packageSelected == self.package[indexPath.row]){
            cell.checkbox.setOn(true, animated: true)
        }
        else{
            cell.checkbox.setOn(false, animated: false)
        }
        
        if let packName = pack.value(forKey: "packageName") as? String{
            if let prices = pack.value(forKey: "packagePrice") as? String{
                
                if let intPrice = Int(prices){
                    let myNumber = NSNumber(value:intPrice)
                    let numberFormatter = NumberFormatter()
                    numberFormatter.numberStyle = .decimal
                    
                    if let prices = numberFormatter.string(from: myNumber){
                        cell.packName.text  = "\(packName)"
                        cell.price.text = "\(prices) Baht"
                    }
                }
            }
        }
        else{
            SVProgressHUD.showError(withStatus: "Data Incomplete.")
            self.navigationController?.popViewController(animated: true)
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        print("select at : \(indexPath.row)")
        let cell = tableView.dequeueReusableCell(withIdentifier: "pack", for: indexPath) as! packSelectCell
        
        cell.checkbox.isHidden = false
        self.packageSelected = self.package[indexPath.row]
        
        if let packName = self.package[indexPath.row].value(forKey: "packageName") as? String{
            if let prices = self.package[indexPath.row].value(forKey: "packagePrice") as? String{
                self.packNames = packName
                self.packProce = prices
            }
        }
        
        DispatchQueue.main.async {
            self.tableView.reloadData()
        }
    }
}

class packSelectCell: UITableViewCell {
    @IBOutlet weak var price: UILabel!
    @IBOutlet weak var packName: UILabel!
    @IBOutlet weak var checkbox: BEMCheckBox!
    @IBOutlet weak var bgView : UIView!
}
