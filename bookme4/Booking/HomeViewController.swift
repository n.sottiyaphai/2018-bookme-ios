//
//  HomeViewController.swift
//  bookme4
//
//  Created by Naphat Sottiyaphai on 14/3/2561 BE.
//  Copyright © 2561 Naphat Sottiyaphai. All rights reserved.
//

import UIKit
import UserNotifications
import Alamofire
import SVProgressHUD
import SDWebImage
import Kingfisher
import FirebaseDatabase
import FirebaseInstanceID
import GoogleSignIn
import StatusAlert
import SwiftHash
import BWWalkthrough
import MapKit
import CRRefresh

class HomeViewController: UIViewController , back2profile , CLLocationManagerDelegate  {
    
    private let sortingPostition                = 0
    private let sortting                        = ["New Comer" , "Popular" , "Recommend" , "Best Review"]
    private lazy var userRef: DatabaseReference = Database.database().reference().child("user")
    var Gallery                                 = [[String]]()
    var catagory                                = [Any]()
    var currentLocation: CLLocation!
    var favCounts                               = [Any]()
    var filter                                  = 0
    var locationManager                         = CLLocationManager()
    var proList                                 = [Any]()
    var selectCatagory                          = ""
    var updateMobile                            = false
    
    func go2profile(id: String) {
        let vc = profileViewController.instantiateFromStoryboard()
        vc.uid = id
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func showAllCat(_ sender: Any) {
        let vc = allCatViewController.instantiateFromStoryboard()
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func notiButton(_ sender: Any) {
        let vc = NotiFeedViewController.instantiateFromStoryboard()
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBOutlet weak var notiButton: UIButton!
    @IBOutlet var bookTalentHeader: UILabel!
    @IBOutlet var tableView: UITableView!
    @IBOutlet var catCollection: UICollectionView!
    @IBOutlet var sortCollection: UICollectionView!
    @IBOutlet var headerView: UIView!
    @IBOutlet var searchView: UIView!
    @IBOutlet var searchTF: UITextField!
    @IBOutlet var collectionView: UICollectionView!
    
    class func instantiateFromStoryboard() -> HomeViewController {
        let storyboard = UIStoryboard(name: "booking", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: String(describing: self)) as! HomeViewController
    }
    
    fileprivate var currentLocalImageIndex = 0 {
        didSet {
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if Reachability.isConnectedToNetwork(){
            print("Internet Connection Available!")
            
            self.tokenUpdate()
            self.callFlashNews()
            self.callCat()
            
            self.tableView.backgroundColor = UIColor.clear
            self.tableView.rowHeight = UITableView.automaticDimension;
            self.tableView.cr.addHeadRefresh(animator: NormalHeaderAnimator()) { [weak self] in
                
                self?.tableView.reloadData()
                
                DispatchQueue.main.async {
                    self?.callProfile()
                }
                
                DispatchQueue.main.asyncAfter(deadline: .now() + 1, execute: {
                    self?.tableView.cr.endHeaderRefresh()
                })
                
            }
            self.tableView.cr.beginHeaderRefresh()
            
            
            
            
            let badgeCount: Int = 0
            let application = UIApplication.shared
            let center = UNUserNotificationCenter.current()
            center.requestAuthorization(options:[.badge, .alert, .sound]) { (granted, error) in
                // Enable or disable features based on authorization.
            }
            application.registerForRemoteNotifications()
            application.applicationIconBadgeNumber = badgeCount
            
            
            let ud = UserDefaults.standard
            if(ud.object(forKey: "Walkthrough") == nil){
                // Get view controllers and build the walkthrough
                let stb = UIStoryboard(name: "Walkthrough", bundle: nil)
                let walkthrough = stb.instantiateViewController(withIdentifier: "walk") as! BWWalkthroughViewController
                let page_zero = stb.instantiateViewController(withIdentifier: "walk0")
                let page_one = stb.instantiateViewController(withIdentifier: "walk1")
                let page_two = stb.instantiateViewController(withIdentifier: "walk2")
                let page_three = stb.instantiateViewController(withIdentifier: "walk3")
                
                // Attach the pages to the master
                walkthrough.delegate = self
                walkthrough.add(viewController:page_zero)
                walkthrough.add(viewController:page_one)
                walkthrough.add(viewController:page_two)
                walkthrough.add(viewController:page_three)
                
                ud.set("true", forKey: "Walkthrough")
                self.present(walkthrough, animated: true, completion: nil)
            }
            else{
                print("Alrady Walkthrough")
            }
            
            
        }else{
            print("Internet Connection not Available!")
            SVProgressHUD.showInfo(withStatus: "Network Connection is not available.\nPlease Check your connection.")
            
            DispatchQueue.main.asyncAfter(deadline: .now() + 3){
                SVProgressHUD.dismiss()
                
                //                let api = Instagram.shared
                //                api.logout()
                
                GIDSignIn.sharedInstance().signOut()
                
                let domain = Bundle.main.bundleIdentifier!
                UserDefaults.standard.removePersistentDomain(forName: domain)
                UserDefaults.standard.synchronize()
                
                let vc = LoginViewController.instantiateFromStoryboard()
                self.navigationController?.present(vc, animated: true, completion: nil)
            }
        }
        
        
        let userDefault = UserDefaults.standard
        
        _ = userDefault.value(forKey: "langDevice") as! String
        
        searchTF.delegate = self
        tableView.delegate = self
        tableView.dataSource = self
        catCollection.delegate = self
        sortCollection.delegate = self
        catCollection.dataSource = self
        sortCollection.dataSource = self
        
        searchView.setRoundWithR(r: commonValue.viewRadius)
        
        let nib = UINib(nibName: "TableSectionHeader", bundle: nil)
        self.tableView.register(nib, forHeaderFooterViewReuseIdentifier: "TableSectionHeader")
        
        //Test UI
        self.notiButton.setRoundWithR(r: Float(self.notiButton.frame.height/2))
        self.searchTF.setTextMedium()
        
        //        self.bookTalentHeader.setBigHeaderTextBold()
        //        self.setBackgroundColor()
        
        self.setBackgroudWallpaper()
        self.setClearNavBar()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        self.setClearNavBar()
        
        if Reachability.isConnectedToNetwork(){
            InstanceID.instanceID().instanceID { (result, error) in
                if let error = error {
                    print("Error fetching remote instange ID: \(error)")
                } else if let result = result {
                    
                    let userDefault = UserDefaults.standard
                    let uid = userDefault.value(forKey: "user_uid") as! String
                    let onlineStatusRef = Database.database().reference().child("user").child(uid)
                    
                    let messageItem : [String : Any] = [
                        "isOnline": true,
                        "token" : result.token
                    ]
                    
                    onlineStatusRef.updateChildValues(messageItem)
                }
            }
        }else{
            print("Internet Connection not Available!")
        }
        
        var headerSize = UIScreen.main.bounds.height*0.60
        
        if(headerSize > 400){
            headerSize = 400
        }
        
        self.tableView.reloadData()
        
        headerView.frame = CGRect.init(x: 0, y: 0, width: UIScreen.main.bounds.width, height: headerSize)
        self.navigationController?.isNavigationBarHidden = true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        self.navigationController?.isNavigationBarHidden = false
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        URLCache.shared.removeAllCachedResponses()
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
    }
    
    func walkthroughPageDidChange(_ pageNumber: Int) {
        print("Current Page \(pageNumber)")
    }
    
    func walkthroughCloseButtonPressed() {
        
        // Ask for Authorisation from the User.
        self.locationManager.requestAlwaysAuthorization()
        
        // For use in foreground
        self.locationManager.requestWhenInUseAuthorization()
        
        if CLLocationManager.locationServicesEnabled() {
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
            //            locationManager.startUpdatingLocation()
        }
        
        
        self.dismiss(animated: true, completion: nil)
    }
    
    func callFlashNews() {
        
        let Url = "\(baseURL.firebaseFunction)/PRNews"
        Alamofire.request(Url , method: .get).responseJSON { (response) in
            
            if let result = response.result.value {
                let dictResult = result as! NSDictionary
                
                if(dictResult.object(forKey: "News") == nil){
                    print("no flashNews")
                }
                else{
                    let newsArr = dictResult.value(forKey: "News") as! NSArray
                    let firstIndex = newsArr[0] as! NSDictionary
                    
                    let flashNews = flashNewsViewController.instantiateFromStoryboard()
                    flashNews.imageURL = firstIndex.value(forKey: "newsImageURL") as! String
                    flashNews.descDetailText = firstIndex.value(forKey: "newsDesc") as! String
                    flashNews.titleTextStr = firstIndex.value(forKey: "newsTitle") as! String
                    self.present(flashNews, animated: true, completion: nil)
                }
                
            }
            else{
                SVProgressHUD.showError(withStatus: response.error?.localizedDescription)
                
                DispatchQueue.main.asyncAfter(deadline: .now()){
                    SVProgressHUD.dismiss()
                }
            }
        }
    }
    
    func textWithThai(){
        
    }
    
    func onlineChecker(uid : String , index : Int) {
        
        var tempProlist = self.proList[index] as! Dictionary<String , Any>
        let professUid = tempProlist["uId"] as! String
        var userOnlineBool = Bool()
        
        userRef.child(professUid).child("isOnline").observe(.value) { (snapshot) in
            
            if(snapshot.value is Bool){
                userOnlineBool = snapshot.value as! Bool
                //                print("\(professUid) is \(userOnlineBool)")
                if(userOnlineBool){
                    tempProlist["online"] = "online"
                }
                else{
                    tempProlist["online"] = "offline"
                }
            }
            else{
                tempProlist["online"] = "offline"
            }
            
            DispatchQueue.main.async {
                self.proList[index] = tempProlist
                self.tableView.reloadData()
            }
        }
    }
    
    
    func tokenUpdate (){
        
//        InstanceID.instanceID().instanceID { (result, error) in
//            if let error = error {
//                print("Error fetching remote instange ID: \(error)")
//            } else if let result = result {
//                print("Remote instance ID token: \(result.token)")
//
//                let tokenFromId = result.token
//                let userDefaults = UserDefaults.standard
//                let uid = userDefaults.value(forKey: "user_uid") as! String
//                let url = "\(baseURL.url)profile/\(uid)/update-fcm"
//
//                let params : [String : Any] = [
//                    "tokenFirebase" : tokenFromId
//                ]
//
//                Alamofire.request(URL(string: url)!, method: .put, parameters: params, encoding: JSONEncoding.default).responseJSON(completionHandler: {(respond) in
//
//                    if let result = respond.result.value{
//                        let responds = result as! NSDictionary
//
//                        let apiRespond = responds.value(forKey: "response") as! Bool
//                        if(apiRespond){
//                            print("*** token update success ***")
//                        }
//                    }
//                    else{
//                        SVProgressHUD.showError(withStatus: "Something wrong")
//
//                        DispatchQueue.main.asyncAfter(deadline: .now() + 2){
//                            SVProgressHUD.dismiss()
//                        }
//
//
//                    }
//                })
//            }
//        }
        
        
    }
    
    
    func callCat() {
        
        let ud = UserDefaults.standard
        var accesstoken = String()
        
        if(ud.object(forKey: "accessToken") != nil){
            accesstoken = ud.value(forKey: "accessToken") as! String
        }
        
        let header : HTTPHeaders = [
            "accessToken" : accesstoken,
            "Content-Type": "application/x-www-form-urlencoded"
        ]
        
        
        self.catagory.removeAll()
        
        Alamofire.request(baseURL.url + "categories", method: .get, encoding: URLEncoding(destination: .httpBody), headers : header)
            .responseJSON(completionHandler: { response in
                
                if let result = response.result.value {
                    
                    let JSON = result as! NSDictionary
                    let JsonResult = JSON.value(forKey: "response") as! Bool
                    
                    if(JsonResult){
                        let data = JSON.value(forKey: "data") as! NSArray
                        for dataInArr in data {
                            self.catagory.append(dataInArr)
                        }
                    }
                    self.catCollection.reloadData()
                }
                else{
                    SVProgressHUD.showError(withStatus: response.error?.localizedDescription)
                    DispatchQueue.main.asyncAfter(deadline: .now() + 2){
                        SVProgressHUD.dismiss()
                    }
                }
            })
    }
    
    
    
    func callProfess() {
        
        self.proList.removeAll()
        self.favCounts.removeAll()
        self.Gallery.removeAll()
        
        let ud = UserDefaults.standard
        var accesstoken = String()
        
        if(ud.object(forKey: "accessToken") != nil){
            accesstoken = ud.value(forKey: "accessToken") as! String
        }
        
        let header : HTTPHeaders = [
            "accessToken" : accesstoken,
            "Content-Type": "application/x-www-form-urlencoded"
        ]
        
        
        let params : [String:Any] = [
            "filter" : self.filter
        ]
        
        
        Alamofire.request(baseURL.url + "professional/filter" , method: .post, parameters: params, encoding: URLEncoding(destination: .httpBody) , headers : header)
            .responseJSON(completionHandler: { response in
                
                print(response.response?.statusCode)
                
                if let result = response.result.value {
                    
                    let JSON = result as! NSDictionary
                    let JsonResult = JSON.value(forKey: "response") as! Bool
                    
                    if(JsonResult){
                        let data = JSON.value(forKey: "data") as! NSArray
                        
                        for dataInArr in data {
                            var portfolio = NSDictionary()
                            var favCount = Int64()
                            var GalleryURL = Array<String>()
                            let dict = dataInArr as! Dictionary<String , Any>
                            
                            if(dict["favCount"] is Int64){
                                favCount = dict["favCount"] as! Int64
                                self.favCounts.append(favCount)
                            }
                            
                            if(dict["portfolio"] is NSDictionary){
                                portfolio = dict["portfolio"] as! NSDictionary
                                self.Gallery.append(GalleryURL)
                            }
                            
                            if(portfolio["gallery"] is Array<String>){
                                GalleryURL = portfolio["gallery"] as! Array<String>
                                self.proList.append(dict as NSDictionary)
                            }
                        }
                        
                        DispatchQueue.main.async {
                            SVProgressHUD.dismiss()
                            self.tableView.reloadData()
                        }
                    }
                        
                    else{
                        if JSON.value(forKey: "err") != nil{
                            let errResult = JSON.value(forKey: "err") as? String
                            SVProgressHUD.showError(withStatus: errResult)
                        }
                        else{
                            let errResult = JSON.value(forKey: "err") as! NSDictionary
                            let message = errResult.value(forKey: "message") as! String
                            SVProgressHUD.showError(withStatus: message)
                        }
                        
                        DispatchQueue.main.asyncAfter(deadline: .now() + 2){
                            SVProgressHUD.dismiss()
                            self.tableView.reloadData()
                        }
                    }
                }
                else{
                    if  (response.response?.statusCode == 401){
                        self.bookmeTokenCreate()
                        self.callProfess()
                    }
                    
                    SVProgressHUD.showError(withStatus: response.error?.localizedDescription)
                    DispatchQueue.main.asyncAfter(deadline: .now() + 2){
                        SVProgressHUD.dismiss()
                    }
                }
            })
    }
    
    func callProfile() {
        
        let ud = UserDefaults.standard
        var accesstoken = String()
        
        if(ud.object(forKey: "accessToken") != nil){
            accesstoken = ud.value(forKey: "accessToken") as! String
        }
        
        let header : HTTPHeaders = [
            "accessToken" : accesstoken,
            "Content-Type": "application/x-www-form-urlencoded"
        ]
        
        
        let userDefaults = UserDefaults.standard
        var uid = String()
        
        if(userDefaults.object(forKey: "user_uid") != nil){
            uid = userDefaults.value(forKey: "user_uid") as! String
        }
        else{
            uid = ""
        }
        
        Alamofire.request(baseURL.url + "profile/\(uid)", method: .get, encoding: URLEncoding(destination: .httpBody)  , headers : header)
            .responseJSON(completionHandler: { response in
                
                if((response.error) != nil){
                    return
                }
                
                if (response.response!.statusCode >= 400){
                    if (response.response?.statusCode == 401){
                        self.bookmeTokenCreate()
                        self.callProfile()
                    }
                    
                    else if (response.response?.statusCode == 404){
                        GIDSignIn.sharedInstance().signOut()
                        
                        let domain = Bundle.main.bundleIdentifier!
                        UserDefaults.standard.removePersistentDomain(forName: domain)
                        UserDefaults.standard.synchronize()
                        
                        let vc = LoginViewController.instantiateFromStoryboard()
                        vc.modalPresentationStyle = .fullScreen
                        self.navigationController?.present(vc, animated: true, completion: nil)
                    }
                    
                    SVProgressHUD.showError(withStatus: response.error?.localizedDescription)
                    DispatchQueue.main.asyncAfter(deadline: .now() + 2){
                        SVProgressHUD.dismiss()
                    }
                    return
                }
                
                if let result = response.result.value {
                    let data = result as! NSDictionary
                    
                    if(data.value(forKey: "data") is NSDictionary){
                        let JSON = data.value(forKey: "data") as! NSDictionary
                        let mobile = JSON.value(forKey: "mobile") as! String
                        
                        if(mobile == "mobile" || mobile == ""){
                            self.updateMobile = true
                        }
                        else{
                            self.updateMobile = false
                        }
                        
                        if let fav = JSON.value(forKey: "favorite"){
                            var favUserPID = [String]()
                            let favArray = fav as! NSArray
                            
                            for favData in favArray{
                                if(favData is NSDictionary){
                                    let dataDict = favData as! NSDictionary
                                    let pid = dataDict.value(forKey: "_id") as! String
                                    favUserPID.append(pid)
                                }
                                else{
                                    print("fav err")
                                }
                                
                            }
                            userDefaults.set(favUserPID, forKey: "userData_FavoritePID")
                            
                        }
                    }
                    
                    let userdefaults = UserDefaults.standard
                    
                    if(userdefaults.object(forKey: "isNonLogin") != nil){
                        let isNonlogin = userdefaults.value(forKey: "isNonLogin") as! Bool
                        if(isNonlogin){
                            
                        }
                        else{
                            if(self.updateMobile){
                                let alertView = UIAlertController.init(title: "Infomation is not Update ", message: "Your phone number and address is not update. Do you want to update now ?", preferredStyle: .alert)
                                
                                let ok = UIAlertAction.init(title: "Update", style: .default , handler: { (UIAlertAction) in
                                    print("Update infomation")
                                    let vc = updateInfoViewController.instantiateFromStoryboard()
                                    self.updateMobile = false
                                    self.present(vc, animated: true, completion: nil)
                                    
                                })
                                
                                let cancel = UIAlertAction(title: "Cancel", style: .cancel, handler: { (UIAlertAction) in
                                    
                                    print("Cancel")
                                })
                                
                                alertView.addAction(ok)
                                alertView.addAction(cancel)
                                
                                self.present(alertView, animated: true, completion: nil)
                            }
                            
                        }
                    }
                    
                    
                    
                    DispatchQueue.main.async {
                        self.callProfess()
                    }
                    
                    
                }
                //                else if (response.result.error != nil){
                //                    SVProgressHUD.showError(withStatus: "Cannot Fetch your profile Please Login again.")
                //
                ////                    let api = Instagram.shared
                ////                    api.logout()
                //
                //                    GIDSignIn.sharedInstance().signOut()
                //
                //                    let domain = Bundle.main.bundleIdentifier!
                //                    UserDefaults.standard.removePersistentDomain(forName: domain)
                //                    UserDefaults.standard.synchronize()
                //
                //                    let vc = LoginViewController.instantiateFromStoryboard()
                //                    self.navigationController?.present(vc, animated: true, completion: nil)
                //                }
            })
    }
    
    @IBAction func FavPush(_ sender: UIButton) {
        
        let userDefaults = UserDefaults.standard
        let index = sender.tag
        print("index of id : \(index)")
        
        let data = proList[index] as! NSDictionary
        let pids = data.value(forKey: "_id") as! String
        let uid = userDefaults.value(forKey: "user_uid") as! String
        
        if (sender.image(for: .normal) == UIImage(named: "icon_fav_red")){
            // Unfavorited
            
            let alearView = UIAlertController(title: "Are you sure ?", message: "Do you want to unfavorite this professional" , preferredStyle: .alert)
            
            let ok = UIAlertAction(title: "OK", style: .default, handler: { (alert) in
                print("Capture action OK")
                self.favoriteDelete(uid: uid, pid: pids)
            })
            let cancel = UIAlertAction(title: "Cancel", style: .cancel, handler: { (alert) in
                print("Capture action Cancel")
                self.dismiss(animated: true, completion: nil)
            })
            
            alearView.addAction(cancel)
            alearView.addAction(ok)
            
            self.present(alearView, animated: true, completion: nil)
            
        }
        else {
            // Add Favorite
            self.favoriteAdd(uid: uid, pid: pids)
        }
        
    }
    
    func favoriteAdd(uid : String , pid : String) -> Void {
        
        let ud = UserDefaults.standard
        var accesstoken = String()
        
        if(ud.object(forKey: "accessToken") != nil){
            accesstoken = ud.value(forKey: "accessToken") as! String
        }
        
        let header : HTTPHeaders = [
            "accessToken" : accesstoken
        ]
        
        let param : [String:Any] = [
            "professId" : pid
        ]
        
        Alamofire.request(baseURL.url + "favorite/\(uid)", method: .post , parameters: param , encoding: JSONEncoding.default , headers : header)
            .responseJSON(completionHandler: { response in
                
                if(response.response!.statusCode >= 400){
                    if(response.response?.statusCode == 401){
                        // Refresh token
                        print("Refresh token")
                        self.bookmeTokenCreate()
                        self.favoriteAdd(uid: uid, pid: pid)
                        return
                        
                    }
                    else if (response.response!.statusCode >= 405){
                        // Show login page
                        print("Show login page")
                        
                        let vc = LoginViewController.instantiateFromStoryboard()
                        self.present(vc, animated: true, completion: nil)
                        
                        return
                    }
                }
                
                
                if let result = response.result.value {
                    let respond = result as! NSDictionary
                    
                    if(respond.value(forKey: "response") as! Bool){
                        
                        // Creating StatusAlert instance
                        let statusAlert = StatusAlert()
                        
                        statusAlert.image = commonImage.iconFavBig
                        statusAlert.title = "Favorited"
                        statusAlert.message =  "You can open favorites list in favorited tab."
                        statusAlert.canBePickedOrDismissed = true
                        
                        // Presenting created instance
                        statusAlert.showInKeyWindow()
                        
                        DispatchQueue.main.async {
                            self.callProfile()
                        }
                    }
                }
                else{
                    SVProgressHUD.showError(withStatus: "Error")
                }
            })
    }
    
    func favoriteDelete(uid : String , pid : String) {
        
        let ud = UserDefaults.standard
        var accesstoken = String()
        
        if(ud.object(forKey: "accessToken") != nil){
            accesstoken = ud.value(forKey: "accessToken") as! String
        }
        
        let header : HTTPHeaders = [
            "accessToken" : accesstoken
        ]
        
        let param : [String:Any] = [
            "professId" : pid
        ]
        
        Alamofire.request(baseURL.url + "favorite/delete/\(uid)", method: .post , parameters: param , encoding: JSONEncoding.default , headers : header)
            .responseJSON(completionHandler: { response in
                
                if let result = response.result.value {
                    let respond = result as! NSDictionary
                    
                    if(respond.value(forKey: "response") as! Bool){
                        // Creating StatusAlert instance
                        let statusAlert = StatusAlert()
                        
                        statusAlert.image = commonImage.iconUnfavBig
                        statusAlert.title = "Unfavorite"
                        statusAlert.message =  "unfavorite complete."
                        statusAlert.canBePickedOrDismissed = true
                        
                        // Presenting created instance
                        statusAlert.showInKeyWindow()
                    }
                    else{
                        print("ERROR : Data type not correct.")
                    }
                    
                    DispatchQueue.main.async {
                        self.callProfile()
                    }
                }
                else{
                    SVProgressHUD.showError(withStatus: "Error")
                }
            })
    }
    
    
}

extension HomeViewController : BWWalkthroughViewControllerDelegate{
    
    func walkthroughNextButtonPressed() {
        
    }
    
}

extension HomeViewController : UICollectionViewDataSource , UICollectionViewDelegate , UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        switch collectionView {
        case catCollection:
            return catagory.count
        case sortCollection:
            return sortting.count
        default:
            if(proList.count <= collectionView.tag){
                return 0
            }
            else{
                let data = proList[collectionView.tag] as! NSDictionary
                
                let portfolio = data.value(forKey: "portfolio") as! NSDictionary
                let Gallery = portfolio.value(forKey: "gallery") as? Array<Any>
                
                return Gallery!.count
            }
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        switch collectionView {
            
        case catCollection:
            let item = collectionView.dequeueReusableCell(withReuseIdentifier: "catagory", for: indexPath) as! catCollectionCell
            
            item.bgView.setRoundWithCommonR()
            item.imageCat.setRoundWithCommonR()
            item.numberLabel.setRoundWithCommonR()
            
            let data = catagory[indexPath.row] as! NSDictionary
            
            let catName = data.value(forKey: "catName") as! String
            var catImage = String()
            if(data.object(forKey: "catUrl") != nil){
                catImage = data.value(forKey: "catUrl") as! String
            }
            else{
                catImage = data.value(forKey: "catURL") as! String
            }
            
            let catCount = data.value(forKey: "catProfessCount") as! Int
            
            item.nameLabel.text = catName
            item.nameLabel.setTextBold()
            
            let processor = DownsamplingImageProcessor(size: item.imageCat.bounds.size)
            item.imageCat.kf.indicatorType = .activity
            item.imageCat.kf.setImage(with: URL(string: catImage),
                                      placeholder: UIImage(named: "bookme_loading"),
                                      options: [
                                        .processor(processor),
                                        .scaleFactor(UIScreen.main.scale),
                                        .transition(.fade(1)),
                                        .cacheOriginalImage
            ])
            
            
            //            item.imageCat.sd_setImage(with: URL(string: catImage)!, placeholderImage: UIImage(named : "bookme_loading"))
            item.numberLabel.setSubTextBold()
            
            if(catCount == 0){
                item.numberLabel.isHidden = true
            }else{
                item.numberLabel.isHidden = false
                item.numberLabel.text = "\(catCount)"
            }
            
            
            //
            //            // Lang Setup
            //            let userDefault = UserDefaults.standard
            //
            //            if(userDefault.object(forKey: "langDevice") != nil){
            //              let  lang = userDefault.value(forKey: "langDevice") as! String
            //            }
            
            
            return item
            
        case sortCollection :
            
            //            print(filter)
            
            if(filter == indexPath.row){
                let item = collectionView.dequeueReusableCell(withReuseIdentifier: "sort", for: indexPath) as! sortCollectionCell
                
                item.bgView.setRoundWithShadow(r: Float(collectionView.bounds.height/2-11))
                item.bgView.clipsToBounds = true
                
                item.textLabel.text = sortting[indexPath.row]
                item.textLabel.setTextBold()
                
                
                
                
                return item
            }
                
            else {
                let item = collectionView.dequeueReusableCell(withReuseIdentifier: "sort2", for: indexPath) as! sortCollectionCell2
                
                item.bgView.setRoundWithR(r: Float(collectionView.bounds.height/2-11))
                //                item.bgView.setRoundWithShadow(r: Float(collectionView.bounds.height/2-11))
                item.bgView.clipsToBounds = true
                
                item.textLabel.text = sortting[indexPath.row]
                item.textLabel.setDescTextLight()
                
                return item
            }
            
        default:
            
            let item = collectionView.dequeueReusableCell(withReuseIdentifier: "image", for: indexPath) as! imageCollectionItem
            
            let data = proList[collectionView.tag] as! NSDictionary
            let portfolio = data.value(forKey: "portfolio") as! NSDictionary
            let Gallery = portfolio.value(forKey: "gallery") as? Array<String>
            //            let pro_uid = data.value(forKey: "profileId") as! String
            
            let url = "\(Gallery![indexPath.row])"
            
            //            item.image.kf.indicatorType = .activity
            item.image.kf.setImage(with: URL(string: url),
                                   placeholder: UIImage(named: "bookme_loading"),
                                   options: [
                                    //                                        .processor(DownsamplingImageProcessor(size: imageView.size))
                                    .scaleFactor(UIScreen.main.scale),
                                    .transition(.fade(0.2)),
            ])
            
            //            item.image.sd_setImage(with: URL(string: url), placeholderImage: UIImage(named: "bookme_loading"))
            item.image.setRoundWithR(r: commonValue.subTadius)
            
            return item
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        switch collectionView {
        case catCollection:
            return CGSize(width: collectionView.bounds.height-40, height: collectionView.bounds.height-40)
        case sortCollection :
            return CGSize(width: collectionView.bounds.width/3, height: collectionView.bounds.height-15)
        default:
            return CGSize(width: collectionView.bounds.height, height: collectionView.bounds.height)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        switch collectionView {
        case catCollection:
            let data = catagory[indexPath.row] as! NSDictionary
            //            print(data)
            let catName = data.value(forKey: "catName") as! String
            let catId = data.value(forKey: "_id") as! String
            let catIcon = data.value(forKey: "catIconURL") as! String
            
            let vc = MainCatViewController.instantiateFromStoryboard()
            vc.tag = catName
            vc.catId = catId
            vc.catIcon = catIcon
            
            self.navigationController?.pushViewController(vc, animated: true)
            
        case sortCollection:
            
            SVProgressHUD.show()
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.1){
                self.filter = indexPath.row
                self.callProfess()
                self.sortCollection.reloadData()
            }
            
            break
            
        default :
            SVProgressHUD.show()
            
            let data = proList[collectionView.tag] as! NSDictionary
            let portfolio = data.value(forKey: "portfolio") as! NSDictionary
            let Gallery = portfolio.value(forKey: "gallery") as? Array<String>
            let uids = data.value(forKey: "_id") as! String
            let profress_uid  = data.value(forKey: "profileId") as! String
            
            let profile = data.value(forKey: "profile") as! NSDictionary
            let profileImage = profile.value(forKey: "profilePhoto") as? String
            let firstName = profile.value(forKey: "firstName") as? String
            let lastName = profile.value(forKey: "lastName") as? String
            
            var url = [URL]()
            
            for URLs in Gallery!{
                let full_url = "\(URLs)"
                //                let full_url = "\(baseURL.photoURL)user/\(md5_uid.lowercased())/bookme/storages/\(URLs)"
                url.append( URL(string: full_url)! )
            }
            
            let vc = PreviewViewController.instantiateFromStoryboard()
            
            vc.urlArray = url
            vc.profileID = uids
            vc.imageProfileURL = profileImage!
            vc.indexPage = indexPath.row
            
            if let first = firstName , let last = lastName {
                vc.profileNameString = "\(first) \(last)"
            }
            
            vc.delegate = self
            
            SVProgressHUD.dismiss()
            present(vc, animated: true, completion: nil)
        }
    }
}

extension HomeViewController : UITableViewDelegate , UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return proList.count
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        //        print(UIScreen.main.bounds.height/4.5)
        //        return 185
        return commonValue.profileCellHeight
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "profiles2", for: indexPath) as! profile2TableCell
        
        if(proList.count <= indexPath.row){
            print("Not in range")
            //            self.callProfess()
        }
        else{
            if (proList[indexPath.row] is NSDictionary){
                let data = proList[indexPath.row] as! NSDictionary
                
                
                if let profiles = data.value(forKey: "profile"){
                    if( profiles is NSDictionary){
                        let profile = profiles as! NSDictionary
                        
                        var profileId = String()
                        var profileImage = String()
                        var firstName = String()
                        var lastName = String()
                        var category = NSDictionary()
                        var catName = String()
                        
                        profileId = data.value(forKey: "_id") as! String
                        profileImage = profile.value(forKey: "profilePhoto") as? String ?? ""
                        firstName = profile.value(forKey: "firstName") as? String ?? ""
                        lastName = profile.value(forKey: "lastName") as? String ?? ""
                        category = data.value(forKey: "categories") as! NSDictionary
                        
                        if(category.object(forKey: "catName") != nil){
                            catName = category.value(forKey: "catName") as! String
                        }
                        else{
                            catName = ""
                        }
                        
                        cell.profileName.text = "\(firstName) \(lastName)"
                        
                        //                        cell.profileImage.sd_setImage(with: URL(string: profileImage), placeholderImage: UIImage(named : "bookme_loading"))
                        cell.profileImage.kf.setImage(with: URL(string: profileImage),
                                                      placeholder: UIImage(named: "bookme_loading"),
                                                      options: [
                                                        .scaleFactor(UIScreen.main.scale),
                                                        .transition(.fade(0.5)),
                                                        .cacheOriginalImage
                        ])
                        
                        cell.profileImage.setRounded()
                        
                        cell.profileDest.text = ""
                        cell.catName.text = catName
                        
                        cell.onlineIndecator.isHidden = true
                        cell.onlineIndecator.setRounded()
                        cell.onlineIndecator.layer.borderWidth = 2
                        cell.onlineIndecator.layer.borderColor = UIColor.white.cgColor
                        
                        
                        // SetText Style
                        cell.profileName.setTextBold()
                        cell.catName.setSubText()
                        
                        if let onlineStatus = data.value(forKey: "online"){
                            let bool = onlineStatus as! String
                            if(bool == "online"){
                                cell.onlineIndecator.isHidden = false
                            }
                            else{
                                cell.onlineIndecator.isHidden = true
                            }
                        }
                        
                        cell.collectionView.delegate = self
                        cell.collectionView.reloadData()
                        
                        let userDefaults = UserDefaults.standard
                        cell.favCount.setDescTextMedium()
                        
                        if let fav = userDefaults.value(forKey: "userData_FavoritePID"){
                            let userFav = fav as! Array<String>
                            if(!userFav.filter{$0 == profileId}.isEmpty){
                                cell.favButton.setImage(UIImage(named: "icon_fav_red"), for: .normal)
                            }
                            else{
                                cell.favButton.setImage(UIImage(named: "icon_fav"), for: .normal)
                            }
                            cell.favButton.tag = indexPath.row
                            cell.favCount.isHidden = false
                            cell.favCount.text = "\(self.favCounts[indexPath.row])"
                        }
                        else{
                            cell.favButton.tag = indexPath.row
                            cell.favCount.isHidden = false
                            cell.favCount.text = "\(self.favCounts[indexPath.row])"
                            print("Error : cannot load userData_FavoritePID")
                        }
                        
                        cell.bgView.backgroundColor = UIColor.white
                        cell.bgView.setRoundWithCommonR()
                    }
                }
            }
            else{
                print("Prolist Error : DATA Type not Correct.")
            }
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        guard let tableViewCell = cell as? profile2TableCell else { return }
        //        guard let tableViewCell = cell as? profileTableCell else { return }
        tableViewCell.setCollectionViewDataSourceDelegate(self, forRow: indexPath.row)
        
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        // Dequeue with the reuse identifier
        let cell = self.tableView.dequeueReusableHeaderFooterView(withIdentifier: "TableSectionHeader")
        
        let header = cell as! TableSectionHeader
        header.titleLabel.text = sortting[filter]
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        switch section {
        case 0:
            return CGFloat(tableViewValue.profileHeaderViewSize)
            
        default:
            return 0
        }
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let vc = profileViewController.instantiateFromStoryboard()
        
        let data = proList[indexPath.row] as! NSDictionary
        let uids = data.value(forKey: "_id") as! String
        
        vc.uid = uids
        
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
}

//extension HomeViewController : ImageViewerDelegate {
//
//    func transitionImageView(for index: Int) -> UIImageView {
//        return UIImageView()
//    }
//
//    func imageViewerDidDisplayImage(at index: Int) {
//        currentLocalImageIndex = index
//    }
//
//}

extension HomeViewController : UITextFieldDelegate {
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        textField.resignFirstResponder()
        let vc = searchViewController.instantiateFromStoryboard()
        self.navigationController?.pushViewController(vc, animated: true)
        //        self.performSegue(withIdentifier: "searching", sender: self)
    }
}

class catCollectionCell : UICollectionViewCell {
    @IBOutlet var bgView : UIView!
    @IBOutlet var imageCat: UIImageView!
    @IBOutlet var nameLabel: UILabel!
    @IBOutlet var numberLabel: UILabel!
    
}

class sortCollectionCell : UICollectionViewCell {
    @IBOutlet var bgView : UIView!
    @IBOutlet var textLabel: UILabel!
}

class sortCollectionCell2 : UICollectionViewCell {
    @IBOutlet var bgView : UIView!
    @IBOutlet var textLabel: UILabel!
}

class imageCollectionItem : UICollectionViewCell {
    
    @IBOutlet weak var deleteButton: UIButton!
    @IBOutlet var image : UIImageView!
}

class profileTableCell: UITableViewCell {
    @IBOutlet var profileImage: UIImageView!
    @IBOutlet var profileName: UILabel!
    @IBOutlet var profileDest: UILabel!
    @IBOutlet var favButton: UIButton!
    @IBOutlet var favCount: UILabel!
    @IBOutlet var onlineIndecator: UIView!
    @IBOutlet var collectionView : UICollectionView!
    @IBOutlet weak var catName: UILabel!
}

class profile2TableCell: UITableViewCell {
    @IBOutlet var profileImage: UIImageView!
    @IBOutlet var profileName: UILabel!
    @IBOutlet var profileDest: UILabel!
    @IBOutlet var favButton: UIButton!
    @IBOutlet var favCount: UILabel!
    @IBOutlet var onlineIndecator: UIView!
    @IBOutlet var collectionView : UICollectionView!
    @IBOutlet weak var catName: UILabel!
    @IBOutlet weak var bgView: UIView!
}

extension profileTableCell{
    
    func setCollectionViewDataSourceDelegate<D: UICollectionViewDataSource & UICollectionViewDelegate>(_ dataSourceDelegate: D, forRow row: Int) {
        
        collectionView.delegate = dataSourceDelegate
        collectionView.dataSource = dataSourceDelegate
        collectionView.tag = row
        // Stops collection view if it was scrolling.
        collectionView.setContentOffset(collectionView.contentOffset, animated:false)
        collectionView.reloadData()
    }
    
    var collectionViewOffset: CGFloat {
        set { collectionView.contentOffset.x = newValue }
        get { return collectionView.contentOffset.x }
    }
}

extension profile2TableCell{
    
    func setCollectionViewDataSourceDelegate<D: UICollectionViewDataSource & UICollectionViewDelegate>(_ dataSourceDelegate: D, forRow row: Int) {
        
        collectionView.delegate = dataSourceDelegate
        collectionView.dataSource = dataSourceDelegate
        collectionView.tag = row
        // Stops collection view if it was scrolling.
        collectionView.setContentOffset(collectionView.contentOffset, animated:false)
        collectionView.reloadData()
    }
    
    var collectionViewOffset: CGFloat {
        set { collectionView.contentOffset.x = newValue }
        get { return collectionView.contentOffset.x }
    }
}



