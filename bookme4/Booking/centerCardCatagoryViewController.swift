//
//  centerCardCatagoryViewController.swift
//  bookme4
//
//  Created by Naphat Sottiyaphai on 27/3/2561 BE.
//  Copyright © 2561 Naphat Sottiyaphai. All rights reserved.
//

import UIKit
import CenteredCollectionView
import Alamofire
import SVProgressHUD
import SwiftHash
import StatusAlert
import GoogleSignIn

class centerCardCatagoryViewController: UIViewController , back2profile {
    
    func go2profile(id: String) {
        let vc = profileViewController.instantiateFromStoryboard()
        vc.uid = id
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    var delegate : back2profile? = nil
    
    public var tag = String()
    private var index = 0
    
    let cellPercentWidth: CGFloat = 0.7
    var centeredCollectionViewFlowLayout: CenteredCollectionViewFlowLayout!
    var proList = [Any]()
    var gallery = [String]()
    
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var titleName: UILabel!
    @IBOutlet var collectionView: UICollectionView!
    
    @IBOutlet weak var profileImage: UIImageView!
    @IBOutlet weak var onlineIndecator: UIView!
    @IBOutlet weak var profileName: UILabel!
    @IBOutlet weak var profileTitle: UILabel!
    
    @IBOutlet weak var favButton: UIButton!
    @IBOutlet weak var favCount: UILabel!
    @IBOutlet weak var imageCollectionView: UICollectionView!
    @IBOutlet weak var bgView: UIView!
    
    
    class func instantiateFromStoryboard() -> centerCardCatagoryViewController {
        let storyboard = UIStoryboard(name: "booking", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: String(describing: self)) as! centerCardCatagoryViewController
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        
        let userDefault = UserDefaults.standard
        
        let dataMode = userDefault.value(forKey: "dataMode") as! String
        
        if(dataMode == "normal"){
            self.ViewSetup()
            self.callProfess()
        }
        else{
            
            let userDefault = UserDefaults.standard
            let lang = userDefault.value(forKey: "langDevice") as! String
            if(lang == "th-TH"){
                //            titleNam.setFontFromLang(lang: "th", style: "bold")
            }
            
//            let proListData = userDefault.value(forKey: "resultData") as! NSArray
            
            self.collectionView.delegate = self
            self.imageCollectionView.delegate = self
            
            self.profileImage.setRounded()
            self.onlineIndecator.setRounded()
            
            let btn1 = UIButton(type: .custom)
            btn1.setImage(UIImage(named: "icon_list"), for: .normal)
            btn1.frame = CGRect(x: 0, y: 0, width: 18  , height: 18)
            //        btn1.addTarget(self, action: #selector(InboxDetail.R_barBut), for: .touchUpInside)
            let item1 = UIBarButtonItem(customView: btn1)
            
            let btn2 = UIButton(type: .custom)
            btn2.setImage(UIImage(named: "icon_search"), for: .normal)
            btn2.frame = CGRect(x: 0, y: 0, width: 18, height: 18)
            //        btn2.addTarget(self, action: #selector(InboxDetail.R_barBut), for: .touchUpInside)
            let item2 = UIBarButtonItem(customView: btn2)
            
            self.navigationItem.setRightBarButtonItems([item2 , item1], animated: true)

            centerCollectionSETUP()
            
//            self.proList = proListData as! [Any]
            
//            for favCount in proList {
//                _ = favCount as! NSDictionary
//            }
//
            DispatchQueue.main.async {
                self.imageCollectionView.reloadData()
            }
        
        }
        
        self.setClearNavBar()
        self.setBackgroundColor()
        self.bgView.setRoundWithR(r: commonValue.viewRadius)
        
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        URLCache.shared.removeAllCachedResponses()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.navigationController?.navigationBar.isHidden = false
        
        if #available(iOS 11.0, *) {
            navigationController?.navigationBar.prefersLargeTitles = false
        } else {
            print("It's down of iOS 11")
        }
    }
    
    func centerCollectionSETUP() {
        // Get the reference to the `CenteredCollectionViewFlowLayout` (REQURED STEP)
        centeredCollectionViewFlowLayout = collectionView.collectionViewLayout as? CenteredCollectionViewFlowLayout
        
        // Modify the collectionView's decelerationRate (REQURED STEP)
        collectionView.decelerationRate = UIScrollView.DecelerationRate.fast
        
        // Configure the required item size (REQURED STEP)
        centeredCollectionViewFlowLayout.itemSize = CGSize(
            width: view.bounds.width * cellPercentWidth,
            height: view.bounds.height * cellPercentWidth * cellPercentWidth
        )
        
        // Configure the optional inter item spacing (OPTIONAL STEP)
        centeredCollectionViewFlowLayout.minimumLineSpacing = 20
        
        // Get rid of scrolling indicators
        collectionView.showsVerticalScrollIndicator = false
        collectionView.showsHorizontalScrollIndicator = false
    }
    
    
    func ViewSetup(){
        let userDefault = UserDefaults.standard
        let lang = userDefault.value(forKey: "langDevice") as! String
        if(lang == "th-TH"){
            //            titleNam.setFontFromLang(lang: "th", style: "bold")
        }
        
        self.collectionView.delegate = self
        self.imageCollectionView.delegate = self
        
        self.profileImage.setRounded()
        self.onlineIndecator.setRounded()
        
//        self.tag = userDefault.value(forKey: "catagory_tag") as! String
        titleName.text = self.tag
        
        let icon = userDefault.value(forKey: "catagory_icon") as! String
        imageView.sd_setImage(with: URL(string: icon))
        
        let btn1 = UIButton(type: .custom)
        btn1.setImage(UIImage(named: "icon_list"), for: .normal)
        btn1.frame = CGRect(x: 0, y: 0, width: 18  , height: 18)
        //        btn1.addTarget(self, action: #selector(InboxDetail.R_barBut), for: .touchUpInside)
        let item1 = UIBarButtonItem(customView: btn1)
        
        let btn2 = UIButton(type: .custom)
        btn2.setImage(UIImage(named: "icon_search"), for: .normal)
        btn2.frame = CGRect(x: 0, y: 0, width: 18, height: 18)
        //        btn2.addTarget(self, action: #selector(InboxDetail.R_barBut), for: .touchUpInside)
        let item2 = UIBarButtonItem(customView: btn2)
        
        self.navigationItem.setRightBarButtonItems([item2 , item1], animated: true)
        
        centerCollectionSETUP()
    }
    
    func callProfess() {
        
        let ud = UserDefaults.standard
        var accesstoken = String()
        
        let userDefault = UserDefaults.standard
        let catId = userDefault.value(forKey: "catagory_id") as! String
        self.tag = userDefault.value(forKey: "catagory_tag") as! String
        titleName.text = self.tag
        
        if(ud.object(forKey: "accessToken") != nil){
            accesstoken = ud.value(forKey: "accessToken") as! String
        }
        
        let header : HTTPHeaders = [
            "accessToken" : accesstoken,
            "cetagoryId" : catId
        ]

        proList.removeAll()
        
        Alamofire.request(baseURL.url + "professional/", method: .get, encoding: JSONEncoding.default , headers : header)
            .responseJSON(completionHandler: { response in
                
                if let result = response.result.value {
                    let JSON = result as! NSDictionary
                    
                    let JsonResult = JSON.value(forKey: "response") as! Bool
                    if(JsonResult){
                        let data = JSON.value(forKey: "data") as! NSArray
                        
                        for dataInArr in data {
                            self.proList.append(dataInArr)
                        }
                        
                        if(data.count != 0){
                            let dataInit = self.proList[self.index] as! NSDictionary
                            let portfolio = dataInit.value(forKey: "portfolio") as! NSDictionary
                            
                            let profile = dataInit.value(forKey: "profile") as! NSDictionary
                            let profileImage = profile.value(forKey: "profilePhoto") as? String
                            let firstName = profile.value(forKey: "firstName") as? String
                            let lastName = profile.value(forKey: "lastName") as? String
                            let title = profile.value(forKey: "tittle") as? String
                            
                            let pids = dataInit.value(forKey: "_id") as! String
                            let favCounts = dataInit.value(forKey: "favCount") as! Int64
                            
                            if let first = firstName , let last = lastName {
                                self.profileName.text = "\(first) \(last)"
                            }
                            
                            self.profileTitle.text = title
                            self.profileImage.kf.setImage(with: URL(string: profileImage!),
                                                          placeholder: UIImage(named: "bookme_loading"),
                                                          options: [
                                                            //                                        .processor(DownsamplingImageProcessor(size: imageView.size))
                                                            .scaleFactor(UIScreen.main.scale),
                                                            .transition(.fade(0.2)),
                                ])
                            
//                            self.profileImage.sd_setImage(with: URL(string: profileImage!), placeholderImage: UIImage(named: "bookme_loading"))
                            
                            self.gallery.removeAll()
                            let Gallery = portfolio.value(forKey: "gallery") as? Array<String>
                            self.gallery = Gallery!
                            
                            self.favButton.isHidden = false
                            
                            let userDefaults = UserDefaults.standard
//                            let uid = userDefaults.value(forKey: "user_uid") as! String
                            
                            if let fav = userDefaults.value(forKey: "userData_FavoritePID"){
                                
                                let userFav = fav as! Array<String>
                                
                                if(!userFav.filter{$0 == pids}.isEmpty){
                                    self.favButton.setImage(UIImage(named: "icon_fav_red"), for: .normal)
                                }
                                else{
                                    self.favButton.setImage(UIImage(named: "icon_fav"), for: .normal)
                                }
                                
                                self.favCount.isHidden = false
                                self.favCount.text = "\(favCounts)"
                            }
                            else{
                                self.favCount.isHidden = false
                                self.favCount.text = "\(favCounts)"
                                print("Error : cannot load userData_FavoritePID")
                            }
                            
                            
                            self.imageCollectionView.reloadData()
                        }
                        else{
                            print("Data Empty")
                        }
                    }
                    
                    SVProgressHUD.dismiss()
                    self.collectionView.reloadData()
                }
                else{
                    SVProgressHUD.showError(withStatus: "Network Error")
                }
            })
        
    }
    
    class func embed(
        parent:UIViewController,
        container:UIView,
        child:UIViewController,
        previous:UIViewController?){
        
        if let previous = previous {
            removeFromParent(vc: previous)
        }
        child.willMove(toParent: parent)
        //        child.willMove(toParentViewController: parent)
        parent.addChild(child)
        //        parent.addChildViewController(child)
        container.addSubview(child.view)
        child.didMove(toParent: parent)
        //        child.didMove(toParentViewController: parent)
        let w = container.frame.size.width;
        let h = container.frame.size.height;
        child.view.frame = CGRect(x: 0, y: 0, width: w, height: h)
    }
    
    class func removeFromParent(vc:UIViewController){
        vc.willMove(toParent: nil)
        //        vc.willMove(toParentViewController: nil)
        vc.view.removeFromSuperview()
        vc.removeFromParent()
        //        vc.removeFromParentViewController()
    }
    
    class func embed(withIdentifier id:String, parent:UIViewController, container:UIView, completion:((UIViewController)->Void)? = nil){
        let vc = parent.storyboard!.instantiateViewController(withIdentifier: id)
        embed(
            parent: parent,
            container: container,
            child: vc,
            previous: parent.children.first
        )
        completion?(vc)
    }
    
    @IBAction func favAction(_ sender: UIButton) {
        
        let userDefaults = UserDefaults.standard
        let index = self.index
        print("index of id : \(index)")
        
        let data = proList[index] as! NSDictionary
        let pids = data.value(forKey: "_id") as! String
        
        // Data From UserDefault
        let uid = userDefaults.value(forKey: "user_uid") as! String
        
        if (sender.image(for: .normal) == UIImage(named: "icon_fav_red")){
            // Already Favorited
            
            let alertAction = UIAlertController.init(title: "Are you sure ?", message: "Do you want to unfavorite this professional" , preferredStyle: .alert)
            
            let ok = UIAlertAction.init(title: "OK", style: .default) { (action) in
                print("Capture action OK")
                self.favoriteDelete(uid: uid, pid: pids)
            }
            
            let cancel = UIAlertAction.init(title: "Cancel", style: .cancel) { (action) in
                 print("Capture action Cancel")
                    self.dismiss(animated: true, completion: nil)
            }
            
            alertAction.addAction(ok)
            alertAction.addAction(cancel)
            
            self.present(alertAction, animated: true, completion: nil)
            
        }
        else {
            // NOT Favorite
            self.favoriteAdd(uid: uid, pid: pids)
        }
        
    }
    
    
    func favoriteAdd(uid : String , pid : String) {
        
        //        let userDefaults = UserDefaults.standard
        let param : [String:Any] = [
            "professId" : pid
        ]
        
        let ud = UserDefaults.standard
        var accesstoken = String()
        
        if(ud.object(forKey: "accessToken") != nil){
            accesstoken = ud.value(forKey: "accessToken") as! String
        }
        
        let header : HTTPHeaders = [
            "accessToken" : accesstoken
        ]
        
        Alamofire.request(baseURL.url + "favorite/" + "\(uid)", method: .post , parameters: param , encoding: JSONEncoding.default , headers : header)
            .responseJSON(completionHandler: { response in
                
                if let result = response.result.value {
                    let respond = result as! NSDictionary
                    
                    if(respond.value(forKey: "response") as! Bool){
                        
                        // Creating StatusAlert instance
                        let statusAlert = StatusAlert()
                        
                        statusAlert.image = commonImage.iconFavBig
                        statusAlert.title = "Favorited"
                        statusAlert.message =  "You can open favorites list in favorited tab."
                        statusAlert.canBePickedOrDismissed = true
                        
                        // Presenting created instance
                        statusAlert.showInKeyWindow()
                        
                        DispatchQueue.main.async {
                            self.callProfile()
                        }
                    }
                }
                else{
                    SVProgressHUD.showError(withStatus: "Error")
                }
            })
    }
    
    func favoriteDelete(uid : String , pid : String) {
        
        //        let userDefaults = UserDefaults.standard
        let param : [String:Any] = [
            "professId" : pid
        ]
        
        let ud = UserDefaults.standard
        var accesstoken = String()
        
        if(ud.object(forKey: "accessToken") != nil){
            accesstoken = ud.value(forKey: "accessToken") as! String
        }
        
        let header : HTTPHeaders = [
            "accessToken" : accesstoken
        ]
        
        print("URL : " + baseURL.url + "favorite/delete/" + "\(uid)")
        
        Alamofire.request(baseURL.url + "favorite/delete/" + "\(uid)", method: .post , parameters: param , encoding: JSONEncoding.default , headers : header)
            .responseJSON(completionHandler: { response in
                
                if let result = response.result.value {
                    let respond = result as! NSDictionary
                    
                    if(respond.value(forKey: "response") as! Bool){
                        
                        // Creating StatusAlert instance
                        let statusAlert = StatusAlert()
                        
                        statusAlert.image = commonImage.iconUnfavBig
                        statusAlert.title = "Unfavorite"
                        statusAlert.message =  "unfavorite complete."
                        statusAlert.canBePickedOrDismissed = true
                        
                        // Presenting created instance
                        statusAlert.showInKeyWindow()
                    }
                    else{
                        print("ERROR : Data type not correct.")
                    }
                    
                    DispatchQueue.main.async {
                        self.callProfile()
                    }
                }
                else{
                    SVProgressHUD.showError(withStatus: "Error")
                }
            })
    }
    
    func initData(dataInit : NSDictionary) {
//        let dataInit = self.proList[self.index] as! NSDictionary
        let portfolio = dataInit.value(forKey: "portfolio") as! NSDictionary
        
        let profile = dataInit.value(forKey: "profile") as! NSDictionary
        let profileImage = profile.value(forKey: "profilePhoto") as? String
        let firstName = profile.value(forKey: "firstName") as? String
        let lastName = profile.value(forKey: "lastName") as? String
        let title = profile.value(forKey: "tittle") as? String
        
        let pids = dataInit.value(forKey: "_id") as! String
        let favCounts = dataInit.value(forKey: "favCount") as! Int64
        
        if let first = firstName , let last = lastName {
            self.profileName.text = "\(first) \(last)"
        }
        
        self.profileTitle.text = title
        self.profileImage.sd_setImage(with: URL(string: profileImage!), placeholderImage: UIImage(named: "bookme_loading"))
        
        self.gallery.removeAll()
        let Gallery = portfolio.value(forKey: "Gallery") as? Array<String>
        self.gallery = Gallery!
        
        self.favButton.isHidden = false
        
        let userDefaults = UserDefaults.standard
//        let uid = userDefaults.value(forKey: "user_uid") as! String
        
        if let fav = userDefaults.value(forKey: "userData_FavoritePID"){
            
            let userFav = fav as! Array<String>
            
            if(!userFav.filter{$0 == pids}.isEmpty){
                self.favButton.setImage(UIImage(named: "icon_fav_red"), for: .normal)
            }
            else{
                self.favButton.setImage(UIImage(named: "icon_fav"), for: .normal)
            }
            
            self.favCount.isHidden = false
            self.favCount.text = "\(favCounts)"
        }
        else{
            self.favCount.isHidden = false
            self.favCount.text = "\(favCounts)"
            print("Error : cannot load userData_FavoritePID")
        }
    }
    
    func callProfile() {
        
        let userDefaults = UserDefaults.standard
        let uid = userDefaults.value(forKey: "user_uid") as! String
        
        Alamofire.request(baseURL.url + "profile/\(uid)", method: .get, encoding: JSONEncoding.default)
            .responseJSON(completionHandler: { response in
                
                if let result = response.result.value {
                    let data = result as! NSDictionary
                    
                    if(data.value(forKey: "data") is NSDictionary){
                        let JSON = data.value(forKey: "data") as! NSDictionary
                        if let fav = JSON.value(forKey: "favorite"){
                            var favUserPID = [String]()
                            let favArray = fav as! NSArray
                            
                            for favData in favArray{
                                let dataDict = favData as! NSDictionary
                                let pid = dataDict.value(forKey: "_id") as! String
                                favUserPID.append(pid)
                            }
                            userDefaults.set(favUserPID, forKey: "userData_FavoritePID")
                        }
                    }
                    
                    self.callProfess()
                    
                }
                else{
                    SVProgressHUD.showError(withStatus: "Cannot Fetch your profile Please Login again.")
//                    
//                    let api = Instagram.shared
//                    api.logout()
                    
                    GIDSignIn.sharedInstance().signOut()
                    
                    let domain = Bundle.main.bundleIdentifier!
                    UserDefaults.standard.removePersistentDomain(forName: domain)
                    UserDefaults.standard.synchronize()
                    
                    let vc = LoginViewController.instantiateFromStoryboard()
                    self.navigationController?.present(vc, animated: true, completion: nil)
                    
                }
            })
    }
    
}

extension centerCardCatagoryViewController: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        switch collectionView {
        case self.collectionView:
            print("Selected Cell #\(indexPath.row)")
            let vc = profileViewController.instantiateFromStoryboard()
            
            let data = proList[indexPath.row] as! NSDictionary
            let uids = data.value(forKey: "_id") as! String
            
            vc.uid = uids
            
            self.navigationController?.pushViewController(vc, animated: true)
        default:
            
            SVProgressHUD.show()
            
            let data = proList[collectionView.tag] as! NSDictionary
            let portfolio = data.value(forKey: "portfolio") as! NSDictionary
            let Gallery = portfolio.value(forKey: "Gallery") as? Array<String>
            let uids = data.value(forKey: "_id") as! String
            let profress_uid  = data.value(forKey: "uId") as! String
            let md5_uid = MD5(profress_uid)
            
            let profile = data.value(forKey: "profile") as! NSDictionary
            let profileImage = profile.value(forKey: "profilePhoto") as? String
            let firstName = profile.value(forKey: "firstName") as? String
            let lastName = profile.value(forKey: "lastName") as? String
//            let title = profile.value(forKey: "tittle") as? String
            
            var url = [URL]()
            
            for URLs in Gallery!{
//                let full_url = "\(baseURL.photoURL)user/\(md5_uid.lowercased())/bookme/storages/\(URLs)"
                url.append( URL(string: URLs)! )
            }
            
            let vc = PreviewViewController.instantiateFromStoryboard()
            
            vc.urlArray = url
            vc.profileID = uids
            vc.imageProfileURL = profileImage!
            vc.indexPage = indexPath.row
            
            if let first = firstName , let last = lastName {
                vc.profileNameString = "\(first) \(last)"
            }
            
            vc.delegate = self
            
            SVProgressHUD.dismiss()
            present(vc, animated: true, completion: nil)
        }
        
        
    }
}

extension centerCardCatagoryViewController: UICollectionViewDataSource , UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        switch collectionView {
        case self.collectionView:
            return self.proList.count
        default:
            return self.gallery.count
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        switch collectionView {
        case self.collectionView:
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: String(describing: "cell"), for: indexPath) as! centerCollCell
            
            let data = proList[indexPath.row] as! NSDictionary
            let portfolio = data.value(forKey: "portfolio") as! NSDictionary
            let Gallery = portfolio.value(forKey: "gallery") as? Array<String>
            let pro_uid = data.value(forKey: "profileId") as! String
            
            let urls = Gallery![0]
            
//            let md5 = MD5(pro_uid)
//            let url = "\(baseURL.photoURL)user/\(md5.lowercased())/bookme/storages/\(urls)"
            
            cell.workImage.sd_setImage(with: URL(string: urls), placeholderImage: UIImage(named: "bookme_loading"))
            cell.workImage.setRoundWithR(r: commonValue.subTadius)
            
            
            return cell
        default:
            let item = collectionView.dequeueReusableCell(withReuseIdentifier: "image", for: indexPath) as! imageCollectionItem
            
            let data = proList[collectionView.tag] as! NSDictionary
            let portfolio = data.value(forKey: "portfolio") as! NSDictionary
            let Gallery = portfolio.value(forKey: "gallery") as? Array<String>
            let pro_uid = data.value(forKey: "profileId") as! String
            
            let md5 = MD5(pro_uid)
            
            if(indexPath.row <= Gallery!.count){
//                let url = "\(baseURL.photoURL)user/\(md5.lowercased())/bookme/thumbs/\(Gallery![indexPath.row])"
                item.image.sd_setImage(with: URL(string: Gallery![indexPath.row]), placeholderImage: UIImage(named: "bookme_loading"))
            }
            else{
                let url = ""
                item.image.sd_setImage(with: URL(string: url), placeholderImage: UIImage(named: "bookme_loading"))
            }
            
            item.image.setRoundWithR(r: commonValue.subTadius)
            
            
            return item
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        switch collectionView {
        case self.collectionView:
        return CGSize(width: view.bounds.width * cellPercentWidth,height: view.bounds.height * cellPercentWidth * cellPercentWidth)
        default:
            return CGSize(width: collectionView.bounds.height, height: collectionView.bounds.height)
        }
    }
    
    
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        
        switch collectionView {
        case self.collectionView:
            print("Current centered index: \(String(describing: centeredCollectionViewFlowLayout.currentCenteredPage ?? nil))")
            
            self.index = centeredCollectionViewFlowLayout.currentCenteredPage!
            
            let data = proList[index] as! NSDictionary
            
            let portfolio = data.value(forKey: "portfolio") as! NSDictionary
            
            let profile = data.value(forKey: "profile") as! NSDictionary
            let profileImage = profile.value(forKey: "profilePhoto") as? String
            let firstName = profile.value(forKey: "firstName") as? String
            let lastName = profile.value(forKey: "lastName") as? String
            let title = profile.value(forKey: "tittle") as? String
            
            let pids = data.value(forKey: "_id") as! String
            let favCounts = data.value(forKey: "favCount") as! Int64
            
            
            self.gallery.removeAll()
            let Gallery = portfolio.value(forKey: "gallery") as? Array<String>
            self.gallery = Gallery!
            
            if let first = firstName , let last = lastName {
                self.profileName.text = "\(first) \(last)"
            }
            
            self.profileTitle.text = title
            self.profileImage.sd_setImage(with: URL(string: profileImage!), placeholderImage: UIImage(named: "bookme_loading"))
            
            self.favButton.isHidden = false
            
            let userDefaults = UserDefaults.standard
            
            if let fav = userDefaults.value(forKey: "userData_FavoritePID"){
                
                let userFav = fav as! Array<String>
                
                if(!userFav.filter{$0 == pids}.isEmpty){
                    self.favButton.setImage(UIImage(named: "icon_fav_red"), for: .normal)
                }
                else{
                    self.favButton.setImage(UIImage(named: "icon_fav"), for: .normal)
                }
                
                self.favCount.isHidden = false
                self.favCount.text = "\(favCounts)"
            }
            else{
                self.favCount.isHidden = false
                self.favCount.text = "\(favCounts)"
                print("Error : cannot load userData_FavoritePID")
            }
            
            self.imageCollectionView.reloadData()
            
        default:
            break
        }
        
    }
}

class centerCollCell : UICollectionViewCell{
    @IBOutlet var workImage: UIImageView!
}
