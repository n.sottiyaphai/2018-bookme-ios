//
//  ePaymentViewController.swift
//  bookme
//
//  Created by Naphat Sottiyaphai on 19/7/2561 BE.
//  Copyright © 2561 Naphat Sottiyaphai. All rights reserved.
//

import UIKit
import Alamofire
import SVProgressHUD
import Alamofire
import SwiftDate
import ACFloatingTextfield_Swift
//import CreditCardValidator


class ePaymentViewController: UIViewController {
    
    @IBOutlet weak var mainButton: UIButton!
    
    var jobData = NSDictionary()
    var lastPrice = String()
    var promoCode = String()
    var paymentToken = String()
    var lastPriceDouble = Double()
    var datePickerView : UIDatePicker!
    
    @IBOutlet weak var cardNumberTF: ACFloatingTextfield!
    @IBOutlet weak var expYear: ACFloatingTextfield!
    @IBOutlet weak var CVCTF: ACFloatingTextfield!
    @IBOutlet weak var HolderNameTF: ACFloatingTextfield!
    @IBOutlet weak var expMonth: ACFloatingTextfield!
    
    @IBOutlet weak var cardTypeImage: UIImageView!
    @IBOutlet weak var mainActionButton: UIButton!
    
    @IBOutlet weak var bgView: UIView!
    
    var redirectStr = String()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        cardNumberTF.delegate = self
        cardTypeImage.setRoundWithR(r: commonValue.viewRadius)
        mainActionButton.setRoundedwithR(r: commonValue.viewRadius)
        
        self.bgView.setRoundWithR(r: commonValue.viewRadius)
        self.setBackgroundColor()
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func getPaymentToken() {
        let userDefaults = UserDefaults.standard
        let bluweoAccessToken = userDefaults.value(forKey: "bluweoAccessToken") as! String
        let url = "\(baseURL.bluweoPayment)2c2p/mobile?access_token=\(bluweoAccessToken)"
        
        let jobNo = self.jobData.value(forKey: "jobNo") as! String
        let timestamp = Date().toFormat("yyddHHmmss")
        let lastNumber = "\(Int(lastPriceDouble*100))"
        
        var priceInvoice = String()
        let loopCount = 12 - lastNumber.count
        for _ in 1...loopCount {
            priceInvoice+="0"
        }
        
        let headers = [
            "client_id" : "1001",
            "secret_key" : "bookme",
            "Content-Type": "application/x-www-form-urlencoded"
        ]
        let params : [String : Any] = [
            "merchant_id" : apiKey.merchantID2c2p,
            "secret_key" : apiKey.secretKey2c2p,
            "description" : "bookme_JN\(jobNo)",
            "invoice_no" : timestamp,
            "currency_code" : "THB",
            "amount" : priceInvoice + lastNumber,
            "payment_channel" : "CC"
            ]
        
        print(headers)
        print(params)
        
        Alamofire.request(URL(string: url)!, method: .post , parameters: params, encoding: URLEncoding(destination: .httpBody) , headers: headers).responseJSON(completionHandler: { (respond) in
            
            if let result = respond.result.value{
                let responsed = result as! NSDictionary
                print(responsed)
                
                let response = responsed.value(forKey: "response") as! Bool
                if(response){
                    let data = responsed.value(forKey: "data") as! NSDictionary
                    print(data)
                    
                    self.paymentToken = data.value(forKey: "token") as! String
                    self.sendPayment(token: self.paymentToken)
                    
                }
                else{
                    let data = responsed.value(forKey: "data") as! NSDictionary
                    let message = data.value(forKey: "message") as! NSArray
                    let msg = message[0] as! NSDictionary
                    let errMsg = msg.value(forKey: "message") as! String
                    print(errMsg)
                    
                    SVProgressHUD.showError(withStatus: errMsg)
                    DispatchQueue.main.asyncAfter(deadline: .now() + 2){
                        SVProgressHUD.dismiss()
                    }
                }
            }
            else{
                print(respond.error?.localizedDescription)
                SVProgressHUD.dismiss()
            }
        })
        
    }
    
    @IBAction func getPayment(_ sender: Any) {
        print("payment add")
        self.getPaymentToken()
    }
    
    
    func sendPayment(token : String) {
        
//        SVProgressHUD.show()
//        print(token)
//
//        //        Construct credit card request
//        let creditCardPayment:CreditCardPayment = CreditCardPaymentBuilder(pan: cardNumberTF.text!)
//            .expiryMonth(Int(expMonth.text!)!)
//            .expiryYear(Int(expYear.text!)!)
//            .securityCode(CVCTF.text!)
//            .build()
//
//        //        Construct transaction request
//        let transactionRequest:TransactionRequest = TransactionRequestBuilder(paymentToken: paymentToken)
//            .withCreditCardPayment(creditCardPayment)
//            .build()
//
//        //Execute payment request
//        PGWSDK.shared.proceedTransaction(transactionRequest: transactionRequest,
//                                         success: { (response:TransactionResultResponse) in
//
//                                            //For 3DS
//                                            if response.responseCode == APIResponseCode.TRANSACTION_AUTHENTICATE {
//
//                                                let redirectUrl:String = response.redirectUrl!
//                                                self.redirectStr = redirectUrl
//                                                self.performSegue(withIdentifier: "3dsPayment", sender: self)
//                                                SVProgressHUD.dismiss()
//
//                                            } else if response.responseCode == APIResponseCode.TRANSACTION_COMPLETED {
//
//                                                //Inquiry payment result by using transaction id.
//                                                let transactionID:String = response.transactionID!
//                                                print(transactionID)
//                                                self.sendTransection()
//                                                SVProgressHUD.showSuccess(withStatus: "Payment Done")
//
//                                            } else {
//                                                //Get error response and display error
//                                                print("error response")
//                                                SVProgressHUD.showError(withStatus: "Payment Error")
//
//                                                DispatchQueue.main.asyncAfter(deadline: .now() + 2){
//                                                    SVProgressHUD.dismiss()
//                                                }
//                                            }
//        }) { (error:NSError) in
//            //Get error response and display error
//        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if(segue.identifier == "3dsPayment"){
            let vc = segue.destination as! ePayment3DsWebViewController
            vc.redirectUrl = self.redirectStr
            vc.jobData = self.jobData
            vc.lastPrice = self.lastPrice
            vc.promoCode = self.promoCode
        }
    }
    
    func sendTransection() {
        
        let cId = self.jobData.value(forKey: "cId") as! String
        let pId = self.jobData.value(forKey: "professId") as! String
        let bookingId = self.jobData.value(forKey: "_id") as! String
        
        let ud = UserDefaults.standard
        var accesstoken = String()
        
        if(ud.object(forKey: "accessToken") != nil){
            accesstoken = ud.value(forKey: "accessToken") as! String
        }
        
        let header : HTTPHeaders = [
            "accessToken" : accesstoken
        ]
        
        
        let params : [String : Any] = [
            "cId": cId,
            "pId": pId,
            "bookingId": bookingId,
            "paymentStatus": 1,
            "sourceBank": [
                "AccountBank": "",
                "AccountNo": "",
                "AccountName": ""
            ],
            "destinationBank": [
                "AccountBank": "",
                "AccountNo": "",
                "AccountName": ""
            ],
            "transactionType": 1,
            "amount": lastPrice ,
            "promocode" : self.promoCode,
            "transferDateTime" : Date().toISO()
        ]
        
        //        print(params)
        
        Alamofire.request(URL(string: "\(baseURL.url)transaction")!, method: .post, parameters: params, encoding: JSONEncoding.default , headers : header).responseJSON(completionHandler: {(res) in
            
            let result = res.result.value
            let JSON = result as! NSDictionary
            
            //            print(JSON)
            let response = JSON.value(forKey: "response")as! Bool
            if(response){
                if (JSON.object(forKey: "data") != nil){
                    SVProgressHUD.dismiss()
                    let vc = thankYouBookingViewController.instantiateFromStoryboard()
                    
                    vc.comfirmation = true
                    vc.makeBooking = true
                    vc.makePayment = true
                    
                    self.navigationController?.pushViewController(vc, animated: true)
                }
            }
            else{
//                let errorMessage = JSON.value(forKey: "err") as! NSDictionary
//                let message = errorMessage.value(forKey: "message") as! String
//                SVProgressHUD.showError(withStatus: "\(message)")
                
//                SVProgressHUD.showError(withStatus: "Bookme Payment Error")
//
//                DispatchQueue.main.asyncAfter(deadline: .now() + 1){
//                    SVProgressHUD.dismiss()
//                }
            }
        })
    }
    
}

extension ePaymentViewController : UITextFieldDelegate{

    func textFieldDidEndEditing(_ textField: UITextField) {
        if(textField == cardNumberTF){
            print("cardNumber Check")
            
//            let number = cardNumberTF.text!
//            let v = CreditCardValidator()
//
//            if v.validate(string : number) {
//                // Card number is valid
//                print("Card number is valid")
//
//                if let type = v.type(from: number) {
//                    print(type.name) // Visa, Mastercard, Amex etc.
//
//                    if(type.name == "Visa"){
//                        self.cardTypeImage.image = UIImage(named: "icon_visa")
//                    }
//                    else if(type.name == "MasterCard"){
//                        self.cardTypeImage.image = UIImage(named: "icon_mastercard")
//                    }
//                    else{
//
//                    }
//
//                } else {
//                    // I Can't detect type of credit card
//                    print("I Can't detect type of credit card")
//                }
//            } else {
//                // Card number is invalid
//                print("Card number is invalid")
//                cardNumberTF.showErrorWithText(errorText: "Invalid Card number")
//            }
//
//
        }
    }

}



