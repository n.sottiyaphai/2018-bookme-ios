//
//  writeReviewViewController.swift
//  bookme
//
//  Created by Naphat Sottiyaphai on 30/8/2561 BE.
//  Copyright © 2561 Naphat Sottiyaphai. All rights reserved.
//

import UIKit
import Cosmos
import Alamofire

class writeReviewViewController: UIViewController {
    
//    @IBOutlet weak var reviewTitle: UITextField!
    @IBOutlet weak var reviewtext: UITextView!
    @IBOutlet weak var cosmosView: CosmosView!
    @IBOutlet weak var mainButton: UIButton!
    
    @IBOutlet weak var bgView: UIView!
    @IBOutlet weak var yellowGB: UIView!
    @IBOutlet weak var whiteBG: UIView!
    @IBOutlet weak var imageProfile: UIImageView!
    @IBOutlet weak var professName: UILabel!
    @IBOutlet weak var titleJobs: UILabel!
    
    var pId = String()
    var bookingID = String()
    var jobData = NSDictionary()
    
    var ratings = 5.0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setBackgroundColor()
        setClearNavBar()

        
        cosmosView.didFinishTouchingCosmos = { rating in
            self.ratings = rating
        }
        
        cosmosView.didTouchCosmos = { rating in
            print(rating)
            
            switch rating {
            case 0:
                self.cosmosView.text = "Poor"
                break
            case 1:
                self.cosmosView.text = "Bad"
                break
            case 2:
                self.cosmosView.text = "Normal"
                break
            case 3:
                self.cosmosView.text = "Good"
                break
            case 4:
                self.cosmosView.text = "Excellent"
                break
            case 5:
                self.cosmosView.text = "Perfect"
                break
            default:
                break
            }
        }
        
        self.viewInit()
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        self.navigationController?.navigationBar.isHidden = false
        
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    @IBAction func writeReview(_ sender: Any) {
        self.postReview()
    }
    
    
    func updateBookingStatus(status : String){
        
        let ud = UserDefaults.standard
        var accesstoken = String()
        
        if(ud.object(forKey: "accessToken") != nil){
            accesstoken = ud.value(forKey: "accessToken") as! String
        }
        
        let header : HTTPHeaders = [
            "accessToken" : accesstoken
        ]
        
        var result = false
        
        let pid = self.jobData.value(forKey: "_id") as! String
        let url = "\(baseURL.url)booking/\(pid)"
        let params : [String : Any]  = [
            "bookingProcessStatus" : status
        ]
        
        Alamofire.request(url, method: .put, parameters: params, encoding: JSONEncoding.default , headers : header).responseJSON(completionHandler: {(respond) in
            
            let results = respond.result.value
            if let JSON = results {
                result = (JSON as! NSDictionary).value(forKey: "response") as! Bool
                
                if(result){
                    self.pushThankyouView(Confirm: true, Booking: true, Payment: true)
                }
            }
            else{
                print("err")
            }
            
        })
    }
    
    func postReview() {
        
        let ud = UserDefaults.standard
        var accesstoken = String()
        
        if(ud.object(forKey: "accessToken") != nil){
            accesstoken = ud.value(forKey: "accessToken") as! String
        }
        
        let header : HTTPHeaders = [
            "accessToken" : accesstoken
        ]
        
        let url = URL(string: "\(baseURL.url)review")
        
        let userDefaults = UserDefaults.standard
        let uid = userDefaults.value(forKey: "user_uid") as! String
        
        let params :  [String : Any] = [
            "profileId" : uid,
            "professId" : pId,
            "bookingId" : bookingID,
            "reviewText" : reviewtext.text!,
            "reviewRate" : self.ratings
        ]
        
        Alamofire.request(url!, method: .post, parameters: params, encoding: JSONEncoding.default , headers : header).responseJSON(completionHandler: {(response) in
            
            if response.result.value != nil{
                print(response.result.value)
                self.updateBookingStatus(status : "8")
            }
        })
    }
    
    func pushThankyouView(Confirm : Bool ,Booking : Bool ,  Payment : Bool) {
        
        let vc = thankYouBookingViewController.instantiateFromStoryboard()
        
//        var titleStr = String()
//        var textString = String()
        
        vc.makeBooking = Booking
        vc.comfirmation = Confirm
        vc.makePayment = Payment
        
        vc.titleStr = "Thank you for you review"
        vc.textString = "Thank you for you review. We hope you enjoy with bookme. And we wait to see your new booking soon. Thank a lots. ...Bookme"
        vc.formOtherSegue = true
        vc.stateIndex = 10
        
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func viewInit(){
        
        // Main button UI Init
        mainButton.setRoundWithR(r: commonValue.viewRadius)
        
        // Review Review Text UI Init
        reviewtext.setRoundWithCommonR()
        reviewtext.textContainerInset = .init(top: 15, left: 15, bottom: 15, right: 15)
        
        // Employer Card
        bgView.setRoundWithCommonR()
        yellowGB.setRoundOnlyRightWithR(r: commonValue.viewRadius)
        
        let ProfessProfile = self.jobData.value(forKey: "profile") as! NSDictionary
        let professFirstName = ProfessProfile.value(forKey: "firstName") as! String
        let professLastName = ProfessProfile.value(forKey: "lastName") as! String
        let professImage = ProfessProfile.value(forKey: "profilePhoto") as! String
        let jobTitle = self.jobData.value(forKey: "jobTitle") as! String
        
        self.imageProfile.setRounded()
        self.imageProfile.sd_setImage(with: URL(string: professImage), placeholderImage: UIImage(named: "bookme_loading"))
        self.professName.text = "\(professFirstName) \(professLastName)"
        self.titleJobs.text = jobTitle
        
    }
    
}
