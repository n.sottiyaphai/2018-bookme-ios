//
//  jobDetailViewController.swift
//  bookme
//
//  Created by Naphat Sottiyaphai on 28/6/2561 BE.
//  Copyright © 2561 Naphat Sottiyaphai. All rights reserved.
//

import UIKit
import Alamofire
import SwiftHash
import Kingfisher
import SVProgressHUD
import FirebaseDatabase
import SwiftDate

class jobDetailViewController: UIViewController {
    
    @IBOutlet weak var EmployerName: UILabel!
    @IBOutlet weak var bgView: UIView!
    @IBOutlet weak var cancelButton: UIButton!
    @IBOutlet weak var cancelHeight: NSLayoutConstraint!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var collectionViewHight: NSLayoutConstraint!
    @IBOutlet weak var dateTime: UILabel!
    @IBOutlet weak var employerImage: UIImageView!
    @IBOutlet weak var jobDescLabel: UITextView!
    @IBOutlet weak var jobNumberLabel: UILabel!
    @IBOutlet weak var jobTitleLabel: UILabel!
    @IBOutlet weak var locationNameLabel: UILabel!
    @IBOutlet weak var mainButton: UIButton!
    @IBOutlet weak var namePlateStatusView: UIView!
    @IBOutlet weak var nameplateBackgroundView: UIView!
    @IBOutlet weak var packName: UILabel!
    @IBOutlet weak var packPrice: UILabel!
    @IBOutlet weak var paddingHeight: NSLayoutConstraint!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var workerName: UILabel!
    @IBOutlet weak var workerProfileImage: UIImageView!
    @IBOutlet weak var workerTitle: UILabel!
    
    var CloseBookingCode       = ""
    var bookingID              = String()
    var bookingImage           = [String]()
    var bookingStatus : Int    = 1
    var bookingTimeline        = [NSDictionary]()
    var callDataByID           = false
    var email                  = String()
    var employerImageURLString = String()
    var employerNameString     = String()
    var employerTitleString    = String()
    var employerUID            = String()
    var firstName              = String()
    var imageURL               = String()
    var jobData                = NSDictionary()
    var lastName               = String()
    var navPush                = false
    var workerImageURLString   = String()
    var workerNameString       = String()
    
    
    class func instantiateFromStoryboard() -> jobDetailViewController {
        let storyboard = UIStoryboard(name: "calendar", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: String(describing: self)) as! jobDetailViewController
    }
    
    private lazy var channelRef: DatabaseReference = Database.database().reference().child("room")
    
    // *** Booking Status ***
    //------------------------------
    //    0 = Cancelled
    //    1 = New Booking
    //    2 = Confirmation
    //    3 = Make Payment
    //    4 = Confirm Payment
    //    5 = Work Complete
    //    6 = Approve Payment
    //    7 = Transfer Payment
    //------------------------------
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if(callDataByID){
            self.callBooking(bookinID: self.bookingID)
        }
        else{
//            self.viewInit()
            
            let userDefault = UserDefaults.standard
            let uid = userDefault.value(forKey: "user_uid") as! String
            
            DispatchQueue.main.async {
                self.callProfile(uid: uid)
                self.callBooking(bookinID: self.jobData.value(forKey: "_id") as! String)
//                self.getProfileCustomer()
            }
        }
        
        self.setBackgroundColor()
        self.nameplateBackgroundView.backgroundColor = UIColor(red: 242, green: 242, blue: 242, alpha: 1)
        self.bgView.setRoundWithR(r: commonValue.viewRadius)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.navigationController?.navigationBar.isHidden = false
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        //        self.navigationController?.navigationBar.isHidden = true
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        sizeHeaderToFit()
    }
    
    func sizeHeaderToFit() {
        let headerView = tableView.tableHeaderView!
        
        headerView.setNeedsLayout()
        headerView.layoutIfNeeded()
        
        let height = headerView.systemLayoutSizeFitting(UIView.layoutFittingCompressedSize).height
        var frame = headerView.frame
        frame.size.height = height
        headerView.frame = frame
        
        tableView.tableHeaderView = headerView
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func callBooking(bookinID : String) {
        
        let ud = UserDefaults.standard
        var accesstoken = String()
        
        if(ud.object(forKey: "accessToken") != nil){
            accesstoken = ud.value(forKey: "accessToken") as! String
        }
        
        
        let header : HTTPHeaders = [
            "accessToken" : accesstoken
        ]
        
        let URL = "\(baseURL.url)booking/\(bookinID)"
        
        Alamofire.request(URL, method: .get, headers: header).responseJSON(completionHandler: { (respond) in
            
            let respond = respond.result.value
            
            if(respond is NSDictionary){
                let jobs = respond as! NSDictionary
                if(jobs.value(forKey: "response") as! Bool){
                    self.jobData = jobs.value(forKey: "data") as! NSDictionary
                    self.bookingStatus = self.jobData.value(forKey: "bookingProcessStatus") as! Int
                    
                    SVProgressHUD.dismiss()
                    
                    DispatchQueue.main.async {
                        let uid = ud.value(forKey: "user_uid") as! String
                        self.callProfile(uid: uid)
//                        self.getProfileCustomer()
                        self.viewInit()
                    }
                }
            }
                
            else{
                SVProgressHUD.showError(withStatus: "Network Error Please try again later.")
                
                DispatchQueue.main.asyncAfter(deadline: .now() + 3) {
                    SVProgressHUD.dismiss()
                    self.tableView.reloadData()
                }
                
            }
            
        })
        
    }
    
    func viewInit(){
        
        let userdefaults = UserDefaults.standard
        
        collectionView.delegate = self
        employerImage.sd_setImage(with: URL(string: employerImageURLString), placeholderImage: UIImage(named: "bookme_loading"))
        
        employerImage.setRounded()
        workerProfileImage.setRounded()
        mainButton.setRoundWithR(r: commonValue.viewRadius)
        cancelButton.setRoundWithR(r: commonValue.viewRadius)
        
        nameplateBackgroundView.setRoundWithR(r: commonValue.viewRadius)
        namePlateStatusView.setRoundOnlyRightWithR(r: commonValue.viewRadius)
        
        // Hiden cancel Button
        paddingHeight.constant = 0
        cancelHeight.constant = 0
        
        let cId = jobData.value(forKey: "customerId") as! String
        let uid = userdefaults.value(forKey: "user_uid") as! String
        
        switch bookingStatus {
        case 0:
            self.mainButton.setTitle("This jobs was Cancel", for: .normal)
            self.mainButton.isEnabled = false
            self.mainButton.isUserInteractionEnabled = false
            self.mainButton.backgroundColor = UIColor.lightGray
        case 1:
            if(uid == cId){
                self.mainButton.setTitle("Waiting Confirm", for: .normal)
                self.mainButton.isEnabled = false
                self.mainButton.isUserInteractionEnabled = false
                self.mainButton.backgroundColor = UIColor.lightGray
            }
            else{
                self.paddingHeight.constant = 15
                self.cancelHeight.constant = 50
                self.mainButton.isEnabled = true
                self.mainButton.isUserInteractionEnabled = true
            }
            break
        case 2:
            if(uid == cId){
                self.mainButton.setTitle("Make Payment", for: .normal)
                self.mainButton.isEnabled = true
                self.mainButton.isUserInteractionEnabled = true
            }else{
                self.mainButton.setTitle("Waiting Payment", for: .normal)
                self.mainButton.isEnabled = false
                self.mainButton.isUserInteractionEnabled = false
                self.mainButton.backgroundColor = UIColor.lightGray
            }
            break
        case 3:
            if(uid == cId){
                self.mainButton.setTitle("Payment in Process", for: .normal)
                self.mainButton.backgroundColor = UIColor(red: 144.0/255.0, green: 177.0/255.0, blue: 187.0/255.0, alpha: 1.0)
            }else{
                self.mainButton.setTitle("Payment in Process", for: .normal)
                self.mainButton.backgroundColor = UIColor(red: 144.0/255.0, green: 177.0/255.0, blue: 187.0/255.0, alpha: 1.0)
            }
            
        case 4:
            if(uid == cId){
                self.mainButton.setTitle("Wait worker complete job.", for: .normal)
                self.mainButton.isEnabled = false
                self.mainButton.isUserInteractionEnabled = false
                self.mainButton.backgroundColor = UIColor.lightGray
            }
            else{
                self.mainButton.setTitle("Complete Job", for: .normal)
                self.mainButton.isEnabled = true
                self.mainButton.isUserInteractionEnabled = true
            }
        case 5:
            if(uid == cId){
                self.mainButton.setTitle("Tranfer Payment", for: .normal)
                self.mainButton.backgroundColor = UIColor(red: 25.0/255.0, green: 169.0/255.0, blue: 141.0/255.0, alpha: 1.0)
                self.mainButton.isEnabled = true
                self.mainButton.isUserInteractionEnabled = true
            }
            else{
                self.mainButton.setTitle("Waiting Tranfer Payment", for: .normal)
                self.mainButton.isEnabled = false
                self.mainButton.isUserInteractionEnabled = false
                self.mainButton.backgroundColor = UIColor.lightGray
            }
        case 6:
            if(uid == cId){
                self.mainButton.setTitle("Write review", for: .normal)
                self.mainButton.backgroundColor = UIColor(red: 25.0/255.0, green: 169.0/255.0, blue: 141.0/255.0, alpha: 1.0)
                self.mainButton.isEnabled = true
                self.mainButton.isUserInteractionEnabled = true
            }
            else{
                self.mainButton.setTitle("Tranfer payment Done", for: .normal)
                self.mainButton.isEnabled = false
                self.mainButton.isUserInteractionEnabled = false
                self.mainButton.backgroundColor = UIColor.lightGray
            }
            
        case 8:
            if(uid == cId){
                self.mainButton.setTitle("Jobs done", for: .normal)
                self.mainButton.backgroundColor = UIColor.lightGray
                self.mainButton.isEnabled = true
                self.mainButton.isUserInteractionEnabled = true
            }
            else{
                self.mainButton.setTitle("Tranfer payment Done", for: .normal)
                self.mainButton.isEnabled = false
                self.mainButton.isUserInteractionEnabled = false
                self.mainButton.backgroundColor = UIColor.lightGray
            }
            
        default:
            self.mainButton.setTitle("", for: .normal)
            self.mainButton.isEnabled = false
            self.mainButton.isUserInteractionEnabled = false
            self.mainButton.backgroundColor = UIColor.lightGray
        }
        
        
        let timeline = jobData.value(forKey: "bookingProcessHistory") as! Array<Any>
        
        for row in timeline{
            let rowObject = row as! NSDictionary
            self.bookingTimeline.append(rowObject)
        }
        
        let profile = jobData.value(forKey: "profile") as! NSDictionary
        let firstName = profile.value(forKey: "firstName") as! String
        let Lastname = profile.value(forKey: "lastName") as! String
        let profileImageURL = profile.value(forKey: "profilePhoto") as! String
        let profileTitle = profile.value(forKey: "title") as! String
        
        let jobTitle = jobData.value(forKey: "jobTitle") as! String
        let jobNo = jobData.value(forKey: "jobNo") as! String
        let jobDescription = jobData.value(forKey: "jobDescription") as! String
        let bookingPackageName = jobData.value(forKey: "bookingPackageName") as! String
        let bookingPackagePrice = jobData.value(forKey: "bookingPackagePrice") as! Int
        
        let location = jobData.value(forKey: "bookingLocation") as! NSDictionary
        let locationName = location.value(forKey: "locationName") as! String
        
        let bookingDate = jobData.value(forKey: "bookingDate") as! Array<Any>
        let showDate = bookingDate[0] as! String
        let datePhase = showDate.toDate()
        let dateFormat = datePhase?.toFormat("dd MMM yyyy")
        
        
        let cusProfile = jobData.value(forKey: "customerProfile") as! NSDictionary
        let urlImage = cusProfile.value(forKey: "profilePhoto") as! String
        let cusfirstName = cusProfile.value(forKey: "firstName") as! String
        let cusLastname = cusProfile.value(forKey: "lastName") as! String
        let cid = cusProfile.value(forKey: "_id") as! String
        
        self.employerImage.sd_setImage(with: URL(string: urlImage), placeholderImage: UIImage(named: "bookme_loading"))
        self.EmployerName.text = "\(cusfirstName) \(cusLastname)"
        self.employerNameString = "\(cusfirstName)"
        self.employerImageURLString = urlImage
        self.employerUID = cid
        
        if(jobData.object(forKey: "bookingWorkUpload") != nil){
//            let bookingImage = jobData.value(forKey: "bookingWorkUpload") as! NSDictionary
            let bookingImageUpload = jobData.value(forKey: "bookingWorkUpload") as! Array<String>
            
            let pro_uid = jobData.value(forKey: "customerId") as! String
            let md5 = MD5(pro_uid)
            for images in bookingImageUpload{
                let full_url = "\(baseURL.photoURL)user/\(md5.lowercased())/bookme/storages/\(images)"
                self.bookingImage.append(full_url)
            }
        }
        
        jobNumberLabel.text = "No. \(jobNo)"
        locationNameLabel.text = locationName
        dateTime.text = dateFormat
        workerName.text = "\(firstName) \(Lastname)"
        workerNameString = "\(firstName)"
        workerImageURLString = profileImageURL
        
        workerProfileImage.sd_setImage(with: URL(string: profileImageURL), placeholderImage: UIImage(named: "bookme_loading"))
        workerTitle.text = profileTitle
        
        jobTitleLabel.text = jobTitle
        jobDescLabel.text = jobDescription
        packName.text = bookingPackageName
        
        
        let myNumber = NSNumber(value:bookingPackagePrice)
        
        let numberFormatter = NumberFormatter()
        numberFormatter.numberStyle = .decimal
        
        if let prices = numberFormatter.string(from: myNumber){
            packPrice.text  = "\(prices) Baht"
        }
        
        if(callDataByID){
            if(!navPush){
                let back = UIButton(type: .custom)
                back.setImage(UIImage(named: "icon-arrow-left"), for: .normal)
                back.setTitleColor(UIColor.black, for: .normal)
                back.imageEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
                back.frame = CGRect(x: 0, y: 0, width: 44  , height: 20)
                back.addTarget(self, action: #selector(self.goMain), for: .touchUpInside)
                let Leftitem = UIBarButtonItem(customView: back)
                self.navigationItem.setLeftBarButton(Leftitem, animated: true)
            }
        }
        
        let btn1 = UIButton(type: .custom)
        btn1.setImage(UIImage(named: "icon_chat"), for: .normal)
        btn1.setTitleColor(UIColor.black, for: .normal)
        btn1.imageEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        btn1.frame = CGRect(x: 0, y: 0, width: 44  , height: 20)
        btn1.addTarget(self, action: #selector(self.go2Chat), for: .touchUpInside)
        let item1 = UIBarButtonItem(customView: btn1)
        self.navigationItem.setRightBarButton(item1, animated: true)
        
        // ReloadData Zone
        self.tableView.reloadData()
        self.collectionView.reloadData()
    }
    
    @objc func goMain() {
        let vc = tabBarViewController.instantiateFromStoryboard()
        self.present(vc, animated: true, completion: nil)
    }
    
    func getProfileCustomer() {
        //        cId
        let ud = UserDefaults.standard
        var accesstoken = String()
        
        if(ud.object(forKey: "accessToken") != nil){
            accesstoken = ud.value(forKey: "accessToken") as! String
        }
        
        let header : HTTPHeaders = [
            "accessToken" : accesstoken
        ]
        
        let cId = jobData.value(forKey: "cId") as! String
        let url = "\(baseURL.url)profile/\(cId)"
        
        
        Alamofire.request(url , headers : header ).responseJSON(completionHandler: {(respond) in
            let result = respond.result.value
            
            
            if(result is NSDictionary){
                let objectData = result as! NSDictionary
                let data = objectData.value(forKey: "data") as! NSDictionary
                
                let urlImage = data.value(forKey: "profilePhoto") as! String
                let firstName = data.value(forKey: "firstName") as! String
                let Lastname = data.value(forKey: "lastName") as! String
                let cid = data.value(forKey: "_id") as! String
                
                self.employerImage.sd_setImage(with: URL(string: urlImage), placeholderImage: UIImage(named: "bookme_loading"))
                self.EmployerName.text = "\(firstName) \(Lastname)"
                self.employerNameString = "\(firstName)"
                self.employerImageURLString = urlImage
                self.employerUID = cid
            }
            
        })
    }
    
    @IBAction func mainAction(_ sender: Any) {
        if(bookingStatus == 1){
            
            let alert = UIAlertController(title: "Are you sure ?", message: "Do you want to Confirm this Job", preferredStyle: .alert)
            
            let ok = UIAlertAction(title: "OK", style: .default) { (action) in
                print("Capture action OK")
                self.updateBookingStatus(status: "2" , reason: "", CloseBookingCode: "")
            }
            
            let cancel = UIAlertAction(title: "Cancel", style: .cancel) { (action) in
                print("Capture action Cancel")
                self.dismiss(animated: true, completion: nil)
            }
            alert.addAction(ok)
            alert.addAction(cancel)
            
            self.present(alert, animated: true, completion: nil)
        }
            
        else if(bookingStatus == 2){
            self.performSegue(withIdentifier: "payment", sender: self)
        }
            
        else if(bookingStatus == 3){
//            self.updateBookingStatus(status: "4", reason: "", CloseBookingCode: "")
        }
            
        else if(bookingStatus == 4){
            
            let bookingDate = jobData.value(forKey: "bookingDate") as! Array<Any>
            let bookingDateCount = bookingDate.count
            let bookingLastDate = bookingDate[bookingDateCount-1] as! String
            
            _ = bookingLastDate.toISODate()
            
            let alert = UIAlertController(title: "Are you sure ?", message: "Do you want to complete this Job", preferredStyle: .alert)
            
            let ok = UIAlertAction(title: "OK", style: .default) { (action) in
                let TextField = alert.textFields![0] as UITextField
                self.updateBookingStatus(status: "5" , reason: "", CloseBookingCode: TextField.text!)
            }
            
            let cancel = UIAlertAction(title: "Cancel", style: .cancel) { (action) in
                print("Capture action Cancel")
//                self.dismiss(animated: true, completion: nil)
            }
            alert.addAction(ok)
            alert.addAction(cancel)
            alert.addTextField {(textfield) in
                textfield.placeholder = "Close code"
            }
            
            self.present(alert, animated: true, completion: nil)
            
        }
            
        else if(bookingStatus == 5){
            let alert = UIAlertController(title: "Are you sure ?", message: "Do you want to transfer payment to worker", preferredStyle: .alert)
            
            let ok = UIAlertAction(title: "OK", style: .default) { (action) in
                print("Capture action OK")
                self.updateBookingStatus(status: "6" , reason: "", CloseBookingCode: "")
            }
            
            let cancel = UIAlertAction(title: "Cancel", style: .cancel) { (action) in
                print("Capture action Cancel")
                self.dismiss(animated: true, completion: nil)
            }
            alert.addAction(ok)
            alert.addAction(cancel)
            
            self.present(alert, animated: true, completion: nil)
            
            
            
            
        }
        else if (bookingStatus == 6){
            print("Review Push")
            self.performSegue(withIdentifier: "review", sender: self)
        }
    }
    
    
    @IBAction func cancelAction(_ sender: Any) {
        if(bookingStatus == 1){
            
            let alert = UIAlertController(title: "Do you want to cancel ?", message: "Please enter why do you want to cancel this booking.", preferredStyle: .alert)
            let ok = UIAlertAction(title: "Submit", style: .default) { (action) in
                let TextField = alert.textFields![0] as UITextField
                self.updateBookingStatus(status: "0", reason: TextField.text!, CloseBookingCode: "")
            }
            let cancel = UIAlertAction(title: "Cancel", style: .cancel) { (action) in
                
            }
            alert.addAction(ok)
            alert.addAction(cancel)
            alert.addTextField {(textfield) in
                textfield.placeholder = "Cancel reason"
            }
            
            self.present(alert, animated: true, completion: nil)
            
            
        }
    }
    
    func updateBookingStatus(status : String , reason : String , CloseBookingCode : String){
        
        let ud = UserDefaults.standard
        var accesstoken = String()
        
        let uid = ud.value(forKey: "user_uid") as! String
        
        if(ud.object(forKey: "accessToken") != nil){
            accesstoken = ud.value(forKey: "accessToken") as! String
        }
        
        let header : HTTPHeaders = [
            "accessToken" : accesstoken
        ]
        
        var result = false
        
        let pid = self.jobData.value(forKey: "_id") as! String
        let url = "\(baseURL.url)booking/\(pid)"
        let params : [String : Any]  = [
            "bookingProcessStatus" : Int(status)!,
            "bookingCancelReason" : reason,
            "jobberCodeClose" : CloseBookingCode,
            "profileId" : uid
        ]
        
        print(pid)
        print(params)
        
        Alamofire.request(url, method: .put, parameters: params, encoding: JSONEncoding.default , headers : header).responseJSON(completionHandler: {(respond) in
            
            if let results = respond.result.value {
                let JSON = results as! NSDictionary
                
                result = JSON.value(forKey: "response") as! Bool
                
                if(result){
                    
                    switch status {
                    case "0" :
                        print("Cancel case")
                        
                        let vc = thankYouBookingViewController.instantiateFromStoryboard()
                        
                        
                        vc.imageName = "image_incorrect"
                        vc.titleStr = "Do you really want to cancel"
                        vc.textString = ""
                        vc.formOtherSegue = true
                        vc.stateIndex = 1
                        
                        self.navigationController?.pushViewController(vc, animated: true)
                        
                    case "1":
                        self.pushThankyouView(Confirm: true, Booking: true, Payment: false, state: 2)
                    case "2":
                        self.pushThankyouView(Confirm: true, Booking: true, Payment: false, state: 3)
                    case "3":
                        self.pushThankyouView(Confirm: true, Booking: true, Payment: true , state: 4)
                    case "4":
                        self.pushThankyouView(Confirm: true, Booking: true, Payment: true , state: 5)
                    case "5":
                        self.pushThankyouView(Confirm: true, Booking: true, Payment: true , state: 6)
                    case "6":
                        self.pushThankyouView(Confirm: true, Booking: true, Payment: true , state: 7)
                    default:
                        break
                    }
                }
                else{
                    SVProgressHUD.showError(withStatus: "Code was wrong\nPlease try again another Code.")
                    DispatchQueue.main.asyncAfter(deadline: .now() + 2){
                        SVProgressHUD.dismiss()
                    }
                }
            }
            else{
                SVProgressHUD.showError(withStatus: "Something wrong\nPlease try again later.")
                DispatchQueue.main.asyncAfter(deadline: .now() + 2){
                    SVProgressHUD.dismiss()
                }
            }
        })
    }
    
    func pushThankyouView(Confirm : Bool ,Booking : Bool ,  Payment : Bool , state : Int) {
        
        let vc = thankYouBookingViewController.instantiateFromStoryboard()
        
        //        var imageName = String()
        //        var titleStr = String()
        //        var textString = String()
        
        vc.makeBooking = Booking
        vc.comfirmation = Confirm
        vc.makePayment = Payment
        vc.stateIndex = state
        
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func callProfile(uid : String){
        
        let ud = UserDefaults.standard
        var accesstoken = String()
        
        if(ud.object(forKey: "accessToken") != nil){
            accesstoken = ud.value(forKey: "accessToken") as! String
        }
        
        let header : HTTPHeaders = [
            "accessToken" : accesstoken
        ]
        
        Alamofire.request(baseURL.url + "profile/\(uid)", method: .get, encoding: JSONEncoding.default , headers : header)
            .responseJSON(completionHandler: { response in
                
                if let result = response.result.value {
                    
                    let data =  result as! NSDictionary
                    _ = String()
                    
                    if(data.object(forKey: "data") != nil){
                        if(data.value(forKey: "data") is NSDictionary){
                            let JSON = data.value(forKey: "data") as! NSDictionary
                            
                            self.firstName = JSON.value(forKey: "firstName") as! String
                            self.lastName = JSON.value(forKey: "lastName") as! String
                            self.imageURL = JSON.value(forKey: "profilePhoto") as! String
                            self.email = JSON.value(forKey: "email") as! String
                        }
                    }
                    else{
                        self.firstName = data.value(forKey: "firstName") as! String
                        self.lastName = data.value(forKey: "lastName") as! String
                        self.imageURL = data.value(forKey: "profilePhoto") as! String
                        self.email = data.value(forKey: "email") as! String
                    }
                    
                }
                else{
                    // Request Error
                    
                    let alert = UIAlertController(title: "Loading Data Error", message: "Please try again later.", preferredStyle: .alert)
                    
                    let ok = UIAlertAction(title: "OK", style: .default) { (action) in
                        print("Capture action OK")
                    }
                    
                    alert.addAction(ok)
                    
                    self.present(alert, animated: true, completion: nil)
                }
            })
        
    }
    
    @objc func go2Chat(){
        
        print("Go 2 Chat")
        
        SVProgressHUD.show()
        print("Go 2 Chat")
        
        let userDefault = UserDefaults.standard
        let myUid = userDefault.value(forKey: "user_uid") as! String
        let myName = userDefault.value(forKey: "user_fullName") as! String
        let myAvatar = userDefault.value(forKey: "user_avatarURL") as! String
        
        let profile = jobData.value(forKey: "profile") as! NSDictionary
        let firstName = profile.value(forKey: "firstName") as! String
        let Lastname = profile.value(forKey: "lastName") as! String
        let profileImage = profile.value(forKey: "profilePhoto") as! String
        let profileID = profile.value(forKey: "_id") as! String
        
        let userdefaults = UserDefaults.standard
        _ = jobData.value(forKey: "cId") as! String
        _ = userdefaults.value(forKey: "user_uid") as! String
        
//        if(uid == cid){
////            print("same uid")
////            profileID = jobData.value(forKey: "pId")
//        }
        
        let params : [String : Any] = [
            "senderID" : myUid,
            "senderName" : myName,
            "senderAvatar" : myAvatar,
            "reciverID" : profileID,
            "reciverName" : "\(firstName) \(Lastname)" ,
            "reciverAvatar" : profileImage
        ]
        
        Alamofire.request(URL(string : "https://us-central1-bookme-1515492592081.cloudfunctions.net/createRoom")!, method: .post, parameters: params, encoding: JSONEncoding.default).responseJSON(completionHandler: { (response) in
            
            if let result = response.result.value{
                _ = result as! NSDictionary
                print(result)
                
                // Send to ChatRoom
                let vc = chatViewController.instantiateFromStoryboard()
                vc.chatSlug = profileID
                vc.roomName = "\(firstName) \(Lastname)"
                vc.avatarURL = profileImage
                
                SVProgressHUD.dismiss()
                self.navigationController?.pushViewController(vc, animated: true)
                
            }
            else{
                SVProgressHUD.showError(withStatus: "Ops! Something wrong.\n Please Try again later.")
                DispatchQueue.main.asyncAfter(deadline: .now() + 2){
                    SVProgressHUD.dismiss()
                }
            }
        })
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if (segue.identifier == "payment"){
            let vc = segue.destination as! termAcceptViewController
            vc.jobData = jobData
        }
        
        if (segue.identifier == "review"){
            let vc = segue.destination as! writeReviewViewController
            vc.jobData = jobData
            vc.bookingID = jobData.value(forKey: "_id") as! String
            vc.pId = jobData.value(forKey: "bookingPackage") as! String
            print(jobData)
        }
    }
    
    func collectCloseCode(){
        
        let alert = UIAlertController(title: "Apply your promo code", message: "Please fill you promocode.", preferredStyle: .alert)
        let ok = UIAlertAction(title: "OK", style: .default) { (action) in
            let TextField = alert.textFields![0] as UITextField
            
            print(TextField.text as Any)
            self.CloseBookingCode = TextField.text ?? ""
            
        }
        let cancel = UIAlertAction(title: "Cancel", style: .cancel) { (action) in
            
        }
        alert.addAction(ok)
        alert.addAction(cancel)
        alert.addTextField {(textfield) in
            textfield.placeholder = "Close Code"
        }
        
        self.present(alert, animated: true, completion: nil)
    }
    
}

extension jobDetailViewController : UITableViewDelegate , UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.bookingTimeline.count
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "timeLine", for: indexPath) as! timeLineCell
        
        if(indexPath.row == 0){
            cell.topLine.isHidden = true
            cell.buttomLine.isHidden = false
        }
        else if (indexPath.row == bookingTimeline.count - 1 ){
            cell.topLine.isHidden = false
            cell.buttomLine.isHidden = true
        }
        else{
            cell.topLine.isHidden = false
            cell.buttomLine.isHidden = false
        }
        
        let bookingTimeData = bookingTimeline[indexPath.row]
        var status = String()
        if(bookingTimeData.value(forKey: "statusUpdate") is Int){
            status = "\(bookingTimeData.value(forKey: "statusUpdate") as! Int)"
        }
        else if(bookingTimeData.value(forKey: "statusUpdate") is String){
            status = bookingTimeData.value(forKey: "statusUpdate") as! String
        }
        
        var timeStamp = TimeInterval()
        if(bookingTimeData.value(forKey: "bookingTimeStamp") is TimeInterval){
            timeStamp = bookingTimeData.value(forKey: "bookingTimeStamp") as! TimeInterval
        }
        
        let myTimeInterval = TimeInterval(timeStamp)
        let time = NSDate(timeIntervalSince1970: TimeInterval(myTimeInterval))
        
        let th = Region(calendar: Calendars.buddhist, zone: Zones.asiaBangkok, locale: Locales.thaiThailand)
        let formated = (time as Date).convertTo(region: th).toFormat("dd MMM yyyy 'at' HH:mm")

        
        cell.Timeline_dateTime.text = formated
        cell.timeLineImage.setRounded()
        
        let workerName = workerNameString
        let employName = employerNameString
        let workerImageURL = URL(string: workerImageURLString)
        let employImageURL = URL(string: employerImageURLString)
        
        
        
        switch status {
        case "0":
            cell.timeLineDest.text = "\(workerName) was cancel this booking."
            cell.timeLineImage.sd_setImage(with: employImageURL, placeholderImage: UIImage(named: "bookme_loading"))
        case "1":
            cell.timeLineDest.text = "\(employName) has create booking."
            cell.timeLineImage.sd_setImage(with: employImageURL, placeholderImage: UIImage(named: "bookme_loading"))
        case "2" :
            cell.timeLineDest.text = "\(workerName) has confirm this job."
            cell.timeLineImage.sd_setImage(with: workerImageURL, placeholderImage: UIImage(named: "bookme_loading"))
        case "3" :
            cell.timeLineDest.text = "\(employName) already make payment for this job."
            cell.timeLineImage.sd_setImage(with: employImageURL, placeholderImage: UIImage(named: "bookme_loading"))
        case "4" :
            cell.timeLineDest.text = "Payment from \(employName) was confirm."
            cell.timeLineImage.sd_setImage(with: employImageURL, placeholderImage: UIImage(named: "bookme_loading"))
        case "5" :
            cell.timeLineDest.text = "\(workerName) was complete work"
            cell.timeLineImage.sd_setImage(with: workerImageURL, placeholderImage: UIImage(named: "bookme_loading"))
        case "6" :
            cell.timeLineDest.text = "\(employName) was approve payment."
            cell.timeLineImage.sd_setImage(with: employImageURL, placeholderImage: UIImage(named: "bookme_loading"))
        case "7" :
            cell.timeLineDest.text = "Transfer Payment done."
            cell.timeLineImage.sd_setImage(with: workerImageURL, placeholderImage: UIImage(named: "bookme_loading"))
        case "8" :
            cell.timeLineDest.text = "Jobs was done."
            cell.timeLineImage.sd_setImage(with: workerImageURL, placeholderImage: UIImage(named: "bookme_loading"))
        default:
            cell.timeLineDest.text = ""
            cell.timeLineImage.image = UIImage(named: "bookme_loading")
            break
        }
        
        // *** Booking Status ***
        //------------------------------
        //    0 = Cancelled
        //    1 = New Booking
        //    2 = Confirmation
        //    3 = Make Payment
        //    4 = Confirm Payment
        //    5 = Work Complete
        //    6 = Approve Payment
        //    7 = Transfer Payment
        //------------------------------
        
        return cell
    }
    
}

extension jobDetailViewController : UICollectionViewDelegate , UICollectionViewDelegateFlowLayout , UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        if(bookingImage.isEmpty){
            collectionViewHight.constant = 0
        }
        
        return bookingImage.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let item = collectionView.dequeueReusableCell(withReuseIdentifier: "image", for: indexPath) as! imageCollectionItem
        
        item.image.kf.setImage(with: URL(string: self.bookingImage[indexPath.row]), placeholder: UIImage(named: "bookme_loading"))
        item.image.setRoundWithR(r: commonValue.subTadius)
        
        return item
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        return CGSize(width: collectionView.bounds.height-10 , height: collectionView.bounds.height-10 )
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        SVProgressHUD.show()
        
        let imageURL = self.bookingImage
        var url = [URL]()
        
        for URLs in imageURL{
            let urlString = URLs as! String
            url.append(URL(string: urlString)!)
        }
        
//        let imageDownloader = AlamofireImageDownloader()
//        let vc = Optik.imageViewer(withURLs: url, initialImageDisplayIndex: indexPath.row, imageDownloader: imageDownloader)
        
        SVProgressHUD.dismiss()
//        present(vc, animated: true, completion: nil)
        
    }
    
    
}

class timeLineCell: UITableViewCell {
    
    @IBOutlet weak var timeLineDest: UILabel!
    @IBOutlet weak var Timeline_dateTime: UILabel!
    @IBOutlet weak var timeLineImage: UIImageView!
    @IBOutlet weak var topLine: UILabel!
    @IBOutlet weak var buttomLine: UILabel!
    
}
