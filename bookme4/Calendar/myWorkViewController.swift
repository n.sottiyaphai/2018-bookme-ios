//
//  myWorkViewController.swift
//  bookme
//
//  Created by Naphat Sottiyaphai on 21/9/2561 BE.
//  Copyright © 2561 Naphat Sottiyaphai. All rights reserved.
//

import UIKit
import ACFloatingTextfield_Swift
import Alamofire
//import SVProgressHUD
import CRRefresh


class myWorkViewController: UIViewController {

    var jobArr = [NSDictionary]()
    var trans = [NSDictionary]()
    var selectIndexRow = Int()
    var jobForSegue =  NSDictionary()
    var bookingStatus = Int()
    
    @IBOutlet weak var tableView: UITableView!
    
    // *** Booking Status ***
    //------------------------------
    //    0 = Cancelled
    //    1 = New Booking
    //    2 = Confirmation
    //    3 = Make Payment
    //    4 = Confirm Payment
    //    5 = Work Complete
    //    6 = Approve Payment
    //    7 = Transfer Payment
    //    8 = Done
    //------------------------------
    
    let status = ["Cancelled" , "New Booking" , "Confirmation" , "Make Payment" , "Confirm Payment" , "Work Complete" , "Approve Payment" , "Transfer Payment" , "Done"]
    
    
    class func instantiateFromStoryboard() -> myWorkViewController {
        let storyboard = UIStoryboard(name: "calendar", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: String(describing: self)) as! myWorkViewController
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setBackgroundColor()
        
        self.tableView.backgroundColor = UIColor.clear
        self.tableView.rowHeight = UITableView.automaticDimension;
        
        /// animator: your customize animator, default is NormalHeaderAnimator
        self.tableView.cr.addHeadRefresh(animator: NormalHeaderAnimator()) { [weak self] in
            /// start refresh
            /// Do anything you want...
            DispatchQueue.main.async {
                self?.getBooking()
            }
            
            DispatchQueue.main.asyncAfter(deadline: .now() + 2, execute: {
                self?.tableView.cr.endHeaderRefresh()
            })
        }
        /// manual refresh
        self.tableView.cr.beginHeaderRefresh()
        
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
    }
    
    func getBooking() {
        
        self.jobArr.removeAll()
        let userDefaults = UserDefaults.standard
        let uid = userDefaults.value(forKey: "user_uid") as! String
        
        var accesstoken = String()
        if(userDefaults.object(forKey: "accessToken") != nil){
            accesstoken = userDefaults.value(forKey: "accessToken") as! String
        }
        
        let header : HTTPHeaders = [
            "type" : "1",
            "accessToken" : accesstoken
        ]
        
        let URL = "\(baseURL.url)booking/byProfile/\(uid)"
        
        Alamofire.request(URL, method: .get, headers: header).responseJSON(completionHandler: { (respond) in
            
            let respond = respond.result.value
            
            if(respond is NSDictionary){
                let jobs = respond as! NSDictionary
                if(jobs.value(forKey: "response") as! Bool){
                    let job_data = jobs.value(forKey: "data") as! Array<Any>
                    
                    for job in job_data{
                        self.jobArr.append(job as! NSDictionary)
                    }
                }
                
//                SVProgressHUD.dismiss()
                
                self.jobArr.sort(by: {
                    (($0.value(forKey: "bookingProcessStatus") as! Int)) > (($1.value(forKey: "bookingProcessStatus") as! Int))
                })
                
                self.tableView.reloadData()
            }
            else{
//                SVProgressHUD.showError(withStatus: "Network Error Please try again later.")
                
                DispatchQueue.main.asyncAfter(deadline: .now() + 3) {
//                    SVProgressHUD.dismiss()
                }
                self.tableView.reloadData()
            }
            
        })
    }
    
    
}

extension myWorkViewController : UITableViewDelegate , UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if(jobArr.count == 0){
            self.tableView.setEmptyMessage("Let's booking the first one.")
        }
        else{
            self.tableView.restore()
        }
        
        return jobArr.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let jobs = tableView.dequeueReusableCell(withIdentifier: "jobs", for: indexPath) as! jobsCell
        
        let rowData = jobArr[indexPath.row]
        
        let ud = UserDefaults.standard
        let uid = ud.value(forKey: "user_uid") as! String
        
        let profile = rowData.value(forKey: "profile") as! NSDictionary
        var profile_imageURL = String()
        var profile_name = String()
        var locationName = String()
        let pid = rowData.value(forKey: "professId") as! String
        let bookingDate = String()
        
        if(profile.value(forKey: "firstName") is String && profile.value(forKey: "lastName") is String ){
            let firstName = profile.value(forKey: "firstName") as! String
            let Lastname = profile.value(forKey: "lastName") as! String
            profile_name = "\(firstName) \(Lastname)"
            jobs.profileName.text = profile_name
        }
        
        if(profile.value(forKey: "profilePhoto") is String){
            profile_imageURL = profile.value(forKey: "profilePhoto") as! String
            jobs.profileImage.sd_setImage(with: URL(string: profile_imageURL), placeholderImage: UIImage(named : "bookme_loading"))
        }
        
        var cellStatus = Int()
        if(rowData.value(forKey: "bookingProcessStatus") is Int){
            let statusInt = rowData.value(forKey: "bookingProcessStatus") as! Int
            cellStatus = statusInt
            jobs.statusText.text = status[statusInt]
        }
        else if (rowData.value(forKey: "bookingProcessStatus") is String){
            let statusString = rowData.value(forKey: "bookingProcessStatus") as! String
            let toInt = Int(statusString)
            
            cellStatus = toInt!
            jobs.statusText.text = status[toInt!]
        }
        
        //        bookingDate
        if(rowData.value(forKey: "bookingDate") is NSArray){
            let statusInt = rowData.value(forKey: "bookingDate") as! NSArray
            let firstDate : String = statusInt[0] as! String
            let toDate  = firstDate.toDate()
            
            jobs.dateTime.text = toDate?.toFormat("dd MMM yyyy")
        }
        else{
//            let statusString = rowData.value(forKey: "bookingDate") as! String
//            let toInt = Int(statusString)
//            jobs.statusText.text = status[toInt!]
        }
        
        var location = NSDictionary()
        
        if(rowData.value(forKey: "bookingLocation") is Array<Any>){
            let locations = rowData.value(forKey: "bookingLocation") as! Array<Any>
            location = locations[0] as! NSDictionary
        }
        else if (rowData.value(forKey: "bookingLocation") is NSDictionary){
            location = rowData.value(forKey: "bookingLocation") as! NSDictionary
        }
        
        if(location.object(forKey: "locationName") != nil){
            locationName = location.value(forKey: "locationName") as! String
        }
        
        if(rowData.value(forKey: "jobTitle") is String){
            let jobT = rowData.value(forKey: "jobTitle") as!  String
            jobs.jobsTitles.text = "Title : \(jobT)"
        }
        
        if(rowData.value(forKey: "jobDescription") is String){
            let jobT = rowData.value(forKey: "jobDescription") as! String
            jobs.caseStatus.text = "Description : \(jobT)"
        }
        
        jobs.locationNameTF.text = locationName
        
        jobs.profileImage.setRounded()
        jobs.notiView.setRounded()
        jobs.statusBGView.setRoundWithR(r: commonValue.subTadius)
        //        jobs.statusBGView.setRoundWithShadow(r: commonValue.subTadius)
        
        switch cellStatus {
        case 0:
            jobs.statusBGView.backgroundColor = UIColor.red
        case 8:
            jobs.statusBGView.backgroundColor = UIColor.gray
        default:
            jobs.statusBGView.backgroundColor = commonColor.mainBlueColor
        }
        
        if(pid == uid){
            jobs.myJobs.isHidden = false
            jobs.myJobs.setRoundWithR(r: commonValue.subTadius)
            
            
        }else{
            jobs.myJobs.isHidden = true
            jobs.myJobs.setRoundWithR(r: commonValue.subTadius)
        }
        
         jobs.bgView.setRoundWithR(r: commonValue.viewRadius)
        
        return jobs
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        self.jobForSegue = jobArr[indexPath.row]
        
        if(self.jobForSegue.value(forKey: "bookingProcessStatus") is String){
            let status = self.jobForSegue.value(forKey: "bookingProcessStatus") as! String
            bookingStatus = Int(status)!
            self.performSegue(withIdentifier: "jobDetail", sender: self)
        }
        else if (self.jobForSegue.value(forKey: "bookingProcessStatus") is Int){
            bookingStatus = self.jobForSegue.value(forKey: "bookingProcessStatus") as! Int
            self.performSegue(withIdentifier: "jobDetail", sender: self)
        }else{
//            SVProgressHUD.showError(withStatus: "data incorrect")
        }
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if(segue.identifier == "jobDetail"){
            let vc = segue.destination as? jobDetailViewController
            
            let profile = jobForSegue.value(forKey: "profile") as! NSDictionary
            let firstName = profile.value(forKey: "firstName") as! String
            let Lastname = profile.value(forKey: "lastName") as! String
            
            vc?.employerNameString = ""
            vc?.employerImageURLString = ""
            
            vc?.workerNameString = "\(firstName) \(Lastname)"
            vc?.bookingStatus = selectIndexRow
            vc?.bookingStatus = bookingStatus
            vc?.jobData = jobForSegue
        }
    }
}
