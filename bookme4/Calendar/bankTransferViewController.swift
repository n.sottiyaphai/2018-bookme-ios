//
//  bankTransferViewController.swift
//  bookme
//
//  Created by Naphat Sottiyaphai on 6/7/2561 BE.
//  Copyright © 2561 Naphat Sottiyaphai. All rights reserved.
//

import UIKit
import BEMCheckBox
import Alamofire
import SVProgressHUD
import SwiftHash

class bankTransferViewController: UIViewController {
    
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var mainAction: UIButton!
    @IBOutlet weak var uploadSlipButton: UIButton!
    
    @IBOutlet weak var bankName: UILabel!
    @IBOutlet weak var nackAcc: UILabel!
    @IBOutlet weak var bankAccName: UILabel!
    @IBOutlet weak var price: UILabel!
    
    var bank = [NSDictionary]()
    var lastSelect = Int()
    var jobData = NSDictionary()
    var lastPrice = String()
    var imageURL = String()
    var promoCode = String()
    
    private var selectBank = String()
    private var selectBankAccount = String()
    private var selectBankAccountNo = String()
    private var bluweoBankId = String()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        DispatchQueue.main.async {
            self.getBankAcc()
        }
        
        collectionView.delegate = self
        
        uploadSlipButton.layer.borderColor = UIColor.darkText.cgColor
        uploadSlipButton.layer.borderWidth = 0.5
        uploadSlipButton.setRoundedwithR(r: commonValue.viewRadius)
        
        mainAction.setRoundedwithR(r: commonValue.viewRadius)
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
            self.navigationController?.navigationBar.isHidden = false
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
    }
    

    
    
    func getBankAcc() {
        
        let ud = UserDefaults.standard
        var accesstoken = String()
        
        if(ud.object(forKey: "accessToken") != nil){
            accesstoken = ud.value(forKey: "accessToken") as! String
        }
        
        let header : HTTPHeaders = [
            "accessToken" : accesstoken
        ]
        
        Alamofire.request(URL(string: "\(baseURL.url)banks")! , headers : header).responseJSON { (response) in
            
            let result = response.result.value as! NSDictionary
//            print(result)
//
            let data = result.value(forKey: "data") as! NSArray
            self.bank = data as! [NSDictionary]
            
            
            self.bankName.text = (self.bank[0].value(forKey: "bankName") as! String)
            self.bankAccName.text = "Account Name : \(self.bank[0].value(forKey: "bankAccountName") as! String)"
            self.nackAcc.text = "Account No : \(self.bank[0].value(forKey: "bankAccountNumber") as! String)"
            self.price.text = "Amount : \(self.lastPrice)"
            
            
            self.selectBank = self.bank[0].value(forKey: "bankName") as! String
            self.selectBankAccount = self.bank[0].value(forKey: "bankAccountName") as! String
            self.selectBankAccountNo = self.bank[0].value(forKey: "bankAccountNumber") as! String
            self.bluweoBankId = self.bank[0].value(forKey: "_id") as! String
            
                
            DispatchQueue.main.async {
                self.collectionView.reloadData()
            }
        }
    }
    
    @IBAction func uploadSlipAction(_ sender: Any) {
        
//        var config = YPImagePickerConfiguration()
//        config.library.onlySquare = false
//        config.onlySquareImagesFromCamera = true
//        config.targetImageSize = .original
//        config.usesFrontCamera = true
//        config.showsPhotoFilters = true
//        config.shouldSaveNewPicturesToAlbum = true
//        config.albumName = "BookMe"
//        config.screens = [.library, .photo]
//        config.startOnScreen = .library
//        config.wordings.libraryTitle = "Gallery"
//        config.hidesStatusBar = false
//        
//        // Build a picker with your configuration
//        let picker = YPImagePicker(configuration: config)
//        
//        picker.didFinishPicking(completion: { [unowned picker] items ,  cancelled  in
//            
//            if let photo = items.singlePhoto {
//                let image = photo.image.jpegData(compressionQuality: commonValue.imageCompress)
//                self.ImageUploading(data: image!)
//            }
//            
//            if cancelled {
//                print("Picker was canceled")
//            }
//            
//            picker.dismiss(animated: true, completion: nil)
//            
//        })
//        
//        self.present(picker, animated: true, completion: nil)
//        
        
        
    }
    
    func ImageUploading(data : Data) {
        
        
        let ud = UserDefaults.standard
        let uid = ud.value(forKey: "user_uid") as! String
        let md5_uid = MD5(uid)
        let url = URL(string: "\(baseURL.media)api/uploads")
        
        
        Alamofire.upload(multipartFormData: { (multipartFormData) in
            
            multipartFormData.append("\(uid)".data(using: String.Encoding.utf8)!, withName: "uId" as String)
            multipartFormData.append(data, withName: "imgFiles", fileName: "\(md5_uid)_swift_file.jpeg", mimeType: "image/jpeg")
            
        },to: url! )
        { (result) in
            switch result {
            case .success(let upload, _, _):
                
                upload.uploadProgress(closure: { (progress) in
                    SVProgressHUD.showProgress(Float(progress.fractionCompleted), status: "Image Uploading")
                })
                
                upload.responseJSON { response in
                    
                    if let result = response.result.value {
                        
                        let results = result as! NSDictionary
                        let ArryResult = results.value(forKey: "data") as! Array<String>
                        
                        self.imageURL = "\(baseURL.photoURL)\(md5_uid.lowercased())/storages/\(ArryResult[0])"
                        print("\(baseURL.photoURL)\(md5_uid.lowercased())/storages/\(ArryResult[0])")
                        
                        self.uploadSlipButton.setTitle("Success! Image was uploaded", for: .normal)
                        self.uploadSlipButton.setImage(UIImage(named: "icon_selected"), for: .normal)
                        
                        SVProgressHUD.dismiss()
                        
                    }
                }
                
            case .failure(let encodingError):
                print("ERROR : \(encodingError)")
                SVProgressHUD.showError(withStatus: "Media Connection Error : \(encodingError)")
                break
            }
        }
    }
    
    @IBAction func getPay(_ sender: Any) {
        
        let alertAction = UIAlertController.init(title: "Are you sure ?", message: "Do you want to payment fot this Job", preferredStyle: .alert)
        
        let ok = UIAlertAction.init(title: "OK", style: .default) { (action) in
            print("Capture action OK")
            
            self.sendTransection()
            
//            let pid = self.jobData.value(forKey: "_id") as! String
//            let url = "\(baseURL.url)booking/\(pid)"
//            let params : [String : Any]  = [
//                "bookingProcessStatus" : "3"
//            ]
//
//            let ud = UserDefaults.standard
//            var accesstoken = String()
//
//            if(ud.object(forKey: "accessToken") != nil){
//                accesstoken = ud.value(forKey: "accessToken") as! String
//            }
//
//            let header : HTTPHeaders = [
//                "accessToken" : accesstoken
//            ]
//
//            print(url)
//
//            Alamofire.request(url, method: .put, parameters: params, encoding: JSONEncoding.default , headers : header).responseJSON(completionHandler: {(respond) in
//
//                let result = respond.result.value
//
//                let vc = thankYouBookingViewController.instantiateFromStoryboard()
//
//                vc.comfirmation = true
//                vc.makeBooking = true
//                vc.makePayment = true
//                vc.stateIndex = 4
//
//                self.navigationController?.pushViewController(vc, animated: true)
//
//            })
            
            
        }
        
        let cancel = UIAlertAction.init(title: "Cancel", style: .cancel) { (action) in
            print("Capture action Cancel")
            self.dismiss(animated: true, completion: nil)
        }
        
        alertAction.addAction(ok)
        alertAction.addAction(cancel)
       
        self.present(alertAction, animated: true, completion: nil)
        
    }
    
    func sendTransection() {
        let cId = jobData.value(forKey: "cId") as! String
        let pId = jobData.value(forKey: "professId") as! String
        let bookingId = jobData.value(forKey: "_id") as! String
        
        let ud = UserDefaults.standard
        var accesstoken = String()
        
        if(ud.object(forKey: "accessToken") != nil){
            accesstoken = ud.value(forKey: "accessToken") as! String
        }
        
        let header : HTTPHeaders = [
            "accessToken" : accesstoken
        ]
        
        
        let params : [String : Any] = [
            "cId": cId,
            "pId": pId,
            "bookingId": bookingId,
            "paymentStatus": 1,
            "bluweoBank" : [
                "bankId" : self.bluweoBankId
            ],
            "sourceBank": [
                "AccountBank": "",
                "AccountNo": "",
                "AccountName": ""
            ],
            "destinationBank": [
                "AccountBank": "",
                "AccountNo": "",
                "AccountName": ""
            ],
            "transactionType": 0,
            "amount": lastPrice ,
            "slipUpload": self.imageURL,
            "promocode" : self.promoCode,
            "transferDateTime" : Date().toISO()
        ]
        
//        print(params)
        
        Alamofire.request(URL(string: "\(baseURL.url)transaction")!, method: .post, parameters: params, encoding: JSONEncoding.default , headers : header).responseJSON(completionHandler: {(res) in
            
            let result = res.result.value
            let JSON = result as! NSDictionary
            
//            print(JSON)
            let response = JSON.value(forKey: "response")as! Bool
            if(response){
                if (JSON.object(forKey: "data") != nil){
                    SVProgressHUD.dismiss()
                    let vc = thankYouBookingViewController.instantiateFromStoryboard()
                    
                    vc.comfirmation = true
                    vc.makeBooking = true
                    vc.makePayment = true
                    
                    self.navigationController?.pushViewController(vc, animated: true)
                }
            }
            else{
                let errorMessage = JSON.value(forKey: "err") as! NSDictionary
                let message = errorMessage.value(forKey: "message") as! String
                SVProgressHUD.showError(withStatus: "\(message)")
                
                DispatchQueue.main.asyncAfter(deadline: .now() + 1){
                    SVProgressHUD.dismiss()
                }
            }
        })
    }
}

extension bankTransferViewController : UICollectionViewDelegate , UICollectionViewDataSource , UICollectionViewDelegateFlowLayout,BEMCheckBoxDelegate{
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.bank.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let item = collectionView.dequeueReusableCell(withReuseIdentifier: "bank", for: indexPath) as! bankCollItem
        
        let bdata = bank[indexPath.row]
        let bankImage = bdata.value(forKey: "bankImage") as! String
        
        item.imageViews.setRoundWithR(r: commonValue.viewRadius)
        item.imageViews.sd_setImage(with: URL(string: bankImage)!)
        
        return item
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        let row = indexPath.row
        
        self.bankName.text = (self.bank[row].value(forKey: "bankName") as! String)
        self.bankAccName.text = "Account Name : \(self.bank[row].value(forKey: "bankAccountName") as! String)"
        self.nackAcc.text = "Account No : \(self.bank[row].value(forKey: "bankAccountNumber") as! String)"
        self.price.text = "Amount : \(lastPrice)"
        
        
        self.selectBank = self.bank[row].value(forKey: "bankName") as! String
        self.selectBankAccount = self.bank[row].value(forKey: "bankAccountName") as! String
        self.selectBankAccountNo = self.bank[row].value(forKey: "bankAccountNumber") as! String
        self.bluweoBankId = self.bank[row].value(forKey: "_id") as! String
        
    }
}

class bankCollItem: UICollectionViewCell {
    @IBOutlet var imageViews : UIImageView!
    @IBOutlet weak var selected_image: UIImageView!
}
