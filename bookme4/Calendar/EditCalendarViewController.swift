//
//  EditCalendarViewController.swift
//  bookme
//
//  Created by Naphat Sottiyaphai on 8/11/2561 BE.
//  Copyright © 2561 Naphat Sottiyaphai. All rights reserved.
//

import UIKit
import Koyomi
import SwiftDate
import Alamofire
import SVProgressHUD

class EditCalendarViewController: UIViewController {

    @IBOutlet weak var busyMark: UIView!
    @IBOutlet weak var mainSelect: UIButton!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet fileprivate weak var koyomi: Koyomi!{
        didSet {
            koyomi.circularViewDiameter = 0.7
            koyomi.calendarDelegate = self
            koyomi.inset = .zero
            koyomi.weeks = ("SU", "MO", "TU", "WE", "TH", "FR", "SA")
            koyomi.style = .standard
            koyomi.dayPosition = .center
            koyomi.selectionMode = .multiple(style: .circle)
            koyomi.selectedStyleColor = UIColor(red: 198.0/255.0, green: 2.0/255.0, blue: 4.0/255.0, alpha: 1.0)
            koyomi
                .setDayFont(size: 14)
                .setWeekFont(size: 14)
        }
    }
    
    var calendarData = [String]()
    var jobDate = [DateInRegion]()
    var BusyDate = [String]()
    var RemoveBusyDate = [String]()
    var EmployDate = [String]()
    var currentMounth = String()
    var bookingRow = [NSDictionary]()
    var jobArr = [NSDictionary]()
    var tableArr = [NSDictionary]()
    var bookingId = [String]()
    var jobArrFilter = [NSDictionary]()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.koyomi.calendarDelegate = self
        dateLabel.text = koyomi.currentDateString(withFormat: "MMM yyyy")
        currentMounth = koyomi.currentDateString(withFormat: "MM")
        
        busyMark.setRounded()
        mainSelect.setTitle("Update", for: .normal)
        mainSelect.setRoundedwithR(r: commonValue.viewRadius)
        
        DispatchQueue.main.async {
            self.fetchCalendarProfile()
        }
        
    }
    
    @IBAction func updateDate(_ sender: Any) {
        self.updateCalendar()
    }
    
    func getLanguageISO() -> String {
        let locale = Locale.current
        guard let languageCode = locale.languageCode,
            let regionCode = locale.regionCode else {
                return "de_DE"
        }
        return languageCode + "_" + regionCode
    }
    
    func setBusyBG() {
        
        for date in self.BusyDate{
            let dates = date.toDate()?.date
            self.koyomi.select(date: dates!)
        }
        
        let device_region = getLanguageISO()
        var jobs = [DateInRegion]()
        var jobsMaping = [Date]()
        
        if(device_region == "en_TH"){
            jobs = self.jobDate.map{$0.convertTo(calendar: Calendars.buddhist, timezone: Zones.asiaBangkok, locale: Locales.thai) + 543.years}
            jobsMaping = jobs.map{$0.date}
        }
        else{
            jobs = self.jobDate.map{$0.convertTo(calendar: Calendars.buddhist, timezone: Zones.asiaBangkok, locale: Locales.thai)}
            jobsMaping = jobs.map{$0.date}
        }
        
//        self.koyomi.select(dates: jobsMaping)
        
        DispatchQueue.main.async {
            self.koyomi.reloadData()
        }
    }
    
    func fetchCalendarProfile(){
        
        jobDate.removeAll()
        BusyDate.removeAll()
        EmployDate.removeAll()
        bookingRow.removeAll()
        jobArrFilter.removeAll()
        bookingId.removeAll()
        
        let userDefault = UserDefaults.standard
        let uid = userDefault.value(forKey: "user_uid") as! String
        
        let ud = UserDefaults.standard
        var accesstoken = String()
        
        if(ud.object(forKey: "accessToken") != nil){
            accesstoken = ud.value(forKey: "accessToken") as! String
        }
        
        let ReqURL = "\(baseURL.url)calendar/byprofile/\(uid)"
        let header : HTTPHeaders = [
            "month" : currentMounth,
            "accessToken" : accesstoken
        ]
        
        Alamofire.request(URL(string: ReqURL)!, method: .get, headers : header).responseJSON(completionHandler: { (Result) in
            
            let result = Result.result.value
            
            if(result is NSDictionary){
                let JSON = result as! NSDictionary
                let apiResponse = JSON.value(forKey: "response") as! Bool
                
                if(apiResponse){
                    let data = JSON.value(forKey: "data") as! NSArray
                    for Once in data{
                        let onceRow = Once as! NSDictionary
                        let calendar = onceRow.value(forKey: "calendar") as! NSDictionary
                        let busyDate = calendar.value(forKey: "busyDate") as! NSArray
                        
                        if(calendar.value(forKey: "employDate") is String){
                            let employDate = calendar.value(forKey: "employDate") as! String
                            self.EmployDate.append(employDate)
                        }
                        
                        if(calendar.value(forKey: "jobDate") is NSArray){
                            let jobDate = calendar.value(forKey: "jobDate") as! NSArray
                            
                            for jobs in jobDate {
                                let date = jobs as! String
                                
                                let thaiRegions = Region(calendar: Calendars.gregorian, zone: Zones.americaNewYork, locale: Locales.english)
                                let dateArr = date.toDate()
                                let toRegionsDate = dateArr?.convertTo(region: thaiRegions)
                                self.jobDate.append(toRegionsDate!)
                            }
                        }
                        
                        let bookinID = onceRow.value(forKey: "bookingId") as! String
                        
                        if(bookinID == ""){
                            for busy in busyDate {
                                let date = busy as! String
                                let toISODate = date.toDate()?.toISO()
                                self.BusyDate.append(toISODate!)
                            }
                        }
                        else{
                            
                            let dataFilter = self.jobArr.filter{($0.value(forKey: "_id") as! String) == bookinID}
                            
                            if(!dataFilter.isEmpty){
                                self.jobArrFilter.append((dataFilter[0]))
                            }
                            self.bookingRow.append(onceRow)
                        }
                    }
                    self.setBusyBG()
                }
                
                else{
                    let apiError = JSON.value(forKey: "err") as! NSDictionary
                    let errMessage = apiError.value(forKey: "message") as! String
                    print("Error : \(errMessage)")
                    
                    SVProgressHUD.dismiss()
                }
            }
            else{
//                DispatchQueue.main.asyncAfter(deadline: .now() + 2){
                    SVProgressHUD.dismiss()
//                }
            }
        })
    }

    func updateCalendar()  {
        
        let userDefault = UserDefaults.standard
        let uid = userDefault.value(forKey: "user_uid") as! String
        
        let ud = UserDefaults.standard
        var accesstoken = String()
        
        if(ud.object(forKey: "accessToken") != nil){
            accesstoken = ud.value(forKey: "accessToken") as! String
        }
        
        let ReqURL = "\(baseURL.url)calendar"
        let header : HTTPHeaders = [
            "accessToken" : accesstoken
        ]
        
        let params : [String:Any] = [
            "uId": uid,
            "busyDate": self.BusyDate
        ]
        
        Alamofire.request(URL(string: ReqURL)!, method: .post, parameters: params, encoding: JSONEncoding.default, headers: header).responseJSON { (response) in
        
            print(response)
            let result = response.result.value as! NSDictionary
            print(result)
            
            SVProgressHUD.showSuccess(withStatus: "Update done")
            DispatchQueue.main.asyncAfter(deadline: .now() + 2){
                SVProgressHUD.dismiss()
                self.navigationController?.popViewController(animated: true)
            }
        }
    }
    
    func removeCalendarBusy() {
        
        let userDefault = UserDefaults.standard
        let uid = userDefault.value(forKey: "user_uid") as! String
        
        let ud = UserDefaults.standard
        var accesstoken = String()
        
        if(ud.object(forKey: "accessToken") != nil){
            accesstoken = ud.value(forKey: "accessToken") as! String
        }
        
        let ReqURL = "\(baseURL.url)calendar"
        let header : HTTPHeaders = [
            "accessToken" : accesstoken
        ]
        
        let params : [String:Any] = [
            "uId": uid,
            "busyDate": self.RemoveBusyDate
        ]
        
        Alamofire.request(URL(string: ReqURL)!, method: .put, parameters: params, encoding: JSONEncoding.default, headers: header).responseJSON { (response) in
            
            print(response)
            let result = response.result.value as! NSDictionary
            print(result)
            
            SVProgressHUD.showSuccess(withStatus: "Update done")
            DispatchQueue.main.asyncAfter(deadline: .now() + 2){
                SVProgressHUD.dismiss()
                self.navigationController?.popViewController(animated: true)
            }
        }
    }
    
    
    @IBAction func selectOption(_ sender: Any) {
        
        let action = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        
        let busy = UIAlertAction.init(title: "Set Busy", style: .default) { (alert) in
            
        }
        let cancel = UIAlertAction(title: "Cancel", style: .cancel) { (action) in
            
        }
        
        action.addAction(busy)
        action.addAction(cancel)
        
        self.present(action, animated: true, completion: nil)
    }
    
    @IBAction func prev(_ sender: Any) {
        let month: MonthType = {
            .previous
        }()
        koyomi.currentDateFormat = "MMM yyyy"
        koyomi.display(in: month)
        
        let busy : [Date] =  self.BusyDate.map{($0.toDate()?.date)!}
        DispatchQueue.main.async {
            self.fetchCalendarProfile()
            self.koyomi.select(dates: busy)
        }
    }
    
    @IBAction func next(_ sender: Any) {
        let month: MonthType = {
            .next
        }()
        koyomi.currentDateFormat = "MMM yyyy"
        koyomi.display(in: month)
        
        let busy : [Date] =  self.BusyDate.map{($0.toDate()?.date)!}
        DispatchQueue.main.async {
            self.fetchCalendarProfile()
            self.koyomi.select(dates: busy)
        }
    }
}

extension EditCalendarViewController : KoyomiDelegate {
    
    func koyomi(_ koyomi: Koyomi, currentDateString dateString: String) {
        
        koyomi.currentDateFormat = "MMMM yyyy"
        dateLabel.text = dateString
        currentMounth = koyomi.currentDateString(withFormat: "MM")
        
    }
    
    func koyomi(_ koyomi: Koyomi, didSelect date: Date?, forItemAt indexPath: IndexPath) {
        
        let selectDate = (date!).toISO()                                                // วันที่ที่เลือก
        let busyFiltered = self.BusyDate.filter{$0 == selectDate}                       // Filter ของ Busy กับวันที่เลือก
        
        print(busyFiltered)
        
        if(busyFiltered.count > 0){                                                     // มีวันที่ที่เลือกอยู่ใน Busy
            self.BusyDate = self.BusyDate.filter{$0 != date?.toISO()}                   // วันที่ซ้ำ ทำการเอาวันที่เลือกออกจาก Array
            self.RemoveBusyDate.append((date?.toISO())!)                                // เพิ่มวันที่เข้าไปใน Remove Array
        }
        else{
            self.BusyDate.append((date?.toISO())!)                                      // เพิ่มวันที่ไม่ว่างลงใน Array
            self.RemoveBusyDate = self.RemoveBusyDate.filter{$0 != date?.toISO()}       // ลบวันที่ออกจาก Remove Array
        }
        
        print(BusyDate)
        print(RemoveBusyDate)
    }
}
