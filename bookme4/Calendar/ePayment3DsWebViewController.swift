//
//  3DsWebViewController.swift
//  bookme
//
//  Created by Naphat Sottiyaphai on 10/5/2562 BE.
//  Copyright © 2562 Naphat Sottiyaphai. All rights reserved.
//

import UIKit
import SVProgressHUD
import Alamofire

class ePayment3DsWebViewController: UIViewController {

    var webView: UIWebView!
//    var pgwWebViewDelegate:PGWUIWebViewDelegate!
    var redirectUrl:String?
    var jobData = NSDictionary()
    var lastPrice = String()
    var promoCode = String()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //Authentication handling for 3DS payment
        let requestUrl:URL = URL.init(string: self.redirectUrl!)!
        let request:URLRequest = URLRequest.init(url: requestUrl)
        
        self.webView = UIWebView(frame: UIScreen.main.bounds)
//        self.webView.delegate = self.transactionResultCallback()
        self.webView.loadRequest(request)
        
        self.view.addSubview(self.webView)
    }
    
//    func transactionResultCallback() -> PGWUIWebViewDelegate {
//        
//        self.pgwWebViewDelegate = PGWUIWebViewDelegate(
//            success: { (response: TransactionResultResponse) in
//                
//                if response.responseCode == APIResponseCode.TRANSACTION_COMPLETED {
//                    
//                    //Inquiry payment result by using transaction id.
//                    let transactionID:String = response.transactionID!
//                    print(transactionID)
//                    
//                    SVProgressHUD.showSuccess(withStatus: "Payment Success")
//                    self.sendTransection(transactionID: transactionID)
//                    
//                    DispatchQueue.main.asyncAfter(deadline: .now() + 2){
//                        SVProgressHUD.dismiss()
//                        self.navigationController?.popToRootViewController(animated: true)
//                    }
//                    
//                    
//                } else {
//                    //Get error response and display error
//                    SVProgressHUD.showError(withStatus: "Payment NOT Success")
//                    DispatchQueue.main.asyncAfter(deadline: .now() + 2){
//                        SVProgressHUD.dismiss()
//                        self.navigationController?.popToRootViewController(animated: true)
//                    }
//                }
//        }, failure: { (error: NSError) in
//            //Get error response and display error
//            SVProgressHUD.showError(withStatus: error.description)
//            DispatchQueue.main.asyncAfter(deadline: .now() + 2){
//                SVProgressHUD.dismiss()
//                self.navigationController?.popToRootViewController(animated: true)
//            }
//        })
//        
//        return self.pgwWebViewDelegate
//    }
//    
    
    func sendTransection(transactionID : String) {
        
        let cId = self.jobData.value(forKey: "cId") as! String
        let pId = self.jobData.value(forKey: "professId") as! String
        let bookingId = self.jobData.value(forKey: "_id") as! String
        
        let ud = UserDefaults.standard
        var accesstoken = String()
        
        if(ud.object(forKey: "accessToken") != nil){
            accesstoken = ud.value(forKey: "accessToken") as! String
        }
        
        let header : HTTPHeaders = [
            "accessToken" : accesstoken
        ]
        
        
        let params : [String : Any] = [
            "cId": cId,
            "professId": pId,
            "bookingId": bookingId,
            "paymentStatus": 1,
            "sourceBank": [
                "AccountBank": "",
                "AccountNo": "",
                "AccountName": ""
            ],
            "destinationBank": [
                "AccountBank": "",
                "AccountNo": "",
                "AccountName": ""
            ],
            "transactionType": 1,
            "payment2C2P" : [
                "transactionId" : transactionID
            ],
            "amount": lastPrice ,
            "promocode" : self.promoCode,
            "transferDateTime" : Date().toISO()
        ]
        
        Alamofire.request(URL(string: "\(baseURL.url)transaction")!, method: .post, parameters: params, encoding: JSONEncoding.default , headers : header).responseJSON(completionHandler: {(res) in
            
            if let result = res.result.value{
                let JSON = result as! NSDictionary
                
                let response = JSON.value(forKey: "response")as! Bool
                if(response){
                    if (JSON.object(forKey: "data") != nil){
                        SVProgressHUD.dismiss()
                        let vc = thankYouBookingViewController.instantiateFromStoryboard()
                        
                        vc.comfirmation = true
                        vc.makeBooking = true
                        vc.makePayment = true
                        
                        self.navigationController?.pushViewController(vc, animated: true)
                    }
                }
                else{
                    print("Payment Fail")
                    print(response)
                }
            }
            else{
                //Get error response and display error
                SVProgressHUD.showError(withStatus: res.error?.localizedDescription)
                
                DispatchQueue.main.asyncAfter(deadline: .now() + 2){
                    SVProgressHUD.dismiss()
                    self.navigationController?.popToRootViewController(animated: true)
                }
            }
           
            
            
        })
    }

}
