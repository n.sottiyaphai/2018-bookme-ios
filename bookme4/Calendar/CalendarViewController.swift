//
//  CalendarViewController.swift
//  bookme
//
//  Created by Naphat Sottiyaphai on 22/6/2561 BE.
//  Copyright © 2561 Naphat Sottiyaphai. All rights reserved.
//

import UIKit
import Parchment

class CalendarViewController: UIViewController {
    
    
    @IBOutlet weak var mainView: UIView!
    
//    var options: PagingMenuControllerCustomizable!
    
    class func instantiateFromStoryboard() -> CalendarViewController {
        let storyboard = UIStoryboard(name: "calendar", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: String(describing: self)) as! CalendarViewController
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        mainView.backgroundColor = commonColor.bgColor
        
        let storyboard = UIStoryboard(name: "calendar", bundle: nil)
        let firstViewController = storyboard.instantiateViewController(withIdentifier: "worksViewController")
        let secondViewController = storyboard.instantiateViewController(withIdentifier: "myWorkViewController")
        let calendarViewController = storyboard.instantiateViewController(withIdentifier: "calendatTableViewController")
        
        
        
        let pagingViewController = FixedPagingViewController(viewControllers: [
          firstViewController,
          secondViewController,
          calendarViewController
        ])
        
        // Customize
        pagingViewController.backgroundColor = commonColor.bgColor
        pagingViewController.selectedBackgroundColor = commonColor.bgColor
        pagingViewController.textColor = UIColor.lightGray
        pagingViewController.selectedTextColor = UIColor.black
        pagingViewController.indicatorColor = commonColor.mainBlueColor
        
        // Make sure you add the PagingViewController as a child view
        // controller and contrain it to the edges of the view.
        addChild(pagingViewController)
        mainView.addSubview(pagingViewController.view)
        mainView.constrainToEdges(pagingViewController.view)
        pagingViewController.didMove(toParent: self)

        
        // Do any additional setup after loading the view.
        self.title = "Schedule"
        
        let btn2 = UIButton(type: .custom)
        btn2.setImage(UIImage(named: "icon_search"), for: .normal)
        btn2.frame = CGRect(x: 0, y: 0, width: 40, height: 18)
        btn2.addTarget(self, action: #selector(self.go2search), for: .touchUpInside)
        let item2 = UIBarButtonItem(customView: btn2)
        self.navigationItem.setRightBarButtonItems([item2], animated: true)
        
        self.setBackgroundColor()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)

        navigationController?.navigationBar.prefersLargeTitles = true
        navigationItem.largeTitleDisplayMode = .always
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        
        
        navigationController?.navigationBar.prefersLargeTitles = false
        
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        URLCache.shared.removeAllCachedResponses()
    }
    
    @objc func go2search() {
        let vc = searchViewController.instantiateFromStoryboard()
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
}
