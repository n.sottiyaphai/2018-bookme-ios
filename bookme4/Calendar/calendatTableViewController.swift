//
//  calendatTableViewController.swift
//  bookme
//
//  Created by Naphat Sottiyaphai on 22/6/2561 BE.
//  Copyright © 2561 Naphat Sottiyaphai. All rights reserved.
//

import UIKit
import Koyomi
import Alamofire
import SwiftDate
import SVProgressHUD

class calendatTableViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var editClendarButton: UIButton!
    @IBOutlet weak var bgViewHeader: UIView!
    
    @IBOutlet fileprivate weak var koyomi: Koyomi!{
        didSet {
            koyomi.circularViewDiameter = 0.7
            koyomi.calendarDelegate = self
            koyomi.inset = .zero
            koyomi.weeks = ("SU", "MO", "TU", "WE", "TH", "FR", "SA")
            koyomi.style = .standard
            koyomi.dayPosition = .center
            koyomi.selectionMode = .multiple(style: .circle)
            koyomi.setDayFont(fontName: "SukhumvitSet-Bold", size: 14)
            koyomi.setWeekFont(fontName: "SukhumvitSet-Bold", size: 14)
            koyomi.selectedStyleColor = UIColor(red: 254.0/255.0, green: 210.0/255.0, blue: 0.0/255.0, alpha: 1.0)
            koyomi
                .setDayFont(size: 14)
                .setWeekFont(size: 14)
        }
    }
    
    var calendarData = [String]()
    var jobDate = [DateInRegion]()
    var BusyDate = [DateInRegion]()
    var EmployDate = [String]()
    var currentMounth = String()
    var bookingRow = [NSDictionary]()
    var jobArr = [NSDictionary]()
    var tableArr = [NSDictionary]()
    var bookingId = [String]()
    var jobArrFilter = [NSDictionary]()
    
    
    let status = ["Cancelled" , "New Booking" , "Confirmation" , "Make Payment" , "Confirm Payment" , "Work Complete" , "Approve Payment" , "Transfer Payment" , "Done"]
    
    
    class func instantiateFromStoryboard() -> calendatTableViewController {
        let storyboard = UIStoryboard(name: "calendar", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: String(describing: self)) as! calendatTableViewController
    }
    
    @IBAction func editCalendar(_ sender: Any) {
        self.performSegue(withIdentifier: "editCalendar", sender: self)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.editClendarButton.setRoundedwithR(r: commonValue.viewRadius)
        self.koyomi.calendarDelegate = self
        
        dateLabel.text = koyomi.currentDateString(withFormat: "MMM YYYY")
        currentMounth = koyomi.currentDateString(withFormat: "MM")
        
        self.tableView.rowHeight = UITableView.automaticDimension;
        self.setBackgroundColor()
        
        self.bgViewHeader.setRoundWithR(r: commonValue.viewRadius)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 1){
            self.getBooking()
        }
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func getLanguageISO() -> String {
        let locale = Locale.current
        guard let languageCode = locale.languageCode,
            let regionCode = locale.regionCode else {
                return "de_DE"
        }
        return languageCode + "_" + regionCode
    }
    
    func setBusyBG() {
        
        for date in self.BusyDate{
            let dates = date.date
            self.koyomi.setDayBackgrondColor(UIColor.groupTableViewBackground, of: dates)
        }
        
        let device_region = getLanguageISO()
        var jobs = [DateInRegion]()
        var jobsMaping = [Date]()
        
        if(device_region == "en_TH"){
            jobs = self.jobDate.map{$0.convertTo(calendar: Calendars.buddhist, timezone: Zones.asiaBangkok, locale: Locales.thai) + 543.years}
            jobsMaping = jobs.map{$0.date}
        }
        else{
            jobs = self.jobDate.map{$0.convertTo(calendar: Calendars.buddhist, timezone: Zones.asiaBangkok, locale: Locales.thai)}
            jobsMaping = jobs.map{$0.date}
        }
        
        self.koyomi.select(dates: jobsMaping)
        
        DispatchQueue.main.async {
            self.koyomi.reloadData()
        }
    }
    
    func getBooking() {
        self.jobArr.removeAll()
        
        let userDefaults = UserDefaults.standard
        let uid = userDefaults.value(forKey: "user_uid") as! String
        
        let ud = UserDefaults.standard
        var accesstoken = String()
        
        if(ud.object(forKey: "accessToken") != nil){
            accesstoken = ud.value(forKey: "accessToken") as! String
        }
        
        
        let URL = "\(baseURL.url)booking/byProfile/\(uid)"
        let header : HTTPHeaders = [
            "type" : "1",
            "accessToken" : accesstoken
        ]
        
        Alamofire.request(URL, method: .get , headers : header).responseJSON(completionHandler: { (respond) in
            
            let respond = respond.result.value
            
            if(respond is NSDictionary){
                let jobs = respond as! NSDictionary
                if(jobs.value(forKey: "response") as! Bool){
                    let job_data = jobs.value(forKey: "data") as! Array<Any>
                    
                    for job in job_data{
                        
                        let bookingData = job as! NSDictionary
                        let bookingID = bookingData.value(forKey: "_id") as! String
                        
                        self.jobArr.append(bookingData)
                        self.bookingId.append(bookingID)
                    }
                    print(self.bookingId)
                    print("Booking load complete")
                    self.fetchCalendarProfile()
                }
            }
            else{
                SVProgressHUD.showError(withStatus: "Network Error Please try again later.")
                self.fetchCalendarProfile()
            }
        })
    }
    
    
    func fetchCalendarProfile(){
        
        jobDate.removeAll()
        BusyDate.removeAll()
        EmployDate.removeAll()
        bookingRow.removeAll()
        jobArrFilter.removeAll()
        bookingId.removeAll()
        
        let userDefault = UserDefaults.standard
        let uid = userDefault.value(forKey: "user_uid") as! String
        
        let ud = UserDefaults.standard
        var accesstoken = String()
        
        if(ud.object(forKey: "accessToken") != nil){
            accesstoken = ud.value(forKey: "accessToken") as! String
        }
        
        let ReqURL = "\(baseURL.url)calendar/byprofile/\(uid)"
        let header : HTTPHeaders = [
            "month" : currentMounth,
            "accessToken" : accesstoken
        ]
        
        Alamofire.request(URL(string: ReqURL)!, method: .get, headers : header).responseJSON(completionHandler: { (Result) in
            
            let result = Result.result.value
            
            if(result is NSDictionary){
                let JSON = result as! NSDictionary
                let apiResponse = JSON.value(forKey: "response") as! Bool
                
                if(apiResponse){
                    let data = JSON.value(forKey: "data") as! NSArray
                    for Once in data{
                        let onceRow = Once as! NSDictionary
                        let calendar = onceRow.value(forKey: "calendar") as! NSDictionary
                        let busyDate = calendar.value(forKey: "busyDate") as! NSArray
                        
                        for busy in busyDate {
                            let date = busy as! String
                            let thaiRegions = Region(calendar: Calendars.gregorian, zone: Zones.americaNewYork, locale: Locales.english)
                            let dateArr = date.toDate()
                            let toRegionsDate = dateArr?.convertTo(region: thaiRegions)
                            
                            self.BusyDate.append(toRegionsDate!)
                            
                        }
                        
                        if(calendar.value(forKey: "employDate") is String){
                            let employDate = calendar.value(forKey: "employDate") as! String
                            self.EmployDate.append(employDate)
                        }
                        
                        if(calendar.value(forKey: "jobDate") is NSArray){
                            let jobDate = calendar.value(forKey: "jobDate") as! NSArray
                            
                            for jobs in jobDate {
                                let date = jobs as! String
                                
                                let thaiRegions = Region(calendar: Calendars.gregorian, zone: Zones.americaNewYork, locale: Locales.english)
                                let dateArr = date.toDate()
                                let toRegionsDate = dateArr?.convertTo(region: thaiRegions)
                                self.jobDate.append(toRegionsDate!)
                            }
                            
                        }
                        
                        let bookinID = onceRow.value(forKey: "bookingId") as! String
//                        if(bookinID == ""){
//                            for busy in busyDate {
//                                let date = busy as! String
//                                self.BusyDate.append(date)
//                            }
//                        }
//                        else{
//
//                            let dataFilter = self.jobArr.filter{($0.value(forKey: "_id") as! String) == bookinID}
//
//                            if(!dataFilter.isEmpty){
//                                self.jobArrFilter.append((dataFilter[0]))
//                            }
//                            self.bookingRow.append(onceRow)
//                        }
                    }
                    
                    self.setBusyBG()
                    
                    DispatchQueue.main.async {
                        SVProgressHUD.dismiss()
                        self.tableView.reloadData()
                    }
                }
                else{
                    let apiError = JSON.value(forKey: "err") as! NSDictionary
                    let errMessage = apiError.value(forKey: "message") as! String
                    print("Error : \(errMessage)")
                }
            }
        })
    }
    
    
    func splitDate(date : Date) -> String{
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        
        let myString = formatter.string(from: date)
        let yourDate = formatter.date(from: myString)
        formatter.dateFormat = "dd MMM yyyy"
        let myStringafd = formatter.string(from: yourDate!)
        
        print(myStringafd)
        print("select date : \(myStringafd)")
        
        return myStringafd
        
    }
    
    @IBAction func prev(_ sender: Any) {
        let month: MonthType = {
            .previous
        }()
        koyomi.currentDateFormat = "MMM YYYY"
        koyomi.display(in: month)
        
        DispatchQueue.main.async {
            self.fetchCalendarProfile()
        }
    }
    @IBAction func next(_ sender: Any) {
        let month: MonthType = {
            .next
        }()
        koyomi.currentDateFormat = "MMM YYYY"
        koyomi.display(in: month)
        
        DispatchQueue.main.async {
            self.fetchCalendarProfile()
        }
    }
    
    
    
}

extension calendatTableViewController : KoyomiDelegate {
    //    @objc(koyomi:shouldSelectDates:to:withPeriodLength:)
    
    func koyomi(_ koyomi: Koyomi, didSelect date: Date?, forItemAt indexPath: IndexPath) {
        //        print("You Selected: \(String(describing: date))")
    }
    
    func koyomi(_ koyomi: Koyomi, currentDateString dateString: String) {
        koyomi.currentDateFormat = "MMMM YYYY"
        dateLabel.text = dateString
        currentMounth = koyomi.currentDateString(withFormat: "MM")
    }
}

extension calendatTableViewController : UITableViewDelegate , UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.jobArrFilter.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let jobs = tableView.dequeueReusableCell(withIdentifier: "jobs", for: indexPath) as! jobsCell
        
        if(jobArrFilter.count >= indexPath.row){
            let rowData = jobArrFilter[indexPath.row]
            
            let ud = UserDefaults.standard
            let uid = ud.value(forKey: "user_uid") as! String
            
            let profile = rowData.value(forKey: "profile") as! NSDictionary
            var profile_imageURL = String()
            var profile_name = String()
            var locationName = String()
            let pid = rowData.value(forKey: "pId") as! String
            
            if(profile.value(forKey: "firstName") is String && profile.value(forKey: "lastName") is String ){
                let firstName = profile.value(forKey: "firstName") as! String
                let Lastname = profile.value(forKey: "lastName") as! String
                profile_name = "\(firstName) \(Lastname)"
                jobs.profileName.text = profile_name
            }
            
            if(profile.value(forKey: "profilePhoto") is String){
                profile_imageURL = profile.value(forKey: "profilePhoto") as! String
                jobs.profileImage.sd_setImage(with: URL(string: profile_imageURL), placeholderImage: UIImage(named : "bookme_loading"))
            }
            
            var cellStatus = Int()
            if(rowData.value(forKey: "bookingProcessStatus") is Int){
                let statusInt = rowData.value(forKey: "bookingProcessStatus") as! Int
                cellStatus = statusInt
                jobs.statusText.text = status[statusInt]
            }
            else if (rowData.value(forKey: "bookingProcessStatus") is String){
                let statusString = rowData.value(forKey: "bookingProcessStatus") as! String
                let toInt = Int(statusString)
                
                cellStatus = toInt!
                jobs.statusText.text = status[toInt!]
            }
            
            
            
            if(rowData.value(forKey: "bookingDate") is NSArray){
                let statusInt = rowData.value(forKey: "bookingDate") as! NSArray
                let firstDate : String = statusInt[0] as! String
                let toDate  = firstDate.toDate()
                
                jobs.dateTime.text = toDate?.toFormat("dd MMM yyyy")
            }
            else{
                //            let statusString = rowData.value(forKey: "bookingDate") as! String
                //            let toInt = Int(statusString)
            }
            
            var location = NSDictionary()
            //        bookingDate
            if(rowData.value(forKey: "bookingLocation") is Array<Any>){
                let locations = rowData.value(forKey: "bookingLocation") as! Array<Any>
                location = locations[0] as! NSDictionary
            }
            else if (rowData.value(forKey: "bookingLocation") is NSDictionary){
                location = rowData.value(forKey: "bookingLocation") as! NSDictionary
            }
            
            if(location.object(forKey: "locationName") != nil){
                locationName = location.value(forKey: "locationName") as! String
            }
            
            if(rowData.value(forKey: "jobTitle") is String){
                let jobT = rowData.value(forKey: "jobTitle") as!  String
                jobs.jobsTitles.text = "Title : \(jobT)"
            }
            
            if(rowData.value(forKey: "jobDescription") is String){
                let jobT = rowData.value(forKey: "jobDescription") as! String
                jobs.caseStatus.text = "Description : \(jobT)"
            }
            
            
            jobs.locationNameTF.text = locationName
            
            jobs.profileImage.setRounded()
            jobs.notiView.setRounded()
            jobs.bgView.setRoundWithR(r: commonValue.viewRadius)
            jobs.statusBGView.setRoundWithR(r: commonValue.subTadius)
            
            switch cellStatus {
            case 0:
                jobs.statusBGView.backgroundColor = UIColor.red
            case 8:
                jobs.statusBGView.backgroundColor = UIColor.gray
            default:
                jobs.statusBGView.backgroundColor = commonColor.mainBlueColor
            }
            //        jobs.statusBGView.setRoundWithShadow(r: commonValue.subTadius)
            
            if(pid == uid){
                jobs.myJobs.isHidden = false
                jobs.myJobs.setRoundWithR(r: commonValue.subTadius)
                
                
            }else{
                jobs.myJobs.isHidden = true
                jobs.myJobs.setRoundWithR(r: commonValue.subTadius)
            }
            
            return jobs
        }
        else{
            print("Cell loading error")
            return UITableViewCell()
        }
        
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print("select booking")
        
        if(jobArrFilter.count >= indexPath.row){
            let rowData = jobArrFilter[indexPath.row]
            let id = rowData.value(forKey: "_id") as! String
            
            let vc = jobDetailViewController.instantiateFromStoryboard()
            
            vc.callDataByID = true
            vc.bookingID = id
            vc.navPush = true
            
            self.navigationController?.pushViewController(vc, animated: true)
        }
        
        
    }
    
}

class dateCell: UITableViewCell {
    
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var jobTypeLabel: UILabel!
    
}
