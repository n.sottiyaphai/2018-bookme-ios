//
//  NotiFeedViewController.swift
//  bookme
//
//  Created by Naphat Sottiyaphai on 1/2/2562 BE.
//  Copyright © 2562 Naphat Sottiyaphai. All rights reserved.
//

import UIKit
import Alamofire
import SVProgressHUD
import SwiftDate

class NotiFeedViewController: UIViewController {

    class func instantiateFromStoryboard() -> NotiFeedViewController {
        let storyboard = UIStoryboard(name: "calendar", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: String(describing: self)) as! NotiFeedViewController
    }
    
    var log = [NSDictionary]()
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.callLogs()
        
        // CG View init
        self.setBackgroundColor()
        self.title = "Notifications"
        self.navigationController?.navigationBar.backgroundColor = UIColor(red: 1, green: 1, blue: 1, alpha: 0)
//        UIApplication.shared.statusBar?.backgroundColor = UIColor(red: 1, green: 1, blue: 1, alpha: 0)
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.navigationController?.navigationBar.isHidden = false
        
        if #available(iOS 11.0, *) {
            navigationController?.navigationBar.prefersLargeTitles = true
        }
    }
    
    func callLogs()  {
        SVProgressHUD.show()
        
        let userDefault = UserDefaults.standard
        let userID = userDefault.value(forKey: "user_uid") as! String
        
        let body : [String : Any] = [
            "userID" : userID
        ]
        
        Alamofire.request(URL(string: "\(baseURL.firebaseFunction)getNotiLogs")!, method: .post, parameters: body, encoding: URLEncoding(destination: .httpBody)).responseJSON { (response) in
            
            if let notiData = response.result.value {
                let noti = notiData as! NSDictionary
                self.log = noti.value(forKey: "noti") as! [NSDictionary]
                let sortedArray = self.log.sorted(by: { (first, sec) -> Bool in
                    let firstDate = (first["date"] as! String).toISODate()
                    let secDate = (sec["date"] as! String).toISODate()
                    return ((firstDate?.earlierDate(secDate!)) != nil)
                })
                self.log = sortedArray as [NSDictionary]
                DispatchQueue.main.async {
                    SVProgressHUD.dismiss()
                    self.tableView.reloadData()
                }
            }
            else{
                return
            }
        }
    }
}

extension NotiFeedViewController : UITableViewDelegate , UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if(log.count == 0){
            self.tableView.setEmptyMessage("Notification is Empty")
        }
        else{
            self.tableView.restore()
        }
        
        return self.log.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        // Prepare Date
        let data = self.log[indexPath.row]
        let title = data.value(forKey: "title") as! String
        let textBody = data.value(forKey: "textBody") as! String
        let date = data.value(forKey: "date") as! String

        // Cell Init
        let cell = tableView.dequeueReusableCell(withIdentifier: "noti", for: indexPath) as! notificationCell
        
        cell.bgView.setRoundWithCommonR()
        cell.title.text = title
        cell.title.setTextBold()
        cell.textBody.text = textBody
        cell.textBody.setSubText()
        cell.dateTime.text = (date.toDate()! + 7.hours).toFormat("dd/MM/yyyy HH:mm")
        
        return cell
    }
    
    
}

class notificationCell: UITableViewCell {
    @IBOutlet weak var title : UILabel!
    @IBOutlet weak var textBody : UILabel!
    @IBOutlet weak var dateTime : UILabel!
    @IBOutlet weak var bgView: UIView!
}
