//
//  paymentViewController.swift
//  bookme
//
//  Created by Naphat Sottiyaphai on 28/6/2561 BE.
//  Copyright © 2561 Naphat Sottiyaphai. All rights reserved.
//

import UIKit
import BEMCheckBox
import OmiseSDK
import SVProgressHUD
import Alamofire

class paymentViewController: UIViewController {
    @IBOutlet weak var cardBG: UIView!
    @IBOutlet weak var bankBG: UIView!
    
    @IBOutlet weak var bankTranferCheckbox: BEMCheckBox!
    @IBOutlet weak var cardCheckbox: BEMCheckBox!
    
    @IBOutlet weak var promoBG: UIView!
    
    @IBOutlet weak var mainButton: UIButton!
//    @IBOutlet weak var containerView: UIView!
    
    @IBOutlet weak var packAmount: UILabel!
    @IBOutlet weak var feeAmount: UILabel!
    @IBOutlet weak var feeVat: UILabel!
    @IBOutlet weak var promoDiscount: UILabel!
    @IBOutlet weak var totalPrice: UILabel!
    
    @IBOutlet weak var selectPaymentBGView: UIView!
    @IBOutlet weak var paymentDetailBGView: UIView!
    @IBOutlet weak var bankSelect: UIButton!
    
    @IBAction func bankSelection(_ sender: Any) {
        bankTranferCheckbox.setOn(true, animated: true)
        cardCheckbox.setOn(false, animated: true)
    }
    @IBAction func cardSelect(_ sender: Any) {
        bankTranferCheckbox.setOn(false, animated: true)
        cardCheckbox.setOn(true, animated: true)
    }
    
    private let publicKey = "pkey_test_5cuadi69d2vr1y2any4"
    var jobData = NSDictionary()
    private var totalPrices = Double()
    private var discountType = String()
    private var discountValue = Double()
    private var promoCode = String()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.priceInit()
        self.setBackgroundColor()
        
        self.cardBG.setRoundWithR(r: commonValue.viewRadius)
        self.bankBG.setRoundWithR(r: commonValue.viewRadius)
        
        self.promoBG.setRoundWithR(r: commonValue.viewRadius)
        self.mainButton.setRoundedwithR(r: commonValue.viewRadius)
        
        self.paymentDetailBGView.setRoundWithR(r: commonValue.viewRadius)
        self.selectPaymentBGView.setRoundWithR(r: commonValue.viewRadius)
        self.title = "Make Payment"
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        if #available(iOS 11.0, *) {
            self.navigationController?.navigationBar.prefersLargeTitles = true
            self.navigationController?.navigationBar.isHidden = false
        } else {
            self.automaticallyAdjustsScrollViewInsets = false
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super .viewWillDisappear(true)
        self.navigationController?.navigationBar.prefersLargeTitles = false
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func getPromoInfo(promo : String) {
        
        let ud = UserDefaults.standard
        var accesstoken = String()
        
        if(ud.object(forKey: "accessToken") != nil){
            accesstoken = ud.value(forKey: "accessToken") as! String
        }
        
        let header : HTTPHeaders = [
            "accessToken" : accesstoken
        ]
        
        let url = URL(string: "\(baseURL.url)promo/checkCode/\(promo)")
        Alamofire.request(url! , headers : header).responseJSON { (response) in
            
            let result = response.result.value as! NSDictionary
            print(result)
            
            let data = result.value(forKey: "data") as! NSArray
            if(data.count > 0){
                self.discountType = (data[0] as! NSDictionary).value(forKey: "promoType") as! String
                let value = (data[0] as! NSDictionary).value(forKey: "promoValue") as! String
                self.discountValue = Double(value)!
                self.promoCode = promo
                SVProgressHUD.showSuccess(withStatus: "Yeah , You got it !!!")
                
                self.priceInit()
                DispatchQueue.main.asyncAfter(deadline: .now() + 1.5){
                    SVProgressHUD.dismiss()
                }
            }
            else{
                SVProgressHUD.showInfo(withStatus: "Don't have promocode Please try other code.")
                DispatchQueue.main.asyncAfter(deadline: .now() + 1.5){
                    SVProgressHUD.dismiss()
                }
            }
        }
    }
    
    
    @IBAction func promoAction(_ sender: Any) {
        
        let alert = UIAlertController(title: "Apply your promo code", message: "Please fill you promocode.", preferredStyle: .alert)
        let ok = UIAlertAction(title: "OK", style: .default) { (action) in
            let TextField = alert.textFields![0] as UITextField
            print(TextField.text)
            self.getPromoInfo(promo: TextField.text!)
        }
        let cancel = UIAlertAction(title: "Cancel", style: .cancel) { (action) in
            
        }
        alert.addAction(ok)
        alert.addAction(cancel)
        alert.addTextField {(textfield) in
            textfield.placeholder = "Promo code"
        }
        
        self.present(alert, animated: true, completion: nil)
    }
    
    func priceInit() {
        let packagePrice = jobData.value(forKey: "bookingPackagePrice") as! Double
        let fee : Double = Double((packagePrice*10)/100)
        
        if(self.discountType == "percent"){
            let discount = ((packagePrice + fee) * self.discountValue)/100
            self.totalPrices = (packagePrice + fee + (fee*0.07)) - discount
            
            self.packAmount.text = "\(packagePrice) THB"
            self.feeAmount.text = "\(fee) THB"
            self.feeVat.text = "\((fee*0.07).roundToDecimal(2)) THB"
            self.promoDiscount.text = "\(discount) THB"
            self.totalPrice.text = "\(self.totalPrices) THB"
        }
        else if(self.discountType == "value"){
            self.totalPrices = (packagePrice + fee + (fee*0.07)) - self.discountValue
            
            self.packAmount.text = "\(packagePrice) THB"
            self.feeAmount.text = "\(fee) THB"
            self.feeVat.text = "\((fee*0.07).roundToDecimal(2)) THB"
            self.promoDiscount.text = "\(self.discountValue) THB"
            self.totalPrice.text = "\(self.totalPrices) THB"
        }
        else{
            self.totalPrices = packagePrice + fee + (fee*0.07)
            
            self.packAmount.text = "\(packagePrice) THB"
            self.feeAmount.text = "\(fee) THB"
            self.feeVat.text = "\((fee*0.07).roundToDecimal(2)) THB"
            self.promoDiscount.text = "\(self.discountValue) THB"
            self.totalPrice.text = "\(totalPrices) THB"
        }
    }
    
    @IBAction func mainAction(_ sender: Any) {
        
        if(bankTranferCheckbox.on){
            self.performSegue(withIdentifier: "bank", sender: self)
        }
        else if (cardCheckbox.on){
            
            
            let creditCardView = CreditCardFormViewController.makeCreditCardFormViewController(withPublicKey: publicKey)
            creditCardView.delegate = self
            creditCardView.handleErrors = true
            
            present(creditCardView, animated: true, completion: nil)
            
        }
        else{
            print("Not selected item")
        }
        
    }
    
    func callCatfromProfess(professId : String) -> String {
        
        let ud = UserDefaults.standard
        var accesstoken = String()
        var returnData = String()
        
        if(ud.object(forKey: "accessToken") != nil){
            accesstoken = ud.value(forKey: "accessToken") as! String
        }
        
        let header : HTTPHeaders = [
            "accessToken" : accesstoken
        ]
        
        let url = URL(string: "\(baseURL.url)/professional/\(professId)")
        Alamofire.request(url! , headers: header).responseJSON { (response) in
            
            print(response)
            
            if let result = response.result.value {
                let responseBody = result as! NSDictionary
                let data = responseBody.value(forKey: "data") as! NSDictionary
                let categories = data.value(forKey: "categories") as! NSDictionary
                let catId = categories.value(forKey: "catId") as! String
                
                returnData = catId
            }
            else{
                returnData = ""
            }
        }
        
        return returnData
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if(segue.identifier == "bank"){
            let vc = segue.destination as! bankTransferViewController
            vc.jobData = jobData
            vc.lastPrice = self.totalPrice.text!
            vc.promoCode = self.promoCode
        }else if (segue.identifier == "card"){
            let vc = segue.destination as! ePaymentViewController
            vc.jobData = jobData
            vc.lastPrice = self.totalPrice.text!
            vc.promoCode = self.promoCode
            vc.lastPriceDouble = self.totalPrices
        }
    }
    
    @objc func dismissCreditCardForm() {
        dismiss(animated: true, completion: nil)
    }
    
    func payWithOmise(omiseToken : String){
        
        let cId = jobData.value(forKey: "customerId") as! String
        let pId = jobData.value(forKey: "professId") as! String
        let bookingId = jobData.value(forKey: "_id") as! String
        
        let ud = UserDefaults.standard
        var accesstoken = String()
        
        if(ud.object(forKey: "accessToken") != nil){
            accesstoken = ud.value(forKey: "accessToken") as! String
        }
        
        let header : HTTPHeaders = [
            "accessToken" : accesstoken
        ]
        
        let params : [String : Any] = [
            "customerId": cId,
            "professId": pId,
            "bookingId": bookingId,
            "transactionType": 1,
            "paymentStatus": 1,
            "paymentRef" : [
                "Transaction2C2PId": "",
                "bluweoBankId" : "",
                "paymentAPITransactionId" : "",
                "slipUpload": "",
                "omiseTokenId": omiseToken,
                "omiseTransectionId" : ""
            ],
            "bookingRef" : [
                "bookingId" : bookingId,
                "amount" : self.totalPrices,
                "promoCodeId": "",
                "bookingPackagePrice": "",
                "serviceFee" : ""
            ],
            "amount": totalPrices ,
        ]
        
        print(params)
        
        Alamofire.request(URL(string: "\(baseURL.url)transactionIn/omise")!, method: .post, parameters: params, encoding: JSONEncoding.default , headers : header).responseJSON(completionHandler: {(res) in
            
            if let result = res.result.value{
                let JSON = result as! NSDictionary
                let response = JSON.value(forKey: "response")as! Bool
                if(response){
                    if (JSON.object(forKey: "data") != nil){
                        SVProgressHUD.dismiss()
                        let vc = thankYouBookingViewController.instantiateFromStoryboard()
                        
                        vc.comfirmation = true
                        vc.makeBooking = true
                        vc.makePayment = true
                        
                        self.navigationController?.pushViewController(vc, animated: true)
                    }
                }
                else{
                    let errorMessage = JSON.value(forKey: "err") as! NSDictionary
                    let message = errorMessage.value(forKey: "message") as! String
                    SVProgressHUD.showError(withStatus: "\(message)")
                    
                    DispatchQueue.main.asyncAfter(deadline: .now() + 1){
                        SVProgressHUD.dismiss()
                    }
                }
                
            }
            else{
                SVProgressHUD.showError(withStatus: res.error?.localizedDescription)
                DispatchQueue.main.asyncAfter(deadline: .now() + 1){
                    SVProgressHUD.dismiss()
                }
            }

        })
    }
}

extension paymentViewController: CreditCardFormViewControllerDelegate {
    func creditCardFormViewControllerDidCancel(_ controller: CreditCardFormViewController) {
        
    }
    
  func creditCardFormViewController(_ controller: CreditCardFormViewController, didSucceedWithToken token: Token) {
    dismissCreditCardForm()

    self.payWithOmise(omiseToken: token.id)
    
    // Sends `Token` to your server to create a charge, or a customer object.
  }

  func creditCardFormViewController(_ controller: CreditCardFormViewController, didFailWithError error: Error) {
    dismissCreditCardForm()

    // Only important if we set `handleErrors = false`.
    // You can send errors to a logging service, or display them to the user here.
  }
}
