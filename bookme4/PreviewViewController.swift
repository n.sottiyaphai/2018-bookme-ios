//
//  PreviewViewController.swift
//  bookme
//
//  Created by Naphat Sottiyaphai on 18/5/2561 BE.
//  Copyright © 2561 Naphat Sottiyaphai. All rights reserved.
//

import UIKit
import ImageSlideshow
import Kingfisher
import SDWebImage
import SwiftHash
import SVProgressHUD

protocol back2profile {
    func go2profile(id : String)
}

class PreviewViewController: UIViewController {
    
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var profileName: UILabel!
    @IBOutlet weak var imageProfile: UIImageView!
    @IBOutlet var slideshow: ImageSlideshow!
    
    var profileNameString = String()
    var imageProfileURL = String()
    var profileID = String()
    var indexPage = Int()
    
    var img = [SDWebImageSource]()
    var urlArray = [URL]()
    
    // making this a weak variable so that it won't create a strong reference cycle
    var delegate : back2profile? = nil
    
    class func instantiateFromStoryboard() -> PreviewViewController {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: String(describing: self)) as! PreviewViewController
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        collectionView.delegate = self
        
        for  imageURL in urlArray {
            self.img.append(SDWebImageSource(url : imageURL ,placeholder: UIImage(named:"bookme_loading_black")))
        }
        
        imageProfile.setRounded()
//        imageProfile.sd_setImage(with: URL(string: self.imageProfileURL), placeholderImage: UIImage(named: "bookme_loading"))
        imageProfile.kf.setImage(with: URL(string: self.imageProfileURL),
                                  placeholder: UIImage(named: "bookme_loading"),
                                  options: [
                                    .scaleFactor(UIScreen.main.scale),
                                    .transition(.fade(0.5)),
                                    .cacheOriginalImage
            ])
        
        profileName.text = self.profileNameString
        
        slideshow.backgroundColor = UIColor.black
        slideshow.slideshowInterval = 9999999999999999999.0
        slideshow.contentScaleMode = UIView.ContentMode.scaleAspectFit
        slideshow.setImageInputs(self.img)
        slideshow.setCurrentPage(indexPage, animated: true)
        slideshow.activityIndicator = DefaultActivityIndicator()
        
        let view = UIView(frame: CGRect(x: 0, y: 0, width: 320, height: 50))
        let gradient = CAGradientLayer()
        
        gradient.frame = view.bounds
        gradient.colors = [UIColor.white.cgColor, UIColor.black.cgColor]
        
        self.setClearNavBar()
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func dismiss(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func go2Profile(_ sender: Any) {
        
        let vc = profileViewController.instantiateFromStoryboard()
        vc.uid = profileID
        delegate?.go2profile(id: profileID)
        
        self.dismiss(animated: true, completion: nil)
    }
}

extension PreviewViewController : UICollectionViewDelegate, UICollectionViewDataSource , UICollectionViewDelegateFlowLayout{
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return urlArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let item = collectionView.dequeueReusableCell(withReuseIdentifier: "image", for: indexPath) as! imageCollectionItem
        let url = urlArray[indexPath.row]
        
        item.image.kf.setImage(with: url,
                                placeholder: UIImage(named: "bookme_loading"),
                                options: [
                                    .scaleFactor(UIScreen.main.scale),
                                    .transition(.fade(0.3)),
                                    .cacheOriginalImage
            ])
//        item.image.sd_setImage(with: url, placeholderImage: UIImage(named: "bookme_loading_black"))
        item.image.setRoundWithR(r: commonValue.subTadius)
        
        return item
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {

        return CGSize(width: collectionView.bounds.height-15, height: collectionView.bounds.height-15)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        self.slideshow.setCurrentPage(indexPath.row, animated: true)
    }
}
