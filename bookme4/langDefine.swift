//
//  langDefine.swift
//  bookme
//
//  Created by Naphat Sottiyaphai on 7/6/2561 BE.
//  Copyright © 2561 Naphat Sottiyaphai. All rights reserved.
//

import Foundation

struct thai {
    static let about                = "เกี่ยวกับ"
    static let feedback             = "ความคิดเห็น"
    static let fillNotValidate      = "กรุณากรอกข้อมูลให้ครบถ้วน"
    static let help                 = "ช่วยเหลือ"
    static let imageProfileSetUp    = "คุณต้องการที่จะเปลี่ยนรูปโปรไฟล์ของคุณใช่หรือไม่"
    static let language             = "ภาษา"
    static let logout               = "ออกจากระบบ"
    static let no                   = "ไม่ใช่"
    static let notification         = "การแจ้งเตือน"
    static let ok                   = "ตกลง"
    static let paymentInProcess     = "การชำระเงินสำเร็จ"
    static let paymentInProcessText = "You have approved the payment already.\nThe payment will be transferes to the professional within 3 days."
    static let profile_create       = "สร้างโปรไฟล์"
    static let profile_update       = "อัพเดทโปรไฟล์"
    static let someThingWrong       = "มีบางอย่างผิดพลาด"
    static let yes                  = "ใช่"
}

struct english {
    static let about                = "About"
    static let feedback             = "Feedback"
    static let fillNotValidate      = "Please fill with all data."
    static let help                 = "Help"
    static let imageProfileSetUp    = "Do you want to change profile's image ?"
    static let language             = "language"
    static let logout               = "Logout"
    static let no                   = "No"
    static let notification         = "Notification"
    static let ok                   = "Ok"
    static let paymentInProcess     = "Payment Approve"
    static let paymentInProcessText = "You have approved the payment already.\nThe payment will be transferes to the professional within 3 days."
    static let profile_create       = "Create profile"
    static let profile_update       = "Update profile"
    static let someThingWrong       = "Some thing wrong !"
    static let yes                  = "Yes"
}
